class SidebarController < ApplicationController
  def tips
  end

  def questions
  end

  def my_hives
  end

  def my_groups
  end

  def my_friends
  end
end
