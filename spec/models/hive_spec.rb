require 'rails_helper'

describe Hive do
  it { is_expected.to have_many(:pictures) }
  it { is_expected.to have_many(:followings) }
  it { is_expected.to have_many(:object_settings) }
  it { is_expected.to have_many(:shares) }
  it { is_expected.to belong_to(:user) }

  describe '#class_name' do
    let(:hive) { build(:hive) }
    it 'returns class name' do
      expect(hive.class_name).to eq 'Hive'
    end
  end

  describe '#kind_id' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    it 'returns pair of class name and id' do
      expect(hive.kind_id).to eq "#{hive.class.name}-#{hive.id}"
    end
  end

  describe '#to_param' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    it 'returns parameterized value of id and name' do
      expect(hive.to_param).to eq "#{hive.id}-#{hive.name.downcase}"
    end
  end

  describe '#name' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    it 'returns hive title' do
      expect(hive.name).to eq "#{hive.title}"
    end
  end

  describe '#pockets' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    subject { hive.pockets }
    it 'returns pockets' do
      is_expected.to eq [Pocket.first]
    end
  end

  describe '#tips' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:tip) { create(:tip, user_id: user.id) }
    before do
      tip.follow(hive)
      tip.follow(hive.pockets.first)
    end
    subject { hive.tips }
    it 'returns tips' do
      is_expected.to eq [tip]
    end
  end

  describe '#questions' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:question) { create(:question, user_id: user.id) }
    before do
      question.follow(hive)
      question.follow(hive.pockets.first)
    end
    subject { hive.questions }
    it 'returns questions' do
      is_expected.to eq [question]
    end
  end

  describe '#users' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    before do
      user.follow(hive)
    end
    subject { hive.users }
    it 'returns users' do
      is_expected.to eq [user]
    end
  end

  describe '#groups' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:group) { create(:group, user_id: user.id) }
    before do
      group.follow(hive)
    end
    subject { hive.groups }
    it 'returns groups' do
      is_expected.to eq [group]
    end
  end

  describe '#friend_lists' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:friend_list) { create(:friend_list) }
    before do
      friend_list.follow(hive)
    end
    subject { hive.friend_lists }
    it 'returns friend_lists' do
      is_expected.to eq [friend_list]
    end
  end

  describe '#users_and_groups' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:group) { create(:group, user_id: user.id) }
    before do
      user.follow(hive)
      group.follow(hive)
    end
    subject { hive.users_and_groups }
    it 'returns users and groups' do
      is_expected.to eq [user, group]
    end
  end

  describe '#users_and_groups_and_friend_lists' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:group) { create(:group, user_id: user.id) }
    let(:friend_list) { create(:friend_list) }
    before do
      user.follow(hive)
      group.follow(hive)
      friend_list.follow(hive)
    end
    subject { hive.users_and_groups_and_friend_lists }
    it 'returns users and groups and friendslists' do
      is_expected.to eq [user, group, friend_list]
    end
  end

  describe '#search_pocket' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    subject { Hive.search_pocket([hive], 'r') }
    it 'returns selected hive pocket' do
      is_expected.to eq [Pocket.first]
    end
  end

  describe '#get_hive_sharing_permission' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }

    context 'when hive is private ' do
      let(:hive) { create(:hive, is_private: true, user_id: user.id) }
      subject { Hive.get_hive_sharing_permission([hive], user) }
      it 'returns private permission ' do
        is_expected.to eq ['private']
      end
    end

    context 'when hive is not private ' do
      let(:hive) { create(:hive, is_private: false, user_id: user.id) }
      subject { Hive.get_hive_sharing_permission([hive], user) }
      it 'returns select_friends permission ' do
        is_expected.to eq ['select_friends']
      end
    end
  end

  describe '#follow_shared_objects' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:friend) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:not_friend) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:group) { create(:group, user_id: user.id) }
    let(:friend_list) { create(:friend_list) }
    before do
      user.follow(hive)
      user.follow(friend)
      friend.follow(user)
      InvitationRequest.current_domain = Domain.root
    end

    context 'when user is friend' do
      subject { hive.follow_shared_objects([group, friend_list, friend]) }
      it 'follow shared object' do
        is_expected.to be_truthy
      end
    end

    context 'when user is not friend then send invitation request before sharing hive' do
      subject { hive.follow_shared_objects([not_friend, friend_list]) }
      it 'create user default settings all friends for this hive' do
        is_expected.to be_truthy
      end
    end
  end
end
