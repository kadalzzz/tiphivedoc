class ChangeColumnUrlInTipLinks < ActiveRecord::Migration
  def change
  	change_column :tip_links, :url, :text
  end
end
