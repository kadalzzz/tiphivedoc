share_users  ||= @selected_friends
share_groups ||= @selected_groups
question     ||= @question

json.response do
  json.is_share !question.is_private
  json.users do
    json.array!(share_users) do |shared_object|
      json.partial! 'api/v1/questions/shared_user', shared_object: shared_object
    end

  end
  json.groups do
    json.array!(share_groups) do |shared_group|
      json.partial! 'api/v1/questions/shared_group', shared_group: shared_group
    end
  end
end

json.status :success
