class CreateTiphiveSettings < ActiveRecord::Migration
  def change
    create_table :tiphive_settings do |t|
      t.string :name, null: false

      t.timestamps
    end

    add_index :tiphive_settings, :name, unique: true
  end
end
