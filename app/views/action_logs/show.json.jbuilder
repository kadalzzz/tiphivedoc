json.extract! @action_log, :id, :file_name, :class_name, :method_name, :line_number, :stack_trace, :message,
              :created_at, :updated_at
