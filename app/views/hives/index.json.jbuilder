json.array!(@hives) do |hive|
  json.extract! hive, :id, :title, :description
  json.url hive_url(hive, format: :json)
end
