require 'rails_helper'

describe InvitationRequest do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to validate_presence_of(:email) }
  describe '#connectable' do
    pending
  end
end
