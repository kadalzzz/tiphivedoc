class AddIndexToSocialFriends < ActiveRecord::Migration
  def change
    add_index :social_friends, :user_id
  end
end
