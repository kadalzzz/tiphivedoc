class Group < ActiveRecord::Base
  include Tenant
  include PublicActivity::Model
  tracked owner: Proc.new { |controller, _model| controller.current_user if controller }

  serialize :admin_ids, Array

  acts_as_followable
  acts_as_follower
  acts_as_taggable_on :allowing_domains, :allowing_locations

  JOIN_TYPES = [
    ['Anyone', :anyone],
    ['Invite only', :invite],
    ['Users with certain email address', :domain],
    ['Users at certain location', :location]
  ]

  GROUP_TYPES = [
    ['General purpose', :general],
    ['Event', :event],
    ['University group', :university],
    ['Enterprise group', :enterprise]
  ]

  COLOR = %w(liteorange orange purple litepurple green liteblue blue)

  before_create :set_color

  validates :title, presence: true

  # extend FriendlyId
  # friendly_id :slug_candidates, use: :slugged
  def to_param
    "#{self.id} #{self.name}".parameterize
  end

  belongs_to :user
  has_many :group_domains # *** THIS IS JUST FOR MIGRATION PURPOSE

  mount_uploader :avatar, AvatarUploader
  mount_uploader :background_image, BackgroundImageUploader
  include GroupObserver
  include TipHiveObject
  include ActsAsSharing

  has_many :pictures, as: :imageable, dependent: :destroy
  attr_accessor :picture_ids

  paginates_per 4

  searchable do
    text :title, :description
    integer :user_id
    string :join_type
    string :group_type
  end

  def slug_candidates
    [
      [:id, :title]
    ]
  end

  def name
    title
  end

  def hives
    following_hives
  end

  def questions
    following_questions
  end

  def members
    user_followers
  end

  def who_can_join
    join_type ? join_type.to_sym : :default
  end

  def allow_domains

  end

  def type
    group_type ? group_type.to_sym : :default
  end

  def admins
    User.where(id: admin_ids)
  end

  def set_color
    self.color = COLOR[rand(COLOR.length)]
  end
end
