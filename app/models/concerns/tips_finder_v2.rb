module TipsFinderV2
  # Tips fetching function
  # @param [Object] base_object
  # @param [Hash] params
  # @return [Relation] tips
  def fetch_tips(base_object = nil, params = {})
    current_user = self
    base_class = base_object.class.name

    # We're going to collect all tip IDs for display
    tip_ids = []

    # Process tips fetching for tipfeed page
    if base_object.nil?
      tip_ids = get_tipfeed_tips(current_user)
    end

    # Process tips fetching for User page
    if base_class == 'User'
      tip_ids = get_user_tips(current_user, base_object)
    end

    # Process tips fetching for Group page
    if base_class == 'Group'
      tip_ids = get_group_tips(current_user, base_object)
    end

    # Process tips fetching for Hive page
    if base_class == 'Hive'
      tip_ids = get_hive_tips(current_user, base_object)
    end

    # Process tips fetching for Pocket page
    if base_class == 'Pocket'
      tip_ids = get_pocket_tips(current_user, base_object)
    end

    # Teefan: don't know what is it doing, but re-implement it from TipsFinder v1
    # Process tips fetching for Domain
    if base_class == 'Domain'
      tip_ids = get_domain_tips(base_object)
    end

    # Old draft implementation from TipsFinder v1
    if params[:draft] == '1'
      # TODO: Find other way to delete unwanted default_scope
      Tip.default_scopes.pop
    end

    tips = Tip.includes([:user, :tip_links, :file_uploads, :pictures]).where(id: tip_ids.flatten.uniq)

    # filtering
    params[:social_filter] = 'only_created' if params[:only_created].present?
    tips = social_filter(tips, params[:social_filter])

    # ordering
    tips = sorting(tips, params[:sort_filter])

    tips.page(params[:page]).per(params[:per])
  end

  # Get display tips for tipfeed page
  def get_tipfeed_tips(current_user)
    # Get all current following tips
    tip_ids = current_user.following_tips.ids

    # Get shared all friends tips where tip owner is in friend list
    tip_ids += Tip.where(shared_all_friends: true).where(user_id: friend_ids).ids

    # Get tip ids of groups user is following
    tip_ids += Follow.where(follower_type: 'Group')
                     .where(follower_id: current_user.following_groups.ids)
                     .where(followable_type: 'Tip')
                     .pluck(:followable_id)

    # Get tip ids of hives user is following
    tip_ids += Follow.where(follower_type: 'Hive')
                     .where(follower_id: current_user.following_hives.ids)
                     .where(followable_type: 'Tip')
                     .pluck(:followable_id)

    tip_ids
  end

  # Get display tips for User show page
  def get_user_tips(current_user, base_user)
    # Prepare all tip ids that belong to base user
    base_user_all_tips = Tip.where(user_id: base_user.id)
    base_user_all_tip_ids = base_user_all_tips.ids

    # Get all current user following tips that belong to base user
    tip_ids = current_user.following_tips.where(id: base_user_all_tip_ids).ids

    # Get shared all friends tips of base user if current user is friend with base user
    tip_ids += base_user_all_tips.where(shared_all_friends: true).ids if base_user.following?(current_user)

    # Get public tips of base user
    tip_ids += base_user_all_tips.where(is_public: true).ids

    tip_ids
  end

  # Get display tips for Group show page
  def get_group_tips(current_user, group)
    # Only get group tips if current user is a group member
    if current_user.following? group
      # Get all group tips
      tip_ids = group.following_tips.ids
    else
      tip_ids = []
    end

    tip_ids
  end

  # Get display tips for Hive show page
  def get_hive_tips(current_user, hive)
    # Prepare all tip ids that belong to this hive
    hive_all_tip_ids = hive.tip_followers.ids

    # Get all current user following tips that belong to this hive
    tip_ids = current_user.following_tips.where(id: hive_all_tip_ids).ids

    # Get group tips that current user is a group member, that belong to this hive
    tip_ids += Follow.where(follower_type: 'Group')
                     .where(follower_id: current_user.following_groups.ids)
                     .where(followable_type: 'Tip')
                     .where(followable_id: hive_all_tip_ids)
                     .pluck(:followable_id)

    # Get shared all friends tips in this hive where tip owner is in friend list
    tip_ids += hive.tip_followers.where(shared_all_friends: true).where(user_id: friend_ids)

    # Get public tips in this hive
    tip_ids += hive.tip_followers.where(is_public: true).ids

    tip_ids
  end

  # Get display tips for Pocket show page
  def get_pocket_tips(current_user, pocket)
    # Prepare all tip ids that belong to this pocket
    pocket_all_tip_ids = pocket.tip_followers.ids

    # Get all current user following tips that belong to this pocket
    tip_ids = current_user.following_tips.where(id: pocket_all_tip_ids).ids

    # Get group tips that current user is a group member, that belong to this pocket
    tip_ids += Follow.where(follower_type: 'Group')
                     .where(follower_id: current_user.following_groups.ids)
                     .where(followable_type: 'Tip')
                     .where(followable_id: pocket_all_tip_ids)
                     .pluck(:followable_id)

    # Get shared all friends tips in this pocket where tip owner is in friend list
    tip_ids += pocket.tip_followers.where(shared_all_friends: true).where(user_id: friend_ids)

    # Get public tips in this pocket
    tip_ids += pocket.tip_followers.where(is_public: true).ids

    tip_ids
  end

  # Get display tips for Domain
  def get_domain_tips(domain)
    # Get group ids of domain
    group_ids = domain.groups.ids

    # Get tip ids of domain groups
    tip_ids = Follow.where(follower_type: 'Group')
                    .where(follower_id: group_ids)
                    .where(followable_type: 'Tip')
                    .pluck(:followable_id)

    tip_ids
  end

  def social_filter(tips, filter)
    case filter
    when 'my_tips', 'only_created'
      tips.where(user_id: id)
    when 'my_liked_tips'
      tips.includes([:votes]).where('votes.voter_id = ?', id).references(:votes)
    when 'friends_tips'
      tips.where(user_id: friend_ids)
    when 'public_tips'
      tips.public_sharing
    else
      tips
    end
  end

  def sorting(tips, order)
    case order
    when 'most_recent'
      tips.order('tips.created_at DESC')
    when 'most_popular'
      tips.order('tips.cached_votes_up DESC')
    else
      tips.order('tips.created_at DESC')
    end
  end
end
