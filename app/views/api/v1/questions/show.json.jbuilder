question ||= @question

json.response do
  json.partial! 'api/v1/questions/question', question: question
end

json.status :success
