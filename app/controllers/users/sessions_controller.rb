class Users::SessionsController < Devise::SessionsController
  layout :set_layout, only: [:new]

  def new
    self.resource = resource_class.new(sign_in_params)
    clean_up_passwords(resource)
    if params[:user].present?
      params[:email] = params[:user][:email]
    end
    respond_with(resource, serialize_options(resource))
  end

  def create
    self.resource = warden.authenticate!(auth_options)
    # set_flash_message(:notice, :signed_in) if is_flashing_format?
    sign_in(resource_name, resource)
    yield resource if block_given?
    respond_with resource, location: after_sign_in_path_for(resource)
  end

  def destroy
    redirect_path = after_sign_out_path_for(resource_name)
    signed_out    = (Devise.sign_out_all_scopes ? sign_out : sign_out(resource_name))
    # set_flash_message :notice, :signed_out if signed_out && is_flashing_format?
    yield resource if block_given?

    # We actually need to hardcode this as Rails default responder doesn't
    # support returning empty response on GET request
    respond_to do |format|
      format.all { head :no_content }
      format.any(*navigational_formats) { redirect_to redirect_path }
    end
  end

  def set_layout
    current_domain.root? ? 'application' : 'domain'
  end
end
