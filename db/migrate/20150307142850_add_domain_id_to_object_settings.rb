class AddDomainIdToObjectSettings < ActiveRecord::Migration
  def up
    add_column :object_settings, :domain_id, :integer
    add_index :object_settings, :domain_id
  end
  def down
    remove_column :object_settings, :domain_id
  end
end
