class Api::V1::RegistrationsController < ApplicationController
  def create
    if params[:user].blank?
      message = 'User parameter could not be blank'
      render json: { response: { error: message }, status: :fail, message: message }
      return
    end

    @user = User.new user_params
    @user.ensure_authentication_token

    if @user.save
      sign_in @user, store: true
      render template: 'api/v1/users/me'
    else
      render json: { response: { error: @user.errors, full_messages: @user.errors.full_messages },
                     status:   :fail, message: @user.errors.full_messages.join(', ') }
    end
  end

  def user_params
    params.require(:user).permit(:email, :password, :first_name, :last_name, :avatar, :background_image)
  end
end
