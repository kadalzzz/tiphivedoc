json.id share_object.id
json.first_name share_object.first_name
json.last_name share_object.last_name
json.avatar_url share_object.avatar.present? ? share_object.avatar.url : ''
