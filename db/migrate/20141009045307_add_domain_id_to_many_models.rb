class AddDomainIdToManyModels < ActiveRecord::Migration
  def change
    add_column :hives, :domain_id, :integer
    add_index :hives, :domain_id
    add_column :comments, :domain_id, :integer
    add_index :comments, :domain_id
    add_column :pictures, :domain_id, :integer
    add_index :pictures, :domain_id
    add_column :pockets, :domain_id, :integer
    add_index :pockets, :domain_id
    add_column :questions, :domain_id, :integer
    add_index :questions, :domain_id
    add_column :tips, :domain_id, :integer
    add_index :tips, :domain_id
  end
end
