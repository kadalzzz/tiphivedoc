class Users::RegistrationsController < Devise::RegistrationsController
  def new
    invitations = verify_invitation_request(params)
    logger.info "*** invitations: #{invitations}"
    return if invitations.blank?

    if params[:cancel].present?
      session['devise.facebook_data']      = nil
      session['devise.twitter_data']       = nil
      session['devise.google_oauth2_data'] = nil
    end

    build_resource({})
    resource.email ||= @invited_email
    respond_with resource
  end

  def create
    invitations = verify_invitation_request(params)
    return if invitations.blank?

    super

    if params[:provider].present? && params[:uid].present?
      identity = Identity.where(provider: params[:provider], uid: params[:uid]).first_or_create
    end

    if identity.present? && identity.user_id != resource.id
      identity.user_id = resource.id
      identity.save
    end

    resource.follow current_domain if resource.persisted? && !current_domain.root?
  end

  def update
    @user = User.find(current_user.id)

    successfully_updated = if needs_password?(@user, params)
                             @user.update_with_password(devise_parameter_sanitizer.sanitize(:account_update))
                           else
                             # remove the virtual current_password attribute
                             # update_without_password doesn't know how to ignore it
                             params[:user].delete(:current_password)
                             @user.update_without_password(devise_parameter_sanitizer.sanitize(:account_update))
                           end

    if successfully_updated
      set_flash_message :notice, :updated
      # Sign in the user bypassing validation in case his password changed
      sign_in @user, bypass: true
      if request.xhr?
        render json: @user
      else
        redirect_to after_update_path_for(@user)
      end
    else
      if request.xhr?
        render json: @user.errors, status: :unprocessable_entity
      else
        render 'edit'
      end
    end
  end

  # The path used after sign up. You need to overwrite this method
  # in your own RegistrationsController.
  def after_sign_up_path_for(resource)
    if resource.avatar.blank?
      session[:user_status] = 'just_signup'
      select_avatar_path
    else
      after_sign_in_path_for(resource)
    end
  end

  # The path used after sign up for inactive accounts. You need to overwrite
  # this method in your own RegistrationsController.
  def after_inactive_sign_up_path_for(resource)
    respond_to?(:root_path) ? root_path : '/'
  end

  # The default url to be used after updating a resource. You need to overwrite
  # this method in your own RegistrationsController.
  def after_update_path_for(resource)
    signed_in_root_path(resource)
  end

  private

  # check if we need password to update user data
  # ie if password or email was changed
  # extend this as needed
  def needs_password?(user, params)
    user.email != params[:user][:email] || params[:user][:password].present?
  end

  def verify_invitation_request(params)
    session['token']      = params[:token] if params[:token].present?
    session['inviter_id'] = params[:inviter_id] if params[:inviter_id].present?

    params[:token]        = session['token'] if params[:token].blank?
    params[:inviter_id]   = session['inviter_id'] if params[:inviter_id].blank?

    logger.info "*** params: #{params[:token]} - #{params[:inviter_id]}"

    invitation = FriendInvitation.where(invitation_token: params[:token]).first
    if invitation.blank?
      flash[:alert] = %q(You don\'t have a valid invitation token.
                       If you believe this is a mistake, please email us at
                       <a href="mailto:support@tiphive.com">support@tiphive.com</a>).html_safe
      logger.info "*** Invalid invitation: #{params}"
      redirect_to new_user_session_path
      return
    end

    all_invitations = []
    @invited_email  = invitation.email
    FriendInvitation.where(email: @invited_email).find_each do |friend_invitation|
      all_invitations << friend_invitation
    end

    all_invitations
  end
end
