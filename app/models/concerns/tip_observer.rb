module TipObserver
  include Twitter::Extractor
  extend ActiveSupport::Concern

  included do
    after_save :add_and_count_pictures
    after_save :add_and_count_files
    after_save :add_and_count_links
    after_save :add_jobs_to_worker
    before_create :generate_hash
    before_update :remove_draft
  end

  def notify_friend(friend)
    return if user_id == friend.id

    user = self.user

    email_params = {
      user_email:            user.email,
      user_first_name:       user.first_name,
      user_name:             user.name,
      friend_email:          friend.email,
      friend_first_name:     friend.first_name,
      friend_name:           friend.name,
      is_shared:             friend.following?(self),
      tip_slug:              to_param,
      tip_hive_pocket_title: hives_and_pockets_title,
      access_key:            access_key,
      domain_id:             domain_id
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{user.name} is sending email to #{friend.email}"
    )
    ## DEBUG LOG END

    EmailSendingWorker.perform_async('UserMailer', 'add_tip_email', email_params)
  end

  def notify_friends
    user             = self.user
    follower_friends = [users].flatten.uniq - [user]
    follower_friends.each do |friend|
      notify_friend(friend) if friend.settings(:email_notifications).friend_adds_tip == 'always'
    end
  end

  def flag_a_tip(user)
    email_params = {
      tip_slug:        to_param,
      user_first_name: user.first_name,
      tip_title:       title
    }

    EmailSendingWorker.perform_async('UserMailer', 'flag_tip_email', email_params)
  end

  def notify_tip_liked(friend)
    return if friend.id == self.user.id
    user = self.user
    return if user.settings(:email_notifications).someone_likes_tip == false

    email_params = {
      user_email:      user.email,
      user_first_name: user.first_name,
      friend_name:     friend.name,
      tip_title:       title,
      tip_slug:        to_param,
      domain_id:       domain_id
    }

    EmailSendingWorker.perform_async('UserMailer', 'like_tip_email', email_params)
  end

  def add_and_count_pictures
    if picture_ids.present? && picture_ids.size > 0
      tip_pictures = Picture.where('id IN (?)', picture_ids.split(','))
      tip_pictures.each do |tip_picture|
        tip_picture.imageable = self
        tip_picture.save
      end

      tip_pictures_count = pictures.count
      update_column(:pictures_count, tip_pictures_count)
    end
  end

  def add_and_count_links
    if link_ids.present? && link_ids.size > 0
      tip_links = TipLink.where('id IN (?)', link_ids.split(','))
      tip_links.each do |tip_link|
        tip_link.tip_id = self.id
        tip_link.save
      end

      tip_links_count = self.tip_links.count
      update_column(:links_count, tip_links_count)
    end
  end

  def add_jobs_to_worker
    if self.id_changed?
      tip_params = { tip_id: id }
      GenerateTipActivitiesWorker.perform_async(tip_params)
    end
  end

  def create_flag_recorded(user)
    flag_params = {
      flagable_id:   id,
      flagable_type: self.class.name,
      flag_id:       user.id,
      flag_type:     user.class.name
    }

    flag        = Flag.new(flag_params)
    flag.save!
  end

  def notify_user_add_tip_on_question(question, user)
    if question && question.user
      return if question.user.id == user.id

      params = {
        user_first_name: question.user.first_name,
        user_email:      question.user.email,
        friend_name:     user.name,
        question_title:  question.name,
        question_slug:   question.to_param,
        domain_id:       question.domain_id
      }

      EmailSendingWorker.perform_async('UserMailer', 'add_tip_on_question_email', params)
    end
  end

  def notify_mentioned_friend_in_tip(recipient, email_template = 'user_mention_email_for_tip')
    email_params = {
      user_email:        user.email,
      user_first_name:   user.first_name,
      user_name:         user.name,
      friend_email:      recipient.email,
      friend_first_name: recipient.first_name,
      friend_name:       recipient.name,
      tip_slug:          to_param,
      tip_title:         title,
      domain_id:         domain_id
    }
    EmailSendingWorker.perform_async('UserMailer', email_template, email_params)
  end

  def notify_mentioned_friends
    user     = self.user
    mentions = extract_mentioned_screen_names(description)
    mentions.each do |username|
      mentioned_user = User.where(username: username).first
      notify_mentioned_friend_in_tip(mentioned_user) if mentioned_user && mentioned_user.email != user.email
    end
  end

  def add_and_count_files
    if file_ids.present? && file_ids.size > 0
      tip_files = FileUpload.where('id IN (?)', file_ids.split(','))
      tip_files.each do |tip_file|
        tip_file.fileable = self
        tip_file.save
      end
    end
  end

  def generate_hash
    seed            = "--#{Time.now}--#{rand(1000)}--#{title}"
    self.access_key = Digest::SHA256.hexdigest seed
  end

  def remove_draft
    self.draft = nil
  end

  def notify_group_member(group)
    group_members = group.members
    group_members.each do |member|
      notify_friend(member) if member.settings(:email_notifications).group_member_adds_a_tip == true
    end
  end
end
