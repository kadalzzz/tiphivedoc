class Subdomain
  def self.matches?(request)
    Domain.active.where(name: request.subdomain).exists?
  end
end
