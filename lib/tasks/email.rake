namespace :email do
  desc "Send daily feed"
  task send_daily_feed: :environment do
    User.find_in_batches(batch_size: 100) do |users|
      users.each do |user|
        user.send_tips_feed(days: -1)
      end
    end
  end

  desc "Send weekly feed"
  task send_weekly_feed: :environment do
    User.find_in_batches(batch_size: 100) do |users|
      users.each do |user|
        user.send_tips_feed(days: -7)
      end
    end
  end

  desc "Send daily tip email"
  task send_daily_tip: :environment do
    TipWorker.perform_async
  end
end
