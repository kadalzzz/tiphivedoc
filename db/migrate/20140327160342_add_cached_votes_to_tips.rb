class AddCachedVotesToTips < ActiveRecord::Migration
  def change
    add_column :tips, :cached_votes_total, :integer, :default => 0
    add_column :tips, :cached_votes_score, :integer, :default => 0
    add_column :tips, :cached_votes_up, :integer, :default => 0
    add_column :tips, :cached_votes_down, :integer, :default => 0
    add_column :tips, :cached_weighted_score, :integer, :default => 0

    add_index  :tips, :cached_votes_total
    add_index  :tips, :cached_votes_score
    add_index  :tips, :cached_votes_up
    add_index  :tips, :cached_votes_down
    add_index  :tips, :cached_weighted_score
  end
end
