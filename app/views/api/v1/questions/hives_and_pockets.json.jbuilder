question ||= @question
pocket_ids = question.pockets.pluck(:id)

json.response do
  json.hives question.hives.map do |hive|
    json.id hive.id
    json.title hive.title
    json.background_image_url hive.background_image.present? ? hive.background_image.url : ''
    json.pockets hive.pockets.reject { |p| pocket_ids.exclude?(p.id) }.map do |pocket|
      json.id pocket.id
      json.title pocket.title
    end
  end
end

json.status :success
