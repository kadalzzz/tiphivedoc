user = comment.user

json.id comment.id
json.body comment.body
json.created_at "#{time_ago_in_words(comment.created_at)} ago"
json.address comment.address
json.location comment.location
json.latitude comment.latitude
json.longitude comment.longitude
json.pictures comment.pictures.map do |picture|
  {
    id:             picture.id,
    title:          picture.title,
    image_url:      picture.image.url,
    imageable_id:   picture.imageable_id,
    imageable_type: picture.imageable_type
  }
end

json.pictures_count comment.pictures.count
json.user_id comment.user_id
json.owner_name comment.user_first_name
json.owner_thumb_url user.avatar.present? ? user.avatar.thumb.url : ''
