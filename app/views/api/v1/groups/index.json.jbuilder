groups ||= @groups

json.response do
  json.array!(groups) do |group|
    json.partial! 'api/v1/groups/group', group: group
  end
end

json.status :success
