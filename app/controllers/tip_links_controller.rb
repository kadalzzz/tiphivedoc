class TipLinksController < ApplicationController
  before_action :set_tip_link, only: [:show, :edit, :update, :destroy]

  # GET /tip_links
  # GET /tip_links.json
  def index
    @tip_links = TipLink.all
  end

  # GET /tip_links/1
  # GET /tip_links/1.json
  def show
  end

  # GET /tip_links/new
  def new
    @tip_link = TipLink.new
  end

  # GET /tip_links/1/edit
  def edit
  end

  # GET /fetch
  def fetch
    @tip_link = TipLink.where(url: params[:url], tip_id: params[:tip_id]).first

    @tip_link = fetch_tip_link(@tip_link, params[:url])

    respond_to do |format|
      if @tip_link.save
        format.html { render partial: 'tips/modules/link', locals: { link: @tip_link } }
        format.json {}
        format.js {}
      else
        format.html { render partial: 'tips/modules/link', locals: { link: @tip_link } }
        format.json {}
        format.js {}
      end
    end
  end

  # POST /tip_links
  # POST /tip_links.json
  def create
    @tip_link         = TipLink.where(url: tip_link_params[:url]).first
    @tip_link         = TipLink.new(tip_link_params) unless @tip_link
    @tip_link.user_id = current_user.id

    if params[:tip_link][:url] !~ /http:\/\//
      params[:tip_link][:url] = 'http://' + params[:tip_link][:url]
    end

    @tip_link       = fetch_tip_link(@tip_link, tip_link_params[:url])

    @tip_link.title = params[:tip_link][:url] if @tip_link.title.nil?

    respond_to do |format|
      if @tip_link.save
        format.html { redirect_to @tip_link, notice: 'Tip link was successfully created.' }
        format.json { render action: 'show', status: :created, location: @tip_link }
        format.js {}
      else
        format.html { render action: 'new' }
        format.json { render json: @tip_link.errors, status: :unprocessable_entity }
        format.js {}
      end
    end
  end

  # PATCH/PUT /tip_links/1
  # PATCH/PUT /tip_links/1.json
  def update
    respond_to do |format|
      if @tip_link.update(tip_link_params)
        format.html { redirect_to @tip_link, notice: 'Tip link was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: 'edit' }
        format.json { render json: @tip_link.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /tip_links/1
  # DELETE /tip_links/1.json
  def destroy
    @tip_link.destroy
    respond_to do |format|
      format.html { redirect_to tip_links_url }
      format.json { head :no_content }
      format.js {}
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_tip_link
    @tip_link = TipLink.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def tip_link_params
    params.require(:tip_link).permit(:url, :title, :description, :image_url, :avatar, :tip_id, :user_id)
  end

  def fetch_tip_link(tip_link, link_url)
    begin
      if tip_link.nil?
        fetched_tip_link = TipLink.new(url: link_url, tip_id: params[:tip_id])
        page             = LinkThumbnailer.generate(link_url) if link_url
        if page
          fetched_tip_link.title       = page.title
          fetched_tip_link.description = page.description
          fetched_tip_link.image_url   = page.images.first.src.to_s
          fetched_tip_link.url         = page.url.to_s
        end
      else
        fetched_tip_link = tip_link
      end
    rescue StandardError => e
      logger.info e.message
    end

    fetched_tip_link
  end
end
