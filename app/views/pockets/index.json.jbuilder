json.array!(@pockets) do |pocket|
  json.extract! pocket, :id, :title, :description
  json.url pocket_url(pocket, format: :json)
end
