class QuestionsController < ApplicationController
  include TipHiveHelper

  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!
  before_action :set_question, only: [:show, :update, :edit, :destroy, :add_existing_tip]

  def index
    add_breadcrumb 'My Hives', my_hives_path, data: { remote: true }, class: 'active-transition'

    @hives = current_user.hives.limit(3)
    if params[:hive_id] && params[:pocket_id]
      @hive = Hive.find(params[:hive_id])
      add_breadcrumb "#{@hive.title}", '#', class: 'no-link'
      add_breadcrumb 'All Pockets', '#', id: 'all_pocket'
      @pocket    = Pocket.find(params[:pocket_id])
      @questions = current_user.fetch_questions_v3(@pocket, params)
    elsif params[:hive_id]
      @hive = Hive.find(params[:hive_id])
      add_breadcrumb "#{@hive.title}", '#', class: 'no-link'
      add_breadcrumb 'All Pockets', '#', id: 'all_pocket'
      @questions = current_user.fetch_questions_v3(@hive, params)
    elsif params[:pocket_id]
      @pocket    = Pocket.find(params[:pocket_id])
      @questions = current_user.fetch_questions_v3(@pocket, params)
    elsif params[:group_id]
      @group = Group.find(params[:group_id])
      add_breadcrumb "All hives in #{@group.title}", my_hives_path(group_id: @group.id),
                     data: { remote: true }, class: 'active-transition'
      @hives     = @group.following_hives
      @questions = current_user.fetch_questions_v3(@group, params)
    elsif params[:user_id]
      @user      = User.friendly.find(params[:user_id])
      @hives     = @user.following_hives
      @questions = current_user.fetch_questions_v3(@user, params)
    elsif params[:only_created].present?
      @questions = current_user.fetch_questions_v3(current_user, params)
    else
      @questions = current_user.fetch_questions_v3(current_user, params)
      @questions_count = current_user.get_total_user_questions(current_user)
      @tips_count = current_user.get_tipfeed_tips(current_user).count
      @friends_count = current_user.friends.reject { |r| current_user.status_with(r) == :is_following }.count
    end

    respond_to do |format|
      format.html {}
      format.js {}
      format.json {}
    end
  end

  def show
    @tips = @question.fetch_tips
  end

  def new
    @question         = current_user.questions.new
    @connected_groups = current_user.following_groups.order(:title)
    @selected_hives   = collect_items(items_list: params[:hives])
    @selected_hives.each do |hive|
      current_user.follow hive
    end
    @connected_hives  = current_user.following_hives.order(:title)
    @selected_pockets = collect_items(items_list: params[:pockets])
    @selected_pockets.each do |pocket|
      current_user.follow pocket
    end
    if @selected_hives.blank? && !@selected_pockets.blank?
      @selected_hives = @selected_pockets.first.hives
    end
    @connected_pockets = current_user.following_pockets
    if @selected_hives.present? && @selected_pockets.blank?
      @connected_pockets = @selected_hives.last.pocket_followers
    end
    @connected_friends_and_groups = []
    if params[:groups].present?
      @connected_friends_and_groups = @selected_friends_and_groups = collect_items(items_list: params[:groups])
    end
  end

  def create
    @question = current_user.questions.new(question_params)

    picture_ids = []
    question_params[:images].each do |image|
      picture = Picture.create image: image
      picture_ids << picture.id.to_s if picture
    end if question_params[:images].present?
    @question.picture_ids        = picture_ids.join(',') if picture_ids.present?

    @question.library            = 1 if params[:add_to_library] == 'on'
    @connected_groups            = current_user.following_groups.order(:title)
    @connected_hives             = current_user.following_hives.order(:title)
    @selected_hives              = collect_items(items_list: params[:hives])
    @connected_pockets           = current_user.following_pockets
    @selected_pockets            = collect_items(items_list: params[:pockets])
    @selected_friends_and_groups = collect_items(items_list: params[:select_friends_and_groups])

    # Allow API client to pass array of new hive names
    params[:new_hives].each do |new_hive_title|
      next if new_hive_title.blank?
      new_hive = current_user.hives.where(title: new_hive_title).first_or_create
      current_user.follow new_hive
      @selected_hives << new_hive
    end if params[:new_hives].present?

    # Allow API client to pass array of new pocket names
    params[:new_pockets].each do |new_pocket_title|
      next if new_pocket_title.blank?
      new_pocket = current_user.pockets.create(title: new_pocket_title)
      current_user.follow new_pocket
      @selected_pockets << new_pocket
    end if params[:new_pockets].present?

    # API: allow passing direct parameters
    @question.is_public          = params[:is_public] if %w(true false).include?(params[:is_public])
    @question.is_private         = params[:is_private] if %w(true false).include?(params[:is_private])
    @question.shared_all_friends = params[:shared_all_friends] if %w(true false).include?(params[:shared_all_friends])

    @group_id  = params[:question][:group_id]
    @hive_id   = params[:question][:hive_id]
    @pocket_id = params[:question][:pocket_id]

    respond_to do |format|
      if @question.save
        current_user.follow @question

        @selected_pockets.each do |selected_pocket|
          @selected_hives.each do |selected_hive|
            selected_pocket.follow selected_hive
          end
          @question.follow selected_pocket
          selected_pocket.following_hives.each do |following_hive|
            @question.follow following_hive
          end
        end

        # Call Question model method to connect @question with share fields recored
        @question.handle_sharing(@selected_friends_and_groups)

        @question.notify_friends if params[:notify_friends] == 'on'

        @question.notify_mentioned_friends

        if Rails.env.production?
          @mixpanel.track 'Question Created',
                          id:          @question.id,
                          description: @question.name,
                          distinct_id: current_user.id
        end

        format.html { redirect_to @question, notice: 'Question was successfully created' }
        format.json { render template: 'api/v1/questions/show' }
        format.js {}
      else
        format.js {}
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @question.errors, full_messages: @question.errors.full_messages },
                         status:   :fail, message: @question.errors.full_messages.join(', ') }
        end
      end
    end
  end

  def edit
    @question                     = current_user.questions.find(params[:id])
    @connected_groups             = current_user.following_groups.order(:title)
    @connected_hives              = current_user.following_hives.order(:title)
    @selected_hives               = @question.following_hives
    @connected_pockets            = current_user.following_pockets
    @selected_pockets             = @question.following_pockets
    @connected_friends_and_groups = @selected_friends_and_groups =
      @question.users_and_groups_and_friend_lists - [current_user]
  end

  def update
    @question             = current_user.questions.find(params[:id])
    @question.picture_ids = @question.pictures.ids.join(',')
    @following_hives      = @question.following_hives
    @following_pockets    = @question.following_pockets

    if Rails.env.production?
      @mixpanel.track 'Question Updated', id: @question.id, title: @question.name, distinct_id: current_user.id
    end

    respond_to do |format|
      if @question.update(question_params)
        if params[:select_friends_and_groups].blank?
          @question.update_attributes(is_private: true)
        end
        @question.notify_mentioned_friends
        format.html { redirect_to @question, notice: 'Question was successfully updated.' }
        format.json { render template: 'api/v1/questions/show' }
      else
        format.html { render action: 'edit' }
        format.json do
          render json: { response: { error: @question.errors, full_messages: @question.errors.full_messages },
                         status:   :fail, message: @question.errors.full_messages.join(', ') }
        end
      end
    end
  end

  def add_existing_tip
    @existing_tips = current_user.following_tips
    @selected_tips = @question.tip_followers
  end

  def like
    @question = Question.find(params[:question_id])
    current_user.likes @question
    @question.notify_question_liked current_user

    respond_to do |format|
      format.js {}
      format.json { render json: { response: { message: 'Question liked', status: :success } } }
    end
  end

  def unlike
    @question = Question.find(params[:question_id])
    current_user.unlike @question

    respond_to do |format|
      format.js {}
      format.json { render json: { response: { message: 'Question unliked', status: :success } } }
    end
  end

  def flag
    @question = Question.find(params[:question_id])
    @question.create_flagable_recorded(current_user, params[:reason])
    @question.flag_a_question(current_user)

    respond_to do |format|
      format.js {}
      format.json { render json: { response: {}, message: 'Question flagged', status: 'success' } }
    end
  end


  def destroy
    @question = current_user.questions.find(params[:id])

    @question.destroy

    if Rails.env.production?
      @mixpanel.track 'Question Destroyed', id: @question.id, title: @question.name, distinct_id: current_user.id
    end

    respond_to do |format|
      format.html { redirect_to questions_url }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  def add_comment
    @question = Question.find(params[:id])
  end

  private

  def set_question
    @question = Question.find(params[:id])
  end

  def question_params
    params.require(:question).permit(:name, :sharing_type, :is_private, :picture_ids, :anonymously, images: [])
  end
end
