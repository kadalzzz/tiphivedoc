$(document).ready(function (e) {
  // instantiate the bloodhound suggestion engine
  var search_results = new Bloodhound({
    datumTokenizer: function (d) {
      return Bloodhound.tokenizers.whitespace(d.value);
    },
    queryTokenizer: Bloodhound.tokenizers.whitespace,
    limit: 10,
    remote: {
      url: '/search.json?q=%QUERY',
      filter: function (results) {
        return $.map(results, function (result) {
          return {
            value: result.value,
            description: result.description,
            url: result.url,
            kind: result.class_name,
            thumb_url: result.thumb_url,
            extra_description: result.extra_description
          };
        });
      }
    }
  });

  // initialize the bloodhound suggestion engine
  search_results.initialize();

  // instantiate the typeahead UI
  $('#top-search-input').typeahead({
    hint: true,
    highlight: true,
    minLength: 1
  }, {
    displayKey: 'value',
    source: search_results.ttAdapter(),
    templates: {
      empty: [
        '<div class="empty-result">',
        'No result',
        '</div>'
      ].join('\n'),
      footer: function(data) {
        return [
          '<a class="empty-result" href="/search?q=' +  data.query + '" id="search-footer">',
          'Search more results for <strong>' + data.query + '</strong>...',
          '</a>'
        ].join('\n');
      },
      suggestion: Handlebars.compile([
        '<div class="media">',
        '<a class="pull-left" href="{{url}}">',
        '<img class="media-object" src="{{thumb_url}}">',
        '</a>',
        '<div class="media-body">',
        '<h4 class="media-heading">',
        '<a href="{{url}}">{{value}}</a>',
        '<small class="small">{{{extra_description}}}</small>',
        '</h4>',
        '<p class="small"><span class="label label-info">{{kind}}</span> {{{description}}}</p>',
        '</div>',
        '</div>'
      ].join('\n'))
    }
  } /* more data sets if needed */);

  $('#top-search-input').on('keydown', function(e) {
    if (e.keyCode == 13) {
      $('#search-form').submit();
    }
  });
});
