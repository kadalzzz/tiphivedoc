ActiveAdmin.register User do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  controller do
    def find_resource
      scoped_collection.where(username: params[:id]).first!
    end
  end

  config.sort_order = "id_desc"
  config.per_page = 10

  index do
    selectable_column
    column :id, sortable: :id do |user|
      link_to user.id, admin_user_path(user)
    end
    column :email
    column :first_name
    column :last_name
    column :username
    column :avatar do |user|
      image_tag user.avatar, height: 50
    end
    column :background_image do |user|
      image_tag user.background_image, height: 50
    end
    column :role
    column :sign_in_count
    column :current_sign_in_at
    column :last_sign_in_at
    column :current_sign_in_ip
    column :last_sign_in_ip
    column :failed_attempts
    column :locked_at
    column :created_at
    actions
  end

  filter :email
  filter :first_name
  filter :last_name
  filter :username
  filter :role
  filter :created_at

end
