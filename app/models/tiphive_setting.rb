class TiphiveSetting < ActiveRecord::Base
  def to_param
    "#{id} #{name}".parameterize
  end

  has_settings do |setting|
    setting.key :emails, defaults: { emails_sending_activated: true }
  end
end
