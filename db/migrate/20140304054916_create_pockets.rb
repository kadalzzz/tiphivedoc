class CreatePockets < ActiveRecord::Migration
  def change
    create_table :pockets do |t|
      t.string :title
      t.text :description
      t.boolean :is_public, default: true
      t.boolean :is_on_profile, default: true
      t.boolean :allow_add_tip, default: true
      t.boolean :allow_friend_share, default: true

      t.timestamps
    end

    add_index :pockets, :is_public
    add_index :pockets, :is_on_profile
    add_index :pockets, :allow_add_tip
    add_index :pockets, :allow_friend_share
  end
end
