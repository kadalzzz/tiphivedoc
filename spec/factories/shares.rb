FactoryGirl.define do
  factory :share do
    sharing_type 'User'
    sharing_id '1'
    shareable_type 'Hive'
    shareable_id '1'
    user_id '1'
    sharing_type_id '1'
  end
end
