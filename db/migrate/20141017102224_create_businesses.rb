class CreateBusinesses < ActiveRecord::Migration
  def change
    create_table :businesses do |t|
      t.string :title
      t.text :description
      t.string :latitude
      t.string :longitude
      t.string :address
      t.string :location
      t.string :photo_url
      t.integer :zip_code

      t.timestamps
    end
  end
end
