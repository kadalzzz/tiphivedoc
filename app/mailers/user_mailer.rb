class UserMailer < ActionMailer::Base
  default from: 'TipHive <tiphive@tiphive.com>'

  def add_friend_email(params)
    logger.info "*** add_friend_email: #{params}"
    get_email_info(params)
    mail(to: @user_email, subject: "#{@friend_name} added you as a friend")
  end

  def accept_friend_email(params)
    logger.info "*** accept_friend_email: #{params}"
    get_email_info(params)
    mail(to: @user_email, subject: "#{@friend_name} accepted your friend request")
  end

  def add_tip_on_question_email(params)
    logger.info "*** add_tip_on_question_email: #{params}"
    get_email_info(params)
    mail(to: @user_email, subject: "#{@friend_email} added a tip on your question")
  end

  def add_tip_email(params)
    logger.info "*** add_tip_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "New tip from #{@user_name}", reply_to: "reply+tips+#{@access_key}@tiphive.com") unless @is_shared
    mail(to: @friend_email, subject: "#{@user_name} shared a tip", reply_to: "reply+tips+#{@access_key}@tiphive.com") if @is_shared
  end

  def add_question_tip_email(params)
    logger.info "*** add_question_tip_email: #{params}"
    get_email_info(params)
    mail(to: @user_email, subject: "New tip added to question from #{@user_name}")
  end

  def add_question_email(params)
    logger.info "*** add_question_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "New question from #{@user_name}", reply_to: "reply+questions+#{@access_key}@tiphive.com") unless @is_shared
    mail(to: @friend_email, subject: "#{@user_name} shared a question", reply_to: "reply+questions+#{@access_key}@tiphive.com") if @is_shared
  end

  def add_tip_comment_email(params)
    logger.info "*** add_tip_comment_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} commented on a tip", reply_to: "reply+tips+#{@access_key}@tiphive.com")
  end

  def add_question_comment_email(params)
    logger.info "*** add_tip_comment_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} commented on a question")
  end

  def comment_mention_email(params)
    logger.info "*** comment_mention_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} mentioned you in a comment")
  end

  def comment_mention_email_for_question(params)
    logger.info "*** comment_mention_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} mentioned you in a comment")
  end

  def user_mention_email_for_question(params)
    logger.info "*** question_mention_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} mentioned you in a question")
  end

  def user_mention_email_for_tip(params)
    logger.info "*** tip_mention_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} mentioned you in a tip")
  end

  def comment_like_email_for_question(params)
    logger.info "*** comment_mention_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} commented on a question you liked")
  end

  def add_hive_email(params)
    logger.info "*** add_hive_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "New hive from #{@user_name}") unless @is_shared
    mail(to: @friend_email, subject: "#{@user_name} shared a hive") if @is_shared
  end

  def add_pocket_email(params)
    logger.info "*** add_pocket_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "New pocket from #{@user_name}") unless @is_shared
    mail(to: @friend_email, subject: "#{@user_name} shared a pocket") if @is_shared
  end

  def added_to_group_email(params)
    logger.info "*** added_to_group_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} added you to a group")
  end

  def like_tip_email(params)
    logger.info "*** like_tip_email: #{params}"
    get_email_info(params)
    mail(to: @user_email, subject: "#{@friend_name} liked your tip")
  end

  def like_question_email(params)
    logger.info "*** like_tip_email: #{params}"
    get_email_info(params)
    mail(to: @user_email, subject: "#{@friend_name} liked your question")
  end

  def group_owner_request_notification_email(params)
    logger.info "*** group_owner_request_notification_email: #{params}"
    get_email_info(params)
    mail(to: @user_email, subject: "Someone requested a group invite")
  end

  def group_invite_request_notification_email(params)
    logger.info "*** group_invite_request_notification_email: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "Your request to join #{@connectable_title} on TipHive")
  end

  def daily_feed_email(params)
    logger.info "*** daily_feed_email: #{params}"
    get_email_info(params)
    @tips = Tip.where('id IN (?)', @tip_ids)
    return if @tips.blank?
    @user = User.where(id: @user_id).first
    if @user.present?
      @user.daily_sent_at = DateTime.now
      @user.save
    end
    mail(to: @user_email, subject: "Check out new tips")
  end

  def daily_tip_email(user, questions)
    logger.info "*** daily tip email ***"
    @user = user
    @questions = questions
    mail(to: @user.email, subject: 'Tip a day')
  end

  def weekly_feed_email(params)
    logger.info "*** weekly_feed_email: #{params}"
    get_email_info(params)
    @tips = Tip.where('id IN (?)', @tip_ids)
    return if @tips.blank?
    @user = User.where(id: @user_id).first
    if @user.present?
      @user.weekly_sent_at = DateTime.now
      @user.save
    end
    mail(to: @user_email, subject: "Check out new tips")
  end

  def test_email(params)
    logger.info "*** test_email: #{params}"
    params['test_email'] = 'long.tran@saigonshare.net' if params['test_email'].blank?
    mail(to: params['test_email'], subject: 'Email sending via Sidekiq is functioning')
  end

  def flag_tip_email(params)
    get_email_info(params)
    logger.info "*** flagged a tip: #{params}"
    mail(to: 'flagged@tiphive.com', subject: "#{@user_first_name} flagged a tip" )
  end

  def flag_question_email(params)
    get_email_info(params)
    logger.info "*** flagged a question: #{params}"
    mail(to: 'flagged@tiphive.com', subject: "#{@user_first_name} flagged a question" )
  end

  def added_hive_to_group(params)
    logger.info "*** added_hive_to_group: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} added hive to a group")
  end

  def add_user_to_domain(params)
    logger.info "*** added user to domain: #{params}"
    get_email_info(params)
    mail(to: @friend_email, subject: "#{@user_name} added #{@friend_email} to domain")
  end

  private

  def get_email_info(params)
    @access_key = params['access_key']

    @comment_id = params['comment_id']

    @connectable_title = params['connectable_title']

    @domain = Domain.active.find_by(id: params['domain_id']) || Domain.root

    @invitation_requests_url = invitation_requests_url(subdomain: @domain.name)

    @friend_email = params['friend_email']
    @friend_first_name = params['friend_first_name']
    @friend_name = params['friend_name']
    @friend_username = params['friend_username']
    @friend_profile_url = user_url(@friend_username, subdomain: @domain.name) if @friend_username.present?

    @group_slug = params['group_slug']
    @group_title = params['group_title']
    @group_url = group_url(@group_slug, subdomain: @domain.name) if @group_slug.present?

    @hive_slug = params['hive_slug']
    @hive_title = params['hive_title']
    @hive_url = hive_url(@hive_slug, subdomain: @domain.name) if @hive_slug.present?
    @hives_title = params['hives_title']

    @is_shared = params['is_shared']

    @pocket_slug = params['pocket_slug']
    @pocket_title = params['pocket_title']
    @pocket_url = pocket_url(@pocket_slug, subdomain: @domain.name) if @pocket_slug.present?

    @question_hive_pocket_title = params['question_hives_and_pockets_title']
    @question_slug = params['question_slug']
    @question_title = params['question_title']
    @question_url = question_url(@question_slug, subdomain: @domain.name) if @question_slug.present?
    @question_url = question_url(@question_slug, subdomain: @domain.name, anchor: "comment-#{@comment_id}") if @question_slug.present? and @comment_id.present?

    @tip_hive_pocket_title = params['tip_hive_pocket_title']
    @tip_ids = params['tip_ids']
    @tip_slug = params['tip_slug']
    @tip_title = params['tip_title']
    @tip_url = tip_url(@tip_slug, subdomain: @domain.name) if @tip_slug.present?
    @tip_url = tip_url(@tip_slug, subdomain: @domain.name, anchor: "comment-#{@comment_id}") if @tip_slug.present? and @comment_id.present?

    @user_email = params['user_email']
    @user_first_name = params['user_first_name']
    @user_id = params['user_id']
    @user_name = params['user_name']
  end

end
