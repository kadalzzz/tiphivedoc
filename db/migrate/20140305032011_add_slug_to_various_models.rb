class AddSlugToVariousModels < ActiveRecord::Migration
  def change
    add_column :groups, :slug, :string
    add_column :hives, :slug, :string
    add_column :pockets, :slug, :string
    add_column :tips, :slug, :string
    add_column :users, :username, :string

    add_index :groups, :slug, unique: true
    add_index :hives, :slug, unique: true
    add_index :pockets, :slug, unique: true
    add_index :tips, :slug, unique: true
    add_index :users, :username, unique: true
  end
end
