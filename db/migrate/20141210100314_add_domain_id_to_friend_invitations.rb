class AddDomainIdToFriendInvitations < ActiveRecord::Migration
  def change
    add_column :friend_invitations, :domain_id, :integer
    add_index :friend_invitations, :domain_id
  end
end
