json.id pocket.id
json.title pocket.title
json.description pocket.description
json.background_image_url pocket.background_image.present? ? pocket.background_image.url : ''
json.tips_count pocket.tip_followers.count
