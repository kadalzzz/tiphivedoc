module ActsAsShareable
  extend ActiveSupport::Concern

  included do
    has_many :shares, as: :shareable_object, dependent: :destroy
  end

  # Creates a new share record for this instance to follow the passed object.
  def share(object, owner)
    shares.find_or_create_by(sharing_object_id: object.id, sharing_object_type: object.class.name, user_id: owner.id)
  end

  # Deletes the share record if it exists
  def stop_sharing(object, owner)
    obj = get_sharing_object(object, owner)
    obj.destroy if obj
  end

  # Get all shared objects
  def get_shared_objects(user)
    shares.includes(:sharing_object)
          .where(user_id: user.id, sharing_object_type: %w(FriendList Group User))
          .map(&:sharing_object)
  end

  protected

  def get_sharing_object(sharing, user)
    shares.where('user_id = ? and sharing_object_id = ? and sharing_object_type = ?',
                 user.id, sharing.id, sharing.class.name).first
  end
end
