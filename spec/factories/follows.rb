FactoryGirl.define do
  factory :follow do
    follower_id 1
    follower_type 'Type'
    followable_id 1
    followable_type 'Type'
  end
end
