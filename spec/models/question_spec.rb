require 'rails_helper'

describe Question do
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to have_many(:tips) }
  it { is_expected.to belong_to(:user) }

  describe '#class_name' do
    let(:question) { build(:question) }
    it 'returns class name' do
      expect(question.class_name).to eq 'Question'
    end
  end

  describe '#kind_id' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:question) { create(:question, user_id: user.id) }
    it 'returns pair of class name and id' do
      expect(question.kind_id).to eq "#{question.class.name}-#{question.id}"
    end
  end

  describe '#to_param' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:question) { create(:question, user_id: user.id) }
    it 'returns parameterized value of id and name' do
      expect(question.to_param).to eq "#{question.id}-#{question.name.downcase}"
    end
  end

  describe '#title' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:question) { create(:question, user_id: user.id) }
    it 'returns question title' do
      expect(question.title).to eq "#{question.name}"
    end
  end

  describe 'scope #library_items' do
    before { 10.times { create(:question, library: true) } }
    before { 10.times { create(:question) } }
    subject { Question.library_items }
    its(:count) { should eq 10 }
    it 'returns only items which has library set to true' do
      expect(subject.map(&:library).uniq).to eq [true]
    end
  end

  describe 'scope #not_sent' do
    before { 10.times { create(:question, sent_at: Time.now) } }
    before { 10.times { create(:question) } }
    subject { Question.not_sent }
    its(:count) { should eq 10 }
    it 'returns only items which has sent_at is nil' do
      expect(subject.map(&:sent_at).uniq).to eq [nil]
    end
  end

  describe 'scope #all_friends_sharing' do
    before do
      5.times { create(:question, shared_all_friends: true) }
      5.times { create(:question, is_public: true) }
    end
    it 'returns items with sharing type all friends' do
      expect(Question.all_friends_sharing.count).to eq 5
      expect(Question.all_friends_sharing.map(&:shared_all_friends).uniq).to eq [true]
    end
  end

  describe 'scope #public_sharing' do
    before do
      5.times { create(:question, shared_all_friends: true) }
      5.times { create(:question, is_public: true) }
    end
    it 'returns items with sharing type public' do
      expect(Question.public_sharing.count).to eq 5
      expect(Question.public_sharing.map(&:is_public).uniq).to eq [true]
    end
  end

  describe 'scope #private_sharing' do
    before do
      5.times { create(:question, is_private: true) }
      5.times { create(:question, is_public: true) }
    end
    it 'returns items with sharing type private' do
      expect(Question.private_sharing.count).to eq 5
      expect(Question.private_sharing.map(&:is_private).uniq).to eq [true]
    end
  end

  describe 'scope #by_owner' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:user1) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    before { 5.times { create(:question, user_id: user.id) } }
    it 'returns owner questions' do
      expect(Question.by_owner(user).count).to eq 5
    end
  end

  describe '#hives' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { create(:pocket) }
    let(:question) { create(:question) }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
    end
    subject { question.hives }
    it 'returns hives' do
      is_expected.to eq [hive]
    end
  end

  describe '#pockets' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { create(:pocket) }
    let(:question) { create(:question) }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
    end
    subject { question.pockets }
    it 'returns pockets' do
      is_expected.to eq [pocket]
    end
  end

  describe '#users' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:question) { create(:question) }
    let(:pocket) { hive.pockets.first }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
      user.follow(question)
    end
    it 'returns users' do
      expect(question.users).to eq [user]
    end
  end

  describe '#groups' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:question) { create(:question, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:group) { create(:group) }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
      group.follow(question)
    end

    it 'returns group followed' do
      expect(question.groups).to eq [group]
    end
  end

  describe '#friend_lists' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:question) { create(:question, user_id: user.id) }
    let(:friend_list) { create(:friend_list) }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
      friend_list.follow(question)
    end

    it 'returns friend_list followed' do
      expect(question.friend_lists).to eq [friend_list]
    end
  end

  describe '#users_and_groups' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:question) { create(:question, user_id: user.id) }
    let(:group) { create(:group) }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
      user.follow(question)
      group.follow(question)
    end

    it 'returns users and group followed' do
      expect(question.users_and_groups).to eq [user, group]
    end
  end

  describe '#users_and_groups_and_friend_lists' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:question) { create(:question, user_id: user.id) }
    let(:group) { create(:group) }
    let(:friend_list) { create(:friend_list) }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
      user.follow(question)
      group.follow(question)
      friend_list.follow(question)
    end

    it 'returns users and group and friend_list followed' do
      expect(question.users_and_groups_and_friend_lists).to eq [user, group, friend_list]
    end
  end

  describe '#hives_and_pockets_title'  do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { create(:pocket) }
    let(:question) { create(:question) }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
    end
    subject { question.hives_and_pockets_title }
    it 'returns hives and pockets title' do
      is_expected.to eq 'Hive / Pocket'
    end
  end

  describe '#set_color' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:question) { build(:question, user_id: user.id) }

    it 'returns set question color' do
      expect(question.color).to eq question.color
    end
  end

  describe '#generate_hash' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:question) { build(:question, user_id: user.id) }

    it 'returns hash generate access key' do
      expect(question.access_key).to eq question.access_key
    end
  end

  describe '#set_question_id_null' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:question) { create(:question, user_id: user.id) }
    let(:tip) { create(:tip, user_id: user.id, question_id: question.id) }

    subject { question.set_question_id_null }
    it 'returns hash generate access key' do
      is_expected.to eq []
    end
  end

  describe '#follow_shared_objects' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:friend) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:not_friend) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:question) { create(:question, user_id: user.id) }
    let(:group) { create(:group) }
    let(:friend_list) { create(:friend_list) }
    before do
      question.follow(hive)
      question.follow(pocket)
      pocket.follow(hive)
      user.follow(question)
      user.follow(friend)
      friend.follow(user)
      group.follow(question)
      friend_list.follow(question)
      InvitationRequest.current_domain = Domain.root
    end

    context 'when user is friend' do
      subject { question.follow_shared_objects([group, friend_list, friend]) }
      it 'follow shared object' do
        is_expected.to be_truthy
      end
    end

    context 'when user is not friend then send invitation request before sharing question' do
      subject { question.follow_shared_objects([not_friend, friend_list]) }
      it 'create user default settings all friends for this hive' do
        is_expected.to be_truthy
      end
    end
  end
end
