class AddMissingIndexes < ActiveRecord::Migration
  def change
    add_index :assignments, :user_id
    add_index :assignments, :role_id

    add_index :notifications, [:sender_id, :sender_type]
    add_index :notifications, [:notified_object_id, :notified_object_type], name: 'index_notifications_on_object_id_and_object_type'
    add_index :receipts, [:receiver_id, :receiver_type]
    add_index :users, [:invited_by_id, :invited_by_type]
  end
end
