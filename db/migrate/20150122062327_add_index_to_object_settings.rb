class AddIndexToObjectSettings < ActiveRecord::Migration
  def change
    add_index :object_settings, :user_id
  end
end
