class AddAdminIdsToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :admin_ids, :string
  end
end
