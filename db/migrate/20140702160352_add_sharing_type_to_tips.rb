class AddSharingTypeToTips < ActiveRecord::Migration
  def change
    add_column :tips, :sharing_type, :string, default: 'all_friends'
    add_index :tips, :sharing_type
  end
end
