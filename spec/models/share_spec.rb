require 'rails_helper'

describe Share do
  it { is_expected.to belong_to(:sharing_object) }
  it { is_expected.to belong_to(:shareable_object) }
  it { is_expected.to belong_to(:user) }

  describe '#share!' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:user1) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    before do
      hive.share(user1, user)
    end
    it { is_expected.to callback(:share!).after(:create) }
  end

  describe '#unshare!' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:user1) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    before do
      hive.share(user1, user)
      hive.stop_sharing(user1, user)
    end
    it { is_expected.to callback(:unshare!).before(:destroy) }
  end
end
