class AddBackgroundColorToHives < ActiveRecord::Migration
  def change
  	add_column :hives, :background_color, :string
  end
end
