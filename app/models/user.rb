class User < ActiveRecord::Base
  acts_as_followable
  acts_as_follower
  acts_as_voter

  enum role: [:normal, :admin, :root]

  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :invitable, :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable,
         :omniauthable, :invitable, :async


  extend FriendlyId
  friendly_id :slug_candidates, use: :slugged, slug_column: :username, sequence_separator: '_'

  # Associations
  has_many :identities, dependent: :destroy
  has_many :hives, dependent: :nullify
  has_many :pockets, dependent: :nullify
  has_many :tips, dependent: :destroy
  has_many :groups, dependent: :destroy
  has_many :comments, dependent: :destroy
  has_many :pictures, dependent: :nullify, inverse_of: :user
  has_many :tip_links, dependent: :nullify, inverse_of: :user
  has_many :invitation_requests
  has_many :friend_invitations
  has_many :questions, dependent: :destroy
  has_many :oauth_applications, class_name: 'Doorkeeper::Application', as: :owner
  has_many :object_settings, foreign_key: 'user_id', dependent: :destroy
  has_many :shares, foreign_key: 'user_id', dependent: :destroy
  has_many :pictures, as: :imageable, dependent: :destroy
  has_many :domains
  has_many :social_friends, dependent: :destroy
  has_many :emails
  # Callbacks
  after_create :default_role
  before_create :set_color

  # Validations
  validates :first_name, presence: true, on: :create

  mount_uploader :avatar, AvatarUploader
  mount_uploader :background_image, BackgroundImageUploader
  include UserObserver
  include TipHiveObject
  include TipsFinderV2
  include QuestionsFinder
  include ActsAsSharing
  include ActsAsFlag

  COLOR = %w(liteorange orange purple litepurple green liteblue blue)

  attr_accessor :picture_ids
  # Shashi: assign current_domain to user model
  cattr_accessor :current_domain

  paginates_per 10

  default_value_for :background_image_top, 0
  default_value_for :background_image_left, 0

  has_settings do |s|
    s.key :email_notifications,
          defaults: {
            friend_adds_tip:               'daily', #['always', 'daily', 'weekly', 'never']
            friend_adds_question:          true,
            someone_likes_tip:             true,
            someone_likes_question:        true,
            friend_joins:                  true,
            someone_adds_me_as_friend:     true,
            someone_invite_me_to_group:    true,
            group_member_adds_a_tip:       true,
            group_member_adds_a_question:  true,
            someone_shared_hive_with_me:   true,
            someone_shared_pocket_with_me: true
          }
  end

  searchable do
    text :name, :email, :username
  end

  def slug_candidates
    [
      [:first_name, :last_name],
      [:first_name, :last_name, :id]
    ]
  end

  # Use default slug, but with underscores
  def normalize_friendly_id(_string)
    super.gsub('-', '_')
  end

  def title
    "#{first_name} #{last_name}"
  end

  def name
    "#{first_name} #{last_name}"
  end

  def short_name
    first_name + " #{last_name.present? ? "#{last_name[0]}." : ''}"
  end

  # This will be called by Devise gem
  def self.new_with_session(params, session)
    super.tap do |user|

      if session['devise.facebook_data'].present?
        data = session['devise.facebook_data']
        kind = 'facebook'
      end

      if session['devise.twitter_data'].present?
        data = session['devise.twitter_data']
        kind = 'twitter'
      end

      if session['devise.google_oauth2_data'].present?
        data = session['devise.google_oauth2_data']
        kind = 'google_oauth2'
      end

      if data.present?
        logger.info "*** data: #{data.info.to_yaml}"
        user.email      = data['email'] if user.email.blank?
        if user.first_name.blank?
          user.first_name = data.info.try(:first_name) || data.info.try(:given_name) || data.info.try(:name)
        end
        user.last_name  = data.info.try(:last_name) || data.info.try(:family_name) if user.last_name.blank?

        if data.info.image.present?
          image_url = data.info.image
          image_url = "#{data.info.image}?type=large" if kind == 'facebook'
          image_url = data.info.image.gsub('normal', 'original') if kind == 'twitter'
          logger.info "*** process_select_avatar - image_url: #{image_url}"
          image_response = HTTParty.get image_url, follow_redirects: false

          logger.info "*** image_response.headers: #{image_response.headers.to_yaml}"

          if image_response.code == 302
            image_url = image_response.headers['location']
            logger.info "*** process_select_avatar - image_url: #{image_url}"

            another_image_response = HTTParty.get image_url, follow_redirects: false
            image_url              = nil if another_image_response.code != 200
          end

          # for twitter user image get response 404
          if image_response.code == 404
            image_url = image_response.headers['location']
            logger.info "*** process_select_avatar - image_url: #{image_url}"
            image_url = nil if image_response.code != 200
          end

          logger.info "*** new_with_session final image url: #{image_url}"
        end

        user.remote_avatar_url = image_url if user.avatar.blank? && data.info.image.present? && image_url.present?
      end
    end
  end

  # check user relationship with other
  def status_with(other_user)
    return :is_me if id == other_user.id

    is_following = self.following?(other_user)
    is_followed  = other_user.following?(self)

    return :is_following if is_following && !is_followed
    return :is_followed if is_followed && !is_following
    return :is_friend if is_following && is_followed
    return :is_nothing if !is_following && !is_followed
  end

  def friend_with?(user)
    status_with(user) == :is_friend ? true : false
  end

  def following_domains
    [Domain.root] + super.active
  end

  # In current definition: friend is someone you follow, not people are following you
  def friends
    current_domain.root? ? following_users : current_domain.members
  end

  def friend_lists
    following_friend_lists
  end

  # Fetch all user which is not current user friends
  def not_friends
    User.where('id NOT IN (?)', friends.ids + [id])
  end

  def friend_ids
    following_users.ids
  end

  def follower_ids
    user_followers.ids
  end

  def connected_hives
    [following_hives, following_groups.map { |g| g.following_hives.all }].flatten
  end

  def send_tips_feed(options = {})
    options[:order] ||= 'updated_at DESC'
    options[:days]  ||= -365 # get tips in 1 year
    days_before     = DateTime.now.advance(days: options[:days])

    today_date    = DateTime.now.to_date

    # daily
    email_sent_at = daily_sent_at.to_date if daily_sent_at.present? && options[:days] == -1
    # weekly
    email_sent_at = weekly_sent_at.to_date if weekly_sent_at.present? && options[:days] == -7

    return if email_sent_at.present? && today_date.to_time - email_sent_at.to_time == 0

    trackable_ids = PublicActivity::Activity.
      where('activities.created_at >= ?', days_before).
      where(key: 'tip.feed', recipient_id: id).
      pluck(:trackable_id)

    tip_ids       = Tip.where('tips.created_at >= ?', days_before).
      where('tips.user_id = ? OR tips.id IN (?)', id, trackable_ids).uniq.
      order("tips.#{options[:order]}").pluck(:id)

    email_params = {
      user_id:    id,
      user_email: email,
      tip_ids:    tip_ids
    }

    if tip_ids.count > 0
      if options[:days] == -1 && settings(:email_notifications).friend_adds_tip == 'daily'
        EmailSendingWorker.perform_async('UserMailer', 'daily_feed_email', email_params)
      end

      if options[:days] == -7 && settings(:email_notifications).friend_adds_tip == 'weekly'
        EmailSendingWorker.perform_async('UserMailer', 'weekly_feed_email', email_params)
      end
    end
  end

  def friends_and_groups
    [friends, following_groups.all].flatten
  end

  def ensure_authentication_token
    if authentication_token.blank?
      self.authentication_token = generate_authentication_token
    end
  end

  def collect_social_friends_email(auth)
    emails = []

    if auth == 'facebook'
      social_friends.each do |friend|
        emails << (friend.username + '@facebook.com') unless friend.username.blank?
      end
    elsif auth == 'google_oauth2'
      social_friends.each do |friend|
        emails << (friend.email) unless friend.email.blank?
      end
    end

    emails.flatten
  end

  # Gives groups that the user is administering
  def administering_groups
    Group.all.select { |g| g.admin_ids.include? id }
  end

  # to check that this user active in 30 days
  def check_active
    today        = DateTime.now
    minimum_days = today - 30.days
    last_tip     = tips.last.created_at if tips.last.present?
    last_hive    = hives.last.created_at if hives.last.present?
    last_pocket  = pockets.last.created_at if pockets.last.present?
    last_comment = comments.last.created_at if comments.last.present?
    last_vote    = votes.last.created_at if votes.last.present?

    active?(minimum_days, last_tip) ||
    active?(minimum_days, last_hive) ||
    active?(minimum_days, last_pocket) ||
    active?(minimum_days, last_comment) ||
    active?(minimum_days, last_vote)
  end

  def active?(minimum, last)
    last.present? ? last > minimum : false
  end

  def matched_percentage_with(another_user)
    my_hives_ids      = following_hives.pluck(:id)
    matched_hives_ids = Follow.where(follower_id: another_user.id, follower_type: 'User', followable_id: my_hives_ids,
                                     followable_type: 'Hive').pluck(:followable_id)
    (matched_hives_ids.count.to_f / my_hives_ids.count.to_f).round(2)
  end

  def set_color
    color = COLOR[rand(COLOR.length)]
  end

  def get_percentage(another_user)
    return if [another_user, self].all?(&:blank?)
    arr_friend   = another_user.following_hives.ids
    arr_current  = following_hives.ids
    intersection = arr_friend & arr_current
    percentage   = percentage_score(arr_current, intersection)
    percentage
  end

  def percentage_score(arr_current, intersection)
    if arr_current.blank?
      percentage = 0
    else
      percentage = (intersection.length.to_f / arr_current.length.to_f) * 100
      percentage = percentage.round(0)
    end
    percentage
  end

  private

  def tip_ids
    ids = []
    following_groups.each do |group|
      ids << group.following_tips.pluck(:id)
    end
    ids.flatten
  end

  def tips_in_circle

  end

  def generate_authentication_token
    loop do
      token = Devise.friendly_token
      break token unless User.where(authentication_token: token).first
    end
  end

  def default_role
    self.normal!
  end
end
