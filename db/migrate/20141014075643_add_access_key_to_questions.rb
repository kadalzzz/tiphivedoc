class AddAccessKeyToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :access_key, :string
    add_index :questions, :access_key
  end
end
