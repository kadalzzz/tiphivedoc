class AddLocationFieldToTips < ActiveRecord::Migration
  def change
    add_column :tips, :location, :string
  end
end
