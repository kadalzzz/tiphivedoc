hive_pockets = hive.pocket_followers.order('id DESC').limit(6)
hive_tips    = hive.tip_followers.order('id DESC')

json.id hive.id
json.title hive.title
json.description hive.description
json.background_image_url hive.background_image.present? ? hive.background_image.url : ''
# json.background_image hive.background_image
json.pockets_count hive.pocket_followers.count
json.tips_count hive.tip_followers.count

json.pockets do
  json.array!(hive_pockets) do |pocket|
    json.partial! 'api/v1/pockets/pocket', pocket: pocket
  end
end if params[:include_pockets].present?

json.tips hive_tips.map do |tip|
  {
    id:          tip.id,
    title:       tip.title,
    description: tip.description,
  }
end if params[:include_tips].present?
