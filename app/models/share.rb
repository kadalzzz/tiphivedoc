class Share < ActiveRecord::Base
  belongs_to :user
  belongs_to :sharing_object, polymorphic: true
  belongs_to :shareable_object, polymorphic: true

  searchable do
    string :sharing_object_type
    string :shareable_object_type

    integer :sharing_object_id
    integer :shareable_object_id
    integer :user_id
  end

  # Shashi: assign current_user to share model
  cattr_accessor :current_user

  after_create :share!
  before_destroy :unshare!

  protected

  def share!
    return if [shareable_object, user, sharing_object, current_user].all?(&:blank?)
    # When a Hive is share with User
    return unless shareable_object.is_a?(Hive) && user == current_user
    case sharing_object
    when User
      if user.status_with(sharing_object) == :is_friend
        # Notify {user}
        shareable_object.notify_friend(sharing_object, user)
        # Let User follow Hive tips
        tip_ids = shareable_object.tips
        tips    = Tip.where(id: tip_ids).by_owner(user)
        tips.each do |tip|
          sharing_object.follow(tip)
          tip.update_attributes!(is_private: false) if tip.is_private?
        end
      end
    when FriendList
      # When a Hive is share with 'All friends or Public'
      # Let 'All friends or Public' follow Hive tips
      tip_ids = shareable_object.tips
      tips    = Tip.where(id: tip_ids).by_owner(user)
      tips.each do |tip|
        # Update tip bolean attributes If tip have private settings
        tip.update_attributes!(is_private: false) if tip.is_private?
        # Update tip bolean attributes If tip share with 'All friends or Public'
        sharing_object.update_object_sharing_status_true(tip)
        # {friend_lists_object} follow tip
        sharing_object.follow(tip)
      end

      return unless sharing_object.name == 'All Friends' || sharing_object.name == 'All Domain Members'
      # Get all user friends
      friends = user.friends
      friends.each do |item|
        if item
          # Notify user to sharable object has been shared and get only first time notification
          shareable_object.notify_friend(item, user) if Follow.status_with(item, shareable_object) != :is_following
          item.follow(shareable_object)
          # Create default sharing setting for user friend to 'All Friends'
          shareable_object.share(sharing_object, item)
        end
      end
    end
  end

  def unshare!
    return if [shareable_object, user, sharing_object, current_user].all?(&:blank?)
    # When a Hive is disconnect with User
    return unless shareable_object.is_a?(Hive) && user == current_user
    case sharing_object
    when User
      # Disconnect all hive tips for this user
      tip_ids = shareable_object.tips
      tips    = Tip.where(id: tip_ids).by_owner(user)
      tips.each do |tip|
        sharing_object.stop_following(tip)
      end
      # When a Hive is disconnect with 'All friends or Public'
    when FriendList
      tip_ids = shareable_object.tips
      tips    = Tip.where(id: tip_ids).by_owner(user)
      tips.each do |tip|
        sharing_object.stop_following(tip)
        sharing_object.update_object_sharing_status_false(tip)
      end
    end
  end
end
