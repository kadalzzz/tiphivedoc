class AddIsAutoAcceptToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :is_auto_accept, :boolean, default: false
  end
end
