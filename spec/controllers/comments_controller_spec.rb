require 'rails_helper'

describe CommentsController do
  describe '#GET index' do
    it 'assigns @comments' do
      comment = create(:comment)
      get :index
      expect(assigns(:comments)).to eq [comment]
    end
  end

  describe '#POST create' do
    context 'when object_id is Tip' do
      let(:tip) { create(:tip) }
      let(:user) { create(:user) }
      let(:params) { {object_kind: 'Tip', object_id: tip.id, comment: {body: 'body', subject: 'subject', user_id: user.id}} }
      before { login_user; post :create, params }
      it 'calls Comment.build_from' do
        Comment.expects(:build_from).returns(true)
      end
    end
  end
end
