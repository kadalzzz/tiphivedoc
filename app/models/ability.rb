class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new

    if user.root?
      can :manage, :all
    elsif user.admin?
      cannot :manage, User
      can :manage, Group do |group|
        group.members.include?(user) && group.admin_ids.include?(user.id)
      end
    else
      can :update, Comment do |comment|
        comment.try(:user_id) == user.id
      end
      can :destroy, Comment do |comment|
        comment.try(:user_id) == user.id
      end
      can :manage, Group do |group|
        group.user_id == user.id
      end
    end

  end
end
