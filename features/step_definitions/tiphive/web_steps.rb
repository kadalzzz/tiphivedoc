# Click on any input with selector
When /^I click on element with selector "([^\"]+)"$/ do |selector|
  patiently do
    evaluate_script "$('#{selector}').click()"
  end
end

# Click on any input that has an ID
When /^I click on element with id "([^\"]+)"$/ do |value|
  patiently do
    evaluate_script "$('##{value}').click()"
  end
end

# Submit form
When /^I submit form with id "([^\"]+)"$/ do |value|
  patiently do
    evaluate_script "$('##{value}').submit()"
  end
end
