ActiveAdmin.register Comment, as: 'Tip Comments' do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  config.sort_order = "id_desc"
  config.per_page = 10

  controller do
    def scoped_collection
      resource_class.where(commentable_type: 'Tip').includes(:user, :commentable) # prevents N+1 queries to database
    end
  end

  index do
    selectable_column
    column :id, sortable: :id do |comment|
      link_to comment.id, admin_comment_path(comment)
    end
    column :title
    column :body do |comment|
      comment.body.html_safe
    end
    column :subject
    column :commentable_type do |comment|
      tip = comment.commentable
      link_to tip.title, admin_tip_path(tip) if tip.present?
    end
    column :user do |comment|
      user = comment.user
      link_to user.name, admin_user_path(user) if user.present?
    end
    column :created_at
    actions
  end

  filter :title
  filter :body
  filter :subject
  filter :commentable_type
  filter :user
  filter :created_at

end
