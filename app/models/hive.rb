class Hive < ActiveRecord::Base
  include Tenant
  include PublicActivity::Model
  include HiveObserver
  include TipHiveObject
  include ActsAsShareable
  include ObjectSettingObserver

  tracked owner: Proc.new { |controller, _model| controller.current_user if controller }

  acts_as_followable

  validates :title, presence: true

  COLOR = %w(liteorange orange purple litepurple green liteblue blue)

  before_create :set_background_color

  mount_uploader :background_image, BackgroundImageUploader

  has_many :pictures, as: :imageable, dependent: :destroy
  belongs_to :user

  attr_accessor :picture_ids

  paginates_per 4

  searchable do
    text :title, :description
    integer :user_id
    boolean :is_public, :is_on_profile, :allow_add_pocket, :allow_friend_share
    string :sharing_type
  end

  # extend FriendlyId
  # friendly_id :slug_candidates, use: :slugged
  def to_param
    "#{id} #{name}".parameterize
  end

  def slug_candidates
    [
      [:id, :title]
    ]
  end

  def name
    title
  end

  def pockets
    pocket_followers
  end

  def tips
    pocket_followers.map(&:tip_followers).flatten
  end

  def questions
    question_followers
  end

  def users
    user_followers
  end

  def groups
    group_followers
  end

  def friend_lists
    friend_list_followers
  end

  def title_with_groups
    return title if group_followers.count == 0
    "#{group_followers.map(&:title).join(', ')} / #{title}"
  end

  def users_and_groups
    [users.all, groups.all].flatten
  end

  def users_and_groups_and_friend_lists
    [users.all, groups.all, friend_lists].flatten
  end

  def self.search_pocket(hives, query = '')
    @pockets = []
    hives.each do |hive|
      @pockets += hive.pockets.where('title like ?', "%#{query}%")
    end
    @pockets.flatten
  end

  def set_background_color
    self.background_color = COLOR[rand(COLOR.length)]
  end

  def self.get_hive_sharing_permission(hives, user)
    results = []
    result  = []

    hives.each do |hive|
      object_setting = hive.find_object_settings(user)
      if object_setting.blank?
        (hive.is_private == false) ? results << 'select_friends' : results << 'private'
      else
        (object_setting.is_private == false) ? results << 'select_friends' : results << 'private'
      end
    end

    if results.include? 'select_friends'
      result << ['select_friends']
    elsif results.include? 'private'
      result << ['private']
    end

    result.flatten
  end

  def handle_sharing(shared_objects)
    owner            = user
    all_friends_list = FriendList.find_by_name 'All Friends'

    return if is_private

    if shared_objects.blank?
      update_attributes(is_private: true)
      return
    end

    shared_objects.each do |object|
      class_name = object.class.name
      # when current_user shares hive with non-friend, send invitation request to user before sharing hive
      if class_name == 'User' && owner.status_with(object) == :is_nothing
        InvitationRequest.send_invitation_request(self, owner, object)
      end

      # Object follow Hive if object class name Group, FriendList, User (user should be friend of current user)
      if (class_name == 'User' && owner.status_with(object) == :is_friend) || %w(Group FriendList).include?(class_name)
        object.follow self
      end

      # create Hive sharing setting for current user
      share(object, owner)

      # If hive share to user, create hive default setting 'All Friends' for user
      share(all_friends_list, object) if class_name == 'User'
    end
  end

  def followed_by_me?(user)
    if user.present?
      user_hives = user.following_hives.collect(&:id)
      user_hives.include?(id)
    end
  end
end
