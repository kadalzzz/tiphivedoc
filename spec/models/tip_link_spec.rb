require 'rails_helper'

describe TipLink do
  it { is_expected.to validate_presence_of(:url) }
  it { is_expected.to belong_to(:tip) }
  it { is_expected.to belong_to(:user) }
end