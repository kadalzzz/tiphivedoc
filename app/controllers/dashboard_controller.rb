class DashboardController < ApplicationController
  http_basic_authenticate_with name: 'tiphive', password: 'givemeaccesstodashboard'
  before_action :authenticate_user!

  def index

  end

  def domains
    if %w(text/plain text/csv).include?(request.format)
      domains_text = "domain,# users,# hives,# tips\n"
      Domain.find_each do |domain|
        domains_text += %W(#{domain.name},#{domain.user_followers.count},#{Hive.unscoped.where(domain_id: domain.id).count},#{Tip.unscoped.where(domain_id: domain.id).count}\n)
      end
    else
      @q       = Domain.ransack(params[:q])
      @domains = @q.result.page(params[:page])
    end

    respond_to do |format|
      format.html {}
      format.txt { render text: domains_text, content_type: 'text/plain' }
      format.csv { render text: domains_text, content_type: 'text/csv' }
    end
  end

  def emails
    if params[:group_id].present?
      @group = Group.find params[:group_id]
      @users = @group.user_followers
    else
      @users = User.all
    end

    if %w(text/plain text/csv).include?(request.format)
      users_text = "email,first name,last name\n"
      @users.each do |user|
        users_text += "#{user.email},#{user.first_name},#{user.last_name}\n"
      end
    else
      @users = @users.page params[:page]
    end

    respond_to do |format|
      format.html {}
      format.txt { render text: users_text, content_type: 'text/plain' }
      format.csv { render text: users_text, content_type: 'text/csv' }
    end
  end
end
