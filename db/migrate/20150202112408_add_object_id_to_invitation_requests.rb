class AddObjectIdToInvitationRequests < ActiveRecord::Migration
  def up
    add_column :invitation_requests, :sharing_id, :integer
    add_column :invitation_requests, :sharing_type, :string
    add_index :invitation_requests, :sharing_id
    add_index :invitation_requests, :sharing_type
  end

  def down
  	remove_column :invitation_requests, :sharing_id, :integer
    remove_column :invitation_requests, :sharing_type, :string
  end
end
