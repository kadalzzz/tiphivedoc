class AddCommentsCountToTips < ActiveRecord::Migration
  def change
    add_column :tips, :comments_count, :integer, default: 0
  end
end
