namespace :user do
  desc "Set User Color"
  task assign_color: :environment do
    User.find_each do |u|
      u.update_attribute(:color, User::COLOR[rand(User::COLOR.length)])
    end
  end
end
