class InvitationRequestsController < ApplicationController
  before_filter :authenticate_user_from_token!, except: [:validates_invitation_token, :invitation_request_from_token]
  before_filter :authenticate_user!, except: [:validates_invitation_token, :invitation_request_from_token]

  def index
    @invitation_requests = current_user.invitation_requests
                                       .order('created_at DESC')
                                       .page(params[:page]).per(params[:per])
  end

  def menu_invitation_requests
    @invitation_requests = current_user.invitation_requests.order('created_at DESC')
  end

  def accept
    @invitation_request = current_user.invitation_requests.find(params[:id])
    @existing_user      = User.find_by_email @invitation_request.email

    if @existing_user.present?
      connectable_object = @invitation_request.connectable
      # fetch non-friend user request is for Hive, Group, Domain, Pocket, Tip, Question
      object             = @invitation_request.connectable_object

      if connectable_object.class == User
        # User accept the friend request
        connectable_object.follow @existing_user
      else
        @existing_user.follow connectable_object
      end

      # check if object is Hive, Group, Domain, Pocket, Tip or Question  
      case object
        # If object is Hive then
      when Hive
        # First user accept the friend request after that user Follow the requested Hive
        connectable_object.follow object
        # notify user(connectable_object), Hive has shared with him
        object.notify_friend(connectable_object, @existing_user)
        # Lets user follow all hive tips
        tip_ids = object.tips
        tips    = Tip.where(id: tip_ids).by_owner(@existing_user)
        tips.each do |tip|
          connectable_object.follow(tip)
          tip.update_attributes!(is_private: false) if tip.is_private?
        end
        # If object is Pocket then
      when Pocket
        # First user accept the friend request after that user Follow the requested Pocket
        connectable_object.follow object
        # notify user(connectable_object), Pocket has shared with him
        object.notify_friend(connectable_object, @existing_user)
        # Lets user follow all pocket tips
        tip_ids = object.tips
        tips    = Tip.where(id: tip_ids).by_owner(@existing_user)
        tips.each do |tip|
          connectable_object.follow(tip)
          tip.update_attributes!(is_private: false) if tip.is_private?
        end
        # If object is Tip then
      when Tip
        # First user accept the friend request after that user Follow the requested Tip
        connectable_object.follow(object)
        # notify user(connectable_object), Tip has shared with him
        object.notify_friend(connectable_object) if connectable_object.settings(:email_notifications).friend_adds_tip == 'always'
        # If object is Question then
      when Question
        # First user accept the friend request after that user Follow the requested Question
        connectable_object.follow(object)
        # notify user(connectable_object), Question has shared with him
        if connectable_object.settings(:email_notifications).friend_adds_question == true
          object.notify_friend(connectable_object)
        end
        # If object is Group then
      when Group
        # First user accept the friend request after that user Follow the requested Group
        connectable_object.follow object
        # notify user(connectable_object), Group has shared with him
        object.notify_friend(connectable_object)
        # If object is Domain then
      when Domain
        # First user accept the friend request after that user Follow the requested Domain
        connectable_object.follow object
        # notify user(connectable_object), Group has shared with him
        object.notify_friend(connectable_object)
      else
        "OBJECT: #{object} not found"
      end
      @invitation_request.destroy
    end

    if @existing_user.blank?
      # main invitation code
      invitation_options = {
        email:            @invitation_request.email,
        invitation_token: Digest::SHA1.hexdigest(Time.now.to_s),
        invitation_type:  'system',
        connectable_type: @invitation_request.connectable_type,
        connectable_id:   @invitation_request.connectable_id
      }

      logger.info "*** invitation_options: #{invitation_options}"

      @friend_invitation = FriendInvitation.new(invitation_options)
      if @friend_invitation.save
        @invitation_request.destroy
      else
      end
    end

    @from = params[:from]
    if @from.present? && @from == 'myfriend'
      friends  = current_user.following_users.order(:first_name)
      friends  = friends.reject { |r| current_user.status_with(r) == :is_following }
      @friends = User.where(id: friends.map(&:id)).page(params[:page]).per(params[:per])
    end

    respond_to do |format|
      format.html { redirect_to invitation_requests_url }
      format.js {}
      format.json { head :no_content }
    end
  end

  def validates_invitation_token
    invitation = FriendInvitation.where(invitation_token: params[:invitation_token]).first
    if invitation.blank?
      render json: { response: 'Invalid invitation token', status: :fail }
    else
      render json: { response: 'Valid invitation token', status: :success }
    end
  end

  def invitation_request_from_token
    invitation = FriendInvitation.where(invitation_token: params[:invitation_token]).first
    if invitation.blank?
      render json: { response: { error: 'No invitation request found' }, status: :fail, message: 'No invitation request found' }
    else
      render json: { response: { invitation: invitation }, status: :success }
    end
  end

  def destroy
    @invitation_request    = current_user.invitation_requests.find(params[:id])
    @invitation_request_id = @invitation_request.id

    @invitation_request.destroy
    respond_to do |format|
      format.html { redirect_to invitation_requests_url }
      format.js {}
      format.json { head :no_content }
    end
  end
end
