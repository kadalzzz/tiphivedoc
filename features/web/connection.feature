@javascript
Feature: Connecting user to Hive and Group
  Background:
    Given a hive exists with title: "John Hive", user_id: 1

  Scenario: User adds a hive, pocket and a tip
    When I login with email: "john.doe@email.com"
    Given I go to first hive edit page
    Then I should see "Editing hive"
    Given I click on element with selector ".selectize-input"
    Given I click on element with selector ".selectize-input input"
    # Then I click on "Mary Jane"
    # Then I should see "Mary Jane"
    # Then I wait for the page to load
    # And I logout

    # Given I am on the homepage
    # Then I should see "Welcome To TipHive."
    # And I should see "Login"
    # When I follow "Login"
    # Then I should be on the sign in page
    # Then I fill in "Email" with "mary.jane@email.com"
    # And I fill in "Password" with "password"
    # Then I press "Log In"
    # Then I should see "Mary"

    # Then I go to my hives page
    # Then I wait for the page to load
    # Then screenshot
    # Then I should see "John Hive"
