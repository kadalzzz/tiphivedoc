module DomainObserver
  extend ActiveSupport::Concern

  def notify_friend(friend)
    return if user_id == friend.id
    user = self.user

    email_params = {
      user_email:   user.email,
      user_name:    user.name,
      friend_email: friend.email,
      friend_name:  friend.name,
      domain_id:    id
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{user.name} is sending email to #{friend.email}"
    )
    ## DEBUG LOG END

    EmailSendingWorker.perform_async('UserMailer', 'add_user_to_domain', email_params)
  end
end
