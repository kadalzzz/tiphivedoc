class CreateFlags < ActiveRecord::Migration
  def up
    create_table :flags do |t|
      t.integer :flagable_id
      t.string :flagable_type
      t.integer :flag_id
      t.string :flag_type

      t.timestamps
    end

    add_index :flags, :flagable_id
    add_index :flags, :flagable_type
    add_index :flags, :flag_id
    add_index :flags, :flag_type
  end

  def down
    drop_table :flags
  end


end
