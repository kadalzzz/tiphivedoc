# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150421124801) do

  create_table "action_logs", force: true do |t|
    t.string   "file_name"
    t.string   "class_name"
    t.string   "method_name"
    t.integer  "line_number"
    t.text     "stack_trace"
    t.text     "message"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "action_logs", ["class_name"], name: "index_action_logs_on_class_name", using: :btree
  add_index "action_logs", ["method_name"], name: "index_action_logs_on_method_name", using: :btree

  create_table "active_admin_comments", force: true do |t|
    t.string   "namespace"
    t.text     "body"
    t.string   "resource_id",   null: false
    t.string   "resource_type", null: false
    t.integer  "author_id"
    t.string   "author_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "active_admin_comments", ["author_id", "author_type"], name: "index_active_admin_comments_on_author_id_and_author_type", using: :btree
  add_index "active_admin_comments", ["author_type", "author_id"], name: "index_active_admin_comments_on_author_type_and_author_id", using: :btree
  add_index "active_admin_comments", ["namespace"], name: "index_active_admin_comments_on_namespace", using: :btree
  add_index "active_admin_comments", ["resource_id", "resource_type"], name: "index_active_admin_comments_on_resource_id_and_resource_type", using: :btree
  add_index "active_admin_comments", ["resource_type", "resource_id"], name: "index_active_admin_comments_on_resource_type_and_resource_id", using: :btree

  create_table "activities", force: true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "admin_users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "admin_users", ["email"], name: "index_admin_users_on_email", unique: true, using: :btree
  add_index "admin_users", ["reset_password_token"], name: "index_admin_users_on_reset_password_token", unique: true, using: :btree

  create_table "businesses", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "address"
    t.string   "location"
    t.string   "photo_url"
    t.integer  "zip_code"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "comments", force: true do |t|
    t.integer  "commentable_id",              default: 0
    t.string   "commentable_type"
    t.string   "title"
    t.text     "body"
    t.string   "subject"
    t.integer  "user_id",                     default: 0, null: false
    t.integer  "parent_id"
    t.integer  "lft"
    t.integer  "rgt"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.float    "longitude",        limit: 24
    t.float    "latitude",         limit: 24
    t.string   "address"
    t.string   "location"
    t.integer  "domain_id"
  end

  add_index "comments", ["commentable_id", "commentable_type"], name: "index_comments_on_commentable_id_and_commentable_type", using: :btree
  add_index "comments", ["domain_id"], name: "index_comments_on_domain_id", using: :btree
  add_index "comments", ["parent_id"], name: "index_comments_on_parent_id", using: :btree
  add_index "comments", ["user_id"], name: "index_comments_on_user_id", using: :btree

  create_table "domains", force: true do |t|
    t.string   "name"
    t.string   "logo"
    t.string   "background"
    t.boolean  "active"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "domains", ["name"], name: "index_domains_on_name", using: :btree
  add_index "domains", ["user_id"], name: "index_domains_on_user_id", using: :btree

  create_table "emails", force: true do |t|
    t.string   "address"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "emails", ["user_id"], name: "index_emails_on_user_id", using: :btree

  create_table "file_uploads", force: true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.string   "file"
    t.integer  "fileable_id"
    t.string   "fileable_type"
    t.integer  "domain_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "file_uploads", ["domain_id"], name: "index_file_uploads_on_domain_id", using: :btree
  add_index "file_uploads", ["fileable_id", "fileable_type"], name: "index_file_uploads_on_fileable_id_and_fileable_type", using: :btree
  add_index "file_uploads", ["fileable_id"], name: "index_file_uploads_on_fileable_id", using: :btree
  add_index "file_uploads", ["fileable_type"], name: "index_file_uploads_on_fileable_type", using: :btree
  add_index "file_uploads", ["user_id"], name: "index_file_uploads_on_user_id", using: :btree

  create_table "flags", force: true do |t|
    t.integer  "flagable_id"
    t.string   "flagable_type"
    t.integer  "flag_id"
    t.string   "flag_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.text     "reason"
  end

  add_index "flags", ["flag_id", "flag_type"], name: "index_flags_on_flag_id_and_flag_type", using: :btree
  add_index "flags", ["flag_id"], name: "index_flags_on_flag_id", using: :btree
  add_index "flags", ["flag_type"], name: "index_flags_on_flag_type", using: :btree
  add_index "flags", ["flagable_id", "flagable_type"], name: "index_flags_on_flagable_id_and_flagable_type", using: :btree
  add_index "flags", ["flagable_id"], name: "index_flags_on_flagable_id", using: :btree
  add_index "flags", ["flagable_type"], name: "index_flags_on_flagable_type", using: :btree

  create_table "follows", force: true do |t|
    t.integer  "followable_id",                   null: false
    t.string   "followable_type",                 null: false
    t.integer  "follower_id",                     null: false
    t.string   "follower_type",                   null: false
    t.boolean  "blocked",         default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["followable_id", "followable_type"], name: "index_follows_on_followable_id_and_followable_type", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "index_follows_on_follower_id_and_follower_type", using: :btree

  create_table "friend_invitations", force: true do |t|
    t.string   "email"
    t.string   "invitation_token"
    t.string   "invitation_type"
    t.integer  "user_id"
    t.string   "connectable_type"
    t.integer  "connectable_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "domain_id"
  end

  add_index "friend_invitations", ["connectable_id", "connectable_type"], name: "index_friend_invitations_on_connectable_id_and_connectable_type", using: :btree
  add_index "friend_invitations", ["connectable_id"], name: "index_friend_invitations_on_connectable_id", using: :btree
  add_index "friend_invitations", ["connectable_type"], name: "index_friend_invitations_on_connectable_type", using: :btree
  add_index "friend_invitations", ["domain_id"], name: "index_friend_invitations_on_domain_id", using: :btree
  add_index "friend_invitations", ["email"], name: "index_friend_invitations_on_email", using: :btree
  add_index "friend_invitations", ["invitation_token"], name: "index_friend_invitations_on_invitation_token", using: :btree
  add_index "friend_invitations", ["invitation_type"], name: "index_friend_invitations_on_invitation_type", using: :btree
  add_index "friend_invitations", ["user_id"], name: "index_friend_invitations_on_user_id", using: :btree

  create_table "friend_lists", force: true do |t|
    t.string   "name"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "friendly_id_slugs", force: true do |t|
    t.string   "slug",                      null: false
    t.integer  "sluggable_id",              null: false
    t.string   "sluggable_type", limit: 50
    t.string   "scope"
    t.datetime "created_at"
  end

  add_index "friendly_id_slugs", ["slug", "sluggable_type", "scope"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type_and_scope", unique: true, using: :btree
  add_index "friendly_id_slugs", ["slug", "sluggable_type"], name: "index_friendly_id_slugs_on_slug_and_sluggable_type", using: :btree
  add_index "friendly_id_slugs", ["sluggable_id"], name: "index_friendly_id_slugs_on_sluggable_id", using: :btree
  add_index "friendly_id_slugs", ["sluggable_type"], name: "index_friendly_id_slugs_on_sluggable_type", using: :btree

  create_table "friendships", force: true do |t|
    t.integer "friendable_id"
    t.integer "friend_id"
    t.integer "blocker_id"
    t.boolean "pending",       default: true
  end

  add_index "friendships", ["friendable_id", "friend_id"], name: "index_friendships_on_friendable_id_and_friend_id", unique: true, using: :btree

  create_table "groups", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.string   "background_image"
    t.integer  "background_image_top",             default: 0
    t.integer  "background_image_left",            default: 0
    t.string   "join_type",                        default: "anyone"
    t.string   "group_type"
    t.string   "address"
    t.string   "location"
    t.string   "zip"
    t.float    "latitude",              limit: 24
    t.float    "longitude",             limit: 24
    t.string   "avatar"
    t.integer  "domain_id"
    t.string   "admin_ids"
    t.string   "color"
    t.boolean  "is_auto_accept",                   default: false
  end

  add_index "groups", ["domain_id"], name: "index_groups_on_domain_id", using: :btree
  add_index "groups", ["group_type"], name: "index_groups_on_group_type", using: :btree
  add_index "groups", ["join_type"], name: "index_groups_on_join_type", using: :btree
  add_index "groups", ["slug"], name: "index_groups_on_slug", unique: true, using: :btree
  add_index "groups", ["title"], name: "index_groups_on_title", using: :btree
  add_index "groups", ["user_id"], name: "index_groups_on_user_id", using: :btree

  create_table "hives", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "is_public",             default: false
    t.boolean  "is_on_profile",         default: true
    t.boolean  "allow_add_pocket",      default: true
    t.boolean  "allow_friend_share",    default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "slug"
    t.string   "background_image"
    t.string   "sharing_type",          default: "select_friends"
    t.boolean  "is_private",            default: false
    t.boolean  "shared_all_friends",    default: false
    t.boolean  "shared_select_friends", default: false
    t.integer  "domain_id"
    t.string   "background_color"
  end

  add_index "hives", ["allow_add_pocket"], name: "index_hives_on_allow_add_pocket", using: :btree
  add_index "hives", ["allow_friend_share"], name: "index_hives_on_allow_friend_share", using: :btree
  add_index "hives", ["domain_id"], name: "index_hives_on_domain_id", using: :btree
  add_index "hives", ["is_on_profile"], name: "index_hives_on_is_on_profile", using: :btree
  add_index "hives", ["is_public"], name: "index_hives_on_is_public", using: :btree
  add_index "hives", ["sharing_type"], name: "index_hives_on_sharing_type", using: :btree
  add_index "hives", ["slug"], name: "index_hives_on_slug", unique: true, using: :btree
  add_index "hives", ["title"], name: "index_hives_on_title", using: :btree
  add_index "hives", ["user_id"], name: "index_hives_on_user_id", using: :btree

  create_table "identities", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "access_token"
  end

  add_index "identities", ["provider"], name: "index_identities_on_provider", using: :btree
  add_index "identities", ["uid"], name: "index_identities_on_uid", using: :btree
  add_index "identities", ["user_id"], name: "index_identities_on_user_id", using: :btree

  create_table "impressions", force: true do |t|
    t.string   "impressionable_type"
    t.integer  "impressionable_id"
    t.integer  "user_id"
    t.string   "controller_name"
    t.string   "action_name"
    t.string   "view_name"
    t.string   "request_hash"
    t.string   "ip_address"
    t.string   "session_hash"
    t.text     "message"
    t.text     "referrer"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "impressions", ["controller_name", "action_name", "ip_address"], name: "controlleraction_ip_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "request_hash"], name: "controlleraction_request_index", using: :btree
  add_index "impressions", ["controller_name", "action_name", "session_hash"], name: "controlleraction_session_index", using: :btree
  add_index "impressions", ["impressionable_id", "impressionable_type"], name: "index_impressions_on_impressionable_id_and_impressionable_type", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "ip_address"], name: "poly_ip_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "request_hash"], name: "poly_request_index", using: :btree
  add_index "impressions", ["impressionable_type", "impressionable_id", "session_hash"], name: "poly_session_index", using: :btree
  add_index "impressions", ["impressionable_type", "message", "impressionable_id"], name: "impressionable_type_message_index", length: {"impressionable_type"=>nil, "message"=>255, "impressionable_id"=>nil}, using: :btree
  add_index "impressions", ["user_id"], name: "index_impressions_on_user_id", using: :btree

  create_table "invitation_requests", force: true do |t|
    t.string   "email"
    t.string   "request_token"
    t.string   "request_type"
    t.string   "connectable_type"
    t.integer  "connectable_id"
    t.string   "other_connections"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "sharing_id"
    t.string   "sharing_type"
    t.integer  "domain_id"
  end

  add_index "invitation_requests", ["connectable_id"], name: "index_invitation_requests_on_connectable_id", using: :btree
  add_index "invitation_requests", ["connectable_type"], name: "index_invitation_requests_on_connectable_type", using: :btree
  add_index "invitation_requests", ["domain_id"], name: "index_invitation_requests_on_domain_id", using: :btree
  add_index "invitation_requests", ["email"], name: "index_invitation_requests_on_email", using: :btree
  add_index "invitation_requests", ["request_token"], name: "index_invitation_requests_on_request_token", using: :btree
  add_index "invitation_requests", ["request_type"], name: "index_invitation_requests_on_request_type", using: :btree
  add_index "invitation_requests", ["sharing_id"], name: "index_invitation_requests_on_sharing_id", using: :btree
  add_index "invitation_requests", ["sharing_type"], name: "index_invitation_requests_on_sharing_type", using: :btree
  add_index "invitation_requests", ["user_id"], name: "index_invitation_requests_on_user_id", using: :btree

  create_table "mailboxer_conversation_opt_outs", force: true do |t|
    t.integer "unsubscriber_id"
    t.string  "unsubscriber_type"
    t.integer "conversation_id"
  end

  add_index "mailboxer_conversation_opt_outs", ["conversation_id"], name: "index_mailboxer_conversation_opt_outs_on_conversation_id", using: :btree
  add_index "mailboxer_conversation_opt_outs", ["unsubscriber_id", "unsubscriber_type"], name: "index_mailboxer_conversation_opt_outs_on_unsubscriber_id_type", using: :btree

  create_table "mailboxer_conversations", force: true do |t|
    t.string   "subject",    default: ""
    t.datetime "created_at",              null: false
    t.datetime "updated_at",              null: false
  end

  create_table "mailboxer_notifications", force: true do |t|
    t.string   "type"
    t.text     "body"
    t.string   "subject",              default: ""
    t.integer  "sender_id"
    t.string   "sender_type"
    t.integer  "conversation_id"
    t.boolean  "draft",                default: false
    t.datetime "updated_at",                           null: false
    t.datetime "created_at",                           null: false
    t.integer  "notified_object_id"
    t.string   "notified_object_type"
    t.string   "notification_code"
    t.string   "attachment"
    t.boolean  "global",               default: false
    t.datetime "expires"
  end

  add_index "mailboxer_notifications", ["conversation_id"], name: "index_mailboxer_notifications_on_conversation_id", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_mailboxer_notifications_on_notified_object_id_and_type", using: :btree
  add_index "mailboxer_notifications", ["notified_object_id", "notified_object_type"], name: "index_notifications_on_object_id_and_object_type", using: :btree
  add_index "mailboxer_notifications", ["sender_id", "sender_type"], name: "index_mailboxer_notifications_on_sender_id_and_sender_type", using: :btree
  add_index "mailboxer_notifications", ["type"], name: "index_mailboxer_notifications_on_type", using: :btree

  create_table "mailboxer_receipts", force: true do |t|
    t.integer  "receiver_id"
    t.string   "receiver_type"
    t.integer  "notification_id",                            null: false
    t.boolean  "is_read",                    default: false
    t.boolean  "trashed",                    default: false
    t.boolean  "deleted",                    default: false
    t.string   "mailbox_type",    limit: 25
    t.datetime "created_at",                                 null: false
    t.datetime "updated_at",                                 null: false
  end

  add_index "mailboxer_receipts", ["notification_id"], name: "index_mailboxer_receipts_on_notification_id", using: :btree
  add_index "mailboxer_receipts", ["receiver_id", "receiver_type"], name: "index_mailboxer_receipts_on_receiver_id_and_receiver_type", using: :btree

  create_table "oauth_access_grants", force: true do |t|
    t.integer  "resource_owner_id", null: false
    t.integer  "application_id",    null: false
    t.string   "token",             null: false
    t.integer  "expires_in",        null: false
    t.text     "redirect_uri",      null: false
    t.datetime "created_at",        null: false
    t.datetime "revoked_at"
    t.string   "scopes"
  end

  add_index "oauth_access_grants", ["application_id"], name: "index_oauth_access_grants_on_application_id", using: :btree
  add_index "oauth_access_grants", ["token"], name: "index_oauth_access_grants_on_token", unique: true, using: :btree

  create_table "oauth_access_tokens", force: true do |t|
    t.integer  "resource_owner_id"
    t.integer  "application_id"
    t.string   "token",             null: false
    t.string   "refresh_token"
    t.integer  "expires_in"
    t.datetime "revoked_at"
    t.datetime "created_at",        null: false
    t.string   "scopes"
  end

  add_index "oauth_access_tokens", ["application_id"], name: "index_oauth_access_tokens_on_application_id", using: :btree
  add_index "oauth_access_tokens", ["refresh_token"], name: "index_oauth_access_tokens_on_refresh_token", unique: true, using: :btree
  add_index "oauth_access_tokens", ["resource_owner_id"], name: "index_oauth_access_tokens_on_resource_owner_id", using: :btree
  add_index "oauth_access_tokens", ["token"], name: "index_oauth_access_tokens_on_token", unique: true, using: :btree

  create_table "oauth_applications", force: true do |t|
    t.string   "name",         null: false
    t.string   "uid",          null: false
    t.string   "secret",       null: false
    t.text     "redirect_uri", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "owner_id"
    t.string   "owner_type"
  end

  add_index "oauth_applications", ["owner_id", "owner_type"], name: "index_oauth_applications_on_owner_id_and_owner_type", using: :btree
  add_index "oauth_applications", ["uid"], name: "index_oauth_applications_on_uid", unique: true, using: :btree

  create_table "object_settings", force: true do |t|
    t.integer  "object_setting_id"
    t.string   "object_setting_type"
    t.integer  "user_id"
    t.boolean  "is_private",          default: false
    t.string   "background_image"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "domain_id"
  end

  add_index "object_settings", ["domain_id"], name: "index_object_settings_on_domain_id", using: :btree
  add_index "object_settings", ["object_setting_id", "object_setting_type"], name: "obj_setting_id_and_type", using: :btree
  add_index "object_settings", ["object_setting_id"], name: "index_object_settings_on_object_setting_id", using: :btree
  add_index "object_settings", ["object_setting_type"], name: "index_object_settings_on_object_setting_type", using: :btree
  add_index "object_settings", ["user_id"], name: "index_object_settings_on_user_id", using: :btree

  create_table "pictures", force: true do |t|
    t.string   "title"
    t.string   "image"
    t.integer  "imageable_id"
    t.string   "imageable_type"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.integer  "domain_id"
  end

  add_index "pictures", ["domain_id"], name: "index_pictures_on_domain_id", using: :btree
  add_index "pictures", ["imageable_id", "imageable_type"], name: "index_pictures_on_imageable_id_and_imageable_type", using: :btree
  add_index "pictures", ["imageable_id"], name: "index_pictures_on_imageable_id", using: :btree
  add_index "pictures", ["imageable_type"], name: "index_pictures_on_imageable_type", using: :btree
  add_index "pictures", ["user_id"], name: "index_pictures_on_user_id", using: :btree

  create_table "pockets", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.boolean  "is_public",             default: false
    t.boolean  "is_on_profile",         default: true
    t.boolean  "allow_add_tip",         default: true
    t.boolean  "allow_friend_share",    default: true
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "user_id"
    t.string   "slug"
    t.string   "background_image"
    t.boolean  "is_asking_for_tips",    default: false
    t.boolean  "is_private",            default: false
    t.boolean  "shared_all_friends",    default: false
    t.boolean  "shared_select_friends", default: false
    t.integer  "domain_id"
  end

  add_index "pockets", ["allow_add_tip"], name: "index_pockets_on_allow_add_tip", using: :btree
  add_index "pockets", ["allow_friend_share"], name: "index_pockets_on_allow_friend_share", using: :btree
  add_index "pockets", ["domain_id"], name: "index_pockets_on_domain_id", using: :btree
  add_index "pockets", ["is_on_profile"], name: "index_pockets_on_is_on_profile", using: :btree
  add_index "pockets", ["is_public"], name: "index_pockets_on_is_public", using: :btree
  add_index "pockets", ["slug"], name: "index_pockets_on_slug", unique: true, using: :btree
  add_index "pockets", ["title"], name: "index_pockets_on_title", using: :btree
  add_index "pockets", ["user_id"], name: "index_pockets_on_user_id", using: :btree

  create_table "questions", force: true do |t|
    t.text     "name"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.boolean  "library"
    t.time     "sent_at"
    t.integer  "comments_count",        default: 0
    t.string   "sharing_type",          default: "all_friends"
    t.integer  "cached_votes_total",    default: 0
    t.integer  "cached_votes_score",    default: 0
    t.integer  "cached_votes_up",       default: 0
    t.integer  "cached_votes_down",     default: 0
    t.integer  "cached_weighted_score", default: 0
    t.boolean  "is_public",             default: false
    t.boolean  "is_private",            default: false
    t.boolean  "shared_all_friends",    default: false
    t.boolean  "shared_select_friends", default: false
    t.integer  "domain_id"
    t.string   "access_key"
    t.string   "color"
    t.integer  "pictures_count"
    t.boolean  "anonymously",           default: false
  end

  add_index "questions", ["access_key"], name: "index_questions_on_access_key", using: :btree
  add_index "questions", ["cached_votes_down"], name: "index_questions_on_cached_votes_down", using: :btree
  add_index "questions", ["cached_votes_score"], name: "index_questions_on_cached_votes_score", using: :btree
  add_index "questions", ["cached_votes_total"], name: "index_questions_on_cached_votes_total", using: :btree
  add_index "questions", ["cached_votes_up"], name: "index_questions_on_cached_votes_up", using: :btree
  add_index "questions", ["cached_weighted_score"], name: "index_questions_on_cached_weighted_score", using: :btree
  add_index "questions", ["domain_id"], name: "index_questions_on_domain_id", using: :btree
  add_index "questions", ["sharing_type"], name: "index_questions_on_sharing_type", using: :btree
  add_index "questions", ["user_id"], name: "index_questions_on_user_id", using: :btree

  create_table "read_marks", force: true do |t|
    t.integer  "readable_id"
    t.integer  "user_id",                  null: false
    t.string   "readable_type", limit: 20, null: false
    t.datetime "timestamp"
  end

  add_index "read_marks", ["readable_id", "readable_type"], name: "index_read_marks_on_readable_id_and_readable_type", using: :btree
  add_index "read_marks", ["user_id", "readable_type", "readable_id"], name: "index_read_marks_on_user_id_and_readable_type_and_readable_id", using: :btree

  create_table "sessions", force: true do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

  create_table "settings", force: true do |t|
    t.string   "var",         null: false
    t.text     "value"
    t.integer  "target_id",   null: false
    t.string   "target_type", null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "settings", ["target_id", "target_type"], name: "index_settings_on_target_id_and_target_type", using: :btree
  add_index "settings", ["target_type", "target_id", "var"], name: "index_settings_on_target_type_and_target_id_and_var", unique: true, using: :btree

  create_table "shares", force: true do |t|
    t.string   "shareable_object_type"
    t.integer  "shareable_object_id"
    t.string   "sharing_object_type",   default: "all_friends"
    t.integer  "sharing_object_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "shares", ["shareable_object_id", "shareable_object_type"], name: "index_shares_on_shareable_object_id_and_shareable_object_type", using: :btree
  add_index "shares", ["shareable_object_id"], name: "index_shares_on_shareable_object_id", using: :btree
  add_index "shares", ["shareable_object_type"], name: "index_shares_on_shareable_object_type", using: :btree
  add_index "shares", ["sharing_object_id", "sharing_object_type"], name: "index_shares_on_sharing_object_id_and_sharing_object_type", using: :btree
  add_index "shares", ["sharing_object_id"], name: "index_shares_on_sharing_object_id", using: :btree
  add_index "shares", ["sharing_object_type"], name: "index_shares_on_sharing_object_type", using: :btree
  add_index "shares", ["user_id"], name: "index_shares_on_user_id", using: :btree

  create_table "social_friends", force: true do |t|
    t.string   "provider"
    t.string   "uid"
    t.string   "name"
    t.string   "email"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username"
  end

  add_index "social_friends", ["user_id"], name: "index_social_friends_on_user_id", using: :btree

  create_table "taggings", force: true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: true do |t|
    t.string  "name"
    t.integer "taggings_count", default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "tip_links", force: true do |t|
    t.text     "url",         null: false
    t.string   "title"
    t.text     "description"
    t.string   "image_url"
    t.string   "avatar"
    t.integer  "tip_id"
    t.integer  "user_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tip_links", ["tip_id"], name: "index_tip_links_on_tip_id", using: :btree
  add_index "tip_links", ["user_id"], name: "index_tip_links_on_user_id", using: :btree

  create_table "tiphive_settings", force: true do |t|
    t.string   "name",       null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "tiphive_settings", ["name"], name: "index_tiphive_settings_on_name", unique: true, using: :btree

  create_table "tips", force: true do |t|
    t.string   "title"
    t.text     "description"
    t.integer  "user_id"
    t.boolean  "is_public",             default: false
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "slug"
    t.integer  "pictures_count",        default: 0
    t.integer  "cached_votes_total",    default: 0
    t.integer  "cached_votes_score",    default: 0
    t.integer  "cached_votes_up",       default: 0
    t.integer  "cached_votes_down",     default: 0
    t.integer  "cached_weighted_score", default: 0
    t.integer  "comments_count",        default: 0
    t.string   "address"
    t.string   "latitude"
    t.string   "longitude"
    t.string   "location"
    t.string   "sharing_type",          default: "select_friends"
    t.integer  "question_id"
    t.integer  "links_count",           default: 0
    t.boolean  "is_private",            default: false
    t.boolean  "shared_all_friends",    default: false
    t.boolean  "shared_select_friends", default: false
    t.integer  "domain_id"
    t.string   "access_key"
    t.integer  "draft"
    t.datetime "deleted_at"
    t.integer  "destroyer_id"
    t.string   "color"
  end

  add_index "tips", ["access_key"], name: "index_tips_on_access_key", using: :btree
  add_index "tips", ["cached_votes_down"], name: "index_tips_on_cached_votes_down", using: :btree
  add_index "tips", ["cached_votes_score"], name: "index_tips_on_cached_votes_score", using: :btree
  add_index "tips", ["cached_votes_total"], name: "index_tips_on_cached_votes_total", using: :btree
  add_index "tips", ["cached_votes_up"], name: "index_tips_on_cached_votes_up", using: :btree
  add_index "tips", ["cached_weighted_score"], name: "index_tips_on_cached_weighted_score", using: :btree
  add_index "tips", ["deleted_at"], name: "index_tips_on_deleted_at", using: :btree
  add_index "tips", ["destroyer_id"], name: "index_tips_on_destroyer_id", using: :btree
  add_index "tips", ["domain_id"], name: "index_tips_on_domain_id", using: :btree
  add_index "tips", ["is_public"], name: "index_tips_on_is_public", using: :btree
  add_index "tips", ["question_id"], name: "index_tips_on_question_id", using: :btree
  add_index "tips", ["sharing_type"], name: "index_tips_on_sharing_type", using: :btree
  add_index "tips", ["slug"], name: "index_tips_on_slug", unique: true, using: :btree
  add_index "tips", ["title"], name: "index_tips_on_title", using: :btree
  add_index "tips", ["user_id"], name: "index_tips_on_user_id", using: :btree

  create_table "users", force: true do |t|
    t.string   "email",                  default: "", null: false
    t.string   "encrypted_password",     default: ""
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.integer  "failed_attempts",        default: 0,  null: false
    t.string   "unlock_token"
    t.datetime "locked_at"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
    t.integer  "invitations_count",      default: 0
    t.string   "first_name"
    t.string   "last_name"
    t.string   "avatar"
    t.string   "username"
    t.string   "background_image"
    t.integer  "background_image_top",   default: 0
    t.integer  "background_image_left",  default: 0
    t.integer  "roles_mask"
    t.datetime "daily_sent_at"
    t.datetime "weekly_sent_at"
    t.string   "authentication_token"
    t.integer  "role"
    t.string   "second_email"
    t.string   "color"
  end

  add_index "users", ["authentication_token"], name: "index_users_on_authentication_token", unique: true, using: :btree
  add_index "users", ["confirmation_token"], name: "index_users_on_confirmation_token", unique: true, using: :btree
  add_index "users", ["email"], name: "index_users_on_email", unique: true, using: :btree
  add_index "users", ["first_name"], name: "index_users_on_first_name", using: :btree
  add_index "users", ["invitation_token"], name: "index_users_on_invitation_token", unique: true, using: :btree
  add_index "users", ["invitations_count"], name: "index_users_on_invitations_count", using: :btree
  add_index "users", ["invited_by_id", "invited_by_type"], name: "index_users_on_invited_by_id_and_invited_by_type", using: :btree
  add_index "users", ["invited_by_id"], name: "index_users_on_invited_by_id", using: :btree
  add_index "users", ["last_name"], name: "index_users_on_last_name", using: :btree
  add_index "users", ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true, using: :btree
  add_index "users", ["unlock_token"], name: "index_users_on_unlock_token", unique: true, using: :btree
  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "votes", force: true do |t|
    t.integer  "votable_id"
    t.string   "votable_type"
    t.integer  "voter_id"
    t.string   "voter_type"
    t.boolean  "vote_flag"
    t.string   "vote_scope"
    t.integer  "vote_weight"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "votes", ["votable_id", "votable_type", "vote_scope"], name: "index_votes_on_votable_id_and_votable_type_and_vote_scope", using: :btree
  add_index "votes", ["votable_id", "votable_type"], name: "index_votes_on_votable_id_and_votable_type", using: :btree
  add_index "votes", ["voter_id", "voter_type", "vote_scope"], name: "index_votes_on_voter_id_and_voter_type_and_vote_scope", using: :btree
  add_index "votes", ["voter_id", "voter_type"], name: "index_votes_on_voter_id_and_voter_type", using: :btree

  add_foreign_key "mailboxer_conversation_opt_outs", "mailboxer_conversations", name: "mb_opt_outs_on_conversations_id", column: "conversation_id"

  add_foreign_key "mailboxer_notifications", "mailboxer_conversations", name: "notifications_on_conversation_id", column: "conversation_id"

  add_foreign_key "mailboxer_receipts", "mailboxer_notifications", name: "receipts_on_notification_id", column: "notification_id"

end
