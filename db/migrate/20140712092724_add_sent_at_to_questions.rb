class AddSentAtToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :sent_at, :time
  end
end
