json.id shared_object.id
json.first_name shared_object.first_name
json.last_name shared_object.last_name
json.avatar_url shared_object.avatar.present? ? shared_object.avatar.url : ''
