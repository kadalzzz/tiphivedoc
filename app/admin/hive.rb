ActiveAdmin.register Hive do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  config.sort_order = "id_desc"
  config.per_page = 10

  controller do
    def scoped_collection
      resource_class.includes(:user) # prevents N+1 queries to database
    end
  end

  index do
    selectable_column
    column :id, sortable: :id do |hive|
      link_to hive.id, admin_hive_path(hive)
    end
    column :title
    column :description
    column :background_image do |hive|
      image_tag hive.background_image, height: 50
    end
    column :is_public
    column :is_on_profile
    column :allow_add_pocket
    column :allow_friend_share
    column :user do |hive|
      user = hive.user
      link_to user.name, admin_user_path(user) if user.present?
    end
    column :created_at
    actions
  end

  filter :title
  filter :description
  filter :is_public
  filter :is_on_profile
  filter :allow_add_pocket
  filter :allow_friend_share
  filter :user
  filter :created_at

end
