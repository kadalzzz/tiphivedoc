class CreateShares < ActiveRecord::Migration
  def up
    create_table :shares do |t|
      t.string :shareable_object_type
      t.integer :shareable_object_id
      t.string :sharing_object_type, default: 'all_friends'
      t.integer :sharing_object_id
      t.integer :user_id
      t.integer :sharing_type_id
      t.timestamps
    end

    add_index :shares, :shareable_object_type
    add_index :shares, :shareable_object_id
    add_index :shares, :user_id
    add_index :shares, :sharing_object_type
    add_index :shares, :sharing_object_id
    add_index :shares, :sharing_type_id
  end

  def down
    remove_column :shares, :shareable_object_type
    remove_column :shares, :shareable_object_id
    remove_column :shares, :user_id
    remove_column :shares, :sharing_object_type
    remove_column :shares, :sharing_object_id
    remove_column :shares, :sharing_type_id
  end
end
