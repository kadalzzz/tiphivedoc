require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
require 'sprockets/railtie'
# require "rails/test_unit/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(:default, Rails.env)

module TipHive
  class Application < Rails::Application
    HOST = ENV['HOST']
    ASSET_HOST = Rails.env.test? ? 'http://127.0.0.1:3000' : ENV['ASSET_HOST']

    # Settings in config/environments/* take precedence over those specified here.
    # Application configuration should go into files in config/initializers
    # -- all .rb files in that directory are automatically loaded.

    # Set Time.zone default to the specified zone and make Active Record auto-convert to this zone.
    # Run "rake -D time" for a list of tasks for finding time zone names. Default is UTC.
    # config.time_zone = 'Central Time (US & Canada)'

    # The default locale is :en and all translations from config/locales/*.rb,yml are auto loaded.
    # config.i18n.load_path += Dir[Rails.root.join('my', 'locales', '*.{rb,yml}').to_s]
    # config.i18n.default_locale = :de
    config.autoload_paths += %W(#{config.root}/lib)
    config.generators do |g|
      g.orm :active_record
      g.template_engine :haml
      g.test_framework false
      g.helper false
      g.stylesheets false
      g.javascripts false
      g.scaffold_controller :scaffold_controller
    end

    config.active_record.raise_in_transactional_callbacks = true

    config.asset_host = ASSET_HOST
    config.default_url_options = { host: HOST }

    config.action_mailer.default content_type: 'text/html'
    config.action_mailer.default_url_options = config.default_url_options.merge(only_path: false)

    config.assets.precompile += %w(user_mailer.css)

    config.after_initialize do
      default_host = config.default_url_options[:host]
      if default_host && %w(production staging beta).include?(Rails.env)
        host_components = default_host.split('.')

        ActionDispatch::Http::URL.tld_length = host_components.count - 1
        ActionDispatch::Http::URL.tld_length -= 1 if host_components.first == 'www'
      end
    end
  end
end
