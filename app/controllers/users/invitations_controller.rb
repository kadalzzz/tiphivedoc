class Users::InvitationsController < Devise::InvitationsController

  private
  def invite_resource
    resource_class.invite!(invite_params, current_inviter)
  end

  def accept_resource
    resource_class.accept_invitation!(update_resource_params)
  end
end
