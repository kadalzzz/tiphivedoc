class AddIsAskingForTipsToPockets < ActiveRecord::Migration
  def change
    add_column :pockets, :is_asking_for_tips, :boolean, default: false
  end
end
