ActiveAdmin.register Picture do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  config.sort_order = "id_desc"
  config.per_page = 10

  index do
    selectable_column
    column :id, sortable: :id do |picture|
      link_to picture.id, admin_picture_path(picture)
    end
    column :image do |picture|
      image_tag picture.image, height: 100
    end
    column :imageable_type
    column :imageable_id
    column :title
    column :created_at
    actions
  end

end
