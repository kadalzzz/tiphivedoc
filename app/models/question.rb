class Question < ActiveRecord::Base
  include Tenant
  include PublicActivity::Model
  include QuestionObserver
  include TipHiveObject
  include ActsAsFlagable

  tracked owner: Proc.new { |controller, _model| controller.current_user if controller }

  acts_as_followable
  acts_as_follower
  acts_as_votable
  acts_as_commentable

  validates :name, presence: true

  has_many :pictures, as: :imageable, dependent: :destroy
  attr_accessor :images
  attr_accessor :picture_ids

  validates :title, presence: true
  COLOR = %w(liteorange orange purple litepurple green liteblue blue)

  paginates_per 6

  before_create :set_color

  scope :library_items, -> { where(library: true) }
  scope :not_sent, -> { where(sent_at: nil) }
  scope :all_friends_sharing, -> { where(shared_all_friends: true) }
  scope :select_friends_sharing, -> { where(sharing_type: :select_friends) }
  scope :public_sharing, -> { where(is_public: true) }
  scope :private_sharing, -> { where(is_private: true) }
  # Get all owner tip
  scope :by_owner, -> (user) { where(user_id: user.id) }

  def to_param
    "#{id} #{name}".parameterize
  end

  belongs_to :user
  has_many :tips

  delegate :first_name, to: :user, prefix: true
  delegate :email, to: :user, prefix: true

  after_destroy :set_question_id_null
  before_create :generate_hash

  searchable do
    text :name
    integer :user_id
  end

  def title
    name
  end

  def hives
    # replace with new one
    following_hives
  end

  def pockets
    following_pockets
  end

  def users
    user_followers
  end

  def groups
    group_followers
  end

  def friend_lists
    friend_lists_followers
  end

  def fetch_tips
    tip_followers
  end

  def users_and_groups_and_friend_lists
    [users.all, groups.all, friend_lists].flatten
  end

  def users_and_groups
    [users.all, groups.all].flatten
  end

  def hives_and_pockets_title
    "#{hives.map(&:title).join(', ')} / #{pockets.map(&:title).join(', ')}"
  end

  def set_question_id_null
    tips.each do |tip|
      tip.question_id = nil
      tip.save
    end
  end

  def generate_hash
    seed            = "--#{Time.now}--#{rand(1000)}--#{title}"
    self.access_key = Digest::SHA256.hexdigest seed
  end

  def set_color
    self.color = COLOR[rand(COLOR.length)]
  end

  def handle_sharing(shared_objects)
    owner = user
    return if is_private

    if shared_objects.blank?
      # If share fields is empty and share? status is yes then we assume question have private setting
      update_attributes(is_private: true)
      return
    end

    shared_objects.each do |object|
      class_name = object.class.name

      # owner shares Question with non-friend, send invitation request to non-friend user before sharing Question
      if class_name == 'User' && owner.status_with(object) == :is_nothing
        InvitationRequest.send_invitation_request(self, owner, object)
      end
      # Object follow Question if object class name Group, FriendList, User(user should be friend of current user)
      if (class_name == 'User' && owner.status_with(object) == :is_friend) || %w(Group FriendList).include?(class_name)
        object.follow self
      end

      # notify user, question has shared with him
      if class_name == 'User' && owner.status_with(object) == :is_friend
        notify_friend(object) if object.settings(:email_notifications).friend_adds_question == true
      end

      # If object is FriendList class object then update Question attribute shared_all_friends or is_public
      object.update_object_sharing_status_true(self) if %w(FriendList).include?(class_name)
    end
  end
end
