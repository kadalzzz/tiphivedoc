require 'rails_helper'

describe FriendList do
  it { is_expected.to have_many(:followings) }
  it { is_expected.to validate_uniqueness_of(:name) }

  describe '#class_name' do
    let(:friend_list) { build(:friend_list) }
    it 'returns class name' do
      expect(friend_list.class_name).to eq 'FriendList'
    end
  end

  describe '#kind_id' do
    let(:friend_list) { build(:friend_list) }
    it 'returns pair of class name and id' do
      expect(friend_list.kind_id).to eq "#{friend_list.class.name}-#{friend_list.id}"
    end
  end

  describe '#update_object_sharing_status_true' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { create(:pocket, user_id: user.id) }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:friend_list) { build(:friend_list) }
    before do
      tip.follow(hive)
      pocket.follow(hive)
      tip.follow(pocket)
      friend_list.update_object_sharing_status_true(tip)
    end
    it 'returns tip attributes shared_all_friends to true' do
      expect(tip.shared_all_friends).to eq true
    end
  end

  describe '#update_object_sharing_status_false' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { create(:pocket, user_id: user.id) }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:friend_list) { build(:friend_list) }
    before do
      tip.follow(hive)
      pocket.follow(hive)
      tip.follow(pocket)
      friend_list.update_object_sharing_status_false(tip)
    end
    it 'returns tip attributes shared_all_friends to false' do
      expect(tip.shared_all_friends).to eq false
    end
  end

  describe '#change_name_based_on_domain' do
    let(:friend_list) { create(:friend_list) }
    before do
      FriendList.current_domain = Domain.root
    end
    it 'returns name attribute value is "All Friends"' do
      expect(friend_list.name).to eq 'All Friends'
    end
  end

  describe '#change_name_based_on_domain for Domain' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:domain) { create(:domain, user_id: user.id, active: true) }
    let(:hive) { create(:hive, user_id: user.id, domain_id: domain.id) }
    let(:pocket) { create(:pocket, user_id: user.id, domain_id: domain.id) }
    let(:tip) { create(:tip, user_id: user.id, domain_id: domain.id) }
    let(:friend_list) { create(:friend_list) }
    before do
      tip.follow(hive)
      pocket.follow(hive)
      tip.follow(pocket)
      friend_list.follow(tip)
      FriendList.current_domain = domain
    end
    it 'returns name attribute value is All Domain Members' do
      expect(tip.friend_lists.first.change_name_based_on_domain).to eq 'All Domain Members'
    end
  end
end
