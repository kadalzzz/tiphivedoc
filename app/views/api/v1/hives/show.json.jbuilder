hive             ||= @hive
friends_count    ||= @friends_count
liked_tips_count ||= @liked_tips_count

json.response do
  json.partial! 'api/v1/hives/hive', hive: hive

  json.questions_count hive.question_followers.count
  json.friends_count friends_count
  json.liked_tips_count liked_tips_count
end

json.status :success
