class CreateInvitationRequests < ActiveRecord::Migration
  def change
    create_table :invitation_requests do |t|
      t.string :email
      t.string :request_token
      t.string :request_type
      t.string :connectable_type
      t.integer :connectable_id
      t.string :other_connections
      t.integer :user_id

      t.timestamps
    end
    add_index :invitation_requests, :email
    add_index :invitation_requests, :request_token
    add_index :invitation_requests, :request_type
    add_index :invitation_requests, :connectable_type
    add_index :invitation_requests, :connectable_id
    add_index :invitation_requests, :user_id
  end
end
