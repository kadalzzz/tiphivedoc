class UpdateDescriptionInGroups < ActiveRecord::Migration
  def change
    change_column :groups, :description, :text, null: false
  end
end
