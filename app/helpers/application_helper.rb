module ApplicationHelper
  require 'htmlentities'

  def bootstrap_class_for(flash_type)
    case flash_type
    when 'success'
      'alert-success'
    when 'error'
      'alert-danger'
    when 'alert'
      'alert-warning'
    when 'notice'
      'alert-info'
    else
      flash_type.to_s
    end
  end

  def get_body_class
    classes = []
    classes << 'active-sidebar' if params[:lsb] == '1'
    classes << "#{params[:controller]}"
    classes << "#{params[:controller]}-#{params[:action]}"
    classes.join(' ')
  end

  def user_avatar(obj, version = :thumb, extra_class = nil)
    size = 33
    size = 33 if version == :tiny
    size = 50 if version == :thumb
    size = 70 if version == :small
    size = 150 if version == :medium
    size = 200 if version == :large

    if obj.class.name == 'User'
      if obj.avatar.present?
        avatar_url = obj.avatar.send(version).url
      elsif obj.try(:first_name).present?
        avatar_url = "#{ENV['AVATAR_HOST']}/#{size}/f2af1f/ffffff.png&text=#{obj.first_name[0]}"
      else
        avatar_url = "#{ENV['AVATAR_HOST']}/#{size}/f2af1f/ffffff.png&text=T"
      end
    elsif obj.class.name == 'Group'
      if obj.avatar.present?
        avatar_url = obj.avatar.send(version).url
      elsif obj.try(:title).present?
        avatar_url = "#{ENV['AVATAR_HOST']}/#{size}/f2af1f/ffffff.png&text=#{obj.title[0]}"
      else
        avatar_url = "#{ENV['AVATAR_HOST']}/#{size}/f2af1f/ffffff.png&text=T"
      end
    end

    image_tag avatar_url, class: "user-avatar-#{version}#{extra_class.present? ? " #{extra_class}" : ''}"
  end

  def filter_display_text(text)
    return '' if text.blank?

    text = text.gsub(/<br>|<br\/>|<br \/>/, "\n")
    text = ApplicationController.helpers.strip_tags(text)

    text.gsub!(/\r\n/, "\n")
    # text = text.strip
    # text.gsub!(/\n/, ' <br /> ')
    text.html_safe
  end

  def current_controller_action
    "#{params[:controller]}-#{params[:action]}"
  end

  def hives_title(item)
    item.following_hives.map { |hive| link_to hive.title, hive_path(hive) }.sample(2).join(', ')
  end

  def pockets_title(item)
    item.following_pockets.map { |pocket| link_to pocket.title, pocket_path(pocket) }.sample(2).join(', ')
  end

  def should_show_top_search?
    return false unless user_signed_in?
    return false if params[:controller] == 'home' && params[:action] == 'index'
    return false if current_controller_action == 'groups-request_invite'
    return true
  end

  def error_messages!(model)
    return '' if model.errors.empty?

    messages = model.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    sentence = I18n.t("errors.messages.not_saved",
                      :count    => model.errors.count,
                      :resource => model.class.model_name.human.downcase)

    html = <<-HTML
    <div id="error_explanation" class="alert alert-danger">
      <h2>#{sentence}</h2>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end

  def comment_box_style
    return 'display: none;' if current_page?(root_path)
    return 'display: none;' if current_page?(tips_path)
    return 'display: none;' if current_page?(questions_path)
    return 'display: none;' if params[:id] && current_page?(question_path(id: params[:id]))
    return 'display: none;' if params[:hive_id] && current_page?(hive_questions_path(hive_id: params[:hive_id]))
    return 'display: none;' if params[:pocket_id] && current_page?(pocket_questions_path(pocket_id: params[:pocket_id]))
    return 'display: none;' if params[:group_id] && current_page?(group_questions_path(group_id: params[:group_id]))
    return 'display: none;' if params[:user_id] && current_page?(user_questions_path(user_id: params[:user_id]))
  end

  def find_object_image(obj)
    setting = obj.find_object_settings(current_user)

    if setting.present?
      background_image_url =
        setting.background_image.present? ? setting.background_image.medium.url : image_url('pockets-no-images.png')
    else
      background_image_url =
        obj.background_image.present? ? obj.background_image.medium.url : image_url('pockets-no-images.png')
    end

    background_image_url
  end

  def truncate_object_description(obj)
    if !obj.description.blank?
      info        = obj.description.strip
      description = info.gsub(/\r\n/, ' ')
      contain     = truncate(description, length: 1500)
      contain
    else
      'Without description'
    end
  end

  def object_color(obj)
    return '#78d9d9' if obj.class.name == 'User'
    return '#add85f' if obj.class.name == 'Group'
    return '#8acfff' if obj.class.name == 'Hive'
    return '#c48aff' if obj.class.name == 'Pocket'
    return '#9898ff' if obj.class.name == 'Tip'
    return '#f598d6' if obj.class.name == 'Question'
    return '#ababab'
  end

  def tip_pictures_url(object)
    return '' if object.blank?
    pictures = []
    object.each do |pic|
      pictures << pic.image.large.url
    end
    pictures
  end

  def decode_helper(desc)
    coder       = HTMLEntities.new
    decode_desc = coder.decode(desc)
    decode_desc.gsub(/\s+/, ' ').strip
  end

  def get_total_liked_tips_group(group_tips)
    total_likes = 0
    # group_tips = group.following_tips
    if group_tips.present?
      group_tips.each do |tip|
        total_likes += tip.votes.count.to_i
      end
    end
    total_likes
  end

  def get_user_from_mail(email)
    User.where(email: email).first
  end

  def get_url(object)
    share_url = ''
    case object.class.name
    when 'Tip'
      share_url = tip_url(object)
    when 'Hive'
      share_url = hive_url(object)
    when 'Question'
      share_url = question_url(object)
    when 'Group'
      share_url = group_url(object)
    when 'Domain'
      share_url = domain_url(object)
    end
    share_url
  end
end
