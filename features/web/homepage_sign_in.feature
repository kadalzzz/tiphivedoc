@javascript
Feature: Homepage and sign in

  Background:
    # Given a user exists with email: "john.doe@email.com", first_name: "John", last_name: "Doe", password: "password"

  Scenario: Visit homepage and able to sign in
    Given I am on the homepage
    Then I should see "Welcome To TipHive."
    And I should see "Login"
    When I follow "Login"
    Then I should be on the sign in page
    Then I fill in "Email" with "john.doe@email.com"
    And I fill in "Password" with "password"
    Then I press "Log In"
    Then I should see "John"
    And I should see "My Hives"
