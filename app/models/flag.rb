class Flag < ActiveRecord::Base
  belongs_to :flagable, polymorphic: true
  belongs_to :flag, polymorphic: true

  searchable do
    string :flagable_type
    string :flag_type
    integer :flagable_id
    integer :flag_id
  end
end
