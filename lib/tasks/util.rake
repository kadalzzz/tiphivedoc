require 'fileutils'

namespace :util do
  desc "Regenerate Group Hive Pocket Tip model slugs"
  task regenerate_slugs: :environment do
    %w(Group Hive Pocket Tip).each do |kind|
      kind.constantize.find_each do |obj|
        obj.slug = nil if obj.respond_to? :slug
        obj.save!
      end
    end
  end

  desc "Regenerate user slugs"
  task regenerate_user_slugs: :environment do
    User.find_each do |obj|
      next if obj.first_name == nil
      obj.username = nil
      obj.save!
    end
  end

  desc "Fetch existing avatars"
  task fetch_existing_avatars: :environment do
    conn = ActiveRecord::Base.connection
    Dir.entries("#{Rails.root}/public/uploads/user/avatar").each do |filename|
      asset_id = filename.to_i
      next if asset_id <= 0
      image_name = nil
      Dir.entries("#{Rails.root}/public/uploads/user/avatar/#{filename}").each do |image_file|
        next if image_file == '.' || image_file == '..'
        next if image_file.to_i > 0
        next if image_file =~ /large_|medium_|small_|tiny_|thumb_/
        next if image_file !~ /.jpg|.jpeg|.png|.gif/i
        image_name = image_file
      end
      next if image_name.nil?
      user = User.where(id: asset_id).first
      next if user.nil?
      conn.execute("UPDATE users SET avatar='#{image_name.downcase.gsub(/'/, '').gsub(/[^A-Za-z0-9\.-_]+/, '-')}' WHERE id=#{asset_id}")
    end
  end

  desc "Generate test data"
  task generate_test_data: :environment do
    email = ENV['EMAIL'] || 'teefan@me.com'
    user = User.where(email: email).first
    user = User.first if user.blank?
    return unless user

    NUM_HIVES = 18
    NUM_POCKETS = 18
    NUM_TIPS = 18

    hives = []
    pockets = []
    tips = []

    (0..NUM_HIVES).each do
      hive = {
        title: Faker::Company.name,
        description: Faker::Company.catch_phrase,
        user_id: user.id
      }
      hive = Hive.create(hive)
      user.follow hive
      hives << hive


      (0..NUM_POCKETS).each do
        pocket = {
          title: Faker::Company.name,
          description: Faker::Company.catch_phrase,
          user_id: user.id
        }
        pocket = Pocket.create(pocket)
        pocket.follow hive
        user.follow pocket
        pockets << pocket

        (0..NUM_TIPS).each do
          tip = {
            title: Faker::Lorem.sentence,
            description: Faker::Lorem.paragraph(4),
            latitude: Faker::Address.latitude,
            longitude: Faker::Address.longitude,
            user_id: user.id
          }
          tip = Tip.create(tip)
          tip.follow pocket
          tip.follow hive
          user.follow tip
          user.likes tip if Random.rand(8) == 1
          tips << tip
        end
      end
    end

  end

  desc "Generate test hives"
  task generate_test_hives: :environment do
    email = ENV['EMAIL'] || 'teefan@me.com'
    user = User.where(email: email).first
    user = User.first if user.blank?
    return unless user

    NUM_HIVES = 18

    hives = []

    (0..NUM_HIVES).each do
      hive = {
        title: Faker::Company.name,
        description: Faker::Company.catch_phrase,
        user_id: user.id
      }
      hive = Hive.create(hive)
      user.follow hive
      hives << hive
    end

  end

  desc "Generate test pockets"
  task generate_test_pockets: :environment do
    email = ENV['EMAIL'] || 'teefan@me.com'
    user = User.where(email: email).first
    user = User.first if user.blank?
    return unless user

    hive_id = ENV['HIVE_ID']
    hive = Hive.where(id: hive_id).first || user.following_hives.first
    return unless hive

    NUM_POCKETS = 18

    pockets = []

    (0..NUM_POCKETS).each do
      pocket = {
        title: Faker::Company.name,
        description: Faker::Company.catch_phrase,
        user_id: user.id
      }
      pocket = Pocket.create(pocket)
      pocket.follow hive
      user.follow pocket
      pockets << pocket
    end

  end

  desc "Generate test tips"
  task generate_test_tips: :environment do
    email = ENV['EMAIL'] || 'teefan@me.com'
    user = User.where(email: email).first
    user = User.first if user.blank?
    return unless user

    pocket_id = ENV['POCKET_ID']
    pocket = Pocket.where(id: pocket_id).first || user.following_pockets.first
    return unless pocket

    hive = pocket.following_hives.first
    return unless hive

    NUM_TIPS = 18

    tips = []

    (0..NUM_TIPS).each do
      tip = {
        title: Faker::Lorem.sentence,
        description: Faker::Lorem.paragraph(4),
        latitude: Faker::Address.latitude,
        longitude: Faker::Address.longitude,
        user_id: user.id
      }
      tip = Tip.create(tip)
      tip.follow pocket
      tip.follow hive
      user.follow tip
      user.likes tip if Random.rand(8) == 1
      tips << tip
    end

  end

  desc "Generate test friends"
  task generate_test_friends: :environment do
    email = ENV['EMAIL'] || 'teefan@me.com'
    user = User.where(email: email).first
    user = User.first if user.blank?
    return unless user

    NUM_FRIENDS = 18

    friends = []

    (0..NUM_FRIENDS).each do
      first_name = Faker::Name.first_name
      last_name = Faker::Name.last_name
      email = Faker::Internet.email "#{first_name} #{last_name}"
      friend = {
        email: email,
        first_name: first_name,
        last_name: last_name,
        password: 'password'
      }
      friend = User.new(friend)
      friend.save!
      friend.follow user
      user.follow friend
      friends << friend
    end

  end

  desc "Generate test users"
  task generate_test_users: :environment do
    NUM_USERS = 10_000

    (0..NUM_USERS).each do
      first_name = Faker::Name.first_name
      last_name = Faker::Name.last_name
      email = Faker::Internet.email "#{first_name} #{last_name}"
      user = {
        email: email,
        first_name: first_name,
        last_name: last_name,
        password: 'password'
      }
      User.create(user)
    end
  end

  desc "Count Tip comments"
  task count_tip_comments: :environment do
    Tip.find_each do |tip|
      tip.comments_count = tip.root_comments.count
      tip.save
    end
  end

  desc "Count Tip likes"
  task count_tip_likes: :environment do
    Tip.find_each do |tip|
      tip.cached_votes_up = tip.likes.size
      tip.cached_votes_total = tip.likes.size
      tip.cached_weighted_score = 1
      tip.cached_votes_score = tip.cached_weighted_score * tip.cached_votes_total
      tip.save
    end
  end

  desc "Clean invalid images"
  task clean_invalid_images: :environment do
    Picture.find_each do |picture|
      next if picture.image.path =~ /jpg$|jpeg$|gif$|png$/i
      puts "picture.image: #{picture.image}"
      puts "picture.imageable_id: #{picture.imageable_id}"
      puts "picture.imageable_type: #{picture.imageable_type}"
      picture.destroy
    end
  end

  desc "Migrate old user avatars"
  task migrate_user_avatars: :environment do
    db_conn = ActiveRecord::Base.connection
    User.find_each do |user|
      user_id = user.id

      picture = Picture.where(imageable_id: user_id, imageable_type: 'User').first
      next if picture.nil?

      filename = File.basename(picture.image.current_path)

      db_conn.execute("UPDATE users SET avatar='#{filename.downcase.gsub(/'/, '').gsub(/[^A-Za-z0-9\.-_]+/, '-')}' WHERE id=#{user_id}")

      asset_id = picture.id

      new_image_path = "#{Rails.root}/public/uploads/user/avatar/#{user_id}/#{filename.downcase.gsub(/'/, '').gsub(/[^A-Za-z0-9\.-_]+/, '-')}"
      existing_image_file = "#{Rails.root}/public/uploads/picture/image/#{asset_id}/#{filename}"

      next unless File.exist? existing_image_file

      FileUtils.mkdir_p "#{Rails.root}/public/uploads/user/avatar/#{user_id}"

      FileUtils.cp existing_image_file, new_image_path
    end
  end

  desc "Migrate old background images"
  task migrate_background_image: :environment do
    db_conn = ActiveRecord::Base.connection
    %w(User Group Hive Pocket).each do |object_class|
      object_class.constantize.find_each do |object|
        object_id = object.id

        if object_class == 'User'
          images_count = Picture.where(imageable_id: object_id, imageable_type: object_class).count
          next if images_count <= 1
          pictures = Picture.where(imageable_id: object_id, imageable_type: object_class).order('id ASC').all
          picture = pictures[1]
        else
          picture = Picture.where(imageable_id: object_id, imageable_type: object_class).first
        end

        next if picture.nil?
        filename = File.basename(picture.image.current_path)

        db_conn.execute("UPDATE #{object_class.tableize} SET background_image='#{filename.downcase.gsub(/'/, '').gsub(/[^A-Za-z0-9\.-_]+/, '-')}' WHERE id=#{object_id}")

        asset_id = picture.id

        new_image_path = "#{Rails.root}/public/uploads/#{object_class.downcase}/background_image/#{object_id}/#{filename.downcase.gsub(/'/, '').gsub(/[^A-Za-z0-9\.-_]+/, '-')}"
        existing_image_file = "#{Rails.root}/public/uploads/picture/image/#{asset_id}/#{filename}"

        next unless File.exist? existing_image_file

        FileUtils.mkdir_p "#{Rails.root}/public/uploads/#{object_class.downcase}/background_image/#{object_id}"

        FileUtils.cp existing_image_file, new_image_path
      end
    end


  end

  desc "Connect Group to Hive"
  task connect_group_to_hive: :environment do
    Follow.where(follower_type: 'Hive', followable_type: 'Group').find_each do |follow|
      hive = follow.follower
      group = follow.followable
      next if hive.nil? || group.nil?
      group.follow hive
    end
  end

  desc "Convert Group type"
  task convert_group_type: :environment do
    Group.find_each do |group|
      group.join_type = 'anyone' if group.join_type == 'all'
      group.join_type = 'domain' if group.join_type == 'email'
      group.join_type = 'location' if group.join_type == 'zip'
      group.save!
    end
  end

  desc "Set Group type"
  task set_group_type: :environment do
    CSV.foreach("#{Rails.root}/public/groups.csv", encoding: 'ISO-8859-1') do |line|
      group_id, group_type = line.split(',')[0]
      next if group_id.nil?

      group = Group.find group_id
      group.group_type = 'general' if group_type.nil?
      group.group_type = group_type.downcase if group_type.present?
      group.save!
    end

  end

  desc "Add Group domain limit"
  task add_group_domain_limit: :environment do
    db_conn = ActiveRecord::Base.connection
    db_conn.rename_table 'group_email_restrictions', 'group_domains' if db_conn.table_exists?('group_email_restrictions')

    Group.find_each do |group|
      domains_text = group.group_domains.map{ |d| d.email.gsub('@', '') }.join(',')
      next if domains_text.blank?

      group.allowing_domain_list = domains_text
      group.save!
    end
  end

  desc "Send test email"
  task send_test_email: :environment do
    EmailSendingWorker.perform_async('UserMailer', 'test_email', {})
  end

end
