module CommentObserver
  include Twitter::Extractor
  extend ActiveSupport::Concern

  ALLOWED_MENTIONED_CLASS = %w(Tip Comment)

  included do
    after_save :notify
    after_save :add_and_count_pictures
  end

  def notify
  end

  def notify_tip_comment
    notify_tip_comment_to_user(commentable.user)
    send_comment_notification
  end

  def notify_question_comment
    notify_question_comment_to_user(commentable.user)
    send_comment_notification
  end

  def notify_tip_comment_to_user(recipient, email_template = 'add_tip_comment_email')
    object = commentable
    return if user_id == recipient.id

    user = self.user

    email_params = {
      user_email:        user.email,
      user_first_name:   user.first_name,
      user_name:         user.name,
      friend_email:      recipient.email,
      friend_first_name: recipient.first_name,
      friend_name:       recipient.name,
      comment_id:        id,
      tip_title:         object.title,
      access_key:        object.access_key,
      domain_id:         domain_id
    }

    if object.class.name == 'Question'
      email_params[:question_slug] = object.to_param
    else
      email_params[:tip_slug] = object.to_param
    end

    EmailSendingWorker.perform_async('UserMailer', email_template, email_params)
  end

  def notify_question_comment_to_user(recipient, email_template = 'add_question_comment_email')
    question = commentable
    return if user_id == recipient.id
    user = self.user

    email_params = {
      user_email: user.email,
      user_first_name: user.first_name,
      user_name: user.name,
      friend_email: recipient.email,
      friend_first_name: recipient.first_name,
      friend_name: recipient.name,
      comment_id: id,
      question_slug: question.to_param,
      question_title: question.title
    }

    EmailSendingWorker.perform_async('UserMailer', email_template, email_params)
  end

  def send_comment_notification
    voters = []
    commentable.likes.by_type(User).find_each do |like|
      voters << like.voter if like.voter.email != commentable.user_email && like.voter.email != user_email
    end
    voters = voters.uniq.compact
    class_name = self.class.name

    mentions = extract_mentioned_screen_names(body)
    mentioned_emails = []

    mentions.each do |username|
      mentioned_user = User.where(username: username).first
      next if mentioned_user.blank?
      mentioned_emails << mentioned_user.email

      if mentioned_user
        next if commentable.user_email == mentioned_user.email || user_email == mentioned_user.email
        mentioned_emails << mentioned_user.email
        if ALLOWED_MENTIONED_CLASS.include? class_name
          notify_tip_comment_to_user(mentioned_user, 'comment_mention_email')
        end
        notify_question_comment_to_user(voter, 'comment_mention_email_for_question') if class_name == 'Question'
      end
    end

    # TODO: add template for likers
    voters.each do |voter|
      next if mentioned_emails.include?(voter.email)
      notify_tip_comment_to_user(voter, 'comment_like_email') if class_name == 'Tip'
      notify_question_comment_to_user(voter, 'comment_like_email_for_question') if class_name == 'Question'
    end

  end

  def add_and_count_pictures
    if picture_ids.present? && picture_ids.size > 0
      tip_pictures = Picture.where('id IN (?)', picture_ids.split(','))
      tip_pictures.each do |tip_picture|
        tip_picture.imageable = self
        tip_picture.save
      end
      # there's no pictures_count column in the comment table
      # update_column(:pictures_count, pictures.count)
    end
  end
end
