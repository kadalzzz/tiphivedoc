function getValueFor(key, array) {
  var arr = $.filter(array, function () {
    return !!~$.inArray(key, $(this));
  });
  return arr;
}

function autoLinkMentionAndHashtag(selector, type) {
  var type = type;
  var $selector = $(selector);
  var display_type = $selector.data('display-type');
  var comment_id = $selector.data('comment-id');
  var tip_id = $selector.data('tip-id');
  var existing_tip_pictures_count = $selector.data('tip-pictures-count');
  var existing_tip_comment_pictures_count = $selector.data('comment-pictures-count');
  var existing_tip_links_count = $selector.data('tip-links-count');
  var pictures_count = parseInt(existing_tip_pictures_count);
  var pictures_comment_count = parseInt(existing_tip_comment_pictures_count);
  var links_count = parseInt(existing_tip_links_count);

  $selector.each(function (index, elem) {
    var $elem = $(elem);
    var html_text = $elem.html();
    html_text = html_text.autoLink({
      target: "_blank", rel: "nofollow", callback: function (url) {
        var url = url.replace(/&amp;/g, '&');
        url = URL.normalize(url);
        var parsedURL = URL(url);
        if (!parsedURL.isValid()) {
          return null;
        }

        var domain = parsedURL.domain();
        var query_string = parsedURL.queryString();
        var path = parsedURL.path();
        var query_params = $.deparam(query_string);
        var carousel_inner_selector = '#tip-' + display_type + '-' + tip_id + ' #carousel-inner-' + tip_id;
        var carousel_inner_comment_selector = '#comment-pictures-' + comment_id + ' #carousel-inner-' + comment_id;

        // mp3 check
        if (/\.(mp3)$/i.test(url)) {
          var class_name = 'item';
          if ((pictures_count == 0) || (pictures_comment_count == 0)) {
            class_name += ' active';
          }
          var item_html = '<div class="' + class_name + '"><audio controls><source src="' + url + '" type="audio/mpeg">Your browser does not support the audio element.</audio></div>';
          $(carousel_inner_selector).append(item_html);
          pictures_count++;
          return ' ';
        }

        // mp4 check
        if (/\.(mp4)$/i.test(url)) {
          var class_name = 'item  player_mask';
          if ((pictures_count == 0) || (pictures_comment_count == 0)) {
            class_name += ' active';
          }
          var item_html = '<div class="' + class_name + '"><video width="100%" height="100%" controls><source src="' + url + '" type="video/mp4">Your browser does not support the video tag.</video></div>';
          $(carousel_inner_selector).append(item_html);
          pictures_count++;
          return ' ';
        }

        if (/\.(gif|png|jpe?g)/i.test(url)) {
          var class_name = 'item';
          if (type == "comment") {
            if (pictures_comment_count == 0) {
              class_name += ' active';
            }
            var item_html = '<a href="' + url + '" class="' + class_name + '" data-lightbox="comment-image-' + comment_id + '" style="background-image: url(' + url + ');"></a>';
            $(carousel_inner_comment_selector).append(item_html);
            pictures_comment_count++;
          } else {
            if (pictures_count == 0) {
              class_name += ' active';
            }
            var item_html = '<a href="' + url + '" class="' + class_name + '" data-lightbox="tip-image-' + tip_id + '" style="background-image: url(' + url + ');"></a>';
            $(carousel_inner_selector).append(item_html);
            pictures_count++;
          }
          return ' ';
        }

        // YouTube check
        if (domain == 'youtube.com') {
          var class_name = 'item';
          if ((pictures_count == 0) || (pictures_comment_count == 0)) {
            class_name += ' active';
          }
          var item_html = '<div class="' + class_name + '"><iframe width="100%" height="100%" src="//www.youtube.com/embed/' + query_params.v + '?rel=0&wmode=transparent' + '" frameborder="0" allowfullscreen wmode="Opaque"></iframe><div class="player_mask"></div></div>';
          if (type == "comment") {
            $(carousel_inner_comment_selector).append(item_html);
            pictures_comment_count++;
          } else {
            $(carousel_inner_selector).append(item_html);
            pictures_count++;
          }
          return ' ';
        }

        // YouTu.be check
        if (domain == 'youtu.be') {
          if (path) {
            var pattern = new RegExp(/^\/(.*?)$/i);
            var matches = pattern.exec(path);
            if (matches) {
              var class_name = 'item';
              if ((pictures_count == 0) || (pictures_comment_count == 0)) {
                class_name += ' active';
              }
              var item_html = '<div class="' + class_name + '"><iframe width="100%" height="100%" src="//www.youtube.com/embed/' + query_params.v + '" frameborder="0" allowfullscreen></iframe></div>';
              if (type == "comment") {
                $(carousel_inner_comment_selector).append(item_html);
                pictures_comment_count++;
              } else {
                $(carousel_inner_selector).append(item_html);
                pictures_count++;
              }
              return ' ';
            }
          }
        }

        // DailyMotion check
        if (domain == 'dailymotion.com') {
          if (path) {
            var pattern = new RegExp(/^\/([\d\w]+_[\d\-\w]+)/i);
            var matches = pattern.exec(path);
            if (matches) {
              var class_name = 'item';
              if ((pictures_count == 0) || (pictures_comment_count == 0)) {
                class_name += ' active';
              }
              var item_html = '<div class="' + class_name + '"><iframe frameborder="0" width="100%" height="100%" src="http://www.dailymotion.com/embed/video/' + matches[1] + '" allowfullscreen></iframe></div>';
              if (type == "comment") {
                $(carousel_inner_comment_selector).append(item_html);
                pictures_comment_count++;
              } else {
                $(carousel_inner_selector).append(item_html);
                pictures_count++;
              }
              return ' ';
            }
          }
        }

        // Vimeo check
        if (domain == 'vimeo.com') {
          if (path) {
            var pattern = new RegExp(/^\/([\d\w]+)/i);
            var matches = pattern.exec(path);
            if (matches) {
              var class_name = 'item';
              if (pictures_count == 0) {
                class_name += ' active';
              }
              var item_html = '<div class="' + class_name + '"><iframe src="//player.vimeo.com/video/' + matches[1] + '?title=0&amp;byline=0&amp;portrait=0&amp;color=33a352" width="100%" height="358" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe></div>';
              if (type == "comment") {
                $(carousel_inner_comment_selector).append(item_html);
                pictures_comment_count++;
              } else {
                $(carousel_inner_selector).append(item_html);
                pictures_count++;
              }
              return ' ';
            }
          }
        }

        // Google Maps check
        var map_match = url.match(/google(\.[a-z]+){1,2}\/maps\/?/i);
        if (map_match) {
          if (query_params.q) {
            getLocationParams(path = null, tip_id, query_params.q);
            var class_name = 'item';
            if ((pictures_count == 0) || (pictures_comment_count == 0)) {
              class_name += ' active';
            }
            var item_html = '<div class="' + class_name + '"><iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/place?q=' + query_params.q + '&zoom=15&&key=AIzaSyB2oQMOU1BattR4gp2aS19TMc0abgxx76I"></iframe></iframe></div>';
            if (type == "comment") {
              $(carousel_inner_comment_selector).append(item_html);
              pictures_comment_count++;
            } else {
              $(carousel_inner_selector).append(item_html);
              pictures_count++;
            }
            return ' ';
          } else if (path) {
            var pattern = new RegExp(/^\/maps\/([place|search|preview]+)\/(.*?)\//i);
            var matches = pattern.exec(path);
            if (matches) {
              getLocationParams(path, tip_id, query_params.q = null);
              var class_name = 'item';
              if ((pictures_count == 0) || (pictures_comment_count == 0)) {
                class_name += ' active';
              }
              var item_html = '<div class="' + class_name + '"><iframe width="100%" height="100%" frameborder="0" style="border:0" src="https://www.google.com/maps/embed/v1/' + matches[1] + '?q=' + matches[2] + '&zoom=15&key=AIzaSyB2oQMOU1BattR4gp2aS19TMc0abgxx76I"></iframe></div>';
              if (type == "comment") {
                $(carousel_inner_comment_selector).append(item_html);
                pictures_comment_count++;
              } else {
                $(carousel_inner_selector).append(item_html);
                pictures_count++;
              }
              return ' ';
            }
          }
        }

        // convert link to preview on tips
        if (type != "comment") {
          if (url) {
            $.ajax({
              url: '/tip_links/fetch',
              async: true,
              data: {url: url, tip_id: tip_id, no_delete: true},
              beforeSend: function (xhr) {

              },
              success: function (data, status, xhr) {
                $('#tip-' + display_type + '-links-' + tip_id).append(data);
              },
              complete: function (xhr, status) {

              }
            });
            links_count++;
            return ' ';
          }
        }

        return null;
      }
    });
    html_text = twttr.txt.autoLinkUsernamesOrLists(html_text, {usernameUrlBase: base_url + 'users/'});
    html_text = twttr.txt.autoLinkHashtags(html_text, {hashtagUrlBase: base_url + 'search?q='});
    $elem.html(html_text);
    $elem.html($elem.html().trim().replace(/(?:\r\n|\r|\n)/g, '<br />'));
  });

  if (type != "comment") {
    if (pictures_count == 0) {
      if ($('#tip-pictures-' + tip_id).find('#tip-map-canvas-' + tip_id).size() == 0) {
        $('#tip-pictures-' + tip_id).remove();
      }
      if (links_count == 0) {
        $('#tip-item-text-' + tip_id).css('height', '420px');
      }
    }

    if (pictures_count == 1) {
      $('#tip-pictures-' + tip_id + ' .carousel-control').remove();
    }

    if (pictures_count > 0 || links_count == 0) {
      $('#tip-item-links-' + tip_id).remove();
    }
  } else {
    if (pictures_comment_count == 0) {
      $('#comment-pictures-' + comment_id).remove();
    }

    if (pictures_comment_count == 1) {
      $('#comment-pictures-' + comment_id + ' .carousel-control').remove();
    }
  }

}

function getLocationParams(path, tip_id, q) {
  if (q == null) {
    var lat = path.split('@')[1].split(",")[0];
    var lng = path.split('@')[1].split(",")[1];
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "http://maps.googleapis.com/maps/api/geocode/json",
      data: {'latlng': lat + "," + lng, 'sensor': false},
      success: function (data) {
        if (data.results.length) {
          var location = data.results[0].formatted_address;
          // saveLocationData(lat, lng, location, tip_id);
          // console.log("lat: "+ lat + " lng: "+ lng + " location: " + location + " Tip: " + tip_id);
        } else {
          console.log("Error! Invalid address");
        }
      }
    });
  } else {
    $.ajax({
      type: "GET",
      dataType: "json",
      url: "http://maps.googleapis.com/maps/api/geocode/json",
      data: {'address': q, 'sensor': false},
      success: function (data) {
        if (data.results.length) {
          var location = data.results[0].formatted_address;
          var lat = data.results[0]["geometry"]["location"].lat;
          var lng = data.results[0]["geometry"]["location"].lng;
          // saveLocationData(lat, lng, location, tip_id);
          // console.log("lat: "+ lat + " lng: "+ lng + " location: " + location + " Tip: " + tip_id);
        } else {
          console.log("Error! Invalid address");
        }
      }
    });
  }
}

function saveLocationData(lat, lng, location, tip_id) {
  $.ajax({
    type: "POST",
    dataType: "json",
    url: '/tips/' + tip_id + '/update_tip_location',
    data: {'lat': lat, 'lng': lng, 'location': location},
    success: function (data) {
      if (data) {
        return true;
      } else {
        console.log("Error! wrong something")
      }
    }
  });
}

function autocompleteMentions(selector) {
  var $textcomplete = $(selector).textcomplete([
    { // mention strategy
      match: /(^|\s)@(\w*)$/,
      search: function (term, callback) {
        $.getJSON('/search_mentions', {q: term})
          .done(function (resp) {
            callback(resp);
          })
          .fail(function () {
            callback([]);
          });
      },
      replace: function (value) {
        return '$1@' + value + ' ';
      },
      maxCount: 3
    }
  ]);
}

function infiniteScroll() {
  var $container = $('.infinite-container');
  $container.infinitescroll({
    navSelector: '.pagination',    // selector for the paged navigation
    nextSelector: '.pagination li.next a',  // selector for the NEXT link (to page 2)
    itemSelector: '.item',     // selector for all items you'll retrieve
    bufferPx: 40,
    prefill: true,
    loading: {
      msgText: "<em>Loading the next set of items...</em>",
      finishedMsg: "<em>You've reached the end.</em>",
      img: '/ajax-loader.gif',
      selector: '#loading'
    }
  });
}

function infiniteScrollTips() {
  var $container = $('#tips-feed-container');
  $container.infinitescroll({
      navSelector: '.pagination',    // selector for the paged navigation
      nextSelector: '.pagination li.next a',  // selector for the NEXT link (to page 2)
      itemSelector: '.tip',     // selector for all items you'll retrieve
      bufferPx: 40,
      prefill: true,
      loading: {
        msgText: "<em>Loading the next set of items...</em>",
        finishedMsg: "<em>You've reached the end.</em>",
        img: '/ajax-loader.gif',
        selector: '#loading'
      }
    },
    // trigger Masonry as a callback
    function (newElements) {
      var oldUrl = $(".pagination li.next a").attr("href");
      var pageNum = parseInt(getParameterByName("page", oldUrl));
      var newUrl = oldUrl.replace('page=' + pageNum, "page=" + (pageNum + 1));
      // set next link
      $(".pagination li.next a").attr("href", newUrl);
      // update url after load
      $container.infinitescroll('update', {
        path: function () {
          return newUrl;
        }
      });

      // hide new items while they are loading
      var $newElems = $(newElements).css({opacity: 0});
      // ensure that images load before adding to masonry layout
      $newElems.imagesLoaded(function () {
        // show elems now they're ready
        $newElems.animate({opacity: 1});
        // $container.masonry( 'appended', $newElems, true );
        // $container.masonry();
        $newElems.each(function (index, elem) {
          var $elem = $(elem);
          var tipId = $elem.attr('id').substr(11);
          autoLinkMentionAndHashtag('#' + $elem.attr('id') + ' .social-text', 'tips');
          autocompleteMentions('#' + $elem.attr('id') + ' .has-mention');
          $elem.find(".location-box").hide();
          $('#comments-' + tipId + ' .comment').each(function () {
            var commentId = $(this).data('id');
            autoLinkMentionAndHashtag('#' + $(this).attr('id') + ' .social-text', 'comment');
            $("a#comment-edit-" + commentId).on('click', function () {
              $('form#edit_comment_' + commentId).removeClass('hide')
            });
            $("a#cancel-comment-edit-" + commentId).on('click', function () {
              $('form#edit_comment_' + commentId).addClass('hide')
            });
          });
          $('#' + $elem.attr('id') + ' .carousel').carousel();

          $('.player_mask').click(function () {
            var iframe = $(this).closest('.item').find('iframe');
            var iframe_source = iframe.attr('src');
            iframe_source = iframe_source + "&autoplay=1";
            iframe.attr('src', iframe_source);
            $(this).hide();
            $('#carousel-detail-tip-' + tipId).carousel('pause');
          });

          // after create comment
          $('#comment-form-' + tipId).bind('ajax:success', function (json, status, xhr) {
            $('a.thumbnail').remove();
            $("#location-box-" + tipId).hide();
            $('#comment-picture-' + tipId).val("");
            $('#comment_location_' + tipId).val("");
            $('#comment_latitude_' + tipId).val("");
            $('#comment_longitude_' + tipId).val("");
          });

          $('#' + $elem.attr('id') + ' .tip-description').readmore({
            speed: 75,
            moreLink: '<a href="#">View more</a>',
            lessLink: '<a href="#">View less</a>',
            maxHeight: 175
          });
        });
      });
    });
}

function masonrizeTips() {
  var $container = $('#tips-container');
  // initialize Masonry after all images have loaded
  $container.imagesLoaded(function () {
    $container.masonry({
      columnWidth: 295,
      itemSelector: '.tip'
    });
  });

  $container.infinitescroll({
      navSelector: '.pagination',    // selector for the paged navigation
      nextSelector: '.pagination li.next a',  // selector for the NEXT link (to page 2)
      itemSelector: '.tip',     // selector for all items you'll retrieve
      bufferPx: 40,
      prefill: true,
      loading: {
        msgText: "<em>Loading the next set of items...</em>",
        finishedMsg: "<em>You've reached the end.</em>",
        img: '/ajax-loader.gif',
        selector: '#loading'
      }
    },
    // trigger Masonry as a callback
    function (newElements) {
      var oldUrl = $(".pagination li.next a").attr("href");
      var pageNum = parseInt(getParameterByName("page", oldUrl));
      var newUrl = oldUrl.replace('page=' + pageNum, "page=" + (pageNum + 1));
      // update next link
      $(".pagination li.next a").attr("href", newUrl);
      // update url after load
      $container.infinitescroll('update', {
        path: function () {
          return newUrl;
        }
      });

      // hide new items while they are loading
      var $newElems = $(newElements).css({opacity: 0});
      // ensure that images load before adding to masonry layout
      $newElems.imagesLoaded(function () {
        // show elems now they're ready
        $newElems.animate({opacity: 1});
        $container.masonry('appended', $newElems, true);
        $container.masonry();
        $newElems.each(function (index, elem) {
          var $elem = $(elem);
          autoLinkMentionAndHashtag('#' + $elem.attr('id') + ' .social-text', 'tips');
          var tipId = $elem.attr('id').match(/\d+$/)[0];
          $("#location-box-" + tipId).hide();
          $('#tip-body-' + tipId).click(function () {
            $('#remote-link-' + tipId).click();
            return false;
          });
          if ($('#tip-pictures-' + tipId).size() > 0) {
            $('#tip-item-text-' + tipId).hide();
            $('#tip-body-' + tipId).hover(function () {
              $('#tip-item-text-' + tipId).slideToggle();
            });
          }
        });
      });
    });
}

function masonrizeHives() {
  var $container = $('#hives-container');
  // initialize Masonry after all images have loaded
  $container.imagesLoaded(function () {
    $container.masonry({
      columnWidth: 235,
      itemSelector: '.hive'
    });
  });

  $container.infinitescroll({
      navSelector: '.pagination',    // selector for the paged navigation
      nextSelector: '.pagination li.next a',  // selector for the NEXT link (to page 2)
      itemSelector: '.hive',     // selector for all items you'll retrieve
      bufferPx: 40,
      prefill: true,
      loading: {
        msgText: "<em>Loading the next set of items...</em>",
        finishedMsg: "<em>You've reached the end.</em>",
        img: '/ajax-loader.gif',
        selector: '#loading'
      }
    },
    // trigger Masonry as a callback
    function (newElements) {
      var oldUrl = $(".pagination li.next a").attr("href");
      var pageNum = parseInt(getParameterByName("page", oldUrl));
      var newUrl = oldUrl.replace('page=' + pageNum, "page=" + (pageNum + 1));
      // update next link
      $(".pagination li.next a").attr("href", newUrl);
      // update url after load
      $container.infinitescroll('update', {
        path: function () {
          return newUrl;
        }
      });

      // hide new items while they are loading
      var $newElems = $(newElements).css({opacity: 0});
      // ensure that images load before adding to masonry layout
      $newElems.imagesLoaded(function () {
        // show elems now they're ready
        $newElems.animate({opacity: 1});
        $container.masonry('appended', $newElems, true);
        $container.masonry();
      });
    });
}

function masonrizePockets() {
  var $container = $('#pockets-container');
  // initialize Masonry after all images have loaded
  $container.imagesLoaded(function () {
    $container.masonry({
      columnWidth: 40,
      itemSelector: '.hex-item'
    });
  });

  $container.infinitescroll({
      navSelector: '.pagination',    // selector for the paged navigation
      nextSelector: '.pagination li.next a',  // selector for the NEXT link (to page 2)
      itemSelector: '.hex-item-pocket',     // selector for all items you'll retrieve
      bufferPx: 40,
      prefill: true,
      loading: {
        msgText: "<em>Loading the next set of items...</em>",
        finishedMsg: "<em>You've reached the end.</em>",
        img: '/ajax-loader.gif',
        selector: '#loading'
      }
    },
    // trigger Masonry as a callback
    function (newElements) {
      var oldUrl = $(".pagination li.next a").attr("href");
      var pageNum = parseInt(getParameterByName("page", oldUrl));
      var newUrl = oldUrl.replace('page=' + pageNum, "page=" + (pageNum + 1));
      // update next link
      $(".pagination li.next a").attr("href", newUrl);
      // update url after load
      $container.infinitescroll('update', {
        path: function () {
          return newUrl;
        }
      });

      // hide new items while they are loading
      var $newElems = $(newElements).css({opacity: 0});

      // ensure that images load before adding to masonry layout
      $newElems.imagesLoaded(function () {
        // show elems now they're ready
        $newElems.animate({opacity: 1});
        $container.masonry('appended', $newElems, true);
        $container.masonry();
      });

    });
}

function masonrizeQuestions() {
  var $container = $('#questions-container');
  // initialize Masonry after all images have loaded
  $container.imagesLoaded(function () {
    $container.masonry({
      columnWidth: 235,
      itemSelector: '.question'
    });
  });

  $container.infinitescroll({
      navSelector: '.pagination',    // selector for the paged navigation
      nextSelector: '.pagination li.next a',  // selector for the NEXT link (to page 2)
      itemSelector: '.question',     // selector for all items you'll retrieve
      bufferPx: 40,
      prefill: true,
      loading: {
        msgText: "<em>Loading the next set of items...</em>",
        finishedMsg: "<em>You've reached the end.</em>",
        img: '/ajax-loader.gif',
        selector: '#loading'
      }
    },
    // trigger Masonry as a callback
    function (newElements) {
      var oldUrl = $(".pagination li.next a").attr("href");
      var pageNum = parseInt(getParameterByName("page", oldUrl));
      var newUrl = oldUrl.replace('page=' + pageNum, "page=" + (pageNum + 1));
      // update next link
      $(".pagination li.next a").attr("href", newUrl);
      // update url after load
      $container.infinitescroll('update', {
        path: function () {
          return newUrl;
        }
      });

      // hide new items while they are loading
      var $newElems = $(newElements).css({opacity: 0});

      // ensure that images load before adding to masonry layout
      $newElems.imagesLoaded(function () {
        // show elems now they're ready
        $newElems.animate({opacity: 1});
        $container.masonry('appended', $newElems, true);
        // $container.masonry();
        $newElems.each(function (index, elem) {
          var $elem = $(elem);
          autoLinkMentionAndHashtag('#' + $elem.attr('id') + ' .social-text');
        });
      });

    });
}

function masonrizeContacts() {
  var $container = $('#contacts-container');
  // initialize Masonry after all images have loaded
  $container.imagesLoaded(function () {
    $container.masonry({
      columnWidth: 270,
      itemSelector: '.contact'
    });
  });

  $container.infinitescroll({
      navSelector: '.pagination',    // selector for the paged navigation
      nextSelector: '.pagination li.next a',  // selector for the NEXT link (to page 2)
      itemSelector: '.contact',     // selector for all items you'll retrieve
      bufferPx: 40,
      prefill: true,
      loading: {
        msgText: "<em>Loading the next set of items...</em>",
        finishedMsg: "<em>You've reached the end.</em>",
        img: '/ajax-loader.gif',
        selector: '#loading'
      }
    },
    // trigger Masonry as a callback
    function (newElements) {
      var oldUrl = $(".pagination li.next a").attr("href");
      var pageNum = parseInt(getParameterByName("page", oldUrl));
      var newUrl = oldUrl.replace('page=' + pageNum, "page=" + (pageNum + 1));
      // update next link
      $(".pagination li.next a").attr("href", newUrl);
      // update url after load
      $container.infinitescroll('update', {
        path: function () {
          return newUrl;
        }
      });

      // hide new items while they are loading
      var $newElems = $(newElements).css({opacity: 0});
      // ensure that images load before adding to masonry layout
      $newElems.imagesLoaded(function () {
        // show elems now they're ready
        $newElems.animate({opacity: 1});
        $container.masonry('appended', $newElems, true);
        $container.masonry();
      });
    });
}

function getParameterByName(name, url) {
  name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
  var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
    results = regex.exec(url);
  return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}
