class FilesController < ApplicationController
  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!
  before_action :set_file, only: [:show, :edit, :update, :destroy]

  # GET /files
  # GET /files.json
  def index
    @files = File.all
  end

  # GET /files/1
  # GET /files/1.json
  def show
  end

  # GET /files/new
  def new
    @file = File.new
  end

  # GET /files/1/edit
  def edit
    @file = File.find(params[:id])
  end

  # POST/PATCH/PUT /upload_file
  # POST/PATCH/PUT /upload_file.json
  def upload
    @file         = FileUpload.new(file_params)
    @file.title   = @file.file.file.filename
    @file.user_id = current_user.id

    if params[:tip_id].present?
      @file.fileable_type = 'Tip'
      @file.fileable_id   = params[:tip_id]
    end

    respond_to do |format|
      if @file.save
        format.html { redirect_to @file, notice: 'File was successfully created.' }
        format.json { render action: 'show' }
      else
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @file.errors, full_messages: @file.errors.full_messages },
                         status:   :fail, message: @file.errors.full_messages.join(', ') }
        end
      end
    end
  end

  def destroy
    @file.destroy
    message = 'File Destroyed'
    respond_to do |format|
      format.html { redirect_to files_url }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_file
    @file = FileUpload.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def file_params
    params.require(:file_upload).permit(:title, :file, :fileable_id, :fileable_type, :user_id)
  end
end
