class CommentsController < ApplicationController
  before_action :set_comment, only: [:show, :edit, :update, :destroy]

  # GET /comments
  # GET /comments.json
  def index
    @comments = comment.all
  end

  # GET /comments/1
  # GET /comments/1.json
  def show
  end

  # GET /comments/new
  def new
    @comment = current_user.comments.new
  end

  # GET /comments/1/edit
  def edit
  end

  # POST /comments
  # POST /comments.json
  def create
    @object = params[:object_kind].constantize.where(id: params[:object_id]).first

    if @object
      @comment = Comment.build_from(@object, current_user.id, comment_params)
    end

    respond_to do |format|
      if @comment.save
        @object.updated_at = Time.now
        @object.save
        @comment.notify_tip_comment if @object.class.name == 'Tip'
        @comment.notify_question_comment if @object.class.name == 'Question'
        format.js {}
        format.html { redirect_to @comment, notice: 'comment was successfully created.' }
        format.json { render template: 'api/v1/comments/show' }
      else
        format.js {}
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @comment.errors, full_messages: @comment.errors.full_messages },
                         status:   :fail, message: @comment.errors.full_messages.join(', ')
                 }
        end
      end
    end
  end

  # PATCH/PUT /comments/1
  # PATCH/PUT /comments/1.json
  def update
    authorize! :update, @comment
    respond_to do |format|
      if @comment.update(comment_params)
        format.html { redirect_to @comment, notice: 'comment was successfully updated.' }
        format.json { render template: 'api/v1/comments/show' }
        format.js {}
      else
        format.html { render action: 'edit' }
        format.json do
          render json: { response: { error: @comment.errors, full_messages: @comment.errors.full_messages },
                         status:   :fail, message: @comment.errors.full_messages.join(', ')
                 }
        end
        format.js {}
      end
    end
  end

  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    authorize! :destroy, @comment
    @comment_id = @comment.id
    @comment.destroy
    respond_to do |format|
      format.js {}
      format.html { redirect_to comments_url }
      format.json { render json: { response: {}, status: :success, message: 'Comment Destroyed' } }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_comment
    @comment = Comment.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def comment_params
    params.require(:comment).permit(:body, :subject, :user_id, :parent_id, :picture_ids, :longitude, :latitude,
                                    :location, :address)
  end
end
