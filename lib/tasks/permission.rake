namespace :permission do
  desc 'Rebuild Hives sharing permission'
  task rebuild_hives_sharing: :environment do
    # TODO: implement sharing permission rebuild

    all_friends_list = FriendList.find_by_name 'All Friends'
    public_list = FriendList.find_by_name 'Public'

    Hive.find_each do |hive|
      owner = hive.user

      if hive.sharing_type == 'private'
        hive.update_attribute('is_private', true)
      end

      if hive.sharing_type == 'all_friends'
        all_friends_list.follow hive
        hive.share(all_friends_list, owner) 
      end

      if hive.sharing_type == 'public'
        public_list.follow hive
        hive.share(public_list, owner)
      end

      followers = hive.followers
      followers.each do |follower|
        next if owner.nil?
        next if follower.class.name == 'User' && follower.id == owner.id
        if %w(FriendList Group User).include? follower.class.name
          hive.share(follower, owner)
        end
      end

    end
  end
  
  desc 'Rebuild Tips sharing permission'
  task rebuild_tips_sharing: :environment do
    all_friends_list = FriendList.find_by_name 'All Friends'
    public_list = FriendList.find_by_name 'Public'

    Tip.find_each do |tip|
      # update exiting tip before new sharing feature

      if tip.sharing_type == 'private'
        tip.update_attribute('is_private', true)
      end

      if tip.sharing_type == 'all_friends'
        all_friends_list.follow tip
        all_friends_list.update_object_sharing_status_true(tip)
      end

      if tip.sharing_type == 'public'
        public_list.follow tip
        public_list.update_object_sharing_status_true(tip)
      end

      # check solid connection for created tip after new sharing feature implemented  
      if tip.is_public == true
        public_list.follow tip
        public_list.update_object_sharing_status_true(tip)
      end 

      if tip.shared_all_friends == true
        all_friends_list.follow tip
        all_friends_list.update_object_sharing_status_true(tip)
      end 
    end
    puts 'Completed'
  end
  

  desc 'Rebuild Questions sharing permission'
  task rebuild_questions_sharing: :environment do
    all_friends_list = FriendList.find_by_name 'All Friends'
    public_list = FriendList.find_by_name 'Public'

    Question.find_each do |question|
      # update exiting question before new sharing feature
      if question.sharing_type == 'private'
        question.update_attribute('is_private', true)
      end

      if question.sharing_type == 'all_friends'
        all_friends_list.follow question
        all_friends_list.update_object_sharing_status_true(question)
      end

      if question.sharing_type == 'public'
        public_list.follow question
        public_list.update_object_sharing_status_true(question)
      end

      # check solid connection for created question after new sharing feature implemented  
      if question.is_public == true
        public_list.follow question
        public_list.update_object_sharing_status_true(question)
      end 

      if question.shared_all_friends == true
        all_friends_list.follow question
        all_friends_list.update_object_sharing_status_true(question)
      end 
    end
    puts 'Completed'
  end
end
