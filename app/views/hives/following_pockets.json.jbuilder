json.array!(@results) do |result|
  json.extract! result, :id, :title
  json.array!(result.pockets) do |pocket|
    json.pocket(pocket, :id, :title)
    json.class_name pocket.class.name
    json.kind_id "#{pocket.class.name}-#{pocket.id}"
    json.value pocket.title
    json.name pocket.title
    json.title result.title
  end
end
