class CreateHives < ActiveRecord::Migration
  def change
    create_table :hives do |t|
      t.string :title
      t.text :description
      t.boolean :is_public, default: true
      t.boolean :is_on_profile, default: true
      t.boolean :allow_add_pocket, default: true
      t.boolean :allow_friend_share, default: true

      t.timestamps
    end

    add_index :hives, :is_public
    add_index :hives, :is_on_profile
    add_index :hives, :allow_add_pocket
    add_index :hives, :allow_friend_share
  end
end
