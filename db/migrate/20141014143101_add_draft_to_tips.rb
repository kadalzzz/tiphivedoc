class AddDraftToTips < ActiveRecord::Migration
  def change
    add_column :tips, :draft, :integer
  end
end
