require 'rails_helper'

describe ObjectSetting do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to belong_to(:object_setting) }
  it { is_expected.to belong_to(:domain) }

  describe '#object_not_shared_with_anyone' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:user1) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id, is_private: true) }
    let(:pocket) { create(:pocket, user_id: user.id) }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:object_setting) { create(:object_setting, object_setting_type: hive.class.name, object_setting_id: hive.id, user_id: user.id) }
    before do
      user.follow(user1)
      user.follow(hive)
      user1.follow(user)
      pocket.follow(hive)
      tip.follow(hive)
      tip.follow(pocket)
      hive.share(user1, user)
    end
    it { is_expected.to callback(:object_not_shared_with_anyone).after(:create) }
  end

  describe '#object_not_shared_with_anyone' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:user1) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id, is_private: true) }
    let(:pocket) { create(:pocket, user_id: user.id) }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:object_setting) { create(:object_setting, object_setting_type: hive.class.name, object_setting_id: hive.id, user_id: user.id) }
    before do
      user.follow(user1)
      user.follow(hive)
      user1.follow(user)
      pocket.follow(hive)
      tip.follow(hive)
      tip.follow(pocket)
      hive.share(user1, user)
    end
    it { is_expected.to callback(:object_not_shared_with_anyone).after(:update) }
  end

end
