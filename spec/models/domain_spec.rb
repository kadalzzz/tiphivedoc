require 'rails_helper'

describe Domain do
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:groups) }
  it { is_expected.to validate_presence_of(:name) }
  it { is_expected.to validate_uniqueness_of(:name) }

  describe '#full_name' do
    let(:domain) { build(:domain, name: 'abc') }
    it 'returns subdomain url' do
      expect(domain.full_name).to eq 'abc.tiphive.com'
    end
  end

  describe '#root?' do
    subject { domain.root? }
    context 'when name does not present' do
      let(:domain) { build(:domain, name: nil) }
      it { is_expected.to be_truthy }
    end
    context 'when name is empty' do
      let(:domain) { build(:domain, name: '') }
      it { is_expected.to be_truthy }
    end
    context 'when name is reserved domain' do
      let(:domain) { build(:domain, name: 'beta') }
      it { is_expected.to be_truthy }
    end
    context 'when name present' do
      let(:domain) { build(:domain, name: 'abcde') }
      it { is_expected.to be_falsey }
    end
  end

  describe '#logo_path' do
    subject { domain.logo_path }
    context 'when domain is root' do
      let(:domain) { build(:root_domain) }
      it { is_expected.to eq 'logo.png' }
    end
    context 'when domain is not root' do
      let(:domain) { build(:domain) }
      it { is_expected.not_to eq 'logo.png' }
    end
  end

  describe '.root' do
    subject { Domain.root }
    it 'returns new instance' do
      is_expected.to be_a_kind_of Domain
    end
    it 'returns root domain' do
      expect(subject.root?).to be_truthy
    end
  end

  describe '#users_count' do
    let(:domain) { create(:domain) }
    before do
      Follow.skip_callback(:create, :after, :create_connections)
      Timecop.travel(3.months.ago)
      10.times do
        user = create(:user)
        user.follow(domain)
      end
      Timecop.return
      Timecop.travel(2.months.ago)
      10.times do
        user = create(:user)
        user.follow(domain)
      end
      Timecop.return
    end
    it 'returns user followers created before the given date' do
      expect(domain.users_count(2.months.ago)).to eq 10
    end
    context 'when date is missing' do
      it 'returns user followers created till date' do
        expect(domain.users_count).to eq 20
      end
    end
  end

  describe '#members' do
    let(:user) { create(:user) }
    let(:domain) { create(:domain, user_id: user.id) }
    before do
      Follow.skip_callback(:create, :after, :create_connections)
      5.times do
        user = create(:user)
        user.follow(domain)
      end
    end
    it 'returns domain members' do
      expect(domain.members.count).to eq 5
    end
  end

  describe '#status_with' do
    let(:domain) { create(:domain) }
    let(:member) { create(:user) }
    let(:not_member) { create(:user) }
    before do
      member.follow(domain)
    end
    it 'returns user status with domain is member' do
      expect(domain.status_with(member)).to eq :is_member
    end

    it 'returns user status with domain is not member' do
      expect(domain.status_with(not_member)).to eq :is_nothing
    end
  end



  describe '#price' do
    before do
      Domain.class_eval("attr_writer :users_count")
    end
    let(:domain) { build(:domain) }
    subject { domain.price }
    context 'when @users_count is zero' do
      it 'returns zero' do
        is_expected.to eq 0.0
      end
    end
    context 'when @users_count is less than 100' do
      it 'returns 100' do
        domain.users_count = 50
        is_expected.to eq 100
      end
    end
    context 'when @users_count is between 100 and 5000' do
      it 'returns 1 per user' do
        domain.users_count = 3000
        is_expected.to eq 3000
      end
    end
    context 'when @users_count is between 5000 and 10000' do
      it 'returns 0.9 per user' do
        domain.users_count = 7000
        is_expected.to eq 6300.0
      end
    end
    context 'when @users_count greater than 10000' do
      it 'returns 0.5 per user' do
        domain.users_count = 110000
        is_expected.to eq 55000.0
      end
    end
  end
end
