class AddMoreMissingIndexes < ActiveRecord::Migration
  def change
    add_index :settings, %w(target_id target_type) unless index_exists? :settings, %w(target_id target_type)
    add_index :votes, %w(votable_id votable_type) unless index_exists? :votes, %w(votable_id votable_type)
    add_index :votes, %w(voter_id voter_type) unless index_exists? :votes, %w(voter_id voter_type)
    add_index :file_uploads, %w(fileable_id fileable_type) unless index_exists? :file_uploads, %w(fileable_id fileable_type)
    add_index :tips, :destroyer_id unless index_exists? :tips, :destroyer_id
    add_index :friend_invitations, %w(connectable_id connectable_type) unless index_exists? :friend_invitations, %w(connectable_id connectable_type)
    add_index :read_marks, %w(readable_id readable_type) unless index_exists? :read_marks, %w(readable_id readable_type)
    add_index :impressions, %w(impressionable_id impressionable_type) unless index_exists? :impressions, %w(impressionable_id impressionable_type)
    add_index :flags, %w(flagable_id flagable_type) unless index_exists? :flags, %w(flagable_id flagable_type)
    add_index :flags, %w(flag_id flag_type) unless index_exists? :flags, %w(flag_id flag_type)
    add_index :follows, %w(followable_id followable_type) unless index_exists? :follows, %w(followable_id followable_type)
    add_index :follows, %w(follower_id follower_type) unless index_exists? :follows, %w(follower_id follower_type)
    add_index :object_settings, %w(object_setting_id object_setting_type), name: 'obj_setting_id_and_type' unless index_exists? :object_settings, %w(object_setting_id object_setting_type), name: 'obj_setting_id_and_type'
    add_index :shares, %w(sharing_object_id sharing_object_type) unless index_exists? :shares, %w(sharing_object_id sharing_object_type)
    add_index :shares, %w(shareable_object_id shareable_object_type) unless index_exists? :shares, %w(shareable_object_id shareable_object_type)
    add_index :invitation_requests, :domain_id unless index_exists? :invitation_requests, :domain_id
    add_index :pictures, %w(imageable_id imageable_type) unless index_exists? :pictures, %w(imageable_id imageable_type)
    add_index :active_admin_comments, %w(resource_id resource_type) unless index_exists? :active_admin_comments, %w(resource_id resource_type)
    add_index :active_admin_comments, %w(author_id author_type) unless index_exists? :active_admin_comments, %w(author_id author_type)
    add_index :oauth_access_grants, :application_id unless index_exists? :oauth_access_grants, :application_id
    add_index :oauth_access_tokens, :application_id unless index_exists? :oauth_access_tokens, :application_id
    add_index :comments, :parent_id unless index_exists? :comments, :parent_id
  end
end
