pockets ||= @pockets

json.response do
  json.array!(pockets) do |pocket|
    json.partial! 'api/v1/pockets/pocket', pocket: pocket
  end
end

json.status :success
