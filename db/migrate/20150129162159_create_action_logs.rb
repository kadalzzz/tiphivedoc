class CreateActionLogs < ActiveRecord::Migration
  def change
    create_table :action_logs do |t|
      t.string :file_name
      t.string :class_name
      t.string :method_name
      t.integer :line_number
      t.text :stack_trace
      t.text :message

      t.timestamps
    end

    add_index :action_logs, :class_name
    add_index :action_logs, :method_name
  end
end
