TOTAL_RAM = 8 * 1024 # MB
CHOSEN_NUMBER_OF_PROCESSES = 2 # CPUs
RAM_PER_PROCESS = 150 # MB

max_app_threads_per_process =
  ((TOTAL_RAM * 0.75) - (CHOSEN_NUMBER_OF_PROCESSES * RAM_PER_PROCESS * 0.9)) /
    (RAM_PER_PROCESS / 10) /
    CHOSEN_NUMBER_OF_PROCESSES

puts "max_app_threads_per_process: #{max_app_threads_per_process}"
