json.array!(@action_logs) do |action_log|
  json.extract! action_log, :id, :file_name, :class_name, :method_name, :line_number, :stack_trace, :message
  json.url action_log_url(action_log, format: :json)
end
