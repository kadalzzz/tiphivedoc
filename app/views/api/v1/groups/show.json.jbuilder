group            ||= @group
friends_count    ||= @friends_count
liked_tips_count ||= @liked_tips_count

json.response do
  json.partial! 'api/v1/groups/group', group: group
  json.friends_count friends_count
  json.liked_tips_count liked_tips_count
end

json.status :success
