class AddUserIdToHives < ActiveRecord::Migration
  def change
    add_column :hives, :user_id, :integer

    add_index :hives, :user_id
  end
end
