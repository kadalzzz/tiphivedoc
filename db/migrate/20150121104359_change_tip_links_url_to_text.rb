class ChangeTipLinksUrlToText < ActiveRecord::Migration
  def up
    change_column(:tip_links, :url, :text)
  end

  def down
    change_column(:tip_links, :url, :string)
  end
end
