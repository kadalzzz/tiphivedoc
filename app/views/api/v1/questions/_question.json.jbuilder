first_hive   = question.hives.first
first_pocket = question.pockets.first
user         = question.user

json.id question.id
json.title question.title
# json.hives_and_pockets_title question.hives_and_pockets_title
# json.hives question.hives.map {|hive| {id: hive.id, title: hive.title} }
# json.pockets question.pockets.map {|pocket| {id: pocket.id, title: pocket.title} }
json.first_hive do
  if first_hive.present?
    json.id first_hive.try(:id)
    json.title first_hive.try(:title)
  end
end

json.first_pocket do
  if first_pocket.present?
    json.id first_pocket.try(:id)
    json.title first_pocket.try(:title)
  end
end
# json.description question.description
# json.address question.address
# json.location question.location
# json.latitude question.latitude
# json.longitude question.longitude
json.pictures question.pictures.map do |picture|
  {
    id:             picture.id,
    title:          picture.title,
    image_url:      picture.image.url,
    # image: picture.image,
    imageable_id:   picture.imageable_id,
    imageable_type: picture.imageable_type
  }
end

json.pictures_count question.pictures_count
json.comments_count question.comments_count
# json.cached_votes_total question.cached_votes_total
json.likes_count question.cached_votes_up
# json.cached_votes_down question.cached_votes_down
json.sharing_type question.sharing_type
# json.question_id question.question_id
json.user_id question.user_id
json.owner_name user.try :name
json.owner_thumb_url user && user.avatar.present? ? user.avatar.thumb.url : ''
json.like current_user.liked?(question)
