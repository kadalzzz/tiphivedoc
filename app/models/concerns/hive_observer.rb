module HiveObserver
  extend ActiveSupport::Concern

  included do
    after_save :notify
    after_create :create_default_pocket
  end

  def notify
  end

  def notify_friend(friend, share_hive_user)
    if domain && !domain.try(:root?)
      friend.follow domain
    end
    return if user_id == friend.id
    return if friend.settings(:email_notifications).someone_shared_hive_with_me == false

    email_params = {
      user_email:        share_hive_user.email,
      user_first_name:   share_hive_user.first_name,
      user_name:         share_hive_user.name,
      friend_email:      friend.email,
      friend_first_name: friend.first_name,
      friend_name:       friend.name,
      is_shared:         friend.following?(self),
      hive_slug:         to_param,
      hive_title:        title,
      domain_id:         domain_id
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{share_hive_user.name} is sending email to #{friend.email}"
    )
    ## DEBUG LOG END

    EmailSendingWorker.perform_async('UserMailer', 'add_hive_email', email_params)
  end

  def notify_friends
    follower_friends = [user.user_followers, users].flatten.uniq - [user]
    follower_friends.each do |friend|
      notify_friend(friend, user) if friend.settings(:email_notifications).someone_shared_hive_with_me == true
    end
  end

  def create_default_pocket
    pocket = Pocket.create(title: 'random', user_id: user.id)
    pocket.follow self
    user.follow pocket
  end
end
