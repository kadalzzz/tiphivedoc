# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

admin_user = AdminUser.first_or_create(email: 'admin@tiphive.com')
admin_user.password = 'password'
admin_user.save

# Role.where(name: 'member').first_or_create
# Role.where(name: 'admin').first_or_create

# User.update_all(role: 'member')
# User.find_each do |user|
#   Assignment.where(user_id: user.id, role_id: 1).first_or_create
# end

# ActiveRecord::Base.connection.execute("INSERT INTO assignments (user_id, role_id, created_at, updated_at) SELECT id, 1, '#{DateTime.now.to_s(:db)}', '#{DateTime.now.to_s(:db)}' FROM users")

FriendList.where(name: 'All Friends').first_or_create
FriendList.where(name: 'Public').first_or_create
