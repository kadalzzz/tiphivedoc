json.array!(@tiphive_settings) do |tiphive_setting|
  json.extract! tiphive_setting, :id, :name
  json.url tiphive_setting_url(tiphive_setting, format: :json)
end
