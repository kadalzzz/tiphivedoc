# Be sure to restart your server when you modify this file.

# TipHive::Application.config.session_store :cookie_store, key: '_TipHive_session'
# TipHive::Application.config.session_store :active_record_store, domain: :all
# TipHive::Application.config.session_store :cookie_store, key: '_TipHive_session', domain: :all
TipHive::Application.config.session_store :active_record_store, domain: :all
