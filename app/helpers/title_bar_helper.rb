module TitleBarHelper
  def simple_title_bar_with_title(title)
    content_for :title_bar do
      render partial: 'shared/title_bars/simple_title', locals: { title: title }
    end
  end

  def title_bar(title, content, container_class = nil)
    content_for :title_bar do
      render partial: 'shared/title_bars/simple_title', locals: { title: title, content: content, container_class: container_class }
    end
  end
end
