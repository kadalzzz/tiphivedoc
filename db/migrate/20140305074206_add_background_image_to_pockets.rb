class AddBackgroundImageToPockets < ActiveRecord::Migration
  def change
    add_column :pockets, :background_image, :string
  end
end
