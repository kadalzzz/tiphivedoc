tip ||= @tip

json.response do
  json.partial! 'api/v1/tips/tip', tip: tip
end

json.status :success
