class CreateSocialFriends < ActiveRecord::Migration
  def change
    create_table :social_friends do |t|
      t.string :provider
      t.string :uid
      t.string :name
      t.string :email
      t.integer :user_id

      t.timestamps
    end
  end
end
