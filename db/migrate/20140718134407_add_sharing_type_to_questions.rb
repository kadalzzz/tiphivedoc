class AddSharingTypeToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :sharing_type, :string, default: 'all_friends'
    add_index :questions, :sharing_type
  end
end
