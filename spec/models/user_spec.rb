require 'rails_helper'

describe User do
  it { is_expected.to validate_presence_of(:first_name) }
  it { is_expected.to have_many(:identities) }
  it { is_expected.to have_many(:hives) }
  it { is_expected.to have_many(:pockets) }
  it { is_expected.to have_many(:tips) }
  it { is_expected.to have_many(:groups) }
  it { is_expected.to have_many(:pictures) }
  it { is_expected.to have_many(:invitation_requests) }
  it { is_expected.to have_many(:friend_invitations) }
  it { is_expected.to have_many(:questions) }
  it { is_expected.to have_many(:pictures) }
  it { is_expected.to have_many(:shares) }
  it { is_expected.to have_many(:tip_links) }
  it { is_expected.to have_many(:object_settings) }
  it { is_expected.to have_many(:domains) }
  it { is_expected.to have_many(:social_friends) }
  it { is_expected.to have_many(:emails) }


  describe '#class_name' do
    let(:user) { build(:user) }
    it 'returns class name' do
      expect(user.class_name).to eq 'User'
    end
  end

  describe '#kind_id' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    it 'returns pair of class name and id' do
      expect(user.kind_id).to eq "#{user.class.name}-#{user.id}"
    end
  end

  describe '#name' do
    let(:first_name) { 'First' }
    let(:last_name) { 'Last' }
    let(:user) { create(:user, first_name: first_name, last_name: last_name) }
    it 'returns concatenated first name and last name' do
      expect(user.name).to eq "#{first_name} #{last_name}"
    end
  end

  describe '#short_name' do
    context 'when has last name' do
     let(:user) { create(:user, first_name: 'First', last_name: 'Last') }
     it 'returns first name and the first character of last name' do
       expect(user.short_name).to eq 'First L.'
     end
    end
    context 'when has no last name' do
      let(:user) { create(:user, first_name: 'First', last_name: '') }
      it 'returns only first name' do
        expect(user.short_name).to eq 'First '
      end
    end
  end

  describe '#status_with' do
    let(:user) { create(:user) }
    subject { user.status_with(other_user) }
    context 'when the other user is same user' do
      let(:other_user) { user }
      it 'returns :is_me' do
        is_expected.to eq :is_me
      end
    end
    context 'when user is following the other user' do
      let(:other_user) { create(:user) }
      before { user.follow(other_user) }
      it 'returns :is_following' do
        is_expected.to eq :is_following
      end
    end

    context 'when the other user is following user' do
      let(:other_user) { create(:user) }
      before { other_user.follow(user) }
      it 'returns :is_followed' do
        is_expected.to eq :is_followed
      end
    end

    context 'when both following each other' do
      let(:other_user) { create(:user) }
      before do
        other_user.follow(user)
        user.follow(other_user)
      end
      it 'returns :is_friend' do
        is_expected.to eq :is_friend
      end
    end

    context 'when there is no relation' do
      let(:other_user) { create(:user) }
      it 'returns :is_nothing' do
        is_expected.to eq :is_nothing
      end
    end
  end

  describe '#is_friend_with?' do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }
    subject { user.friend_with?(other_user) }
    context 'when both are friend' do
      before do
        user.follow(other_user)
        other_user.follow(user)
      end
      it { is_expected.to be_truthy }
    end

    context 'when both are not friend' do
      it { is_expected.to be_falsey }
    end
  end

  describe '#friends' do
    before do
      User.current_domain = Domain.root
    end
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }

    subject { user.friends }

    context "return user followers" do
      before { user.follow(other_user) }

      it { should include(other_user) }
    end

  end

  describe '#friend_ids' do
    let(:user) { create(:user) }
    let(:other_user) { create(:user) }

    subject { user.friend_ids }

    context "with no relationship" do
      it { should_not include(other_user.id) }
    end

    context "with one-sided relationship" do
      before { user.follow(other_user) }

      it { should include(other_user.id) }
    end
  end

  describe '#connected_hives' do
    let(:user) { create(:user) }
    let(:hive) { create(:hive, user_id: user.id ) }
    let(:group) { create(:group, user_id: user.id) }
    before do
      user.follow(hive)
      user.follow(group)
      group.follow(hive)
    end
    subject { user.connected_hives }
    it 'returns all hives' do
      is_expected.to eq [hive, hive]
    end
  end

  describe '#not_friends' do
    let(:user) { create(:user) }
    let(:user1) { create(:user) }
    let(:user2) { create(:user) }
    before do
      User.current_domain = Domain.root
    end
    subject { user.not_friends }
    context "returns non user friends" do
      it { should include(user1, user2) }
    end
  end

  describe '#not_friends' do
    let(:user) { create(:user) }
    let(:user1) { create(:user) }


    before do
      User.current_domain = Domain.root
      user.follow(user1)
      user1.follow(user)
    end

    subject { user.follower_ids }
    it 'returns follower ids' do
      is_expected.to eq [user1.id]
    end
  end

  describe '#friends_and_groups' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:user1) { create(:user) }
    let(:group) { create(:group, user_id: user.id) }
    before do
      user.follow(user1)
      user1.follow(user)
      user.follow(group)
    end
    subject { user.friends_and_groups }
    it 'returns frieds and groups' do
      is_expected.to eq [user1, group]
    end
  end

  describe '#matched_percentage_with' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:user1) { create(:user) }
    let(:hive) { create(:hive, user_id: user.id) }
    before do
      user.follow(user1)
      user1.follow(user)
      user.follow(hive)
      user1.follow(hive)
    end

    subject { user.matched_percentage_with(user1) }
    it 'returns percentage with another user' do
      is_expected.to eq 1.0
    end
  end






end
