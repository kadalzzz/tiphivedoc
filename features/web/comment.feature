@javascript
Feature: Comment on Tip and Question

  Background:
    # Given a user exists with email: "john.doe@email.com", first_name: "John", last_name: "Doe", password: "password"
    Given I login with email: "john.doe@email.com"
    Given a tip exists with title: "A wonderful tip", is_public: "true", user_id: 1
    Given a question exists with name: "A cool question", user_id: 1
    Given user with email: "john.doe@email.com" connected to tip with title: "A wonderful tip"

  Scenario: Add tip comment in Tip page
    When I go to the first tip page
    Then I should see "A wonderful tip"
    When I click on element with id "comment-tip-box-1"
    Then I fill in "comment-body-1" with "My awesome tip comment"
    Then I press "Comment"
    Then I should see "My awesome tip comment"

  Scenario: Add question comment in Question page
    When I go to the first question page
    Then I should see "A cool question"
    When I follow "Comment"
    Then I should see "Add Comment"
    Then I fill in "comment-body-1" with "My cool question comment"
    Then I press "Comment"
    Then I should see "My cool question comment"

