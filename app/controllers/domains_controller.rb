class DomainsController < ApplicationController
  before_filter :authenticate_user_from_token!
  before_action :authenticate_user!, except: [:home]
  layout 'domain', only: [:home]


  def index
    @domain = current_user.domains.new
  end

  def create
    @domain = current_user.domains.new(domain_params)
    if @domain.save
      current_user.follow @domain
      respond_to do |format|
        format.html { redirect_to domains_path, notice: 'Domain was successfully created' }
        format.js {}
        format.json { render template: 'api/v1/domains/show' }
      end
    else
      respond_to do |format|
        format.html { render :index }
        format.js {}
        format.json do
          render json: { response: { error: @domain.errors, full_messages: @domain.errors.full_messages },
                         status:   :fail, message: @domain.errors.full_messages.join(', ')
                 }
        end
      end
    end
  end

  def home
    redirect_to tips_path if user_signed_in?
  end

  def show
    @domain = current_user.domains.find(params[:id])
  end

  def destroy
    @domain = current_user.domains.find(params[:id])
    @domain.destroy
    respond_to do |format|
      format.html { redirect_to domains_path, notice: 'Domain deleted' }
      format.json { render json: { response: {}, status: :success, message: 'Domain deleted' } }
    end
  end

  def update
    @domain = current_user.domains.find(params[:id])
    if @domain.update(domain_params)
      respond_to do |format|
        format.html { redirect_to domains_path, notice: 'Domain was successfully updated' }
        format.js {}
        format.json { render template: 'api/v1/domains/show' }
      end
    else
      respond_to do |format|
        format.html { render :show }
        format.js {}
        format.json do
          render json: { response: { error: @domain.errors, full_messages: @domain.errors.full_messages },
                         status:   :fail, message: @domain.errors.full_messages.join(', ')
                 }
        end
      end
    end

  end

  def members
    @domain     = current_user.domains.find(params[:id])
    @members_az = {}
    ('A'..'Z').each do |char|
      char_members      = {
        count:   @domain.user_followers.where("first_name LIKE '#{char}%'").count,
        members: @domain.user_followers.where("first_name LIKE '#{char}%'").order(:first_name)
      }
      @members_az[char] = char_members
    end
    if params[:initial].present?
      @members = @domain.user_followers.where("first_name LIKE '#{params[:initial]}%'").order(:first_name).page params[:page]
    else
      @members = @domain.user_followers.order(:first_name).page params[:page]
    end

    respond_to do |format|
      format.html {}
      format.json {}
    end
  end

  def share
    @domain                       = current_user.domains.find(params[:id])
    @connected_friends_and_groups = @selected_friends_and_groups = @domain.members - [current_user]
  end

  def billing
    @domain = current_user.domains.find(params[:id])
  end

  def upload_image
    @domain = current_user.domains.find(params[:id])
    respond_to do |format|
      if @domain.update(image_params)
        format.html { render :show }
        format.json {}
      else
        format.html { render :show }
        format.json do
          render json: { response: { error: @domain.errors, full_messages: @domain.errors.full_messages },
                         status: :fail, message: @domain.errors.full_messages.join(', ') }
        end
      end
    end
  end

  private
  def domain_params
    params.require(:domain).permit(:name, :logo, :background, :active, :background_cache, :logo_cache,
                                   :remove_background, :remove_logo, :domain_id)
  end

  def image_params
    params.require(:domain_image).permit(:title, :logo, :background)
  end
end
