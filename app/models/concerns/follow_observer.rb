module FollowObserver
  extend ActiveSupport::Concern

  included do
    after_save :notify_user_follow
    after_create :create_connections
    before_destroy :destroy_connections
  end

  # TODO: Should consider moving these heavy tasks to background job [Teefan Jun-21 2014]
  def create_connections
    logger.info "*** Connect #{follower_type} ##{follower_id} to #{followable_type} ##{followable_id}"

    # When a Group is connecting to a Hive
    if follower_type == 'Group' && followable_type == 'Hive'
      hive  = followable
      group = follower
      # Shashi: return if hive and group blank
      return if group.nil?
      return if [hive, group].all?(&:blank?)
      # Let Group follow Hive tips
      hive.tips.each do |tip|
        group.follow tip
      end

      # Let Member of Group follow Hive
      group.followers.each do |member|
        return if member.blank?
        member.follow hive
        hive.share(group, member)
        group.notify_group_member(current_user, hive, member) if current_user
      end

    end

    # When User is becoming member of a Group
    if follower_type == 'User' && followable_type == 'Group'
      user  = follower
      group = followable
      # Shashi: return if user and group blank
      return if group.nil?
      return if [user, group].all?(&:blank?)
      # Let User follow all Group hives
      group.following_hives.each do |hive|
        if hive.present?
          user.follow hive
          hive.share(group, user)
        end
      end
    end

    # When a Pocket is connecting to a Hive
    if follower_type == 'Pocket' && followable_type == 'Hive'
      hive   = followable
      pocket = follower
      # Shashi: return if hive and pocket blank
      return if [hive, pocket].all?(&:blank?)
      # Connect Pocket tips to the Hive
      pocket.tips.each do |tip|
        tip.follow hive
      end
    end

    # When a Question is connecting to a Group
    if follower_type == 'Group' && followable_type == 'Question'
      question = followable
      group    = follower
      # Shashi: return if question and group blank
      return if [question, group].all?(&:blank?)
      # Send notification to Group members
      group_members = group.members
      group_members.each do |member|
        # TODO - Teefan: we should have a group member question adding notification setting
        question.notify_friend(member) if member.settings(:email_notifications).group_member_adds_a_question == true
      end
    end

    # When a Tip is connecting to a Question
    if follower_type == 'Tip' && followable_type == 'Question'
      tip      = follower
      question = followable
      # Shashi: return if question and group blank
      return if [tip, question].all?(&:blank?)
      # Let Tip follow Hive and Pocket question
      question.following_hives.each do |hive|
        tip.follow hive
      end
      question.following_pockets.each do |pocket|
        tip.follow pocket
      end
    end

    # When Tip is share to All Friends
    if follower_type == 'FriendList' && followable_type == 'Tip'
      if (follower.name == 'All Friends' || follower.name == 'All Domain Members') && current_user.present?
        tip     = followable
        friends = current_user.friends
        friends.each do |item|
          tip.notify_friend(item) if item.settings(:email_notifications).friend_adds_tip == 'always'
        end
      end
    end

    # When Question is share to All Friends
    if follower_type == 'FriendList' && followable_type == 'Question'
      if (follower.name == 'All Friends' || follower.name == 'All Domain Members') && current_user.present?
        question = followable
        friends  = current_user.friends
        friends.each do |item|
          question.notify_friend(item) if item.settings(:email_notifications).friend_adds_question == true
        end
      end
    end

    # When User is becoming member of a Group
    if follower_type == 'User' && followable_type == 'Domain'
      user   = follower
      domain = followable
      # Shashi: return if user and domain blank
      return if [user, domain].all?(&:blank?)
      # Let User follow all Group hives
      domain.notify_friend(user)
    end
  end


  # TODO: Should consider moving these heavy tasks to background job [Teefan Jun-21 2014]
  def destroy_connections
    logger.info "*** Disconnect #{follower_type} ##{follower_id} from #{followable_type} ##{followable_id}"
    # When a Group is disconnecting to a Hive
    if follower_type == 'Group' && followable_type == 'Hive'
      hive  = followable
      group = follower
      # Shashi: return if hive and group blank
      return if [hive, group].all?(&:blank?)
      # Have Group stop following tips
      hive.tips.each do |tip|
        group.stop_following tip
      end
    end

    # When a Pocket is disconnecting to a Hive
    if follower_type == 'Pocket' && followable_type == 'Hive'
      hive   = followable
      pocket = follower
      # Shashi: return if hive and pocket blank
      return if [hive, pocket].all?(&:blank?)
      # Disconnect Pocket tips from the Hive
      pocket.tips.each do |tip|
        tip.stop_following hive
      end
    end

    # When a Tip is disconnecting to a Hive
    if follower_type == 'Tip' && followable_type == 'Hive'
      hive = followable
      tip  = follower
      # Shashi: return if hive and pocket blank
      return if [hive, tip].all?(&:blank?)
      # Disconnect this Tip to Hive groups too
      hive.group_followers.each do |group|
        group.stop_following tip
      end
    end
  end

  def notify_user_follow
    logger.info '*** FollowObserver#notify_user_follow'
    return true if followable_type != 'User' || follower_type != 'User'

    user   = followable
    friend = follower

    email_params = {
      user_email:        user.email,
      user_first_name:   user.first_name,
      friend_first_name: friend.first_name,
      friend_name:       friend.name,
      friend_username:   friend.username,
      friend_id:         friend.id
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{user.name} is sending email to #{friend.email}"
    )
    ## DEBUG LOG END

    if user.following?(friend)
      EmailSendingWorker.perform_async('UserMailer', 'accept_friend_email', email_params)
    else
      EmailSendingWorker.perform_async('UserMailer', 'add_friend_email', email_params)
    end
  end
end
