Airbrake.configure do |config|
  config.api_key          = 'c8b36544c7e6b7793c582b84b57ff803'
  config.host             = 'errors.tiphive.com'
  config.port             = 80
  config.secure           = config.port == 443
  config.user_attributes  = %w(id name email username)
end
