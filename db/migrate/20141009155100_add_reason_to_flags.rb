class AddReasonToFlags < ActiveRecord::Migration
  def change
    add_column :flags, :reason, :text
  end
end
