@javascript
Feature: Basic operations: create Hive, Pocket, Tip and Question

  Background:
    # Given a user exists with email: "john.doe@email.com", first_name: "John", last_name: "Doe", password: "password"

  Scenario: User adds a hive, pocket and a tip
    When I login with email: "john.doe@email.com"

    Given I go to add group page
    Then I should see "New group"
    When I fill in "Group name" with "My Test Group title"
    And I fill in "Description" with "My Test Group description"
    And I press "Save"
    Then I should see "My Test Group title"

    When I click on element with id "dropdown-menu"
    Then I should see "Add member"
    Given I click on "Add member"
    Then I should see "Add member to group"

    Given I go to add hive page
    Then I should see "New hive"
    And I fill in "Title" with "My Hive title"
    And I fill in "Description" with "My Hive description"
    And I press "Save"
    Then I should see "My Hive title"

    Then I click on "Share"
    Then I should see "Share this hive"

    Given I go to add pocket page with first hive
    Then I should see "New pocket"
    And I fill in "Title" with "My Pocket title"
    And I fill in "Description" with "My Pocket description"
    And I press "Save"
    Then I should see "My Pocket title"

    Given I go to add tip page with first hive and first pocket
    Then I should see "New tip"
    Then I fill in "tip_title" with "My Tip title"
    And I fill in "tip_description" with "My Tip description"
    And I press "Save"
    Then I should see "My Tip title"

    Given I go to add question page with first hive and first pocket
    Then I should see "Ask for tips"
    Then I fill in "question_name" with "My Question name"
    # And I fill in "question_description" with "My Question description"
    And I press "Save"
    Then I should see "My Question name"
