module Helpers
  # fill_in_selectized('candidate_offices', 'Santa Monica')
  # fill_in_selectized('candidate_offices', ['San Francisco', 'Santa Monica'])
  def fill_in_selectized(key, *values)
    values.flatten.each do |value|
      page.execute_script(%{
        $('##{key} .selectize-input input').val('#{value}');
        $('##{key}').selectize()[0].selectize.createItem();
      })
    end
  end

  # selectize "Single-choice field", with: "Only option"
  # selectize "Multi-choice field", with: ["Option A", "Option B"]
  def selectize(key, with:)
    field = page.find_field(key, visible: false)
    case field.tag_name
      when "select"
        page.execute_script(%{
      $("select[name='#{field["name"]}']")
        .selectize()[0].selectize.setValue(#{Array(with)});
    })
      else
        Array(with).each do |value|
          page.execute_script(%{
        $("input[name='#{field["name"]}']")
          .next()
          .find(".selectize-input input").val('#{value}')
          .end()
          .prev()
          .selectize()[0].selectize.createItem();
      })
        end
    end
  end
end

include Warden::Test::Helpers
Warden.test_mode!

World(Helpers)
