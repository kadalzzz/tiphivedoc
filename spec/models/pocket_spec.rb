require 'rails_helper'

describe Pocket do
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:pictures) }
  it { is_expected.to have_many(:followings) }
  it { is_expected.to have_many(:follows) }
  it { is_expected.to have_many(:object_settings) }
  it { is_expected.to have_many(:shares) }

  describe '#to_param' do
    let(:pocket) { create(:pocket) }
    it 'returns parameterized value of id and name' do
      expect(pocket.to_param).to eq "#{pocket.id}-#{pocket.name.downcase}"
    end
  end

  describe '#class_name' do
    let(:pocket) { build(:pocket) }
    it 'returns class name' do
      expect(pocket.class_name).to eq 'Pocket'
    end
  end

  describe '#kind_id' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:pocket) { create(:pocket, user_id: user.id) }
    it 'returns pair of class name and id' do
      expect(pocket.kind_id).to eq "#{pocket.class.name}-#{pocket.id}"
    end
  end

  describe '#name' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:pocket) { create(:pocket, user_id: user.id) }
    it 'returns hive title' do
      expect(pocket.name).to eq "#{pocket.title}"
    end
  end

  describe '#hives_title' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive1) { create(:hive, user_id: user.id) }
    let(:hive2) { create(:hive, user_id: user.id) }
    let(:pocket) { create(:pocket) }
    before do
      pocket.follow(hive1)
      pocket.follow(hive2)
    end
    it 'returns concatenated hives title' do
      expect(pocket.hives_title).to eq "Hive, Hive"
    end
  end

  describe '#hives' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive1) { create(:hive, user_id: user.id) }
    let(:pocket) { create(:pocket) }
    before do
      pocket.follow(hive1)
    end
    subject { pocket.hives }
    it 'returns hives' do
      is_expected.to eq [hive1]
    end
  end

  describe '#tips' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:tip) { create(:tip, user_id: user.id) }
    before do
      tip.follow(hive)
      tip.follow(hive.pockets.first)
    end
    subject { hive.tips }
    it 'returns tips' do
      is_expected.to eq [tip]
    end
  end


  describe '#users' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:pocket) { create(:pocket, user_id: user.id) }
    before do
      user.follow(pocket)
    end
    subject { pocket.users }
    it 'returns users' do
      is_expected.to eq [user]
    end
  end

  describe '#groups' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:pocket) { create(:pocket, user_id: user.id) }
    let(:group) { create(:group, user_id: user.id) }
    before do
      group.follow(pocket)
    end
    subject { pocket.groups }
    it 'returns groups' do
      is_expected.to eq [group]
    end
  end

  describe '#users_and_groups' do
    let(:pocket) { create(:pocket) }
    let(:user) { create(:user) }
    let(:group) { create(:group) }
    before do
      user.follow(pocket)
      group.follow(pocket)
    end
    subject { pocket.users_and_groups }
    it 'returns users and groups' do
      is_expected.to eq [user, group]
    end
  end

  describe '#follow_shared_objects' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:friend) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:not_friend) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:pocket) { create(:pocket, user_id: user.id) }
    let(:group) { create(:group, user_id: user.id) }
    let(:friend_list) { create(:friend_list) }
    before do
      user.follow(pocket)
      user.follow(friend)
      friend.follow(user)
      InvitationRequest.current_domain = Domain.root
    end

    context 'when user is friend' do
      subject { pocket.follow_shared_objects([group, friend_list, friend]) }
      it 'follow shared object' do
        is_expected.to be_truthy
      end
    end

    context 'when user is not friend then send invitation request before sharing pocket' do
      subject { pocket.follow_shared_objects([not_friend, friend_list]) }
      it 'create user default settings all friends for this pocket' do
        is_expected.to be_truthy
      end
    end
  end
end
