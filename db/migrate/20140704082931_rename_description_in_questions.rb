class RenameDescriptionInQuestions < ActiveRecord::Migration
  def change
    rename_column :questions, :description, :name
  end
end
