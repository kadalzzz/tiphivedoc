# config valid only for Capistrano 3.2.1
lock '3.2.1'

set :application, 'tiphive'
set :repo_url, 'git@github.com:groupstance/tiphive.git'

# Default branch is :master
# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }
set :branch, 'master'

# Default deploy_to directory is /var/www/my_app
set :deploy_to, '/var/www/vhosts/newprod.tiphive.com'

set :rvm_type, :auto                        # Defaults to: :auto
set :rvm_ruby_version, '2.1.3@tiphive' # Defaults to: 'default'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, %w{config/database.yml config/sunspot.yml .env}

# Default value for linked_dirs is []
set :linked_dirs, %w{bin log solr tmp/pids tmp/cache tmp/sockets vendor/bundle public/system public/uploads public/assets}

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

set :sidekiq_pid, -> { release_path.join("tmp/pids/sidekiq.pid") }
set :sidekiq_options, -> { "-c 2 -q default -q mailer -e #{fetch(:rails_env, 'production')} -L #{current_path}/log/sidekiq.log" }

set :slack_team, "tiphive"
set :slack_token, "LjhwtfO1JH0vSYEynqOouBQb"

set :slack_icon_url,     -> { 'http://icons.iconarchive.com/icons/devcom/network/256/globe-Vista-icon.png' }
set :slack_icon_emoji,   -> { nil } # will override icon_url
set :slack_channel,      -> { '#general' }
set :slack_username,     -> { 'deploy' }
set :slack_run_starting, -> { true }
set :slack_run_finished, -> { true }
set :slack_run_failed,   -> { true }
set :slack_msg_starting, -> { "#{ENV['USER'] || ENV['USERNAME']} has started deploying branch #{fetch :branch} of #{fetch :application} to #{fetch :rails_env, 'production'}." }
set :slack_msg_finished, -> { "#{ENV['USER'] || ENV['USERNAME']} has finished deploying branch #{fetch :branch} of #{fetch :application} to #{fetch :rails_env, 'production'}." }
set :slack_msg_failed,   -> { "*ERROR!* #{ENV['USER'] || ENV['USERNAME']} failed to deploy branch #{fetch :branch} of #{fetch :application} to #{fetch :rails_env, 'production'}." }

set :whenever_identifier, ->{ "#{fetch(:application)}_#{fetch(:stage)}" }

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      # Your restart mechanism here, for example:
      execute :touch, release_path.join('tmp/restart.txt')
    end
  end

  desc 'Start Solr search engine'
  task :start_solr => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "sunspot:solr:start"
        end
      end
    end
  end

  desc 'Stop Solr search engine'
  task :stop_solr => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "sunspot:solr:stop"
        end
      end
    end
  end

  desc 'Reindex'
  task :reindex => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "sunspot:solr:reindex"
        end
      end
    end
  end

  desc 'Restart Solr search engine'
  task :restart_solr => [:set_rails_env] do
    on roles fetch(:bundle_roles), in: :sequence, wait: 60 do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "sunspot:solr:stop"
          execute :rake, "sunspot:solr:start"
          execute :rake, "sunspot:solr:reindex"
        end
      end
    end
  end

  desc 'Recreate Group Hive Pocket image versions'
  task :recreate_group_hive_pocket_image_versions => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "image:recreate_group_hive_pocket_image_versions"
        end
      end
    end
  end

  desc 'Rebuild Hives sharing permissions'
  task :rebuild_hives_sharing_permissions => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "permission:rebuild_hives_sharing"
        end
      end
    end
  end

  desc 'Rebuild Tips sharing permissions'
  task :rebuild_tips_sharing_permissions => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "permission:rebuild_tips_sharing"
        end
      end
    end
  end

  desc 'Rebuild Questions sharing permissions'
  task :rebuild_questions_sharing_permissions => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "permission:rebuild_questions_sharing"
        end
      end
    end
  end

  desc 'Recreate Tip image versions'
  task :recreate_tip_image_versions => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "image:recreate_tip_image_versions"
        end
      end
    end
  end

  desc 'Recreate Question image versions'
  task :recreate_question_image_versions => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "image:recreate_question_image_versions"
        end
      end
    end
  end

  desc 'Fetch existing avatars'
  task :fetch_existing_avatars => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "util:fetch_existing_avatars"
        end
      end
    end
  end

  desc 'Recreate User avatar versions'
  task :recreate_user_avatar_versions => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "image:recreate_user_avatar_versions"
        end
      end
    end
  end

  desc 'Regenerate ALL model slugs'
  task :regenerate_slugs => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "util:regenerate_slugs"
        end
      end
    end
  end

  desc 'Regenerate user slugs'
  task :regenerate_user_slugs => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "util:regenerate_user_slugs"
        end
      end
    end
  end

  desc 'Count Tip comments'
  task :count_tip_comments => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "util:count_tip_comments"
        end
      end
    end
  end

  desc 'Count Tip likes'
  task :count_tip_likes => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "util:count_tip_likes"
        end
      end
    end
  end

  desc 'Make question Tips follow the question'
  task :make_tips_follow_question => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "question:make_tips_follow"
        end
      end
    end
  end

  desc 'Assign colors'
  task :assign_colors => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "tip:assign_color"
          execute :rake, "question:assign_color"
          execute :rake, "hive:assign_color"
          execute :rake, "group:assign_color"
          execute :rake, "user:assign_color"
        end
      end
    end
  end


  desc 'Send test email'
  task :send_test_email => [:set_rails_env] do
    on roles fetch(:bundle_roles) do
      within release_path do
        with rails_env: fetch(:rails_env) do
          execute :rake, "util:send_test_email"
        end
      end
    end
  end

  # after :restart, :restart_solr
  # after :finished, :restart_solr
  after :finished, :send_test_email
  after :finished, 'mailman:restart'

  after :publishing, :restart

  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

end
