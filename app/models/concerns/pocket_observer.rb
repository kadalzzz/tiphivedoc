module PocketObserver
  extend ActiveSupport::Concern

  included do
    after_save :notify
  end

  def notify_friend(friend, share_pocket_user)
    return if user_id == friend.id
    return if friend.settings(:email_notifications).someone_shared_pocket_with_me == false

    email_params = {
      user_email:        share_pocket_user.email,
      user_first_name:   share_pocket_user.first_name,
      user_name:         share_pocket_user.name,
      friend_email:      friend.email,
      friend_first_name: friend.first_name,
      friend_name:       friend.name,
      is_shared:         friend.following?(self),
      pocket_slug:       to_param,
      hives_title:       hives_title,
      domain_id:         domain_id
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{share_pocket_user.name} is sending email to #{friend.email}"
    )
    ## DEBUG LOG END

    EmailSendingWorker.perform_async('UserMailer', 'add_pocket_email', email_params)
  end

  def notify_friends
    user             = self.user
    follower_friends = [users].flatten.uniq - [user]
    follower_friends.each do |friend|
      notify_friend(friend, user)
    end
  end

  def notify
  end
end
