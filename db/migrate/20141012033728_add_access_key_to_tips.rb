class AddAccessKeyToTips < ActiveRecord::Migration
  def change
    add_column :tips, :access_key, :string
    add_index :tips, :access_key
  end
end
