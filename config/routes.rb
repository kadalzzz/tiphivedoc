require 'subdomain'
TipHive::Application.routes.draw do

  resources :action_logs

  post '/tiphive_settings' => 'tiphive_settings#index'
  get '/switch_group' => 'tiphive_settings#switch_domain_setting'
  resources :tiphive_settings do 
    collection do
      post :get_domain
      get :hives
    end
  end

  get 'sidebar/tips'
  get 'sidebar/questions'
  get 'sidebar/my_hives'
  get 'sidebar/my_groups'
  get 'sidebar/my_friends'

  constraints(Subdomain) do
    get '/' => 'domains#home'
  end

  use_doorkeeper do
    controllers :applications => 'oauth/applications'
  end

  devise_for :admin_users, ActiveAdmin::Devise.config
  ActiveAdmin.routes(self)

  namespace :api, defaults: { format: 'json' } do
    namespace :v1 do
      devise_scope :user do
        resources :users
        resources :comments
        resources :groups do
          resources :tips
        end
        resources :hives do
          resources :tips
        end
        resources :pockets do
          resources :tips
        end
        resources :tips
        resources :questions

        post '/sign_in' => 'sessions#create'
        delete '/sign_out' => 'sessions#destroy'
        post '/sign_up' => 'registrations#create'
        get '/me' => 'users#me'
        get '/friends' => 'users#my_friends'

        get '/search' => 'search#index'
        get '/search_mentions' => 'search#mentions'
        get '/search_users' => 'search#users'
        get '/search_groups' => 'search#groups'
        get '/search_invitable_groups' => 'search#invitable_groups'
        get '/search_users_and_groups' => 'search#users_and_groups'
        get '/search_hives' => 'search#hives'
        get '/search_pockets' => 'search#pockets'
        get '/search_tips' => 'search#tips'
        get '/search_connections' => 'search#connections'
        get '/user_following_friends' => 'search#user_following_friends'
        get '/user_following_friends_and_groups' => 'search#user_following_friends_and_groups'
        get '/user_following_groups' => 'search#user_following_groups'
        get '/user_following_hives' => 'search#user_following_hives'
        get '/user_following_pockets' => 'search#user_following_pockets'
        get '/user_following_tips' => 'search#user_following_tips'
        get '/share_type_permission' => 'search#share_type_permission'
        get '/hives_following_users_and_groups' => 'search#hives_following_users_and_groups'
        post '/connect_sharing_objects' => 'home#connect_sharing_objects', as: :connect_sharing_objects
        post '/disconnect_sharing_objects' => 'home#disconnect_sharing_objects', as: :disconnect_sharing_objects
        # use_link_thumbnailer
        resources :pictures
        match '/upload_picture', controller: :pictures, action: :upload, via: [:post, :put, :patch]
        match '/upload_comment_picture', controller: :pictures, action: :upload_comment_picture, via: [:post, :put, :patch]
        match '/upload_file', controller: :files, action: :upload, via: [:post, :put, :patch]
        resources :comments

        resources :files

        get '/validates_invitation_token' => 'invitation_requests#validates_invitation_token', as: :validates_invitation_token
        get '/invitation_request_from_token' => 'invitation_requests#invitation_request_from_token', as: :invitation_request_from_token
        get '/invitation_requests/:id/accept' => 'invitation_requests#accept', as: :accept_invitation_request
        get '/menu_invitation_requests' => 'invitation_requests#menu_invitation_requests'
        resources :invitation_requests

        resources :groups do
          resources :hives
          resources :questions
          member do
            get :leave
            get :join
            get :share
            get :request_invite
            post :request_invite
            get :members
            get :no_access
          end
        end
        get '/groups/:id/members/:initial' => 'groups#members', as: :group_members_by_initial
        get '/domains/:id/members/:initial' => 'domains#members', as: :domain_members_by_initial

        get '/my_groups' => 'groups#my_groups'
        get '/menu_groups' => 'groups#menu_groups'

        resources :tip_links do
          collection do
            get '/fetch' => 'tip_links#fetch'
          end
        end
        resources :tips do
          post '/like' => 'tips#like'
          post '/unlike' => 'tips#unlike'
          get '/get_tip' => 'tips#get_tip'
          post '/flag' => 'tips#flag'
          post '/update_tip_location' => 'tips#update_tip_location'
          collection do
            post '/business' => 'tips#business'
          end
          member do
            get :share
            get :hives_and_pockets
          end
        end

        resources :questions do
          post '/like' => 'questions#like'
          post '/unlike' => 'questions#unlike'
          post '/flag' => 'questions#flag'
          member do
            get :add_existing_tip
            get :add_comment
            get :hives_and_pockets
            get :shared_objects
          end
        end


        resources :pockets do
          resources :questions
          member do
            get :leave
            get :join
            get :share
            get :object_setting_model
            post :object_setting
            put :object_setting
          end
        end
        get '/my_pockets' => 'pockets#my_pockets'

        resources :hives do
          resources :pockets
          resources :questions
          get '/my_pockets' => 'pockets#my_pockets'
          member do
            get :leave
            get :join
            get :share
            get :friends
            post :object_setting
            put :object_setting
          end
          collection do
            get :following_pockets
          end
        end
        get '/my_hives' => 'hives#my_hives'
        get '/menu_hives' => 'hives#menu_hives'

        get '/tips' => 'tips#index', as: :user_root

        resources :users do
          resources :hives
          resources :questions
          member do
            get :friends
          end
        end
        resources :domains do
          member do
            get :members
            get :billing
            get :share
          end
        end

        post '/upload_image' => 'pictures#upload_image'
        post '/upload_background_image' => 'pictures#upload_background_image'
        post '/upload_avatar_image' => 'pictures#upload_avatar_image'
        post '/upload_avatar' => 'users#upload_avatar'
        get '/select_avatar' => 'users#select_avatar'
        get '/email_notifications' => 'users#email_notifications'
        post '/email_notifications' => 'users#email_notifications'
        post '/add_as_friend/:user_id' => 'users#add_as_friend', as: :add_as_friend
        post '/add_as_friends' => 'users#add_as_friends', as: :add_as_friends
        post '/unfriend/:user_id' => 'users#unfriend', as: :unfriend
        get '/status_with/:user_id' => 'users#status_with', as: :status_with

        get '/users/:id' => 'users#show', as: :user_profile
        get '/my_friends/:initial' => 'users#my_friends', as: :my_friends_by_initial
        get '/my_friends' => 'users#my_friends'
        get '/add_friends' => 'users#add_friends'
        post '/add_friends' => 'users#add_friends'
        get '/count_element' => 'users#user_count_element'

        post '/connect_following_objects' => 'home#connect_following_objects', as: :connect_following_objects
        post '/disconnect_following_objects' => 'home#disconnect_following_objects', as: :disconnect_following_objects
        post '/update_background_image_position' => 'home#update_background_image_position'
      end

      get '*unmatched_route', to: 'application#raise_not_found!'
      post '*unmatched_route', to: 'application#raise_not_found!'
      put '*unmatched_route', to: 'application#raise_not_found!'
      delete '*unmatched_route', to: 'application#raise_not_found!'
    end
  end

  get '/search' => 'search#index'
  get '/search_mentions' => 'search#mentions'
  get '/search_users' => 'search#users'
  get '/search_groups' => 'search#groups'
  get '/search_invitable_groups' => 'search#invitable_groups'
  get '/search_users_and_groups' => 'search#users_and_groups'
  get '/search_hives' => 'search#hives'
  get '/search_pockets' => 'search#pockets'
  get '/search_tips' => 'search#tips'
  get '/search_connections' => 'search#connections'
  get '/search_social_friends' =>  'search#social_friends'
  post '/search_social_friends' =>  'search#social_friends'
  get '/user_following_friends' => 'search#user_following_friends'
  get '/user_following_friends_and_groups' => 'search#user_following_friends_and_groups'
  get '/user_following_groups' => 'search#user_following_groups'
  get '/user_following_hives' => 'search#user_following_hives'
  get '/user_following_pockets' => 'search#user_following_pockets'
  get '/user_following_tips' => 'search#user_following_tips'
  get '/share_type_permission' => 'search#share_type_permission'
  get '/hives_following_users_and_groups' => 'search#hives_following_users_and_groups'
  post '/connect_sharing_objects' => 'home#connect_sharing_objects', as: :connect_sharing_objects
  post '/disconnect_sharing_objects' => 'home#disconnect_sharing_objects', as: :disconnect_sharing_objects
  # use_link_thumbnailer
  resources :pictures do
    collection do
      post 'tag'
    end
    member do
      post 'remove_tag', :as => :remove_tag
    end
  end
  match '/upload_picture', controller: :pictures, action: :upload, via: [:post, :put, :patch]
  match '/upload_comment_picture', controller: :pictures, action: :upload_comment_picture, via: [:post, :put, :patch]

  resources :files
  match '/upload_file', controller: :files, action: :upload, via: [:post, :put, :patch]
  resources :comments

  get '/menu_friend_invitations' => 'friend_invitations#menu_friend_invitations'
  resources :friend_invitations

  get '/invitation_requests/:id/accept' => 'invitation_requests#accept', as: :accept_invitation_request
  get '/menu_invitation_requests' => 'invitation_requests#menu_invitation_requests'
  resources :invitation_requests

  resources :groups do
    resources :hives
    resources :questions
    member do
      get :leave
      get :join
      get :share
      get :request_invite
      post :request_invite
      get :members
      get :no_access
    end
  end
  get '/groups/:id/members/:initial' => 'groups#members', as: :group_members_by_initial
  get '/domains/:id/members/:initial' => 'domains#members', as: :domain_members_by_initial
  get '/meet_people/:initial' => 'users#meet_people', as: :meet_people_by_initial
  get '/my_groups' => 'groups#my_groups'
  get '/menu_groups' => 'groups#menu_groups'

  resources :tip_links do
    collection do
      get '/fetch' => 'tip_links#fetch'
    end
  end
  resources :tips do
    post '/like' => 'tips#like'
    post '/unlike' => 'tips#unlike'
    get '/get_tip' => 'tips#get_tip'
    post '/flag' => 'tips#flag'
    post '/update_tip_location' => 'tips#update_tip_location'
    collection do
      post '/business' => 'tips#business'
    end
    member do
      get :share
      get :hives_and_pockets
    end
  end

  resources :questions do
    post '/like' => 'questions#like'
    post '/unlike' => 'questions#unlike'
    post '/flag' => 'questions#flag'
    member do
      get :add_existing_tip
      get :add_comment
      get :hives_and_pockets
    end
  end


  resources :pockets do
    resources :questions
    member do
      get :leave
      get :join
      get :share
      get :object_setting_model
      post :object_setting
      put :object_setting
    end
  end
  get '/my_pockets' => 'pockets#my_pockets'
  get '/menu_pockets' => 'pockets#menu_pockets'

  resources :hives do
    resources :pockets
    resources :questions
    resources :users
    get '/my_pockets' => 'pockets#my_pockets'
    member do
      get :leave
      get :join
      get :share
      get :friends
      get :object_setting_model
      post :object_setting
      put :object_setting
    end
    collection do
      get :following_pockets
    end
  end
  get '/my_hives' => 'hives#my_hives'
  get '/menu_hives' => 'hives#menu_hives'

  get '/tips' => 'tips#index', as: :user_root

  devise_for :users, :controllers => {
    :invitations => 'users/invitations',
    :omniauth_callbacks => 'users/omniauth_callbacks',
    :registrations => 'users/registrations',
    :sessions => 'users/sessions'
  }

  post '/system_invitation' => 'users#system_invitation'
  resources :users do
    resources :hives
    resources :questions
    member do
      get :settings
      post 'settings' => 'users#update_permission'
      get :groups
      post :add_group
      post :remove_group
      get :friends
    end
  end
  resources :domains do
    member do
      get :members
      get :billing
      get :share
      post :upload_image
      patch :upload_image
      put :upload_image
    end
  end

  resources :emails do
    member do
      get :primary
    end
  end

  post '/upload_image' => 'pictures#upload_image'
  post '/upload_background_image' => 'pictures#upload_background_image'
  post '/upload_avatar_image' => 'pictures#upload_avatar_image'
  post '/upload_avatar' => 'users#upload_avatar'
  get '/select_avatar' => 'users#select_avatar'
  get '/email_notifications' => 'users#email_notifications'
  post '/email_notifications' => 'users#email_notifications'
  post '/add_as_friend/:user_id' => 'users#add_as_friend', as: :add_as_friend
  post '/add_as_friends' => 'users#add_as_friends'
  post '/unfriend/:user_id' => 'users#unfriend', as: :unfriend
  get '/status_with/:user_id' => 'users#status_with', as: :status_with

  get '/users/:id' => 'users#show', as: :user_profile
  get '/my_friends/:initial' => 'users#my_friends', as: :my_friends_by_initial
  get '/my_friends' => 'users#my_friends'
  get '/people' => 'users#people'
  get '/my_friend_waiting_response' => 'users#my_friend_waiting_response'
  get '/add_friends' => 'users#add_friends'
  post '/add_friends' => 'users#add_friends'
  get 'meet_people' => 'users#meet_people'

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'

  resources :dashboard, only: [:index] do
    collection do
      get :domains
      get :emails
    end
  end

  get '/debug_object/:object_kind_and_id' => 'home#debug_object'
  post '/connect_following_objects' => 'home#connect_following_objects', as: :connect_following_objects
  post '/disconnect_following_objects' => 'home#disconnect_following_objects', as: :disconnect_following_objects
  post '/update_background_image_position' => 'home#update_background_image_position'
  get '/internal_server_error' => 'home#internal_server_error'
  get '/not_found' => 'home#not_found'
  get '/intended_error' => 'home#intended_error'
  get '/tiphive_protocol_redirect' => 'home#tiphive_protocol_redirect'

  get '*unmatched_route', to: 'application#raise_not_found!'
  post '*unmatched_route', to: 'application#raise_not_found!'
  put '*unmatched_route', to: 'application#raise_not_found!'
  delete '*unmatched_route', to: 'application#raise_not_found!'

  authenticated :user do
    root to: 'tips#index', as: :authenticated_root
  end

  root to: 'home#index'
end
