json.id domain.id
json.full_name domain.full_name
json.active domain.active
json.logo_url domain.logo.url
json.background_url domain.background.url
