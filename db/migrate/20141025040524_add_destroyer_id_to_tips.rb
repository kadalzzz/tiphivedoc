class AddDestroyerIdToTips < ActiveRecord::Migration
  def change
    add_column :tips, :destroyer_id, :integer
  end
end
