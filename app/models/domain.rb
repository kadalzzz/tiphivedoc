class Domain < ActiveRecord::Base
  acts_as_followable
  acts_as_follower

  include DomainObserver

  # Constants
  RESERVED_DOMAINS = %w(
    about
    api
    assets
    beta
    cdn
    ci
    db
    dev
    development
    enterprise
    errors
    newprod
    prod
    production
    staging
    team
    tiphive
    www
  )

  # Assocations
  has_many :comments
  has_many :friend_invitations, inverse_of: :domain
  has_many :groups
  has_many :hives
  has_many :pictures
  has_many :pockets
  has_many :questions
  has_many :tips
  belongs_to :user

  mount_uploader :logo, LogoUploader
  mount_uploader :background, BackgroundImageUploader

  # Validations
  validates :name, presence: true, uniqueness: true,
            exclusion:       { in: RESERVED_DOMAINS, message: '%{value} is reserved.' }

  # Callbacks
  before_validation :parameterize_name
  before_create :activate

  # Scopes
  scope :active, -> { where(active: true) }

  alias_attribute :title, :name

  def full_name
    %w(staging beta).include?(Rails.env) ? "#{name}.#{Rails.env}.tiphive.com" : "#{name}.tiphive.com"
  end

  def root?
    Domain::RESERVED_DOMAINS.include?(name) || !name.present?
  end

  def logo_path
    root? ? 'logo.png' : logo.thumb.url
  end

  def self.current
    Thread.current[:domain] || Domain.root
  end

  def self.current=(domain)
    Thread.current[:domain] = domain
  end

  def to_param
    "#{id} #{name}".parameterize
  end

  def users_count(date = Time.now)
    @users_count ||= user_followers.where('follows.created_at < ?', date).count
  end

  def members
    user_followers.where('users.id != ?', user.id)
  end

  def status_with(member)
    return :is_me if user_id == member.id
    is_followed = member.following?(self)
    return :is_member if is_followed
    return :is_nothing if !is_followed
  end

  def price
    return 0.0 if @users_count.nil? || @users_count.zero?
    if @users_count < 100
      100
    elsif @users_count.between?(100, 4999)
      @users_count
    elsif @users_count.between?(5000, 9999)
      0.9 * @users_count
    else
      0.5 * @users_count
    end
  end

  private

  def parameterize_name
    return 'unnamed' if name.blank?
    self.name = name.parameterize
  end

  def activate
    self.active = true
  end

  class << self
    def root
      @root ||= new(active: true).freeze
    end
  end
end
