require 'fileutils'
@current_dir = __dir__

def convert_assets(dir)
  parent_path = "#{@current_dir}/#{dir}"
  id_dirs = []
  Dir.foreach(parent_path).each do |id_dir|
    next if %w(. ..).include?(id_dir)
    next if id_dir.to_i == 0

    id_dirs << id_dir
  end if Dir.exist?(parent_path)

  id_dirs.each do |id_dir|
    id_path = "#{parent_path}/#{id_dir}"
    original_dir = "#{id_path}/original"

    Dir.foreach(original_dir).each do |original_file|
      next if %w(. ..).include?(original_file)
      file = "#{original_dir}/#{original_file}"

      if file =~ /\.jpg|\.jpeg|\.png|\.gif/i && !File.directory?(file)
        puts "- Moving #{file} -> #{id_path}"
        FileUtils.mv(file, id_path, :force => true)
      else
        puts "*** Removing these invalid file: #{id_dir} | #{file}"
        FileUtils.rm(file, :force => true) unless File.directory?(file)
        FileUtils.remove_dir(file, true) if File.directory?(file)
      end
    end if Dir.exist?(original_dir)

    Dir.foreach(id_path).each do |id_file|
      next if %w(. ..).include?(id_file)

      if id_file == 'original'
        original_files_count = 0
        Dir.foreach(original_dir).each do |original_file|
          next if %w(. ..).include?(original_file)
          original_files_count += 1
        end if Dir.exist?(original_dir)

        next if original_files_count > 0
      end

      file_path = "#{id_path}/#{id_file}"
      if File.directory?(file_path)
        puts "*** Removing unused folders: #{file_path}"
        FileUtils.remove_dir(file_path, true)
      end
    end if Dir.exist?(id_path)

  end

end

convert_assets('avatar')
convert_assets('image')
