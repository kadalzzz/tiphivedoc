class CreateSharingTypes < ActiveRecord::Migration
  def up
    create_table :sharing_types do |t|
      t.string :sharing_object_type
      t.integer :sharing_object_id
      t.string :sharing_type, default: 'all_friends'
      t.integer :user_id

      t.timestamps
    end

    add_index :sharing_types, :sharing_object_type
    add_index :sharing_types, :sharing_object_id
    add_index :sharing_types, :user_id
  end

  def down
    remove_column :sharing_types, :user_id
    remove_column :sharing_types, :sharing_object_type
    remove_column :sharing_types, :sharing_object_id
    remove_column :sharing_types, :sharing_type
  end
end
