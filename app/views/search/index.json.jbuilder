json.array!(@results) do |result|
  if result.class.name == 'User'
    json.extract! result, :id, :name, :email, :username
  else
    if result.class.name == 'Question'
      json.extract! result, :id, :name
    else
      json.extract! result, :id, :title
    end
  end

  json.url user_profile_url(result) if result.class.name == 'User'
  json.url group_url(result) if result.class.name == 'Group'
  json.url hive_url(result) if result.class.name == 'Hive'
  json.url pocket_url(result) if result.class.name == 'Pocket'
  json.url tip_url(result) if result.class.name == 'Tip'
  json.url question_url(result) if result.class.name == 'Question'

  json.class_name result.class.name
  json.kind_id "#{result.class.name}-#{result.id}"
  if %w(Group Hive Pocket).include?(result.class.name) && result.background_image.present?
    json.thumb_url result.background_image.small.url
  end
  if ['Group'].include?(result.class.name) && result.background_image.present?
    json.background_image_url result.background_image.small.url
  end
  if %w(Group Hive Pocket).include?(result.class.name) && result.background_image.blank?
    json.thumb_url image_path('pockets-no-images.png')
  end
  if ['Group'].include?(result.class.name) && result.background_image.blank?
    json.background_image_url image_path('pockets-no-images.png')
  end
  json.thumb_url image_path('pockets-no-images.png') if result.class.name == 'Question'
  json.thumb_url result.pictures.first.image.small.url if result.class.name == 'Tip' && result.pictures.count > 0
  json.thumb_url image_path('pockets-no-images.png') if result.class.name == 'Tip' && result.pictures.count == 0
  json.thumb_url result.avatar.thumb.url if result.class.name == 'User'

  if result.class.name == 'User'
    json.value result.name
    json.name result.name
    json.description "#{result.tips.count} tips"
    json.slug result.username
  else
    user = result.user
    json.owner_name user.try :name
    json.owner_thumb_url user && user.avatar.present? ? url_for(user.avatar.thumb.url) : ''
    json.value result.title if result.try(:title)
    json.value result.name if result.try(:name)
    json.name result.title if result.try(:title)
    json.slug result.to_param
    json.description truncate(decode_helper(result.description).html_safe, length: 40) if result.try(:description)
    json.full_description decode_helper(result.description).html_safe if result.try(:description)
  end

  if result.class.name == 'Tip'
    if result.pictures.count > 0
      json.tip_pictures tip_pictures_url(result.pictures)
    end
    json.extra_description "#{hives_title(result)} / #{pockets_title(result)}"
  elsif result.class.name == 'Pocket'
    json.extra_description hives_title(result)
  else
    json.extra_description ''
  end
end
