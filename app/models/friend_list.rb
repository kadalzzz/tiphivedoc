class FriendList < ActiveRecord::Base
  acts_as_follower
  acts_as_followable

  include TipHiveObject
  include ActsAsSharing
  validates :name, uniqueness: true

  # Shashi: assign current_domain to user model
  cattr_accessor :current_domain

  def update_object_sharing_status_true(item)
    return if item.blank?
    case name
    when 'Public' then
      item.update_attributes(is_public: true)
    when 'All Friends' then
      item.update_attributes(shared_all_friends: true)
    when 'All Domain Members' then
      item.update_attributes(shared_all_friends: true)
    end
  end

  def update_object_sharing_status_false(item)
    return if item.blank?
    case name
    when 'Public' then
      item.update_attributes(is_public: false)
    when 'All Friends' then
      item.update_attributes(shared_all_friends: false)
    when 'All Domain Members' then
      item.update_attributes(shared_all_friends: false)
    end
  end

  def name
    display_name = super
    if current_domain && !current_domain.root?
      return 'All Domain Members' if display_name == 'All Friends'
    end
    display_name
  end
end
