pocket ||= @pocket

json.response do
  json.partial! 'api/v1/pockets/pocket', pocket: pocket
end

json.status :success
