domain ||= @domain

json.response do
  json.partial! 'api/v1/domains/domain', domain: domain
end

json.status :success
