class EmailsController < ApplicationController
  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!

  def create
    email = current_user.emails.new(address: params[:email])
    email.save ? flash[:notice] = 'Email added.' : flash[:alert] = email.errors.full_messages.first
  end

  def destroy
    email = current_user.emails.find(params[:id])
    email.destroy
    redirect_to edit_user_registration_path, notice: 'Email removed.'
  end

  def primary
    old_email          = User.where(id: current_user.id).first.email
    email              = current_user.emails.find(params[:id])
    current_user.email = email.address
    email.address      = old_email
    current_user.save
    email.save
    redirect_to edit_user_registration_path, notice: 'Primary email changed.'
  end
end
