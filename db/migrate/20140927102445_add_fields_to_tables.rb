class AddFieldsToTables < ActiveRecord::Migration
  def up
    #hives
  	change_column :hives, :is_public, :boolean, default: false
    add_column :hives, :is_private, :boolean, default: false
    add_column :hives, :shared_all_friends, :boolean, default: true
    add_column :hives, :shared_select_friends, :boolean, default: false
    #pockets
    change_column :pockets, :is_public, :boolean, default: false
    add_column :pockets, :is_private, :boolean, default: false
    add_column :pockets, :shared_all_friends, :boolean, default: true
    add_column :pockets, :shared_select_friends, :boolean, default: false
    #tips
    change_column :tips, :is_public, :boolean, default: false
    add_column :tips, :is_private, :boolean, default: false
    add_column :tips, :shared_all_friends, :boolean, default: true
    add_column :tips, :shared_select_friends, :boolean, default: false
    #questions
    change_column :questions, :is_public, :boolean, default: false
    add_column :questions, :is_private, :boolean, default: false
    add_column :questions, :shared_all_friends, :boolean, default: true
    add_column :questions, :shared_select_friends, :boolean, default: false
    #object_settings
    rename_column :object_settings, :is_public, :is_private
    change_column :object_settings, :is_private, :boolean, default: false
  end

  def down
     #hives
    remove_column :hives, :is_private
    remove_column :hives, :shared_all_friends
    remove_column :hives, :shared_select_friends
    #pockets
    remove_column :pockets, :is_private
    remove_column :pockets, :shared_all_friends
    remove_column :pockets, :shared_select_friends
    #tips
    remove_column :tips, :is_private
    remove_column :tips, :shared_all_friends
    remove_column :tips, :shared_select_friends
    #questions
    remove_column :questions, :is_private
    remove_column :questions, :shared_all_friends
    remove_column :questions, :shared_select_friends
  end
end
