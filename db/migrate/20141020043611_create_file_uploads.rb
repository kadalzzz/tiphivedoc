class CreateFileUploads < ActiveRecord::Migration
  def change
    create_table :file_uploads do |t|
      t.integer :user_id
      t.string :title
      t.string :file
      t.integer :fileable_id
      t.string :fileable_type
      t.integer :domain_id
      t.timestamps
    end

    add_index :file_uploads, :fileable_id
    add_index :file_uploads, :fileable_type
    add_index :file_uploads, :user_id
    add_index :file_uploads, :domain_id
  end
end
