module NavigationHelpers
  # Maps a name to a path. Used by the
  #
  #   When /^I go to (.+)$/ do |page_name|
  #
  # step definition in web_steps.rb
  #
  def path_to(page_name)
    case page_name

    when /the home\s?page/
      '/'
    when /the tipfeed page/
      '/tips'
    when /the questions page/
      '/questions'
    when /the sign in page/
      '/users/sign_in'
    when /add hive page/
      '/hives/new'
    when /add pocket page/
      '/pockets/new'
    when /add pocket page with first hive/
      '/pockets/new?hives[]=Hive-1'
    when /add tip page/
      '/tips/new'
    when /add tip page with first hive and first pocket/
      '/tips/new?hives[]=Hive-1&pockets[]=Pocket-1'
    when /add question page with first hive and first pocket/
      '/questions/new?hives[]=Hive-1&pockets[]=Pocket-1'
    when /add group page/
      '/groups/new'
    when /my hives page/
      '/my_hives'
    when /my groups page/
      '/my_groups'
    when /first group page/
      '/groups/1'
    when /first hive page/
      '/hives/1'
    when /first hive edit page/
      '/hives/1/edit'
    when /first pocket page/
      '/pockets/1'
    when /first tip page/
      '/tips/1'
    when /first user page/
      '/users/1'

    # the following are examples using path_to_pickle

    when /^#{capture_model}(?:'s)? page$/                           # eg. the forum's page
      path_to_pickle $1

    when /^#{capture_model}(?:'s)? #{capture_model}(?:'s)? page$/   # eg. the forum's post's page
      path_to_pickle $1, $2

    when /^#{capture_model}(?:'s)? #{capture_model}'s (.+?) page$/  # eg. the forum's post's comments page
      path_to_pickle $1, $2, extra: $3 #  or the forum's post's edit page

    when /^#{capture_model}(?:'s)? (.+?) page$/                     # eg. the forum's posts page
      path_to_pickle $1, extra: $2 #  or the forum's edit page

    # Add more mappings here.
    # Here is an example that pulls values out of the Regexp:
    #
    #   when /^(.*)'s profile page$/i
    #     user_profile_path(User.find_by_login($1))

    else
      begin
        page_name =~ /the (.*) page/
        path_components = $1.split(/\s+/)
        send(path_components.push('path').join('_').to_sym)
      rescue Object => _e
        raise "Can't find mapping from \"#{page_name}\" to a path.\n" +
          "Now, go and add a mapping in #{__FILE__}"
      end
    end
  end
end

World(NavigationHelpers)
