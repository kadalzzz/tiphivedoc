class Users::OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def facebook
    client = OAuth2::Client.new(FACEBOOK_APP_ID, FACEBOOK_APP_SECRET, site: 'https://graph.facebook.com')
    process_callback(request.env['omniauth.auth'], request.env['omniauth.params'], 'facebook', client)
  end

  def twitter
    client = OAuth2::Client.new(TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET, site: 'https://www.twitter.com')
    process_callback(request.env['omniauth.auth'], request.env['omniauth.params'], 'twitter', client)
  end

  def google_oauth2
    client = OAuth2::Client.new(GOOGLE_CLIENT_ID, GOOGLE_CLIENT_SECRET, site: 'https://www.google.com')
    process_callback(request.env['omniauth.auth'], request.env['omniauth.params'], 'google_oauth2', client)
  end

  def failure
    logger.info "*** OmniAuth failure: #{failure_message}"
    set_flash_message :alert, :failure, kind: OmniAuth::Utils.camelize(failed_strategy.name), reason: failure_message
    redirect_to after_omniauth_failure_path_for(resource_name)
  end

  protected

  def after_omniauth_failure_path_for(scope)
    # new_session_path(scope)
    root_path
  end

  private
  def process_callback(auth, auth_params, kind, client = nil)
    clear_identity_sessions
    session["devise.#{kind}_data"]   = auth
    session["devise.#{kind}_params"] = auth_params

    logger.info "*** process_callback: #{auth_params} "

    @identity = Identity.find_with_omniauth(auth)
    @user     = @identity.user

    if @identity.user.blank? && user_signed_in?
      @identity.user = current_user
    end

    @identity.access_token = auth.credentials.token
    @identity.save

    @token = OAuth2::AccessToken.new client, @identity.access_token

    redirect_to new_user_session_path if auth_params.blank? || auth_params['from'].blank?

    if auth_params['from'] == 'sign_up'
      process_sign_up(@user, kind, auth_params)
    end

    if auth_params['from'] == 'sign_in'
      process_sign_in(@user, kind)
    end

    if auth_params['from'] == 'add_friends'
      process_add_friends(kind)
    end

    if auth_params['from'] == 'select_avatar'
      process_select_avatar(auth, kind)
    end

  end

  private
  def process_sign_up(user, kind, auth_params)
    if user.present? && user.persisted?
      sign_in_and_redirect user, :event => :authentication #this will throw if @user is not activated
      set_flash_message(:notice, :success, :kind => kind.titleize) if is_navigational_format?
    else
      redirect_to new_user_registration_path(inviter_id: auth_params['inviter_id'],
                                             token:      auth_params['invitation_token'])
    end
  end

  def process_sign_in(user, kind)
    if user.present? && user.persisted?
      sign_in_and_redirect user, event: :authentication # this will throw if @user is not activated
      set_flash_message(:notice, :success, kind: kind.titleize) if is_navigational_format?
    else
      redirect_to new_user_session_path
    end
  end

  def process_add_friends(kind)
    logger.info "*** process_add_friends: #{kind}"

    if kind == 'facebook'
      response  = @token.get('/me/friends')
      json_data = JSON.parse(response.body)

      json_data['data'].each do |key, _value|
        identity = current_user.social_friends.where(uid: key['id'], provider: 'facebook').first
        if identity.blank?
          response  = @token.get("/#{key['id']}?fields=username")
          json_data = JSON.parse(response.body)
          current_user.social_friends.create(provider: 'facebook', name: key['name'],
                                             uid:      key['id'], username: json_data['username'])
        end
      end
    end

    if kind == 'google_oauth2'
      response   = @token.get('/m8/feeds/contacts/default/full?alt=json')
      @json_data = JSON.parse(response.body)
      @json_data['feed']['entry'].each do |contact|
        identity = current_user.social_friends.where(email:    contact['gd$email'][0]['address'],
                                                     provider: 'google_oauth2').first
        if identity.blank?
          current_user.social_friends.create(provider: 'google_oauth2', email: contact['gd$email'][0]['address'])
        end
      end
    end

    if user_signed_in?
      clear_identity_sessions
      redirect_to add_friends_path(auth: kind)
    else
      redirect_to new_user_session_path
    end
  end

  def process_select_avatar(auth, kind)
    if user_signed_in?
      image_url = auth.info.image
      image_url = "#{auth.info.image}?type=large" if kind == 'facebook'
      image_url = auth.info.image.gsub('_normal', '') if kind == 'twitter'
      logger.info "*** process_select_avatar - image_url: #{image_url}"
      image_response = HTTParty.get image_url, follow_redirects: false

      logger.info "*** image_response.headers: #{image_response.headers.to_yaml}"

      if image_response.code == 302 || image_response.code == 404
        image_url = image_response.headers['location']
        logger.info "*** process_select_avatar - image_url: #{image_url}"
      end

      logger.info "*** process_select_avatar final image url: #{image_url}"
      current_user.remote_avatar_url = image_url if image_url.present?
      current_user.save
      error = current_user.errors.full_messages.last

      if error.present?
        logger.info "*** Error: #{error}"
        flash['error'] = error
      end

      redirect_to select_avatar_path
    else
      redirect_to new_user_session_path
    end
  end

  def clear_identity_sessions
    session['devise.facebook_data']      = nil
    session['devise.twitter_data']       = nil
    session['devise.google_oauth2_data'] = nil

    session['devise.facebook_params']      = nil
    session['devise.twitter_params']       = nil
    session['devise.google_oauth2_params'] = nil
  end
end
