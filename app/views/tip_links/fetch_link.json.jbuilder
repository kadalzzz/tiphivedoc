tip_link ||= @tip_link

json.id tip_link.id
json.url tip_link.url
json.description tip_link.description
json.image_url tip_link.image_url
json.tip_id tip_link.tip_id
json.user_id tip_link.user_id
