class CreateTipLinks < ActiveRecord::Migration
  def change
    create_table :tip_links do |t|
      t.string :url, null: false
      t.string :title
      t.text :description
      t.string :image_url
      t.string :avatar
      t.integer :tip_id
      t.integer :user_id

      t.timestamps
    end

    add_index :tip_links, :tip_id
    add_index :tip_links, :user_id
  end
end
