first_hive   = tip.hives.first
first_pocket = tip.pockets.first
user         = tip.user

json.id tip.id
json.title tip.title
# json.hives_and_pockets_title tip.hives_and_pockets_title
# json.hives tip.hives.map {|hive| {id: hive.id, title: hive.title} }
# json.pockets tip.pockets.map {|pocket| {id: pocket.id, title: pocket.title} }
json.first_hive do
  json.id first_hive.try(:id)
  json.title first_hive.try(:title)
end

json.first_pocket do
  json.id first_pocket.try(:id)
  json.title first_pocket.try(:title)
end

json.description tip.description
json.links URI.extract(tip.description, %w(http https))
json.address tip.address
json.location tip.location
json.latitude tip.latitude
json.longitude tip.longitude
json.pictures tip.pictures.map do |picture|
  {
    id:             picture.id,
    title:          picture.title,
    image_url:      picture.image.url,
    # image: picture.image,
    imageable_id:   picture.imageable_id,
    imageable_type: picture.imageable_type
  }
end
json.pictures_count tip.pictures_count
json.comments_count tip.comments_count
# json.cached_votes_total tip.cached_votes_total
json.likes_count tip.cached_votes_up
# json.cached_votes_down tip.cached_votes_down
json.sharing_type tip.sharing_type
json.question_id tip.question_id
json.user_id tip.user_id
json.owner_name user.try(:name)
json.owner_thumb_url user && user.avatar.present? ? user.avatar.thumb.url : ''
json.like current_user.liked?(tip)
