module ObjectSettingObserver
  extend ActiveSupport::Concern

  included do
    has_many :object_settings, as: :object_setting, dependent: :destroy
  end

  def create_settings_object(user)
    object_params = {
      user_id:          user.id,
      is_private:       is_private,
      background_image: get_object_image(self)
    }
    object_settings.find_or_create_by(object_params)
  end

  # update hive permission in ObjectSettings
  def update_object_setting_recorded(user, image)
    object = find_object_settings(user)
    object.blank? ? create_settings_object(user) : object.update!(is_private: is_private, background_image: image)
  end

  # create or update hive object setting
  def create_or_update_object_setting(user, params)
    object = find_object_settings(user)
    if object.blank?
      object_params = {
        user_id:          user.id,
        is_private:       params['is_private'],
        background_image: get_object_image(self)
      }
      object_settings.find_or_create_by(object_params)
    else
      if user.id == self.user.id
        object.update!(params)
        self.is_private = object.is_private
        self.save!
      else
        object.update!(params)
      end
    end
  end

  # find sharing type object
  def find_object_settings(user)
    object_settings.where('user_id = ?', user.id).first
  end

  def self.search_object_setting(user_id, hive_id)
    where('user_id = ? and object_setting_id = ?', user_id, hive_id).first
  end

  # destroy object_setting and all share record if user leave Hive
  def destroy_object_setting_and_share_record(user)
    return if user.blank?
    shares = []
    object = object_settings.where('user_id=?', user.id).first
    object.destroy unless object.blank?
    shares << self.shares.where('user_id=?', user.id)
    shares << self.shares.where(sharing_object_type: 'User', sharing_object_id: user.id)
    return if shares.blank?
    shares.flatten.each(&:destroy)
  end

  protected

  def get_object_image(obj)
    obj.background_image.url
  end
end
