ActiveAdmin.register Group do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  config.sort_order = "id_desc"
  config.per_page = 10

  controller do
    def scoped_collection
      resource_class.includes(:user) # prevents N+1 queries to database
    end
  end

  index do
    selectable_column
    column :id, sortable: :id do |group|
      link_to group.id, admin_group_path(group)
    end
    column :title
    column :description
    column :join_type
    column :background_image do |group|
      image_tag group.background_image, height: 50
    end
    column :group_type
    column :address
    column :location
    column :zip
    column :user do |group|
      user = group.user
      link_to user.name, admin_user_path(user) if user.present?
    end
    column :created_at
    actions
  end

  filter :title
  filter :description
  filter :group_type
  filter :address
  filter :location
  filter :zip
  filter :user
  filter :created_at

end
