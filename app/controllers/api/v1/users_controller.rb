class Api::V1::UsersController < UsersController
  def me
    @user = current_user
  end

  def show
    super
    @following_status = current_user.status_with(@user).to_s
  end

  def my_friends
    if params[:initial].present?
      @friends = current_user.following_users.where("first_name LIKE '#{params[:initial]}%'")
                   .order(:first_name)
                   .page(params[:page])
                   .per(params[:per])
    else
      @friends = current_user.following_users.order(:first_name).page(params[:page]).per(params[:per])
    end
  end
end
