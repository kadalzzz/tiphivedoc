class AddMoreIndexToTables < ActiveRecord::Migration
  def change
    add_index :hives, :title
    add_index :groups, :title
    add_index :pockets, :title
    add_index :tips, :title
  end
end
