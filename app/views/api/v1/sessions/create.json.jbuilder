user ||= @user

json.response do
  json.partial! 'api/v1/users/user', user: user
  json.authentication_token user.authentication_token
end

json.status :success
