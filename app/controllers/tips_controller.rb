class TipsController < ApplicationController
  include TipHiveHelper
  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!
  before_action :set_tip, only: [:show, :edit, :update, :destroy, :hives_and_pockets]

  # GET /tips
  # GET /tips.json
  def index
    add_breadcrumb 'My Hives', my_hives_path, data: { remote: true }, class: 'active-transition'

    if params[:group_id].present?
      @group = Group.find params[:group_id]
      @tips  = current_user.fetch_tips(@group, params)
    elsif params[:hive_id].present?
      @hive = Hive.find params[:hive_id]
      @tips = current_user.fetch_tips(@hive, params)
    elsif params[:pocket_id].present?
      @pocket = Pocket.find params[:pocket_id]
      @tips   = current_user.fetch_tips(@pocket, params)
    elsif params[:question_id].present?
      @question = Question.find params[:question_id]
      @tips     = @question.tip_followers.order('created_at DESC').page(params[:page]).per(params[:per])
    elsif params[:all_mine].present?
      @tips = current_user.tips.order('created_at DESC').page(params[:page]).per(params[:per])
    elsif params[:user_id].present?
      @user = User.find(params[:user_id])
      @tips = current_user.fetch_tips(@user, params)
    else
      @tips = current_user.fetch_tips(nil, params) # nil object for tipfeed
      @questions_count = current_user.get_total_user_questions(current_user)
      @tips_count = current_user.get_tipfeed_tips(current_user).count
      @friends_count = current_user.friends.reject { |r| current_user.status_with(r) == :is_following }.count
    end
    @hives = current_user.hives.limit(3)

    if Rails.env.production?
      @mixpanel.track 'Tip Feed', id: current_user.id, title: 'Tip Feed', distinct_id: current_user.id
    end
  end

  # GET /tips/1
  # GET /tips/1.json
  def show
    @is_comment = params[:comment]
    if Rails.env.production?
      @mixpanel.track 'Tip Viewed', id: @tip.id, title: @tip.title, distinct_id: current_user.id
    end
  end

  # GET /tips/1/hives_and_pockets
  # GET /tips/1/hives_and_pockets.json
  def hives_and_pockets
  end

  # GET /tips/new
  def new
    @tip              = current_user.tips.new
    @tip.question     = Question.find_by_id(params[:question_id])
    @connected_groups = current_user.following_groups.order(:title)
    @selected_hives   = collect_items(items_list: params[:hives])
    @selected_hives.each do |hive|
      current_user.follow hive
    end
    @connected_hives  = current_user.following_hives.order(:title)
    @selected_pockets = collect_items(items_list: params[:pockets])
    @selected_pockets.each do |pocket|
      current_user.follow pocket
    end
    @selected_hives    = @selected_pockets.first.hives if @selected_hives.blank? && !@selected_pockets.blank?
    @connected_pockets = current_user.following_pockets
    if @selected_hives.present? && @selected_pockets.blank?
      @connected_pockets = @selected_hives.last.pocket_followers
    end
    @connected_friends_and_groups = []
    if params[:groups]
      @connected_friends_and_groups = @selected_friends_and_groups = collect_items(items_list: params[:groups])
    end
  end

  # GET /tips/1/edit
  def edit
    @tip                          = current_user.tips.find(params[:id])
    @connected_groups             = current_user.following_groups.order(:title)
    @connected_hives              = current_user.following_hives.order(:title)
    @selected_hives               = @tip.following_hives
    @connected_pockets            = current_user.following_pockets
    @selected_pockets             = @tip.following_pockets
    @connected_friends_and_groups = @selected_friends_and_groups =
      @tip.users_and_groups_and_friend_lists - [current_user]

    logger.info "*** @selected_friends_and_groups: #{@selected_friends_and_groups.inspect}"
  end

  def like
    @tip = Tip.find(params[:tip_id])
    current_user.likes @tip
    @tip.notify_tip_liked current_user if @tip.user.friend_ids.include?(current_user.id)
    respond_to do |format|
      format.js {}
      format.json { render json: { response: { message: 'Tip liked', status: :success } } }
    end
  end

  def unlike
    @tip = Tip.find(params[:tip_id])
    current_user.unlike @tip
    respond_to do |format|
      format.js {}
      format.json { render json: { response: { message: 'Tip unliked', status: :success } } }
    end
  end

  def flag
    @tip = Tip.find(params[:tip_id])
    @tip.create_flagable_recorded(current_user, params[:reason])
    @tip.flag_a_tip(current_user)
    respond_to do |format|
      format.js {}
      format.json { render json: { response: {}, message: 'Tip flagged', status: 'success' } }
    end
  end

  # POST /tips
  # POST /tips.json
  def create
    @tip = current_user.tips.new(tip_params)

    picture_ids = []
    tip_params[:images].each do |image|
      picture = Picture.create image: image
      picture_ids << picture.id.to_s if picture
    end if tip_params[:images].present?
    @tip.picture_ids             = picture_ids.join(',') if picture_ids.present?

    @question                    = Question.find_by_id(params[:tip][:question_id]) if params[:tip][:question_id]
    @connected_groups            = current_user.following_groups.order(:title)
    @connected_hives             = current_user.following_hives.order(:title)
    @selected_hives              = collect_items(items_list: params[:hives])
    @connected_pockets           = current_user.following_pockets
    @selected_pockets            = collect_items(items_list: params[:pockets])
    @selected_friends_and_groups = collect_items(items_list: params[:select_friends_and_groups])

    # Allow API client to pass array of new hive names
    params[:new_hives].each do |new_hive_title|
      next if new_hive_title.blank?
      new_hive = current_user.hives.where(title: new_hive_title).first_or_create
      current_user.follow new_hive
      @selected_hives << new_hive
    end if params[:new_hives].present?

    # Allow API client to pass array of new pocket names
    params[:new_pockets].each do |new_pocket_title|
      next if new_pocket_title.blank?
      new_pocket = current_user.pockets.create(title: new_pocket_title)
      current_user.follow new_pocket
      @selected_pockets << new_pocket
    end if params[:new_pockets].present?

    # API: allow passing direct parameters
    @tip.is_public          = params[:is_public] if %w(true false).include?(params[:is_public])
    @tip.is_private         = params[:is_private] if %w(true false).include?(params[:is_private])
    @tip.shared_all_friends = params[:shared_all_friends] if %w(true false).include?(params[:shared_all_friends])

    @group_id  = params[:tip][:group_id]
    @hive_id   = params[:tip][:hive_id]
    @pocket_id = params[:tip][:pocket_id]

    respond_to do |format|
      if @tip.save
        current_user.follow @tip
        @selected_pockets.each do |selected_pocket|
          @selected_hives.each do |selected_hive|
            selected_pocket.follow selected_hive
          end
          @tip.follow selected_pocket
          selected_pocket.following_hives.each do |following_hive|
            @tip.follow following_hive
          end
        end

        # Call Tip model method to connect @tip with share fields recored 
        @tip.handle_sharing(@selected_friends_and_groups)

        @tip.notify_mentioned_friends
        if Rails.env.production?
          @mixpanel.track 'Tip Created', id: @tip.id, title: @tip.title, distinct_id: current_user.id
        end

        if @question
          @tip.notify_user_add_tip_on_question(@question, current_user)
          @tip.follow @question
          format.html { redirect_to question_path(@question), notice: 'Tip was successfully created.' }
        else
          format.html { redirect_to @tip, notice: 'Tip was successfully created.' }
        end
        format.js {}
        format.json { render template: 'api/v1/tips/show' }
      else
        format.js {}
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @tip.errors, full_messages: @tip.errors.full_messages },
                         status:   :fail, message: @tip.errors.full_messages.join(', ') }
        end
      end
    end
  end

  # PATCH/PUT /tips/1
  # PATCH/PUT /tips/1.json
  def update
    @tip = current_user.tips.find(params[:id])

    @tip.picture_ids              = @tip.pictures.map { |p| p.id }.join(',')
    @following_hives              = @selected_hives = @tip.following_hives
    @following_pockets            = @selected_pockets = @tip.following_pockets
    @connected_groups             = current_user.following_groups.order(:title)
    @connected_hives              = current_user.following_hives.order(:title)
    @connected_pockets            = current_user.following_pockets
    @connected_friends_and_groups = @selected_friends_and_groups =
      @tip.users_and_groups_and_friend_lists - [current_user]

    if Rails.env.production?
      @mixpanel.track 'Tip Updated', id: @tip.id, title: @tip.title, distinct_id: current_user.id
    end

    respond_to do |format|
      if @tip.update(tip_params)
        if params[:select_friends_and_groups].blank?
          @tip.update_attributes(is_private: true)
        end
        @tip.notify_mentioned_friends
        format.html { redirect_to @tip, notice: 'Tip was successfully updated.' }
        format.json { render template: 'api/v1/tips/show' }
      else
        format.html { render action: 'edit' }
        format.json do
          render json: { response: { error: @tip.errors, full_messages: @tip.errors.full_messages },
                         status:   :fail, message: @tip.errors.full_messages.join(', ') }
        end
      end
    end
  end

  # DELETE /tips/1
  # DELETE /tips/1.json
  def destroy
    @tip = current_user.tips.find(params[:id])

    @tip.destroy

    if Rails.env.production?
      @mixpanel.track 'Tip Destroyed', id: @tip.id, title: @tip.title, distinct_id: current_user.id
    end

    message = 'Tip Destroyed'
    respond_to do |format|
      format.html { redirect_to tips_url }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  def get_tip
    tip = Tip.find_by_id(params[:tip_id])
    respond_to do |format|
      format.js { render partial: 'map_tip_form_modal', locals: { tip: tip } }
    end
  end

  def update_tip_location
    tip = Tip.find_by_id(params[:tip_id])
    tip && tip.update_attributes(latitude:  params[:lat],
                                 longitude: params[:lng],
                                 address:   params[:location],
                                 location:  params[:location]
    )
    render json: true
  end

  def business
    Business.create(title:       params[:title],
                    description: params[:des],
                    latitude:    params[:lat],
                    longitude:   params[:lng],
                    address:     params[:address],
                    location:    params[:location],
                    zip_code:    params[:zip_code],
                    photo_url:   params[:photo_url]
    )
    render json: true
  end

  def share
    @tip = Tip.find(params[:id])
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_tip
    if params[:draft] == '1'
      @tip = Tip.unscope(where: :draft).find(params[:id])
    else
      @tip = Tip.find(params[:id])
    end
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def tip_params
    params.require(:tip).permit(:title, :description, :picture_ids, :link_ids, :location, :address, :latitude,
                                :longitude, :sharing_type, :question_id, :is_private, :file_ids, images: [], files: [])
  end
end
