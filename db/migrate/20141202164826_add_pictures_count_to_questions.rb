class AddPicturesCountToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :pictures_count, :integer
  end
end
