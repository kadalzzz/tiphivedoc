class HivesController < ApplicationController
  include TipHiveHelper

  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!
  before_action :set_hive, only: [:show, :edit, :friends, :update, :destroy]
  before_action :get_object_setting_object, only: [:share, :object_setting_model]

  # GET /hives
  # GET /hives.json
  def index
    params[:order] = :title if params[:order].blank?
    if params[:user_id]
      @user  = User.friendly.find(params[:user_id])
      @hives = @user.following_hives.order(params[:order]).page(params[:page]).per(params[:per])
    elsif params[:group_id].present?
      @group = Group.find(params[:group_id])
      @hives = @group.following_hives.order(params[:order]).page(params[:page]).per(params[:per])
    elsif params[:all].present?
      @hives = Hive.order(params[:order]).page(params[:page]).per(params[:per])
    elsif params[:all_mine].present?
      @hives = current_user.hives.order(params[:order]).page(params[:page]).per(params[:per])
    elsif params[:suggested].present?
      @hives = Hive.order('created_at DESC').limit(10)
    else
      @hives = current_user.following_hives.order(params[:order]).page(params[:page]).per(params[:per])
      render 'my_hives'
    end
  end

  # GET /my_hives
  # GET /my_hives.json
  def my_hives
    if params[:group_id].present?
      @group = Group.find(params[:group_id])
      @hives = @group.following_hives.order(:title).page(params[:page]).per(params[:per])
    elsif params[:type_page].present? && params[:type_page] == 'show_all'
      @hives = Hive.all.order(:title).page(params[:page]).per(params[:per])
    else
      @hives = current_user.following_hives.order(:title).page(params[:page]).per(params[:per])
    end
    hash_mp = { id: current_user.id, title: 'My Hives', distinct_id: (current_user.nil? ? 0 :
      current_user.id) }
    @mixpanel.track 'My Hives Viewed', hash_mp if Rails.env.production?
    respond_to do |format|
      format.html {}
      format.js do
        @my_hives  = current_user.following_hives.order(:title)
        @all_hives = Hive.all.order(:title)
      end
      format.json {}
    end
  end

  # GET /menu_hives
  # GET /menu_hives.json
  def menu_hives
    if params[:group_id].present?
      @group = Group.find(params[:group_id])
      @hives = @group.following_hives.order(:title)
    else
      @hives = current_user.following_hives.order(:title)
    end
  end

  def friends
    @hive_friends  = current_user.following_users.where(id: @hive.user_followers.pluck(:id))
    @friends       = @hive_friends.page(params[:page]).per(params[:per])
    @friends_count = @hive_friends.count
  end

  # GET /following_pockets
  # GET /following_pockets.json
  def following_pockets
    hives           = params[:hives][0].split(',')
    @selected_hives = collect_items(items_list: hives)
    @results        = @selected_hives
    render template: 'hives/following_pockets'
  end

  # GET /hives/1
  # GET /hives/1.json
  def show
    add_breadcrumb 'My Hives', my_hives_path, data: { remote: true }, class: 'active-transition'
    add_breadcrumb "#{@hive.title}", '#', class: 'no-link'
    add_breadcrumb 'All Pockets', '#', id: 'all_pocket'

    # SHASHI: if pcoket id present then we are shwoing pocket tips only
    if params[:pocket_id]
      @pocket    = Pocket.find(params[:pocket_id])
      @tips      = current_user.fetch_tips(@pocket, params)
      @questions = current_user.fetch_tips(@pocket, params)
      # SHASHI: If pocket id not present we are showing all @hive tips
    else
      @tips      = current_user.fetch_tips(@hive, params)
      @questions = current_user.fetch_questions_v3(@hive, params)
    end
    @friends_count    = current_user.following_users.where(id: @hive.user_followers.pluck(:id)).count
    @liked_tips_count = @tips.includes([:votes]).where('votes.voter_id = ?', current_user.id).references(:votes).count

    if Rails.env.production?
      @mixpanel.track 'Hive Viewed',
                      id:          @hive.id,
                      title:       @hive.title,
                      distinct_id: current_user.nil? ? 0 : current_user.id
    end

    # SHASHI: I need to find another way to showing flash message for adding shareable user to hive share list 
    # if @hive.user.id != current_user.id
    #   share_hive_owner = @hive.shares.where(sharing_object_type: 'User' ,sharing_object_id: current_user.id).first
    #   return if share_hive_owner.blank?
    #   @count = 0
    #   flash[:notice] = %Q[Do you want to share #{@hive.title} to #{share_hive_owner.user.name} <a href="/hives/#{@hive.to_param}?share=yes">Yes</a>/<a href="/hives/#{@hive.to_param}?share=no">No</a>].html_safe if @count == 0
    #   if params[:share] == 'yes'
    #     @hive.share(share_hive_owner.user, current_user)
    #     flash.clear
    #     @count = @count + 1
    #   elsif params[:share] == 'no'
    #     flash.clear
    #     @count = @count + 1
    #   end
    # end

    respond_to do |format|
      format.html {}
      format.js {}
      format.json {}
    end
  end

  # GET /hives/new
  def new
    @hive                         = current_user.hives.new
    if params[:groups].blank?
      @selected_friends_and_groups = collect_items(items_list: ['FriendList-1'])
    else
      @selected_friends_and_groups = collect_items(items_list: params[:groups])
    end
  end

  # GET /hives/1/edit
  def edit
    @hive                         = current_user.hives.find(params[:id])
    @selected_friends_and_groups  = @hive.get_shared_objects(current_user)
  end

  # POST /hives
  # POST /hives.json
  def create
    # Check if there is a Hive with given name.
    # If so, follow the Hive without creating a new one.
    if hive = Hive.where('lower(title) =?', params[:hive][:title].downcase).first
      current_user.follow hive
      return respond_to do |format|
        message = "Hive #{hive.title} already exists."
        format.html { redirect_to hive_path(hive), notice: message }
        format.json { render json: { response: { error: message }, status: :fail, message: message } }
        format.js { render js: "alert('#{message}');" }
      end
    end
    file_upload                      = read_upload_file_from_request

    params[:hive][:background_image] = file_upload if file_upload.present?

    @hive                        = current_user.hives.new(hive_params)
    @selected_friends_and_groups = collect_items(items_list: params[:select_friends_and_groups])

    if Rails.env.production?
      @mixpanel.track 'Hive Created',
                      id:          @hive.id,
                      title:       @hive.title,
                      distinct_id: current_user.nil? ? 0 : current_user.id
    end

    # @existing_hive = scan_existing_hive(hive_params)
    # return if @existing_hive

    respond_to do |format|
      if @hive.save
        current_user.follow @hive
        # Call Hive model method to connect @hive with share fields recored 
        @hive.handle_sharing(@selected_friends_and_groups)
        # create object settings recorded for Hive
        @hive.create_settings_object(current_user)
        format.html { redirect_to @hive, notice: 'Hive was successfully created.' }
        format.json { render template: 'api/v1/hives/show' }
        format.js {}
      else
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @hive.errors, full_messages: @hive.errors.full_messages },
                         status:   :fail,
                         message:  @hive.errors.full_messages.join(', ') }
        end
        format.js { render js: "alert('#{@hive.errors.full_messages.join(', ')}');" }
      end
    end
  end

  # PATCH/PUT /hives/1
  # PATCH/PUT /hives/1.json
  def update
    file_upload = read_upload_file_from_request

    if file_upload.present?
      params[:hive][:background_image] = file_upload
    end

    @hive                         = current_user.hives.find(params[:id])
    @selected_friends_and_groups  = @hive.get_shared_objects(current_user)

    if Rails.env.production?
      @mixpanel.track 'Hive Updated',
                      id:          @hive.id,
                      title:       @hive.title,
                      distinct_id: current_user.nil? ? 0 : current_user.id
    end


    respond_to do |format|
      if @hive.update(hive_params)
        if params[:select_friends_and_groups].blank?
          # don't set is_private if request from update background
          # not sure why file_upload is nill, so use params[:hive][:background_image] instead file_upload
          unless params[:hive][:background_image].present?
            @hive.update_attributes(is_private: true)
          end
        end
        @hive.update_object_setting_recorded(current_user, params[:hive][:background_image])
        format.html { redirect_to @hive, notice: 'Hive was successfully updated.' }
        format.json { render template: 'api/v1/hives/show' }
      else
        format.html { render action: 'edit' }
        format.json do
          render json: { response: { error: @hive.errors, full_messages: @hive.errors.full_messages },
                         status:   :fail, message: @hive.errors.full_messages.join(', ') }
        end
      end
    end
  end

  # GET /hives/1/share
  def share
    @selected_friends_and_groups  = @hive.get_shared_objects(current_user)
  end

  def object_setting_model
    @selected_friends_and_groups  = @hive.get_shared_objects(current_user)
  end

  def object_setting
    file_upload = read_upload_file_from_request
    if file_upload.present?
      params[:object_settings][:background_image] = file_upload
    end
    @hive = Hive.find(params[:id])
    @hive.create_or_update_object_setting(current_user, object_setting_params)
    respond_to do |format|
      format.html { redirect_to @hive, notice: 'Hive setting was successfully updated.' }
      format.js { render text: 'success' }
    end
  end

  # GET /hives/1/join
  def join
    @hive = Hive.find(params[:id])

    current_user.follow @hive
    flash[:notice] = "Joined #{@hive.title}"

    if Rails.env.production?
      @mixpanel.track 'Hive Joined',
                      id:          @hive.id,
                      title:       @hive.title,
                      distinct_id: current_user.nil? ? 0 : current_user.id
    end

    message = "Joined #{@hive.title}"
    respond_to do |format|
      format.html { redirect_to hive_path(@hive) }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  # GET /hives/1/leave
  def leave
    @hive = Hive.find(params[:id])

    if current_user.id == @hive.user.id
      flash[:notice] = "You can't unfollow your own hive"
      redirect_to hive_path(@hive)
      return
    else
      current_user.stop_following @hive
      # destroy all object_setting && sharing record if user leave Hive
      @hive.destroy_object_setting_and_share_record(current_user) if current_user
    end

    if Rails.env.production?
      @mixpanel.track 'Hive Left',
                      id:          @hive.id,
                      title:       @hive.title,
                      distinct_id: current_user.nil? ? 0 : current_user.id
    end

    message = "Left #{@hive.title}"
    respond_to do |format|
      format.html { redirect_to user_profile_path(current_user) }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  # DELETE /hives/1
  # DELETE /hives/1.json
  def destroy
    @hive = current_user.hives.find(params[:id])

    @hive.destroy

    @mixpanel.track 'Hive Destroyed', { id: @hive.id, title: @hive.title, distinct_id: current_user.nil? ? 0 : current_user.id } if Rails.env.production?
    message = 'Hive Destroyed'
    respond_to do |format|
      format.html { redirect_to my_hives_url }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_hive
    @hive = Hive.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def hive_params
    params.require(:hive).permit(:title, :description, :background_image, :background_image_cache,
                                 :remove_background_image, :sharing_type, :is_private)
  end

  def object_setting_params
    params.require(:object_setting).permit(:object_setting_type, :object_setting_id, :user_id, :background_image,
                                           :background_image_cache, :remove_background_image, :is_private)
  end

  def scan_existing_hive(hive_params)
    # follow Hive if existed
    existing_hive = Hive.where(title: hive_params['title']).first

    if existing_hive
      current_user.follow existing_hive
      respond_to do |format|
        format.html { redirect_to existing_hive, notice: 'Hive was successfully created.' }
        format.json { render action: 'show' }
      end
    end

    existing_hive
  end

  def get_object_setting_object
    @hive = Hive.find(params[:id])
    obj   = @hive.find_object_settings(current_user)
    obj.blank? ? @object_setting = ObjectSetting.new : @object_setting = obj
    # get current user followingd groups
    @groups         = current_user.following_groups
  end
end
