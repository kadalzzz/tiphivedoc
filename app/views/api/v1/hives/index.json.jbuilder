hives ||= @hives

json.response do
  json.array!(hives) do |hive|
    json.partial! 'api/v1/hives/hive', hive: hive
  end
end

json.status :success
