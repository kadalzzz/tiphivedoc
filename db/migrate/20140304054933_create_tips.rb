class CreateTips < ActiveRecord::Migration
  def change
    create_table :tips do |t|
      t.string :title
      t.text :description
      t.integer :user_id
      t.boolean :is_public, default: true

      t.timestamps
    end

    add_index :tips, :user_id
    add_index :tips, :is_public
  end
end
