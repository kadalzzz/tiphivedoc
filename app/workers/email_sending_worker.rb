class EmailSendingWorker
  include Sidekiq::Worker

  def perform(mailer, mailer_method, params)
    default_setting = TiphiveSetting.where(name: 'Default').first_or_create
    return unless default_setting.settings(:emails).emails_sending_activated

    logger.info "#{mailer}##{mailer_method}: #{params}"
    mailer.constantize.send(mailer_method, params).deliver
  end
end
