Given(/^a user signed in with email: "(.*?)", password: "(.*?)"$/) do |email, password|
  user = User.create email: email, password: password
  login_as(user, scope: :user)
end

Given(/^I logout$/) do
  logout
end

When(/^I login with email: "(.*?)"$/) do |email|
  user = User.where(email: email).first
  login_as(user, scope: :user)
end

Given(/^user with email: "(.*?)" connected to tip with title: "(.*?)"$/) do |email, tip_title|
  user = User.where(email: email).first
  tip = Tip.where(title: tip_title).first

  user.follow tip
end
