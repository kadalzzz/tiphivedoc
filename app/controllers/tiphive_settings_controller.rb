class TiphiveSettingsController < ApplicationController
  before_filter :authenticate_user!
  http_basic_authenticate_with name: 'tiphive', password: 'givemeaccesstosettings'
  # before_action :set_tiphive_setting, only: [:show, :edit, :update, :destroy]

  # GET /tiphive_settings
  # GET /tiphive_settings.json
  def index
    @default_setting = TiphiveSetting.where(name: 'Default').first_or_create
    # @tiphive_settings = TiphiveSetting.all

    if params[:scope].present? && params[:type].present? && params[:value].present? && request.method == 'POST'
      params[:value] = false if params[:value] == 'false'
      params[:value] = true if params[:value] == 'true'
      return if [true, false].exclude?(params[:value])
      @default_setting.settings(params[:scope].to_sym).update_attributes! params[:type] => params[:value]
    end

    if request.method == 'POST' && params[:test_email].present?
      EmailSendingWorker.perform_async('UserMailer', 'test_email', { test_email: params[:test_email] })
    end
  end

  def hives
    @hives = Hive.unscoped.order('title ASC').page(params[:page]).per(params[:per] || 50)
  end

  def switch_domain_setting
    @hives   = Hive.unscoped.all
    @groups  = Group.unscoped.all
    @domains = Domain.unscoped.all
  end

  def get_domain
    @domain = Domain.find(params[:domain_id])

    if params[:group_id].present?
      @group = Group.unscoped.find(params[:group_id])

      # all group hives, tips and wanted switch to domain
      @group.hives.each do |hive|
        hive.pockets.update_all(domain_id: @domain.id) unless hive.pockets.blank?
        hive.update_attributes(domain_id: @domain.id)
      end
      @group.following_tips.update_all(domain_id: @domain.id)
      @group.following_questions.update_all(domain_id: @domain.id)

      # All group member follow domain
      @group.following_users.each do |item|
        item.follow(@domain) unless item.nil?
      end

      # Switch group to domain 
      @group.update_attributes(domain_id: @domain.id)
    end

    if params[:hive_id].present?
      @hives = []
      params[:hive_id].each do |item_str|
        kind, id = item_str.split('-')
        next if kind.blank? || id.blank?
        persistent_item = kind.constantize.unscoped.where(id: id).first
        @hives << persistent_item if persistent_item.present?
      end
      # All hive following pockets switch to domain
      @hives.each do |hive|
        hive.pockets.each do |pocket|
          pocket.tips.update_all(domain_id: @domain.id)
          pocket.question_followers.update_all(domain_id: @domain.id)
          pocket.update_attributes(domain_id: @domain.id)
        end
        hive.update_attributes(domain_id: @domain.id)
      end
    end

    respond_to do |format|
      format.html { redirect_to switch_group_path, notice: "Group successfully switch to #{@domain.name} subdomain" }
    end

  end
end
