json.array!(@my_friends) do |friend|
  json.extract! friend, :id, :first_name, :last_name, :email
  json.url pocket_url(friend, format: :json)
end
