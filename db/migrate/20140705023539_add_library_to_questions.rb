class AddLibraryToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :library, :boolean
  end
end
