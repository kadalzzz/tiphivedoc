class Api::V1::HivesController < HivesController
  def share
    @selected_friends_and_groups = @hive.get_shared_objects(current_user)
    @selected_friends            = []
    @selected_groups             = []
    @selected_friends_and_groups.each do |selected|
      if selected.class == User
        @selected_friends << selected if !@selected_friends.map(&:id).include?(selected.id)
      elsif selected.class == Group
        @selected_groups << selected if !@selected_groups.map(&:id).include?(selected.id)
      end
    end
  end
end
