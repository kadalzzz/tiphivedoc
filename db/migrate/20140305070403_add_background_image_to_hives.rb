class AddBackgroundImageToHives < ActiveRecord::Migration
  def change
    add_column :hives, :background_image, :string
  end
end
