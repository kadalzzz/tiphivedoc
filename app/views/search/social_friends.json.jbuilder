json.array!(@results) do |result|
  json.uid result.uid
  json.name result.name
  json.provider result.provider
  json.email result.email
end
