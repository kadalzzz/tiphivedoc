require 'rails_helper'

describe GroupsController do
  context 'when not logged in' do
    describe '#GET index' do
      before { get :index }
      it { expect(response).to have_http_status(302) }
      it 'sets alert message' do
        expect(flash.alert).to eq 'You need to sign in or sign up before continuing.'
      end
    end
  end

  context 'when loggned in' do
    describe '#GET index' do
      let(:user) { create(:user) }
      let(:group) { create(:group) }
      before do
        login_user(user)
        user.follow(group)
        get :index
      end
      it 'assigns @groups' do
        expect(assigns(:groups)).to eq [group]
      end
    end

    describe '#GET my_groups' do
      let(:user) { create(:user) }
      let(:group) { create(:group) }
      before do
        login_user(user)
        user.follow(group)
        get :index
      end
      it 'assigns @groups' do
        expect(assigns(:groups)).to eq [group]
      end
    end

    describe '#GET menu_groups' do
      let(:user) { create(:user) }
      let(:group) { create(:group) }
      before do
        login_user(user)
        user.follow(group)
        get :index
      end
      it 'assigns @groups' do
        expect(assigns(:groups)).to eq [group]
      end
    end

    describe '#GET members' do
      let(:user) { create(:user) }
      let(:group) { create(:group) }
      before do
        login_user(user)
        user.follow(group)
      end
      context 'when params[:initial] is missing' do
        before { get :members, id: group.id }
        it 'returns members' do
          expect(assigns(:members)).to eq [user]
        end
      end
      context 'when params[:initial] is given' do
        it 'returns members whose first_name starts from given initial' do
          get :members, id: group.id, initial: 'A'
          expect(assigns(:members)).to eq [user]
        end
        context 'and when it matches no users' do
          it 'returns no users' do
            get :members, id: group.id, initial: 'Z'
            expect(assigns(:members)).to eq []
          end
        end
      end

    end

    describe '#GET show' do
    end

    describe '#GET new ' do
      let(:user) { create(:user) }
      before do
        login_user(user)
        get :new
      end
      it 'assigns @group' do
        expect(assigns(:group)).to be_a_new(Group)
      end
      it 'assigns @connected_friends_and_groups' do
        expect(assigns(:connected_friends_and_groups)).to eq []
      end
    end

    describe '#GET edit' do
      let(:user) { create(:user) }
      let(:user1) { create(:user) }
      let(:group) { create(:group, user: user) }
      before do
        login_user(user)
        user.follow(group)
        user1.follow(group)
        get :edit, id: group.id
      end
      it 'assigns @group' do
        expect(assigns(:group)).to eq group
      end
      it 'assigns @connected_friends_and_groups' do
        expect(assigns(:connected_friends_and_groups)).to eq [user1]
      end
      it 'assigns @selected_friends_and_groups' do
        expect(assigns(:selected_friends_and_groups)).to eq [user1]
      end
    end

    describe '#POST create' do
      let(:user) { create(:user) }
      before do
        login_user(user)
        post :create, group: attributes_for(:group)
      end
      it 'creates new group' do
        expect(assigns(:group)).to be_a_kind_of(Group)
      end
      it 'follows the new group' do
        expect(user.following_groups.count).to eq 1
      end
      it 'sets flash message' do
        expect(flash.notice).to eq 'Group was successfully created.'
      end
    end

    describe '#PATCH update' do
      let(:user) { create(:user) }
      let(:group) { create(:group, user: user) }
      before do
        login_user(user)
        patch :update, id: group.id, group: {description: 'New description'}
      end

      it 'updates group' do
        expect(group.reload.description).to eq 'New description'
      end

      it 'sets correct flash message' do
        expect(flash.notice).to eq 'Group was successfully updated.'
      end
    end

    describe '#GET sharre' do
      let(:group) { create(:group) }
      let(:user) { create(:user) }
      before do
        login_user(user)
        get :share, id: group.id
      end
      it 'assigns @group' do
        expect(assigns(:group)).to eq group
      end
    end

  end
end
