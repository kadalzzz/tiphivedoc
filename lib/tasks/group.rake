namespace :group do
  desc "Set Group Color"
  task assign_color: :environment do
    Group.find_each do |g|
      g.update_attribute(:color, Group::COLOR[rand(Group::COLOR.length)])
    end
  end
end
