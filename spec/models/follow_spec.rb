require 'rails_helper'

describe Follow do
  it { is_expected.to belong_to(:followable) }
  it { is_expected.to belong_to(:follower) }

  describe '#block!' do
    let(:follow) { create(:follow) }
    subject { follow.block! }
    it { expect{subject}.to change{follow.blocked}.from(false).to(true) }
  end
end
