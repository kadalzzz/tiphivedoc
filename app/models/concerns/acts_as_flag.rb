module ActsAsFlag
  extend ActiveSupport::Concern

  included do
    has_many :flags, as: :flag, dependent: :destroy
  end

  def flagged?(tip)
    flag = flags.find_by(flagable_type: 'Tip', flagable_id: tip.id)
    flag.blank? ? true : false
  end
end
