module ApplicationObserver
  extend ActiveSupport::Concern

  included do
    before_filter :configure_permitted_parameters, if: :devise_controller?
    before_filter :preload_data
    before_filter :cleanup_temp, only: [:new, :edit]
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.for(:sign_up) << :first_name
    devise_parameter_sanitizer.for(:sign_up) << :last_name
    devise_parameter_sanitizer.for(:sign_up) << :second_email
    devise_parameter_sanitizer.for(:sign_up) << :avatar
    devise_parameter_sanitizer.for(:sign_up) << :avatar_cache
    devise_parameter_sanitizer.for(:sign_up) << :remote_avatar_url

    devise_parameter_sanitizer.for(:account_update) << :first_name
    devise_parameter_sanitizer.for(:account_update) << :last_name
    devise_parameter_sanitizer.for(:account_update) << :avatar
    devise_parameter_sanitizer.for(:account_update) << :avatar_cache
    devise_parameter_sanitizer.for(:account_update) << :remote_avatar_url
    devise_parameter_sanitizer.for(:account_update) << :remove_avatar
    devise_parameter_sanitizer.for(:account_update) << :background_image
    devise_parameter_sanitizer.for(:account_update) << :background_image_cache
    devise_parameter_sanitizer.for(:account_update) << :password
    devise_parameter_sanitizer.for(:account_update) << :password_confirmation
  end

  private
  def preload_data
    if user_signed_in?
    end
  end

  def cleanup_temp
    # delete temp uploaded pictures that belongs to no object
    @temp_pictures = current_user.pictures.where(imageable_type: nil, imageable_id: nil).destroy_all if user_signed_in?
  end
end
