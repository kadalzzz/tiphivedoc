module ActsAsSharing
  extend ActiveSupport::Concern

  included do
    has_many :shares, as: :sharing_object, dependent: :destroy
  end
end
