module QuestionsFinder
  def fetch_questions_v3(current_object = nil, params = {})
    friend_ids   = self.friend_ids
    questions    = Question.includes([:user])

    # get viewable question IDs by specific question permissions
    question_ids = []
    if current_object.is_a?(Hive)
      question_ids = fetch_hive_question_ids(current_object, params)
    end
    if current_object.is_a?(Pocket)
      question_ids = fetch_pocket_question_ids(current_object, params)
    end
    if current_object.is_a?(User)
      question_ids = fetch_user_question_ids(current_object, params)
    end
    if current_object.is_a?(Group)
      question_ids = fetch_group_question_ids(current_object, params)
    end

    # social filters
    questions = questions.where(id: question_ids).order('questions.created_at DESC')
    if params[:social_filter] == 'my_wants'
      questions = questions.where(user_id: id).order('questions.created_at DESC')
    end
    if params[:social_filter] == 'my_liked_wants'
      questions = questions.includes([:votes]).where('votes.voter_id = ?', id)
                           .references(:votes).order('questions.created_at DESC')
    end
    if params[:social_filter] == 'friends_wants'
      questions = questions.where(user_id: friend_ids).order('questions.created_at DESC')
    end
    if params[:social_filter] == 'public_wants'
      questions = questions.public_sharing.order('questions.created_at DESC')
    end

    questions.page(params[:page]).per(params[:per])
  end

  def fetch_questions_hive_and_pocket(hive, pocket, params = {})
    questions    = Question.includes([:user])
    question_ids = fetch_hive_question_ids(hive, params)
    question_ids += fetch_pocket_question_ids(pocket, params)
    questions.where(id: question_ids)
  end

  def get_total_user_questions(user, params = {})
    # Check if question count is nil
    if fetch_user_question_ids(user, params).nil?
      0
    else
      fetch_user_question_ids(user, params).count
    end
  end

  private

  def fetch_hive_question_ids(hive, params = {})
    friend_ids          = self.friend_ids
    me_and_friend_ids   = friend_ids + [id]

    # Get all questions belong to pockets of the hive
    pocket_ids          = hive.pockets.pluck(:id)
    pocket_question_ids = question_follower_ids_by_type('Pocket', pocket_ids)

    # Get owner question id
    question_ids        = fetch_owner_question(pocket_question_ids, self)

    # User following questions
    question_ids        += user_following_question('User', pocket_question_ids)

    # all friends questions
    question_ids        += all_friend_in_questions_ids(pocket_question_ids)

    # public questions
    question_ids        += public_questions_ids(pocket_question_ids)

    # Exclude private Questions if not belong to current user
    question_ids        = question_ids - private_question_ids(question_ids)

    # Exclude all-friends sharing Questions if current user is not friend
    question_ids        = question_ids - all_friends_question_ids(question_ids, me_and_friend_ids)

    # Exclude selected-friends sharing Questions if current user is not friend but include my connected Questions
    question_ids        = question_ids.uniq #+ connected_question_ids
  end

  def fetch_pocket_question_ids(pocket, params = {})
    friend_ids        = self.friend_ids
    me_and_friend_ids = friend_ids + [id]

    # Get all questions belong to pocket
    pocket_ids        = pocket.question_followers.pluck(:id)

    # Get Owenr questions
    question_ids      = fetch_owner_question(pocket_ids, self)

    # User following questions
    question_ids      += user_following_question('User', pocket_ids)

    # public questions
    question_ids      += public_questions_ids(pocket_ids)

    # all friends questions
    question_ids      += all_friend_in_questions_ids(pocket_ids)

    # Exclude private Questions if not belong to current user
    question_ids      = question_ids - private_question_ids(question_ids)

    # Exclude all-friends sharing Questions if current user is not friend
    question_ids      = question_ids - all_friends_question_ids(question_ids, me_and_friend_ids)

    # Exclude selected-friends sharing Questions if current user is not friend but include my connected Questions
    question_ids      = question_ids.uniq #+ connected_question_ids

    question_ids.uniq
  end

  def fetch_group_question_ids(group, params = {})
    friend_ids        = self.friend_ids
    me_and_friend_ids = friend_ids + [id]

    # Get all questions belong to the group
    question_ids      = group.following_questions.pluck(:id)
    # connected_question_ids = self.following_questions.where(id: question_ids).pluck(:id)

    # TODO: Get all questions from all connected hives and pockets
    # ---

    # Exclude private questions if not belong to current user
    question_ids      = question_ids - private_question_ids(question_ids)

    # Exclude all-friends sharing questions if current user is not friend
    # question_ids = question_ids - all_friends_question_ids(question_ids, me_and_friend_ids)

    # Exclude selected-friends sharing questions if current user is not friend but include my connected questions
    # question_ids = (question_ids - selected_friends_question_ids(question_ids)) + connected_question_ids

    question_ids.uniq
  end

  def fetch_user_question_ids(user, params = {})
    friend_ids        = self.friend_ids
    me_and_friend_ids = friend_ids + [id]

    if params[:controller] == 'users' && params[:action] == 'show'

      case status_with(user)
      when :is_me
        user_question = user.questions.pluck(:id)
        tip_ids       = fetch_owner_question(user_question, user)
      when :is_friend
        user_question_ids = user.questions.pluck(:id)
        question_ids      = fetch_owner_question(user_question_ids, user)
        question_ids      += user_following_question('User', user_question_ids)
      when :is_nothing
        user_questions = user.questions.pluck(:id)
        question_ids   = public_questions_ids(user_questions)
        # If user is a member of group
        question_ids   += get_group_questions(user)
      else
        question_ids = []
      end

    else
      # get all questions user following hive
      hive_ids          = user.following_hives.pluck(:id)

      # User follow Hive and User is owner of the question
      hive_question_ids = question_follower_ids_by_type('Hive', hive_ids)

      # Get Owenr questions
      question_ids      = fetch_owner_question(hive_question_ids, self)

      # User follow Hive And if tip shared with the user directly
      question_ids      += user_following_question('User', hive_question_ids)

      # all friends questions
      question_ids      += all_friend_in_questions_ids(hive_question_ids)

      # Public questions
      question_ids      += public_questions_ids(hive_question_ids)

      # Exclude all-friends sharing questions if current user is not friend
      question_ids      = question_ids - all_friends_question_ids(question_ids, me_and_friend_ids)
    end

    # Exclude private questions if not belong to current user
    question_ids = question_ids - private_question_ids(question_ids) unless question_ids.blank?

    # Exclude selected-friends sharing questions if current user is not friend but include my connected questions
    # question_ids = question_ids + connected_question_ids
    # question_ids = question_ids
    question_ids.uniq unless question_ids.blank?
  end

  def question_follower_ids_by_type(type_name, ids_list)
    if %w(Group).include?(type_name)
      Follow.where(followable_type: 'Question', follower_type: type_name, follower_id: ids_list).pluck(:followable_id)
    else
      Follow.where(followable_type: type_name, followable_id: ids_list, follower_type: 'Question').pluck(:follower_id)
    end
  end

  def private_question_ids(question_ids)
    Question.where(id: question_ids).where('user_id != ?', self.id).private_sharing.pluck(:id)
  end

  def all_friends_question_ids(question_ids, me_and_friend_ids)
    Question.where(id: question_ids).where('user_id NOT IN (?)', me_and_friend_ids).all_friends_sharing.pluck(:id)
  end

  def fetch_owner_question(question_ids, owner)
    Question.where(id: question_ids).by_owner(owner).pluck(:id)
  end

  def user_following_question(type_name, ids_list)
    Follow.where(followable_type: 'Question', followable_id: ids_list, follower_type: type_name, follower_id: id)
          .pluck(:followable_id)
  end

  # all friends tip
  def all_friend_in_questions_ids(question_ids)
    Question.where(id: question_ids).all_friends_sharing.pluck(:id)
  end

  # public tip
  def public_questions_ids(question_ids)
    Question.where(id: question_ids).public_sharing.pluck(:id)
  end

  def get_group_questions(user)
    group_question_ids = []
    user_group_ids     = user.following_groups.pluck(:id)
    owner_group_ids    = following_groups.pluck(:id)

    user_group_ids.each do |user_group|
      owner_group_ids.each do |owner_group|
        if user_group == owner_group
          group_question_ids += question_follower_ids_by_type('Group', user_group)
        end
      end
    end

    group_question_ids
  end
end
