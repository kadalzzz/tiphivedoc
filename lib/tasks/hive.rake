namespace :hive do
  desc "Set Hive Color"
  task assign_color: :environment do
    Hive.find_each do |h|
      h.update_attribute(:background_color, Hive::COLOR[rand(Hive::COLOR.length)])
    end
  end
end
