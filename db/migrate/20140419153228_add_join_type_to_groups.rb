class AddJoinTypeToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :join_type, :string, default: 'anyone'
    add_index :groups, :join_type
  end
end
