class CreateObjectSettings < ActiveRecord::Migration
  def up
    create_table :object_settings do |t|
      t.integer :object_setting_id
      t.string :object_setting_type
      t.integer :user_id
      t.boolean :is_public, default: false
      t.string :background_image

      t.timestamps
    end
    add_index :object_settings, :object_setting_id
    add_index :object_settings, :object_setting_type
  end

  def down
    drop_table :object_settings
  end
end
