class AddPicturesCountToTips < ActiveRecord::Migration
  def change
    add_column :tips, :pictures_count, :integer, default: 0
  end
end
