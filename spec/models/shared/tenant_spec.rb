require 'rails_helper'

RSpec.shared_examples "a tenant" do |tenant: Tenant.new|

  subject { tenant }

  it { should belong_to(:domain) }

  describe "default_scope" do
    pending
  end

  describe "#subdomain" do
    subject { tenant.subdomain }

    context "when #domain is set" do
      let(:domain) do
        double("Domain").tap do |domain|
          allow(domain).to receive(:name).and_return("domainname")
        end
      end

      before { expect(tenant).to receive(:domain).and_return(domain) }

      it { should eql("domainname") }
    end

    context "when #domain is not set" do
      it { should eql(nil) }
    end
  end

end
