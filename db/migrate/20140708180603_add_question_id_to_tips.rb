class AddQuestionIdToTips < ActiveRecord::Migration
  def change
    add_column :tips, :question_id, :integer
    add_index :tips, :question_id
  end
end
