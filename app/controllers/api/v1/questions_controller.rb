class Api::V1::QuestionsController < QuestionsController
  def hives_and_pockets
    @question = Question.find(params[:id])
  end

  # Get share status and list shared objects
  def shared_objects
    @question = Question.find(params[:id])
    @selected_friends = []
    @selected_groups = []
    @selected_friends_and_groups = @question.users_and_groups_and_friend_lists - [current_user, @question.user]
    @selected_friends_and_groups.each do |selected|
      if selected.class == User
        @selected_friends << selected
      elsif selected.class == Group
        @selected_groups << selected
      end
    end
    @selected_friends.uniq
    @selected_groups.uniq
  end
end
