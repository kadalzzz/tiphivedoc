json.extract! @tip_link, :id, :url, :title, :description, :image_url, :avatar, :tip_id,
              :user_id, :created_at, :updated_at
