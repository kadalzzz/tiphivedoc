user             ||= @user
tips             ||= @tips
tips_count       ||= @tips_count
questions        ||= @questions
liked_tips       ||= @liked_tips
friends          ||= @friends
following_status ||= @following_status

json.response do
  json.partial! 'api/v1/users/user', user: user

  json.tips_count tips_count
  json.questions_count questions.count
  json.liked_tips_count liked_tips.count
  json.friends_count friends.count
  json.following_status = following_status
end

json.status :success
