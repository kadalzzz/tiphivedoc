json.array!(@tips) do |result|
  json.extract! result, :id, :title, :description, :user_id
  json.url tip_url(result, format: :json)
end
