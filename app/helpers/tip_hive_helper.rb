module TipHiveHelper
  # Search for TipHive objects by params ['Hive-2', 'Pocket-23', ...]
  def collect_items(params)
    return [] if params.blank? || params[:items_list].blank?
    items = []
    params[:items_list].each do |item_str|
      kind, id = item_str.split('-')
      next if kind.blank? || id.blank?
      persistent_item = kind.constantize.where(id: id).first
      items << persistent_item if persistent_item.present?
    end
    items
  end

  def user_following_connections(params)
    params[:follower_type] = 'User'
    params[:follower_id]   = current_user.id if current_user
    params[:follower_id]   = params[:user_id] if params[:user_id].present?
    search_connections(params)
  end

  def search_connections(params)
    logger.info "*** search_connections params: #{params}"
    search = Follow.solr_search do
      fulltext(params[:q]) if params[:q].present?

      if params[:followable_types].present?
        followable_types = params[:followable_types].split(',')
        any_of do
          followable_types.each do |followable_type|
            with :followable_type, followable_type
          end
        end
      end

      if params[:follower_types].present?
        follower_types = params[:follower_types].split(',')
        any_of do
          follower_types.each do |follower_type|
            with :follower_type, follower_type
          end
        end
      end

      with :followable_type, params[:followable_type] if params[:followable_type].present?
      with :follower_type, params[:follower_type] if params[:follower_type].present?
      with :followable_id, params[:followable_id] if params[:followable_id].present?
      with :follower_id, params[:follower_id] if params[:follower_id].present?
    end

    search.results
  end

  def search_objects(object_classes, query = '')
    logger.info "*** search_objects object_classes: #{object_classes} - query: #{query}"
    search = Sunspot.search(object_classes) do
      fulltext(query) do
        if params[:fields].present?
          search_fields = params[:fields].split(',')
          search_fields.each do |search_field|
            fields(search_field)
          end
        end
        fields params[:field] if params[:field].present?
      end if query.present?

      if params[:page].present?
        current_page = params[:page].to_i > 0 ? params[:page].to_i : 1
        per_page     = params[:per].to_i > 0 ? params[:per].to_i : 6
        paginate(page: current_page, per_page: per_page)
      end
    end

    search
  end
end
