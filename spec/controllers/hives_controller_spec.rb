require 'rails_helper'

describe HivesController do
  let(:user) { create(:user) }
  let(:group) { create(:group) }
  describe '#GET index' do
    before do
      10.times { hive = create(:hive); group.follow(hive) }
      login_user(user)
      get :index, group_id: group.id
    end
    it 'assigns @group' do
      expect(assigns(:group)).to eq group
    end
    it 'assigns @hives' do
      expect(assigns(:hives).count).to eq 4
    end
  end

  describe '#GET my_hives' do
    before do
      login_user(user)
      2.times { hive = create(:hive); user.follow(hive) }
      get :my_hives
    end
    it 'assigns @hives' do
      expect(assigns(:hives).count).to eq 2
    end
  end

  describe '#GET following_pockets' do
    before do
      login_user(user)
      get :following_pockets
    end
    it 'assigns @selected_hives' do
      expect(assigns(:selected_hives)).to be_a(Array)
    end
  end

  describe '#GET create' do
    let(:friend) { create(:user) }
    context 'with valid attribute' do
      before do
        login_user(user)
        user.follow(friend)
        post :create, hive: attributes_for(:hive),select_friends_and_groups: ["User-#{friend.id}"]
      end
      it 'new hive' do
          expect(assigns(:hive)).to be_a_kind_of(Hive)
      end
      it 'follows the new hive' do
        expect(user.following_hives.count).to eq 1
      end
      it 'follows the selected friends' do
        expect(user.following_users.count).to eq 1
      end
      it 'sets flash message' do
        expect(flash.notice).to eq 'Hive was successfully created.'
      end
    end

    context 'with invalid attribute' do
      before do
        login_user(user)
        user.follow(friend)
        post :create, hive: attributes_for(:hive, title: '')
      end
      it { expect(response).to have_http_status(200) }
    end
  end

  describe '#GET share' do
    let(:friend) { create(:user) }
    let(:hive) { create(:hive) }
    context 'with valid attribute' do
      before do
        login_user(user)
        user.follow(friend)
        user.follow(hive)
        xhr :get, :share, id: hive.id, :format => :js
      end
      it { expect(response).to have_http_status(200) }
    end
  end

  describe '#Post sharing_type' do
    let(:friend) { create(:user) }
    let(:hive) { create(:hive) }
    context 'with valid attribute' do
      before do
        login_user(user)
        user.follow(friend)
        user.follow(hive)
        xhr :post, :sharing_type, id: hive.id, sharing_type: attributes_for(:sharing_type), :format => :js
      end
      it { expect(response).to have_http_status(200) }
    end
  end

  describe '#Put sharing_type' do
    let(:friend) { create(:user) }
    let(:hive) { create(:hive) }
    before do
      login_user(user)
      user.follow(friend)
      user.follow(hive)
      @sharing_obj = create(:sharing_type,sharing_object_id:hive.id,user_id: user.id)
      xhr :put, :sharing_type, id: hive.id, sharing_type: { sharing_type: 'public' }, :format => :js
    end
    it 'updates share_type' do
      expect(@sharing_obj.reload.sharing_type).to eq 'public'
    end
  end
end
