module TipHiveObject
  extend ActiveSupport::Concern

  def class_name
    self.class.name
  end

  def kind_id
    "#{self.class.name}-#{id}"
  end
end
