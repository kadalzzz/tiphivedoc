class Comment < ActiveRecord::Base
  include Tenant
  acts_as_nested_set scope: [:commentable_id, :commentable_type]
  include PublicActivity::Model
  tracked owner: Proc.new { |controller, _model| controller.current_user if controller }

  validates :body, presence: true
  validates :user, presence: true

  # NOTE: install the acts_as_votable plugin if you
  # want user to vote on the quality of comments.
  acts_as_votable

  belongs_to :commentable, polymorphic: true, counter_cache: true

  # NOTE: Comments belong to a user
  belongs_to :user

  delegate :first_name, to: :user, prefix: true
  delegate :email, to: :user, prefix: true

  has_many :pictures, as: :imageable, dependent: :destroy
  attr_accessor :picture_ids

  # Helper class method to lookup all comments assigned
  # to all commentable types for a given user.
  # Shashi: commented it because we did not use it
  # scope :find_comments_by_user, lambda { |user|
  #   where(:user_id => user.id).order('created_at DESC')
  # }

  # Helper class method to look up all comments for
  # commentable class name and commentable id.
  # Shashi: commented it because we did not use it
  # scope :find_comments_for_commentable, lambda { |commentable_str, commentable_id|
  #   where(:commentable_type => commentable_str.to_s, :commentable_id => commentable_id).order('created_at DESC')
  # }


  # Callbacks
  after_destroy do |_comment|
    if commentable && commentable = self.commentable
      self.commentable.comments_count = commentable.comments_count - 1
      self.commentable.save
    end
  end

  include CommentObserver

  # Helper class method that allows you to build a comment
  # by passing a commentable object, a user_id, and comment text
  # example in readme
  def self.build_from(obj, user_id, comment)
    new \
      commentable: obj,
      body:        comment['body'],
      user_id:     user_id,
      longitude:   comment['longitude'],
      latitude:    comment['latitude'],
      address:     comment['address'],
      location:    comment['location'],
      picture_ids: comment['picture_ids']
  end

  # helper method to check if a comment has children
  def has_children?
    children.any?
  end

  # returns latitude and longitude
  def has_map?
    latitude && longitude
  end

  # Helper class method to look up a commentable object
  # given the commentable class name and id
  def self.find_commentable(commentable_str, commentable_id)
    commentable_str.constantize.find(commentable_id)
  end
end
