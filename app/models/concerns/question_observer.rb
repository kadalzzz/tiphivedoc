module QuestionObserver
  include Twitter::Extractor
  extend ActiveSupport::Concern

  included do
    after_save :add_and_count_pictures
  end

  def notify_friend(friend)
    return if user_id == friend.id
    return if friend.settings(:email_notifications).friend_adds_question == false

    user = self.user

    email_params = {
      user_email:                       user.email,
      user_first_name:                  user.first_name,
      user_name:                        user.name,
      friend_email:                     friend.email,
      friend_first_name:                friend.first_name,
      friend_name:                      friend.name,
      is_shared:                        friend.following?(self),
      question_slug:                    to_param,
      question_id:                      id,
      question_hives_and_pockets_title: hives_and_pockets_title,
      access_key:                       access_key,
      domain_id:                        domain_id
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{user.name} is sending email to #{friend.email}"
    )
    ## DEBUG LOG END

    EmailSendingWorker.perform_async('UserMailer', 'add_question_email', email_params)
  end

  def notify_friends
    user             = self.user
    follower_friends = [users].flatten.uniq - [user]
    follower_friends.each do |friend|
      notify_friend(friend) if friend.settings(:email_notifications).friend_adds_question == true
    end
  end

  def notify_mentioned_friend(recipient, email_template = 'user_mention_email_for_question')
    user = self.user

    email_params = {
      user_email:                       user.email,
      user_first_name:                  user.first_name,
      user_name:                        user.name,
      friend_email:                     recipient.email,
      friend_first_name:                recipient.first_name,
      friend_name:                      recipient.name,
      question_slug:                    to_param,
      question_title:                   title,
      question_id:                      id,
      question_hives_and_pockets_title: hives_and_pockets_title
    }
    EmailSendingWorker.perform_async('UserMailer', email_template, email_params)
  end

  def notify_mentioned_friends
    user     = self.user
    mentions = extract_mentioned_screen_names(name)
    mentions.each do |username|
      mentioned_user = User.where(username: username).first
      if mentioned_user && mentioned_user.email != user.email
        notify_mentioned_friend(mentioned_user)
      end
    end
  end

  def notify_question_liked(friend)
    return if friend.id == user_id
    user = self.user
    return if user.settings(:email_notifications).someone_likes_question == false

    email_params = {
      user_email:                       user.email,
      user_first_name:                  user.first_name,
      friend_name:                      friend.name,
      question_title:                   name,
      question_slug:                    to_param,
      question_id:                      id,
      question_hives_and_pockets_title: hives_and_pockets_title,
      domain_id:                        domain_id
    }

    EmailSendingWorker.perform_async('UserMailer', 'like_question_email', email_params)
  end

  def add_and_count_pictures
    if picture_ids.present? && picture_ids.size > 0
      question_pictures = Picture.where('id IN (?)', picture_ids.split(','))
      question_pictures.each do |question_picture|
        question_picture.imageable = self
        question_picture.save
      end

      update_column(:pictures_count, pictures.count)
    end
  end

  def flag_a_question(user)
    email_params = {
      question_slug:   to_param,
      user_first_name: user.first_name,
      question_title:  title
    }
    EmailSendingWorker.perform_async('UserMailer', 'flag_question_email', email_params)
  end
end
