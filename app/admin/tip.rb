ActiveAdmin.register Tip do


  # See permitted parameters documentation:
  # https://github.com/gregbell/active_admin/blob/master/docs/2-resource-customization.md#setting-up-strong-parameters
  #
  # permit_params :list, :of, :attributes, :on, :model
  #
  # or
  #
  # permit_params do
  #  permitted = [:permitted, :attributes]
  #  permitted << :other if resource.something?
  #  permitted
  # end

  config.sort_order = "id_desc"
  config.per_page = 10

  controller do
    def scoped_collection
      resource_class.includes(:user) # prevents N+1 queries to database
    end
  end

  index do
    selectable_column
    column :id, sortable: :id do |tip|
      link_to tip.id, admin_tip_path(tip)
    end
    column :title
    column :description
    column :is_public
    column :longitude
    column :latitude
    column :address
    column :location

    column :user do |tip|
      user = tip.user
      link_to user.name, admin_user_path(user) if user.present?
    end
    column :created_at
    actions
  end

  filter :title
  filter :description
  filter :is_public
  filter :address
  filter :location
  filter :user
  filter :created_at

end
