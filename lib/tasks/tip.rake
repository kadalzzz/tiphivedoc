namespace :tip do
  desc "Set Tip Color"
  task assign_color: :environment do
    Tip.find_each do |t|
      t.update_attribute(:color, Tip::COLOR[rand(Tip::COLOR.length)])
    end
  end
end
