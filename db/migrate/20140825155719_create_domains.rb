class CreateDomains < ActiveRecord::Migration
  def change
    create_table :domains do |t|
      t.string :name
      t.string :logo
      t.string :background
      t.boolean :active
      t.references :user, index: true

      t.timestamps
    end
  end
end
