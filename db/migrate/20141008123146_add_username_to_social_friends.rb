class AddUsernameToSocialFriends < ActiveRecord::Migration
  def change
    add_column :social_friends, :username, :string
  end
end
