class Api::V1::CommentsController < CommentsController
  before_filter :authenticate_user_from_token!, only: [:index]

  def index
    if params[:tip_id]
      tip       = Tip.find(params[:tip_id])
      @comments = tip.comment_threads
    elsif params[:question_id]
      question  = Question.find(params[:question_id])
      @comments = question.comment_threads
    else
      @comments = Comment.all
    end
  end
end
