class InvitationRequest < ActiveRecord::Base
  belongs_to :user
  belongs_to :hive
  belongs_to :tip
  belongs_to :pocket
  belongs_to :group
  belongs_to :domain

  include InvitationRequestObserver
  include Tenant
  validates :email, presence: true

  paginates_per 10

  # Shashi: assign current_domain to user model
  cattr_accessor :current_domain

  def connectable
    connectable_type.constantize.where(id: connectable_id).first
  end

  # retrun non-friend requested object
  def connectable_object
    return if sharing_type.nil?
    sharing_type.constantize.where(id: sharing_id).first
  end

  # send invitation request to user who is not friend of current user
  def self.send_invitation_request(root_obj, sender, receiver)
    # If user is not friend of current user then current user follow to user
    sender.follow(receiver) if %w(Group Hive Pocket Question Tip).include?(root_obj.class.name)
    # before sending request user should be follow active domain
    receiver.follow(current_domain) unless current_domain.root?

    invitation_request_options = {
      email:            sender.email,
      user_id:          receiver.id,
      request_token:    Digest::SHA1.hexdigest(Time.now.to_s),
      connectable_type: receiver.class.name,
      connectable_id:   receiver.id,
      sharing_type:     root_obj.class.name,
      sharing_id:       root_obj.id
    }

    find_or_create_by(invitation_request_options)
  end
end
