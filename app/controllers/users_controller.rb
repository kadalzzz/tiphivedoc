class UsersController < ApplicationController
  #http_basic_authenticate_with name: "tiphive", password: "export", only: [:index]
  include TipHiveHelper

  before_filter :authenticate_user_from_token!, except: [:show, :user_count_element]
  before_filter :authenticate_user!, except: [:show, :system_invitation]
  before_filter :define_friends, only: [:my_friends, :people]
  before_filter :define_not_friends, only: [:meet_people]

  # GET /users
  def index
    add_breadcrumb 'My Hives', my_hives_path, data: { remote: true }, class: 'active-transition'

    if params[:hive_id] && params[:pocket_id]
      @hive = Hive.find params[:hive_id]
      add_breadcrumb "#{@hive.title}", '#', class: 'no-link'
      add_breadcrumb 'All Pockets', '#', id: 'all_pocket'
      @pocket      = Pocket.find params[:pocket_id]
      user_hives   = @hive.user_followers.ids
      user_pockets = @pocket.user_followers.ids
      user_ids     = user_hives & user_pockets
      @users       = User.where(id: user_ids)
    elsif params[:hive_id]
      @hive = Hive.find params[:hive_id]
      add_breadcrumb "#{@hive.title}", '#', class: 'no-link'
      add_breadcrumb 'All Pockets', '#', id: 'all_pocket'
      @users = @hive.user_followers
    else
      @users = User.all
    end

    @users = @users.page(params[:page]).per(params[:per])

    respond_to do |format|
      format.html {}
      format.js {}
      format.json {}
    end
  end

  # GET /select_avatar
  def select_avatar
    @redirect_to = user_root_path

    if session[:user_status] && session[:user_status] == 'just_signup'
      following_objects = []
      following_objects << current_user.following_groups.to_a
      following_objects << current_user.following_hives.to_a
      following_objects = following_objects.flatten

      if following_objects.count > 0
        following_object = following_objects.first
        if following_object.present? && %w(Hive Group).include?(following_object.class.name)
          @redirect_to = url_for(following_object)
        end
      end
    end
  end

  # POST,PATCH,PUT /upload_avatar.json
  def upload_avatar
    current_user.avatar = params[:files].first
    current_user.save

    render json: { avatar_url: current_user.avatar.medium.url, status: :success }
  end

  # GET /status_with/1.json
  def status_with
    @user        = User.find(params[:user_id])
    relationship = current_user.status_with(@user)

    respond_to do |format|
      format.json { render json: { response: { relationship: relationship }, message: relationship, status: :success } }
    end
  end

  # POST /add_as_friend/1.json
  def add_as_friend
    @user = User.find(params[:user_id])
    if @user.nil? || current_user.id == @user.id
      render json: { message: 'No invitable user found', status: :fail }
      return
    end

    current_user.follow @user

    @user.follow(current_domain) unless current_domain.root?

    request_token = Digest::SHA1.hexdigest(
      "#{current_user.email}-#{@user.id}-#{Time.now.to_s}"
    )

    invitation_request_options = {
      email:            current_user.email,
      user_id:          @user.id,
      request_token:    request_token,
      connectable_type: @user.class.name,
      connectable_id:   @user.id
    }

    @invitation_request = InvitationRequest.new(invitation_request_options)
    if @invitation_request.save
      respond_to do |format|
        format.js {}
        format.json { render json: { message: 'Friend added', status: :success } }
      end
    end
  end

  # POST /add_as_friends
  def add_as_friends
    if params[:user_ids].blank? || params[:user_ids].class.name != 'Array'
      render json: { message: 'Need user_ids parameter array', status: :fail }
      return
    end

    user_ids = params[:user_ids] - [current_user.id.to_s]
    @users   = User.find(user_ids)

    if @users.blank?
      render json: { message: "No invitable users found for user_ids=#{params[:user_ids].to_s}", status: :fail }
      return
    end

    @invitations = []
    @users.each do |user|
      current_user.follow user

      request_token = Digest::SHA1.hexdigest(
        "#{current_user.email}-#{user.id}-#{Time.now.to_s}"
      )

      invitation_request_options = {
        email:            current_user.email,
        user_id:          user.id,
        request_token:    request_token,
        connectable_type: user.class.name,
        connectable_id:   user.id
      }

      invitation_request = InvitationRequest.new(invitation_request_options)
      if invitation_request.save
        @invitations << invitation_request
      end
    end

    respond_to do |format|
      format.js {}
      format.json { render json: { message: 'Friends added', status: :success } }
    end
  end

  # POST /unfriend.json
  def unfriend
    @user = User.find(params[:user_id])
    if @user.nil? || current_user.id == @user.id
      render json: { message: 'User not found', status: :fail }
      return
    end
    # Check if user unfriend request is from Domain or Root
    if params[:user_type].present? && params[:user_type] == 'domain user'
      # Domain: stop user to following Domain
      if params[:domain_id].present?
        @domain = current_user.domains.find(params[:domain_id])
        @user.stop_following(@domain)
      end
    else
      # Root: current user stop following user
      current_user.stop_following(@user)
    end

    @invitation_request = current_user.invitation_requests.where(connectable_type: 'User', connectable_id: @user.id)

    respond_to do |format|
      format.js {}
      format.json { render json: { message: 'Friend removed', status: :success } }
    end
  end

  # GET /my_friends
  # GET /my_friends.json
  def my_friends
    @friends_count = current_user.friends.count
    @friends_az    = {}
    ('A'..'Z').each do |char|
      char_friends      = {
        count:   User.where(id: current_user.friends.map(&:id)).where("first_name LIKE '#{char}%'").count,
        friends: User.where(id: current_user.friends.map(&:id)).where("first_name LIKE '#{char}%'").order(:first_name)
      }
      @friends_az[char] = char_friends
    end
    @invitation_requests = current_user.invitation_requests.where(['connectable_type = ?', 'User']).order('created_at DESC').page(params[:page]).per(params[:per])

    respond_to do |format|
      format.html {}
      format.js {}
      format.json {}
    end
  end

  # GET /add_friends
  def add_friends
    params[:auth] = 'email' if params[:auth].blank?

    @contacts           = []
    @contacts_paginated = []

    if current_user.social_friends.present?
      if params[:auth] == 'facebook'
        @contacts           = current_user.social_friends.where(provider: 'facebook')
        @contacts_paginated = @contacts # Kaminari.paginate_array(@contacts).page(params[:page]).per(10)

        @contacts_paginated.each do |contact|
          # logger.info contact.inspect
          identity = Identity.where(uid: contact['id'], provider: 'facebook').first
          if identity && identity.user
            user                = identity.user
            contact['user_id']  = user.id
            contact['username'] = user.username
          end
        end
      end

      if params[:auth] == 'google_oauth2'
        @contacts           = current_user.social_friends.where(provider: 'google_oauth2')
        @contacts_paginated = @contacts # Kaminari.paginate_array(@contacts).page(params[:page]).per(10)

        @contacts_paginated.each do |contact|
          # logger.info contact.inspect
          user = User.where(email: contact.email).first
          if user.present?
            contact['user_id']  = user.id
            contact['username'] = user.username
          end
        end
      end
    end

    if params[:state] && params[:type] == 'facebook'
      friends_email = current_user.collect_social_friends_email(params[:type])
      params.update(connectable_type: current_user.class.name, connectable_id: current_user.id, friends: { emails: friends_email })
    end

    if params[:state] && params[:type] == 'google_oauth2'
      friends_email = current_user.collect_social_friends_email(params[:type])
      params.update(connectable_type: current_user.class.name, connectable_id: current_user.id, friends: { emails: friends_email })
    end

    if request.method == 'POST' && params[:friends].present?
      if params[:friends][:emails].blank?
        respond_to do |format|
          format.html {}
          format.js {}
          format.json { render json: { message: 'Need at least one email', status: :fail } }
        end
        return
      end

      friends_emails = params[:friends][:emails].split(',')
      logger.info "*** friends_emails: #{friends_emails}"

      @own_emails             = []
      @invalid_emails         = []
      @already_friends_emails = []
      @existing_users         = []
      @already_invited_emails = []
      @invited_emails         = []
      @not_invited_emails     = []

      friends_emails
        .flatten
        .uniq
        .compact
        .collect(&:to_s)
        .collect(&:strip)
        .collect(&:downcase)
        .each do |email|
        # check for invalid emails
        if email !~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
          @invalid_emails << email
          next
        end

        # check if user is inviting himself
        if email == current_user.email
          @own_emails << email
          next
        end

        # check if they're already friends
        if current_user.friends
             .collect(&:email)
             .collect(&:downcase)
             .include?(email)
          @already_friends_emails << email
          next
        end

        # existing users, no need to invite
        existing_user = User.where(email: email).first
        if existing_user.present?
          @existing_users << existing_user
          next
        end

        # existing invitations, don't invite again
        existing_invitation = FriendInvitation.where(email: email, user_id: current_user.id)
        if existing_invitation.present?
          @already_invited_emails << email
          next
        end

        invitation_token = Digest::SHA1.hexdigest(
          "#{email}-#{current_user.id}-#{params[:connectable_type]}-#{params[:connectable_id]}-#{Time.now.to_s}"
        )

        # main invitation code
        invitation_options = {
          email:            email,
          invitation_token: invitation_token,
          invitation_type:  'friend',
          user_id:          current_user.id,
          connectable_type: params[:connectable_type],
          connectable_id:   params[:connectable_id]
        }

        logger.info "*** invitation_options: #{invitation_options}"

        @friend_invitation = FriendInvitation.new(invitation_options)

        if @friend_invitation.save
          @invited_emails << email
        else
          @not_invited_emails << email
        end
      end

      respond_to do |format|
        format.html {}
        format.js {}
        format.json do
          render json: { response: { invited_emails: @invited_emails, not_invited_emails: @not_invited_emails },
                         message:  'Friends invited', status: :success }
        end
      end
      return
    end

    respond_to do |format|
      format.html {}
      format.js {}
      format.json { render json: { message: 'Seems to be missing friends parameter', status: :fail } }
    end
  end

  def system_invitation
    email = params[:email].to_s.strip

    if email !~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\Z/i
      @error_text = 'Email is invalid'
      return
    end

    @existing_user = User.where(email: email).first
    if @existing_user.present?
      flash[:notice] = 'This email is registered'
      redirect_to new_user_session_path(email: email)
      return
    end

    group = Group.where(title: 'Friends of TipHive').first
    if group.blank?
      @error_text = "We're sorry. TipHive invitation is not opening at the moment. Please try again later. Thank you."
      return
    end

    respond_to do |format|
      format.html { redirect_to request_invite_group_path(group, 'request_invite[email]' => email) }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  def show
    @user                  = User.friendly.find(params[:id])
    params[:social_filter] = self? ? 'my_tips' : 'shared_tips'
    @tips                  = user_signed_in? ? current_user.fetch_tips(@user, params) : {}
    @tips_count            = @user.tips.count
    @questions             = user_signed_in? ? current_user.fetch_questions_v3(@user, params) : {}
    @liked_tips            = Tip.includes([:votes]).where('votes.voter_id = ?', @user.id).references(:votes)
    @friends               = @user.following_users

    respond_to do |format|
      format.html {}
      format.js {}
      format.json {}
    end
  end

  def email_notifications
    if request.method == 'POST' && user_signed_in?
      logger.info "*** updating user notification settings: #{params}"

      params[:value] = false if params[:value] == 'false'
      params[:value] = true if params[:value] == 'true'
      return if ['always', 'daily', 'weekly', 'never', true, false].exclude?(params[:value])

      current_user.settings(:email_notifications).update_attributes! params[:type] => params[:value]
    end
  end

  def settings
    @user = User.friendly.find(params[:id])
    authorize! :update, @user
    @all_groups      = []
    @selected_groups = current_user.administering_groups
  end

  def update_permission
    user = User.friendly.find(params[:id])
    authorize! :update, @user
    (params[:user][:admin] == 'true') ? user.admin! : user.normal!

    respond_to do |format|
      format.html { redirect_to settings_user_path(id: params[:id]) }
      format.js {}
      format.json { render json: { message: 'Permission updated.', status: :success } }
    end
  end

  # Returns all user friends
  def friends
    @user    = User.friendly.find(params[:id])
    @friends = @user.following_users.order(:first_name).page(params[:page]).per(params[:per])
  end

  # Returns groups that the user is administering
  def groups
    @user = User.friendly.find(params[:id])
    authorize! :update, @user
    @groups = @user.following_groups
  end

  # Adds user params[:id] as an admin for group params[:group_id]
  def add_group
    user = User.friendly.find(params[:id])
    authorize! :update, user
    group = Group.find(params[:group_id].split('-').last)
    group.admin_ids << user.id
    group.admin_ids.uniq!
    group.save
    render json: { message: 'OK' }
  end

  # Revoke admin permission of user params[:id] from group params[:group_id]
  def remove_group
    user = User.friendly.find(params[:id])
    authorize! :update, user
    group = Group.unscoped.find(params[:group_id].split('-').last)
    group.admin_ids.delete(user.id)
    group.admin_ids.uniq!
    group.save
    render json: { message: 'OK' }
  end

  def user_count_element
    if params[:user_id].blank?
      render json: { message: 'Need user_id parameter', status: :fail }
      return
    end
    user             = User.find(params[:user_id])
    @tips            = user.following_tips.size
    @questions       = user.following_questions.size
    @liked_tips      = user.votes.up.for_type(Tip).size
    @liked_questions = user.votes.up.for_type(Question).size
    @hives           = user.following_hives.size
    @pockets         = user.following_pockets.size
    @groups          = user.following_groups.size
    @friends         = user.friends.size
    render json: { total_tips:     @tips, liked_tips: @liked_tips, liked_questions: @liked_questions,
                   total_quesions: @questions, total_hive: @hives, total_friends: @friends, total_group: @groups,
                   total_pockets:  @pockets, status: :success }
  end

  def my_friend_waiting_response
    @friends_count = current_user.friends.count
    @friends_az    = {}
    ('A'..'Z').each do |char|
      char_friends      = {
        count:   User.where(id: current_user.friends.map(&:id)).where("first_name LIKE '#{char}%'").count,
        friends: User.where(id: current_user.friends.map(&:id)).where("first_name LIKE '#{char}%'").order(:first_name)
      }
      @friends_az[char] = char_friends
    end
    @invitation_requests     = current_user.invitation_requests
                                 .where(['connectable_type = ?', 'User'])
                                 .order('created_at DESC').page(params[:page]).per(params[:per])
    friends                  = current_user.following_users.order(:first_name)
    waiting_response_friends  = friends.reject { |r| current_user.status_with(r) != :is_following }
    @waiting_response_friends = User.where(id: waiting_response_friends.map(&:id)).page(params[:page]).per(params[:per])

    respond_to do |format|
      format.html {}
      format.js {}
      format.json { render json: { message: 'Seems to be missing friends parameter', status: :fail } }
    end
  end

  def people
    add_breadcrumb 'My Hives', my_hives_path, data: { remote: true }, class: 'active-transition'
    @hives = current_user.hives.limit(3)
    @questions_count = current_user.get_total_user_questions(current_user)
    @tips_count = current_user.get_tipfeed_tips(current_user).count
    @friends_count = current_user.friends.reject { |r| current_user.status_with(r) == :is_following }.count
  end

  def meet_people
    # we don't use A-Z feature for now
  end

  private

  def self?
    current_user == @user
  end

  def define_friends
    if params[:initial].present?
      friends = User.where(id: current_user.friends.map(&:id)).where("first_name LIKE '#{params[:initial]}%'").order(:first_name)
    else
      friends = User.where(id: current_user.friends.map(&:id)).order(:first_name)
    end
    # convert array to active record
    waiting_response_friends = friends.reject { |r| current_user.status_with(r) != :is_following }
    if current_domain.root?
      friends = friends.reject { |r| current_user.status_with(r) == :is_following }
    end
    friends                  = friends.sort_by { |friend| current_user.get_percentage(friend).to_i }.reverse
    @friends                 = User.where(id: friends.map(&:id)).page(params[:page]).per(params[:per])
    @waiting_response_friends = User.where(id: waiting_response_friends.map(&:id)).page(params[:page]).per(params[:per])
  end

  def define_not_friends
    users = current_user.not_friends
    if params[:initial].present?
      users = users.where("first_name LIKE '#{params[:initial]}%'").order(:first_name).page(params[:page]).per(params[:per])
    else
      users = users.sort_by { |friend| current_user.get_percentage(friend).to_i }.reverse
    end

    @users = User.where(id: users.map(&:id)).page(params[:page]).per(params[:per])
  end
end
