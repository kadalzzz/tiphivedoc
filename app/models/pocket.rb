class Pocket < ActiveRecord::Base
  include Tenant
  include PublicActivity::Model
  tracked owner: Proc.new { |controller, _model| controller.current_user if controller }

  acts_as_followable
  acts_as_follower

  validates :title, presence: true

  paginates_per 21

  # extend FriendlyId
  # friendly_id :slug_candidates, use: :slugged
  def to_param
    "#{id} #{name}".parameterize
  end

  belongs_to :user

  mount_uploader :background_image, BackgroundImageUploader
  include PocketObserver
  include TipHiveObject
  include ActsAsShareable
  include ObjectSettingObserver

  has_many :pictures, as: :imageable, dependent: :destroy
  attr_accessor :picture_ids

  searchable do
    text :title, :description
    integer :user_id
    boolean :is_public, :is_on_profile, :allow_add_tip, :allow_friend_share
  end

  def slug_candidates
    [
      [:id, :title]
    ]
  end

  def name
    title
  end

  def hives_title
    "#{hives.map(&:title).join(', ')}"
  end

  def hives
    following_hives
  end

  def tips
    tip_followers
  end

  def users
    user_followers
  end

  def friend_lists
    friend_list_followers
  end

  def groups
    group_followers
  end

  def users_and_groups
    [users.all, groups.all].flatten
  end

  def handle_sharing(shared_objects)
    owner            = user
    all_friends_list = FriendList.find_by_name 'All Friends'
    return if is_private

    if shared_objects.blank?
      update_attributes(is_private: true)
      return
    end

    shared_objects.each do |object|
      class_name = object.class.name

      # when current_user shares Pocket with non-friend, send invitation request to user before sharing Pocket
      if class_name == 'User' && owner.status_with(object) == :is_nothing
        InvitationRequest.send_invitation_request(self, owner, object)
      end

      # Object follow Pocket if object class name Group, FriendList, User (user should be friend of current user)
      if (class_name == 'User' && owner.status_with(obj) == :is_friend) || %w(Group FriendList).include?(class_name)
        object.follow self
      end

      # create Pocket sharing setting for current user
      share(object, owner)

      # If Pocket share to user, create Pocket default setting 'All Friends' for user
      share(all_friends_list, object) if class_name == 'User'
    end
  end
end
