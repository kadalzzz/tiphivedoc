class FriendInvitation < ActiveRecord::Base
  include Tenant

  belongs_to :connectable, -> { unscope(where: :domain_id) }, polymorphic: true
  belongs_to :user

  alias :inviter :user

  delegate(
    :first_name,
    :id,
    :name,
    allow_nil: true,
    prefix:    :inviter,
    to:        :user,
  )

  before_validation :set_domain_to_connectable
  after_create :send_invite_email

  paginates_per 10

  def subdomain
    connectable.is_a?(Domain) ? connectable.name : domain.try(:name)
  end

  protected

  def mailer_method
    type = connectable.try(:class).try(:name).to_s.underscore

    # Special cases for Group
    if connectable.is_a?(Group)
      case invitation_type.to_sym
      when :system then
        type.prepend('system_')
      when :friend
      else
        type = nil
      end
    end

    # Teefan: allow system invitation without any restriction
    type = 'system_invitation' if connectable_id.blank? && invitation_type == 'system'

    FriendInvitationMailer.respond_to?(type.to_sym) ? type.to_sym : nil
  end

  def send_invite_email
    return unless mailer_method.present?

    EmailSendingWorker.perform_async('FriendInvitationMailer', mailer_method, { invitation_id: id })
  end

  def set_domain_to_connectable
    self.domain = connectable if connectable.is_a?(Domain)
  end
end
