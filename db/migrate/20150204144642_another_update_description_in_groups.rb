class AnotherUpdateDescriptionInGroups < ActiveRecord::Migration
  def change
    change_column :groups, :description, :text, null: true
  end
end
