class Picture < ActiveRecord::Base
  include Tenant
  acts_as_taggable_on :users
  belongs_to :imageable, polymorphic: true
  belongs_to :user

  attr_accessor :images

  mount_uploader :image, ImageUploader

  validates :image, presence: true

  def width
    MiniMagick::Image.open(image.path)[:width] rescue nil
  end
end
