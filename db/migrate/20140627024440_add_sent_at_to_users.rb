class AddSentAtToUsers < ActiveRecord::Migration
  def change
    add_column :users, :daily_sent_at, :datetime
    add_column :users, :weekly_sent_at, :datetime
  end
end
