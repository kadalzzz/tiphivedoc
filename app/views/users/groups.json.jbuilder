json.array!(@groups) do |group|
  json.id group.id
  json.name group.title
  json.kind_id "#{group.class.name}-#{group.id}"
  json.class_name group.class.name
end
