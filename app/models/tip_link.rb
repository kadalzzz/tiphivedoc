class TipLink < ActiveRecord::Base
  validates :url, presence: true

  belongs_to :tip, inverse_of: :tip_links
  belongs_to :user, inverse_of: :tip_links

  mount_uploader :avatar, AvatarUploader
end
