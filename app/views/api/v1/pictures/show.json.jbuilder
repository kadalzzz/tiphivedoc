picture ||= @picture

json.response do
  json.id picture.id
  json.title picture.title
  json.image_url picture.image.url
  # json.image picture.image
  json.imageable_id picture.imageable_id
  json.imageable_type picture.imageable_type
  json.user_id picture.user_id
end

json.status :success
