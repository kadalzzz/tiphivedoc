# Simple Role Syntax
# ==================
# Supports bulk-adding hosts to roles, the primary
# server in each group is considered to be the first
# unless any hosts have the primary property set.
# Don't declare `role :all`, it's a meta role
role :app, %w{deploy@beta.tiphive.com}
role :web, %w{deploy@beta.tiphive.com}
role :db,  %w{deploy@beta.tiphive.com}

# Extended Server Syntax
# ======================
# This can be used to drop a more detailed server
# definition into the server list. The second argument
# something that quacks like a hash can be used to set
# extended properties on the server.
server 'beta.tiphive.com', user: 'deploy', roles: %w{web app db}, my_property: :my_value
#server 'beta.tiphive.com', :app, :web, :db, :primary => true

# you can set custom ssh options
# it's possible to pass any option but you need to keep in mind that net/ssh understand limited list of options
# you can see them in [net/ssh documentation](http://net-ssh.github.io/net-ssh/classes/Net/SSH.html#method-c-start)
# set it globally
set :ssh_options, {
  keys: %w(~/.ssh/id_rsa),
  forward_agent: true,
  auth_methods: %w(publickey)
}
# and/or per server
# server 'example.com',
#   user: 'user_name',
#   roles: %w{web app},
#   ssh_options: {
#     user: 'user_name', # overrides user setting above
#     keys: %w(/home/user_name/.ssh/id_rsa),
#     forward_agent: false,
#     auth_methods: %w(publickey password)
#     # password: 'please use keys'
#   }
# setting per server overrides global ssh_options
set :application, 'tiphive.com'
set :branch, ENV['BRANCH'] || 'development'
set :user, 'deploy'
set :group_user, 'apache'
set :group, 'apache'
set :deploy_to, '/var/www/vhosts/beta.tiphive.com/'
set :deploy_via, :remote_cache
set :rails_env, 'beta'
set :use_sudo, false
