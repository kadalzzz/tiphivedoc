tip        ||= @tip
pocket_ids = tip.pockets.pluck(:id)

json.response do
  json.hives tip.hives.map do |hive|
    json.id hive.id
    json.title hive.title
    json.pockets hive.pockets.reject { |p| pocket_ids.exclude?(p.id) }.map do |pocket|
      json.id pocket.id
      json.title pocket.title
    end
  end
end

json.status :success
