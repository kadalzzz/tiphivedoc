class EmailMessage
  def initialize(message, params)
    @user = User.find_by(email: message.from)
    @message = message
    @params = params
  end

  def process(method = :default)
    if method == :default

    elsif method == :tip_reply
      tip = Tip.find_by(access_key: @params['access_key'])
      comment = {}
      comment['body'] = @message.text_part.body.to_s
      Comment.build_from(tip, @user.id, comment).save!
    elsif method == :question_reply
      question = Question.find_by(access_key: @params['access_key'])
      body = @message.text_part.body.to_s
      tip = Tip.create!(title: body.split[0...3].join(' '), description: body,  user: @user, question: question)
      question.following_hives.each do |hive|
        tip.follow hive
      end
      question.following_pockets.each do |pocket|
        tip.follow pocket
      end
    elsif method == :tip
      tip = Tip.create!(title: @message.subject, description: @message.text_part.body.to_s, user: @user, draft: 1)
      @user.follow tip
    end
  end
end
