class CreateIdentities < ActiveRecord::Migration
  def change
    create_table :identities do |t|
      t.string :provider
      t.string :uid
      t.integer :user_id

      t.timestamps
    end

    add_index :identities, :provider
    add_index :identities, :uid
    add_index :identities, :user_id
  end
end
