class AddDefaultsValueToTables < ActiveRecord::Migration
  def change
    change_column :hives, :is_public, :boolean, default: false
    change_column :hives, :sharing_type, :string, default: 'select_friends'
    change_column :tips, :sharing_type, :string, default: 'select_friends'
    change_column :tips, :is_public, :boolean, default: false
    add_column :questions, :is_public, :boolean, default: false
  end
end
