module ActsAsFlagable
  extend ActiveSupport::Concern

  included do
    has_many :flags, as: :flagable, dependent: :destroy
  end

  def create_flagable_recorded(user, reason)
    flag_params = {
      flagable_type: self.class.name,
      flagable_id:   id,
      flag_id:       user.id,
      flag_type:     user.class.name,
      reason:        reason
    }

    flag = Flag.new(flag_params)
    if flag.save!
      # notify_admin(user, self)
    end
  end

  def notify_admin(user, flagable)
    email_params = {
      tip_slug:        to_param,
      user_first_name: user.first_name,
      tip_title:       flagable.title
    }

    EmailSendingWorker.perform_async('UserMailer', 'flag_tip_email', email_params)
  end
end
