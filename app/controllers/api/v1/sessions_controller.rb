class Api::V1::SessionsController < ApplicationController
  def create
    if params[:user].blank?
      render json: { response: { error: 'User parameter could not be blank' },
                     status:   :fail, message: 'User parameter could not be blank' }
      return
    end

    if params[:user][:email].blank? || params[:user][:password].blank?
      render json: { response: { error: 'Email or password could not be blank' },
                     status:   :fail, message: 'Email or password could not be blank' }
      return
    end

    @user = User.find_for_database_authentication(email: params[:user][:email])

    if @user.blank?
      render json: { response: { error: 'Account not found' }, status: :fail, message: 'Account not found' }
      return
    end

    unless @user.valid_password?(params[:user][:password])
      render json: { response: { error: 'Incorrect password' }, status: :fail, message: 'Incorrect password' }
      return
    end

    allow_params_authentication!
    @user = warden.authenticate!(scope: :user)
    sign_in @user, store: true

    @user.ensure_authentication_token
    @user.save

    if !current_domain.root? && current_domain.active?
      status = current_domain.status_with(current_user) if user_signed_in?
      if %w(is_member is_me).include?(status.to_s)
        render template: 'api/v1/users/me'
      else
        redirect_to(root_url(subdomain: nil), notice: "You don't have access for this domain.")
      end
    else
      render template: 'api/v1/users/me'
    end
  end

  def destroy
    sign_out :user
    render json: { response: 'Signed out', status: :success, message: 'Signed out' }
  end
end
