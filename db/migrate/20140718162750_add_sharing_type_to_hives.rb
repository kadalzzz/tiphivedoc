class AddSharingTypeToHives < ActiveRecord::Migration
  def change
    add_column :hives, :sharing_type, :string, default: 'all_friends'
    add_index :hives, :sharing_type
  end
end
