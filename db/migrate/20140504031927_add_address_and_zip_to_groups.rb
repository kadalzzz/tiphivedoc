class AddAddressAndZipToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :address, :string
    add_column :groups, :location, :string
    add_column :groups, :zip, :string
  end
end
