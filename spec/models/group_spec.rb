require 'rails_helper'

describe Group do
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:pictures) }

  let(:group) { create(:group) }

  describe '#name' do
    it 'returns title' do
      expect(group.name).to eq group.title
    end
  end

  describe '#to_param' do
    it 'returns combination of id and name' do
      expect(group.to_param).to eq "#{group.id}-#{group.name.downcase}"
    end
  end

end
