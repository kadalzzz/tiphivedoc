class Identity < ActiveRecord::Base
  belongs_to :user

  validates :provider, presence: true
  validates :uid, presence: true

  def self.find_with_omniauth(auth)
    find_or_create_by provider: auth.provider, uid: auth.uid
  end

  def self.create_with_omniauth(auth)
    create(provider: auth.provider, uid: auth.uid)
  end
end
