share_users  ||= @selected_friends
share_groups ||= @selected_groups
hive         ||= @hive

json.response do
  json.is_share !hive.is_private
  json.users do
    json.array!(share_users) do |share_object|
      json.partial! 'api/v1/hives/share_object', share_object: share_object
    end

  end
  json.groups do
    json.array!(share_groups) do |share_group|
      json.partial! 'api/v1/hives/share_group', share_group: share_group
    end
  end
end

json.status :success
