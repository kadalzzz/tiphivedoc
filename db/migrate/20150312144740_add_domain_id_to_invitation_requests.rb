class AddDomainIdToInvitationRequests < ActiveRecord::Migration
  def change
    add_column :invitation_requests, :domain_id, :integer
  end
end
