class AddLocationToTips < ActiveRecord::Migration
  def change
    add_column :tips, :address, :string
    add_column :tips, :latitude, :string
    add_column :tips, :longitude, :string
  end
end
