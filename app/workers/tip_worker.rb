class TipWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform
    default_setting = TiphiveSetting.where(name: 'Default').first_or_create
    return unless default_setting.settings(:emails).emails_sending_activated

    User.find_in_batches(batch_size: 100) do |user|
      questions = []
      hives = user.hives
      hives.each do |hive|
        questions = hive.questions.library_items.order(:created_at).reverse_order.not_sent
      end
      UserMailer.daily_tip_email(user, questions)
      questions.each do |question|
        question.sent_at = Time.now
        question.save
      end
    end
  end
end
