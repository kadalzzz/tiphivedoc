class Api::V1::TipsController < TipsController
  def index
    super
    if params[:user_id].present?
      @user = User.find(params[:user_id])
      @tips = get_tips_for_user(@user)
    end
  end

  private

  def get_tips_for_user(user)
    base_user_all_tip_ids = user.tips.ids
    # Get all current user following tips that belong to base user
    tip_ids               = current_user.following_tips.where(id: base_user_all_tip_ids).ids
    # Get shared all friends tips of base user where tip owner is in friend list
    tip_ids               += user.tips.where(shared_all_friends: true).where(user_id: current_user.friend_ids)
    get_tips(tip_ids)
  end

  def get_tips(tip_ids)
    tips = Tip.includes(:user).where(id: tip_ids.flatten.uniq)
    tips.page(params[:page]).per(params[:per])
  end
end
