tips ||= @tips

json.response do
  json.array!(tips) do |tip|
    json.partial! 'api/v1/tips/tip', tip: tip
  end
end

json.status :success
