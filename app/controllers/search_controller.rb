class SearchController < ApplicationController
  include TipHiveHelper

  # GET /search
  # GET /search.json
  def index
    params[:type] = 'Tip' if params[:type].blank?

    search_classes = [User, Group, Hive, Tip, Question]
    search_classes = [Tip] if params[:type] == 'Tip'
    search_classes = [Hive] if params[:type] == 'Hive'
    search_classes = [User] if params[:type] == 'User'
    search_classes = [Group] if params[:type] == 'Group'
    search_classes = [Question] if params[:type] == 'Question'

    search_objects_and_render(search_classes, params[:q])
  end

  # GET /search_users
  # GET /search_users.json
  def users
    search_objects_and_render([User], params[:q])
  end

  # GET /search_groups
  # GET /search_groups.json
  def groups
    search_objects_and_render([Group], params[:q])
  end

  # TODO: may need to refactor
  def invitable_groups
    params[:type] = 'Group'

    @search = Group.solr_search do
      fulltext(params[:q]) do
        if params[:fields].present?
          search_fields = params[:fields].split(',')
          search_fields.each do |search_field|
            fields(search_field)
          end
        end
        fields params[:field] if params[:field].present?
      end if params[:q].present?

      with :group_type, params[:group_type] if %w(university enterprise event).include?(params[:group_type])

      current_page = params[:page].to_i > 0 ? params[:page].to_i : 1
      per_page     = params[:per].to_i > 0 ? params[:per].to_i : 6
      paginate(page: current_page, per_page: per_page)
    end

    @results = @search.results

    render template: 'search/index'
  end

  # GET /search_users_and_groups
  # GET /search_users_and_groups.json
  def users_and_groups
    search_objects_and_render([User, Group], params[:q])
  end

  # GET /search_hives
  # GET /search_hives.json
  def hives
    search_objects_and_render([Hive], params[:q])
  end

  # GET /search_pockets
  # GET /search_pockets.json
  def pockets
    if params[:hives].include?('null')
      search_objects_and_render([Pocket], params[:q])
    else
      hives          = params[:hives][0].split(',')
      selected_hives = collect_items(items_list: hives)
      @results       = Hive.search_pocket(selected_hives, params[:q])
      render template: 'search/index'
    end
  end

  # GET /search_tips
  # GET /search_tips.json
  def tips
    search_objects_and_render([Tip], params[:q])
  end

  # GET /user_following_friends_and_groups
  # GET /user_following_friends_and_groups.json
  def user_following_friends_and_groups
    params[:followable_types] = 'User,Group'
    params[:followable_types] = params[:only] if params[:only].present?
    user_following_connections_and_render(params)
  end

  # GET /user_following_friends
  # GET /user_following_friends.json
  def user_following_friends
    params[:followable_type] = 'User'
    user_following_connections_and_render(params)
  end

  # GET /user_following_groups
  # GET /user_following_groups.json
  def user_following_groups
    params[:followable_type] = 'Group'
    user_following_connections_and_render(params)
  end

  # GET /user_following_hives
  # GET /user_following_hives.json
  def user_following_hives
    params[:followable_type] = 'Hive'
    user_following_connections_and_render(params)
  end

  # GET /user_following_pockets
  # GET /user_following_pockets.json
  def user_following_pockets
    params[:followable_type] = 'Pocket'
    user_following_connections_and_render(params)
  end

  # GET /user_following_tips
  # GET /user_following_tips.json
  def user_following_tips
    params[:followable_type] = 'Tip'
    user_following_connections_and_render(params)
  end

  # GET /search_connections
  # GET /search_connections.json
  def user_following_connections_and_render(params)
    @results = []
    @results += user_following_connections(params)
    @results += search_objects([User], params[:q]).results - [current_user] if params[:q].present?

    if params[:followable_types] == 'User,Group' || params[:followable_type] == 'User'
      @results += FriendList.all if params[:only].blank? && params[:type] != 'without_friend_lists'
    end

    render template: 'search/connections'
  end

  def mentions
    params[:field] = :username
    @search        = search_objects([User], params[:q])
    @results       = @search.results

    respond_to do |format|
      format.json { render json: @results.map(&:username).reject(&:blank?) }
    end
  end

  # GET /share_type_permission
  # GET /share_type_permission.json
  def share_type_permission
    @result        = []
    hives          = params[:hives][0].split(',')
    selected_hives = collect_items(items_list: hives)
    @result        = Hive.get_hive_sharing_permission(selected_hives, current_user) if current_user
    render template: 'search/share_types'
  end

  # GET /hives_following_users_and_groups
  # GET /hives_following_users_and_groups.js
  # if hive permission is selected friends/groups
  def hives_following_users_and_groups
    @object = Tip.new if params[:object].include? ('Tip')
    @object = Question.new if params[:object].include? ('Question')
    @object = Pocket.new if params[:object].include? ('Pocket')
    @object = collect_items(items_list: params[:object]).first if params[:edit]

    hives          = params[:hives][0].split(',')
    selected_hives = collect_items(items_list: hives)
    @selected_friends_and_groups = []
    selected_hives.each do |hive|
      shared_objects = hive.get_shared_objects(current_user)
      next unless shared_objects
      @selected_friends_and_groups << (shared_objects - [current_user])
    end
    @selected_friends_and_groups = @selected_friends_and_groups.flatten

    if params[:edit].present?
      # Disconnect existing followers
      @tips_followers = []
      @tips_followers << @object.users
      @tips_followers << @object.groups
      @tips_followers << @object.friend_lists

      @tips_followers.flatten.each do |item|
        item.stop_following @object
        # when item is FriendList class object then update #{@object} attributes  
        item.update_object_sharing_status_false(@object) if item.class.name == 'FriendList'
      end

      # Connected new followers
      if @object.is_private == false
        @selected_friends_and_groups.each do |item|
          item.follow @object
          # when item is FriendList class object then update #{@object} attributes 
          item.update_object_sharing_status_true(@object) if item.class.name == 'FriendList'
        end
      end
    end
  end

  def social_friends
    if params[:q] && params[:type]
      if params[:type] == 'facebook'
        @results = current_user.social_friends.where('name like ? AND provider = ?', "%#{params[:q]}%", params[:type])
      elsif params[:type] == 'google_oauth2'
        @results = current_user.social_friends.where('email like ? AND provider = ?', "%#{params[:q]}%", params[:type])
      end
      render template: 'search/social_friends'
    else
      @contacts = current_user.social_friends.where(uid: params[:uid]) if params[:uid]
      @contacts = current_user.social_friends.where(email: params[:email]) if params[:email]
      respond_to do |format|
        format.js
      end
    end
  end

  private
  def search_objects_and_render(object_classes, query = '')
    @search  = search_objects(object_classes, query)
    @results = @search.results
    if api?
      render template: 'api/v1/search/index'
    else
      render template: 'search/index'
    end
  end
end
