source 'https://rubygems.org'
ruby '2.1.3'

gem 'dotenv-rails'

# Bundle edge Rails instead: gem 'rails', github: 'rails/rails'
gem 'rails', '4.2.1'
gem 'rails-observers'
gem 'default_value_for', '~> 3.0.0'
gem 'bitmask_attributes'
gem 'activerecord-session_store'
gem 'public_activity'
gem 'activeadmin', github: 'gregbell/active_admin' # Admin interface
gem 'impressionist' # impressions or views count
gem 'ledermann-rails-settings', require: 'rails-settings'
gem 'friendly_id', '~> 5.0.0'
gem 'kaminari' # for pagination
gem 'kaminari-bootstrap', '~> 3.0.1'
gem 'ffi', '1.9.5'

# External connections and HTML parsing
gem 'open_uri_redirections'
gem 'nokogiri'
gem 'httparty'
gem 'rest-client'
gem 'addressable'
gem 'link_thumbnailer'
gem 'twitter-text'
gem 'url_link'
gem 'fastimage'
# gem 'gemoji' #, github: 'github/gemoji'
# gem 'rumoji', github: 'mwunsch/rumoji'
gem 'sanitize'

# Search engine
gem 'progress_bar'
gem 'sunspot_rails'
gem 'sunspot_solr'

# Objects connection
gem 'awesome_nested_set', '~> 3.0.0.rc.3'
gem 'acts_as_follower'
gem 'acts_as_votable', '~> 0.8.0'
gem 'acts_as_commentable_with_threading'
gem 'acts-as-taggable-on'

# Message and Notification
gem 'unread'
gem 'mailboxer', github: 'lacco/mailboxer'

# CDN Storage
gem 'unf'
gem 'fog'
gem 'asset_sync'
gem 'yaml_db'

# File upload
gem 'carrierwave'
gem 'carrierwave_backgrounder'
gem 'jquery-fileupload-rails'
gem 'mini_magick'
gem 'rmagick', '2.13.2'

# Geocoding
gem 'geocoder'
gem 'gmaps4rails'

# Use mysql as the database for Active Record
gem 'mysql2'

# Authentication
gem 'certified'
gem 'devise'
gem 'devise_invitable'
gem 'devise-async'
gem 'omniauth-facebook'
gem 'omniauth-twitter'
gem 'omniauth-google-oauth2'
gem 'google-api-client'
gem 'doorkeeper'
gem 'oauth2'

# Authorization
gem 'cancancan'

# Use SCSS for stylesheets
gem 'sass-rails', '~> 4.0.0'
# gem 'bootstrap-sass', '~> 3.1.1'
# gem 'font-awesome-sass'
gem 'compass-rails'
gem 'haml-rails'
gem 'premailer-rails' # Inline CSS for email

# Use Uglifier as compressor for JavaScript assets
gem 'uglifier', '>= 1.3.0'

# Use CoffeeScript for .js.coffee assets and views
gem 'coffee-rails', '~> 4.0.0'

# See https://github.com/sstephenson/execjs#readme for more supported runtimes
gem 'therubyracer', platforms: :ruby, group: :production

# Use jquery as the JavaScript library
gem 'jquery-rails'
gem 'jquery-ui-rails'
gem 'gon'

gem 'wiselinks'
gem 'pace-rails'
gem 'messengerjs-rails', '~> 1.4.1'

# Build JSON APIs with ease. Read more: https://github.com/rails/jbuilder
gem 'jbuilder', '~> 1.2'

gem 'faker' # generate fake data

# Soft Delete
gem 'paranoia', github: 'ashishbista/paranoia', branch: 'rails4'

group :doc do
  # bundle exec rake doc:rails generates the API under doc/api.
  gem 'sdoc', require: false
end

# Spring speeds up development by keeping your application running in the background. Read more: https://github.com/rails/spring
gem 'spring', group: :development

# Use ActiveModel has_secure_password
gem 'bcrypt', '~> 3.1.5'

# Use unicorn as the app server
gem 'rack-timeout'
# gem 'unicorn'
gem 'thin', group: [:development, :test]
gem 'exception_notification', '~> 4.0.1'
gem 'newrelic_rpm'
gem 'airbrake'
gem 'le'

# Enable Cross-origin resource sharing (CORS)
gem 'rack-cors', require: 'rack/cors'

# Profile Rails app
gem 'rack-mini-profiler'

gem 'multi_json', '~> 1.9.0'
# background processing
gem 'sinatra', '>= 1.4.4', require: nil
gem 'sidekiq'
gem 'sidekiq-unique-jobs'
gem 'whenever', require: false

# Receive emails
gem 'mailman', require: false
gem 'daemons', require: false

# Use Breadcrumbs
gem 'breadcrumbs_on_rails'

group :development do
  # Use Capistrano for deployment
  gem 'capistrano', '~> 3.1'
  gem 'capistrano-rails', '~> 1.1'
  gem 'capistrano-bundler', '~> 1.1.2'
  gem 'capistrano-rvm'
  gem 'capistrano-sidekiq'
  gem 'slackistrano', require: false
  gem 'foreman'
  gem 'meta_request'
  gem 'quiet_assets'
  gem 'colored'
  gem 'better_errors'
  gem 'binding_of_caller'
  gem 'bullet'
  gem 'rails_best_practices'
  gem 'rubocop', require: false
end

group :development, :test do
  gem 'spring-commands-rspec'
  gem 'rspec-rails', '~> 3.0.2'
  gem 'rspec-its', '~> 1.0.1'
  gem 'pry-rails'
  gem 'pry-nav'
  gem 'pry-doc'
  gem 'factory_girl_rails'
  # Call 'byebug' anywhere in the code to stop execution and get a debugger console
  gem 'byebug'
  # Access an IRB console on exception pages or by using <%= console %> in views
  gem 'web-console', '~> 2.0'
end

group :test do
  gem 'shoulda-matchers'
  gem 'shoulda-callback-matchers'
  gem 'simplecov', :require => false
  gem 'timecop'
  gem 'mocha', '~> 1.1.0'
  gem 'cucumber-rails', :require => false
  gem 'pickle'
  # Spreewald is a collection of useful steps for cucumber.
  gem 'spreewald'
  # gem 'selenium-webdriver'
  gem 'capybara-webkit'
  gem 'database_cleaner'
  gem 'capybara'
  gem 'lunchy'
  # gem 'poltergeist'
  gem 'capybara-screenshot'
end
gem 'mixpanel'

# gem 'debugger', group: [:development, :test]
