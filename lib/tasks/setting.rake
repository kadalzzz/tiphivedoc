namespace :setting do
  namespace :email_notifications do
    desc 'Change default email notification to daily'
    task set_daily: :environment do
      User.find_each do |user|
        email_sending_setting = user.settings(:email_notifications).friend_adds_tip

        if email_sending_setting == 'always'
          puts "*** Changed user #{user.email} email setting from Always to Daily"
          user.settings(:email_notifications).friend_adds_tip = 'daily'
          user.save!
        end
      end
    end
  end
end
