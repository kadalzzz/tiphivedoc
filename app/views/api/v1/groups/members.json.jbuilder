members ||= @members

json.response do
  json.array!(members) do |member|
    json.partial! 'api/v1/users/user', user: member
    json.is_friend current_user.following?(member)
  end
end

json.status :success
