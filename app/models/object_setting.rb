class ObjectSetting < ActiveRecord::Base
  include Tenant

  include PublicActivity::Model
  tracked owner: Proc.new { |controller, _model| controller.current_user if controller }

  belongs_to :object_setting, polymorphic: true
  belongs_to :user

  mount_uploader :background_image, BackgroundImageUploader

  searchable do
    string :object_setting_type
    integer :object_setting_id
    integer :user_id
  end

  after_create :object_not_shared_with_anyone
  after_update :object_not_shared_with_anyone

  protected

  # If object settings is private then all tips belongs to this object should be private
  def object_not_shared_with_anyone
    # Disconnect all hive tips for this user
    return unless is_private?

    if object_setting
      tip_ids = object_setting.tips
      tips    = Tip.where(id: tip_ids).by_owner(user)

      share = object_setting.get_shared_objects(user)
      share.each do |item|
        object_setting.stop_sharing(item, user)
        tips.each do |tip|
          tip.update_attributes!(is_private: true)
          item.stop_following tip
        end
      end
    end
  end
end
