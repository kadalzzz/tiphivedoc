class FriendInvitationMailer < ActionMailer::Base

  default from: "TipHive <tiphive@tiphive.com>"
  layout "user_mailer"

  def domain(params)
    @invitation = get_invitation(params['invitation_id']) or return

    mail(
      subject:  "You are invited to join #{@invitation.connectable.full_name}.",
      to:       @invitation.email,
    )
  end

  def group(params)
    @invitation = get_invitation(params['invitation_id']) or return

    mail(
      subject:  "#{@invitation.inviter_first_name} invited you to a group",
      to:       @invitation.email,
    )
  end

  def hive(params)
    @invitation = get_invitation(params['invitation_id']) or return

    mail(
      subject:  "#{@invitation.inviter_first_name} invited you to a hive",
      to:       @invitation.email,
    )
  end

  def system_group(params)
    @invitation = get_invitation(params['invitation_id']) or return

    mail(
      subject:  "You are invited to join #{@invitation.connectable.title}.",
      to:       @invitation.email,
    )
  end

  def system_invitation(params)
    @invitation = get_invitation(params['invitation_id']) or return

    mail(
      subject:  "You are invited to join TipHive.",
      to:       @invitation.email,
    )
  end

  def user(params)
    @invitation = get_invitation(params['invitation_id']) or return

    mail(
      subject:  "Check out #{@invitation.inviter_first_name}'s tips on TipHive",
      to:       @invitation.email,
    )
  end

  protected

  def get_invitation(invitation_id)
    unless invitation = FriendInvitation.unscoped.find_by(id: invitation_id)
      Rails.logger.warn("No FriendInvitation with ID #{invitation_id.inspect}")
    end

    invitation
  end

end
