module InvitationRequestObserver
  extend ActiveSupport::Concern

  included do
    # after_save :notify
    after_create :notify_group_owner
  end

  def notify_group_owner
    logger.info '*** calling notify_group_owner'
    return if connectable_type != 'Group'

    user = self.user

    email_params = {
      user_first_name:   user.first_name,
      user_name:         user.name,
      user_username:     user.username,
      user_email:        user.email,
      friend_email:      email,
      connectable_type:  connectable_type,
      connectable_title: connectable.title
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{user.name} is sending email to #{email}"
    )
    ## DEBUG LOG END

    EmailSendingWorker.perform_async('UserMailer', 'group_owner_request_notification_email', email_params)
    EmailSendingWorker.perform_async('UserMailer', 'group_invite_request_notification_email',
                                     email_params) if connectable.join_type == 'invite'
  end

  def notify_requester
    logger.info '*** calling notify_requester'
  end
end
