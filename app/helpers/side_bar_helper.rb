module SideBarHelper
  def should_render_tipfeed_sidebar?
    return true if params[:controller] == 'tips' && %w(index).include?(params[:action])
    return true if params[:controller] == 'questions' && %w(index).include?(params[:action])
    return false
  end

  def should_render_group_sidebar?
    return true if @group && params[:controller] == 'groups'
    return true if @group && params[:group_id].present?
    return false
  end

  def should_render_hive_sidebar?
    return true if @hive && params[:controller] == 'hives' && %w(show).include?(params[:action])
    return true if @hive && params[:hive_id].present?
    return false
  end

  def should_render_pocket_sidebar?
    return true if @pocket && params[:controller] == 'pockets' && %w(show).include?(params[:action])
    return true if @pocket && params[:pocket_id].present?
    return false
  end
end
