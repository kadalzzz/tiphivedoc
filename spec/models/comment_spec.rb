require 'rails_helper'

describe Comment do
  it { should validate_presence_of(:body) }
  it { should validate_presence_of(:user) }
  it { should belong_to(:commentable) }
  it { should belong_to(:user) }
  it { should belong_to(:domain) }
  it { should have_many(:pictures) }

  describe '#has_children?' do
    let(:comment) { create(:comment) }
    let(:child) { create(:comment) }
    subject { comment.has_children? }
    context 'when has children' do
      before { comment.child_ids = [child.id]; comment.save }
      it { is_expected.to be_truthy }
    end
    context 'when has no children' do
      it { is_expected.to be_falsey }
    end
  end

  describe '#build_from' do
    let(:user) { create(:user) }
    let(:comment) { create(:comment) }
    let(:obj) { create(:tip) }

    subject { Comment.build_from(obj, user.id, comment) }
    it 'returns comment build object' do
      is_expected.to be_truthy
    end
  end

  describe '#has_map?' do
    let(:comment) { create(:comment, latitude: '28.09', longitude: '30.00') }

    subject { comment.has_map? }
    it 'returns latitude and longitude' do
      is_expected.to be_truthy
    end
  end
end
