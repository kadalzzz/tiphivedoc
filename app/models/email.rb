class Email < ActiveRecord::Base
  enum type: [:normal, :primary]

  # Associations
  belongs_to :user

  # Validations
  validates :address, presence: true
  validate :validate_uniqueness_of_address
  validates :address, format: Devise.email_regexp

  private

  def validate_uniqueness_of_address
    if User.find_by(email: address) || Email.find_by(address: address)
      errors.add(:address, 'is already taken')
    end
  end
end
