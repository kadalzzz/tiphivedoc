class ChangeDefaultSettings < ActiveRecord::Migration
  def change
    change_column :hives, :is_public, :boolean, default: true
    change_column :tips, :is_public, :boolean, default: true
    change_column :questions, :is_public, :boolean, default: true
    change_column :object_settings, :is_public, :boolean, default: true
  end
end
