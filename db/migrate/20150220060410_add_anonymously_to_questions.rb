class AddAnonymouslyToQuestions < ActiveRecord::Migration
  def change
    add_column :questions, :anonymously, :boolean, default: false
  end
end
