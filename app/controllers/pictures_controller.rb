class PicturesController < ApplicationController
  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!
  before_action :set_picture, only: [:show, :edit, :update, :destroy]

  # GET /pictures
  # GET /pictures.json
  def index
    @pictures = Picture.all
  end

  # GET /pictures/1
  # GET /pictures/1.json
  def show
    @picture  = Picture.find(params[:id])
    @model    = @picture.imageable_type.constantize.find(@picture.imageable_id)
    @pictures = @model.pictures
  end

  # GET /pictures/new
  def new
    @picture = Picture.new
  end

  # GET /pictures/1/edit
  def edit
    @picture = Picture.find(params[:id])
  end

  # POST /pictures
  # POST /pictures.json
  def create
    file_upload = read_upload_file_from_request

    if file_upload.present?
      params[:picture][:image] = file_upload
    end

    @picture         = Picture.new(picture_params)
    @picture.user_id = current_user.id

    respond_to do |format|
      if @picture.save
        format.html { redirect_to @picture, notice: 'Picture was successfully created.' }
        format.json { render action: 'show' }
      else
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @picture.errors, full_messages: @picture.errors.full_messages },
                         status:   :fail, message: @picture.errors.full_messages.join(', ') }
        end
      end
    end
  end

  # POST /upload_image
  # POST /upload_image.json
  def upload_image
    uploaded_file = read_upload_file_from_request

    @picture = Picture.new(image: uploaded_file, title: params[:title])

    @picture.user_id = current_user.id

    if params[:tip_id].present?
      @picture.imageable_type = 'Tip'
      @picture.imageable_id   = params[:tip_id]
    end

    if @picture.save
      render json: { response: { id: @picture.id, title: @picture.title, image: @picture.image },
                     status:   :success, message: 'File uploaded' }
    else
      render json: { response: { error: @picture.errors, full_messages: @picture.errors.full_messages },
                     status:   :fail, message: @picture.errors.full_messages.join(', ') }
    end
  end

  # POST /upload_background_image
  # POST /upload_background_image.json
  def upload_background_image
    uploaded_file = read_upload_file_from_request

    if params[:group_id].present?
      @object = current_user.groups.find params[:group_id]
    elsif params[:hive_id].present?
      @object = current_user.hives.find params[:hive_id]
    elsif params[:pocket_id].present?
      @object = current_user.pockets.find params[:pocket_id]
    else
      @object = current_user
    end

    if @object.respond_to?(:user_id) && @object.user_id != current_user.id
      message = "You don't have permission to update background image"
      render json: { response: message, status: :fail, message: message }
      return
    end

    @object.background_image = uploaded_file

    if @object.save
      message = "#{@object.class.name} background image updated"
      render json: { response: message, status: :success, message: message }
    else
      render json: { response: { error: @object.errors, full_messages: @object.errors.full_messages },
                     status:   :fail, message: @object.errors.full_messages.join(', ') }
    end
  end

  # POST /upload_avatar_image
  # POST /upload_avatar_image.json
  def upload_avatar_image
    uploaded_file = read_upload_file_from_request

    if params[:group_id].present?
      @object = current_user.groups.find params[:group_id]
    else
      @object = current_user
    end

    if @object.respond_to?(:user_id) && @object.user_id != current_user.id
      message = "You don't have permission to avatar image"
      render json: { response: message, status: :fail, message: message }
      return
    end

    @object.avatar = uploaded_file

    if @object.save
      message = "#{@object.class.name} avatar updated"
      render json: { response: message, status: :success, message: message }
    else
      render json: { response: { error: @object.errors, full_messages: @object.errors.full_messages },
                     status:   :fail, message: @object.errors.full_messages.join(', ') }
    end
  end

  # POST/PATCH/PUT /upload_picture
  # POST/PATCH/PUT /upload_picture.json
  def upload
    @picture = Picture.new(picture_params)

    @picture.user_id = current_user.id

    if params[:tip_id].present?
      @picture.imageable_type = 'Tip'
      @picture.imageable_id   = params[:tip_id]
    end

    respond_to do |format|
      if @picture.save
        format.html { redirect_to @picture, notice: 'Picture was successfully created.' }
        format.json { render action: 'show' }
      else
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @picture.errors, full_messages: @picture.errors.full_messages },
                         status:   :fail, message: @picture.errors.full_messages.join(', ') }
        end
      end
    end
  end

  # PATCH/PUT /pictures/1
  # PATCH/PUT /pictures/1.json
  def update
    file_upload = read_upload_file_from_request

    if file_upload.present?
      params[:picture][:image] = file_upload
    end

    @picture = Picture.find(params[:id])

    respond_to do |format|
      if @picture.update(picture_params)
        format.html { redirect_to @picture, notice: 'Picture was successfully updated.' }
        format.json { render action: 'show' }
      else
        format.html { render action: 'edit' }
        format.json do
          render json: { response: { error: @picture.errors, full_messages: @picture.errors.full_messages },
                         status:   :fail, message: @picture.errors.full_messages.join(', ') }
        end
      end
    end
  end

  # DELETE /pictures/1
  # DELETE /pictures/1.json
  def destroy
    @picture = Picture.find(params[:id])

    @picture.destroy
    message = 'Picture Destroyed'
    respond_to do |format|
      format.html { redirect_to pictures_url }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  def upload_comment_picture
    @picture = Picture.new(picture_params)

    @picture.user_id = current_user.id

    if params[:comment_id].present?
      @picture.imageable_type = 'Comment'
      @picture.imageable_id   = params[:comment_id]
    end

    respond_to do |format|
      if @picture.save
        format.html { redirect_to @picture, notice: 'Picture was successfully created.' }
        format.json { render action: 'show' }
      else
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @picture.errors, full_messages: @picture.errors.full_messages },
                         status:   :fail, message: @picture.errors.full_messages.join(', ') }
        end
      end
    end
  end

  def tag
    @picture_id = params[:picture_id]
    @picture    = Picture.find(@picture_id)
    @model      = @picture.imageable_type.constantize.find(@picture.imageable_id)
    @pictures   = @model.pictures
    @array_tag  = params[:tag].split(',')
    @array_tag.each do |value|
      @picture.user_list << value
      @picture.save
    end
    render :show
  end

  def remove_tag
    @picture  = Picture.find(params[:id])
    @model    = @picture.imageable_type.constantize.find(@picture.imageable_id)
    @pictures = @model.pictures
    @picture.user_list.remove(params[:tag])
    @picture.save
    render :show
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_picture
    @picture = Picture.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def picture_params
    params.require(:picture).permit(:title, :image, :imageable_id, :imageable_type, :user_id)
  end
end
