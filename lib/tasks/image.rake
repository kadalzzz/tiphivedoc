namespace :image do
  desc "Recreate all image versions"
  task recreate_group_hive_pocket_image_versions: :environment do
    Group.find_each do |g|
      g.background_image.recreate_versions! if g.background_image.present?
    end

    Hive.find_each do |h|
      h.background_image.recreate_versions! if h.background_image.present?
    end

    Pocket.find_each do |p|
      p.background_image.recreate_versions! if p.background_image.present?
    end
  end
  
  desc "Recreate Tip image versions"
  task recreate_tip_image_versions: :environment do
    Tip.find_each do |tip|
      tip.pictures.find_each do |pic|
        pic.image.recreate_versions!(:extra_large) if pic.image.present?
      end
    end
  end
  
  desc "Recreate Question image versions"
  task recreate_question_image_versions: :environment do
    Question.find_each do |question|
      question.pictures.find_each do |pic|
        pic.image.recreate_versions!(:extra_large) if pic.image.present?
      end
    end
  end

  desc "Recreate all avatar versions"
  task recreate_user_avatar_versions: :environment do
    User.find_each do |u|
      u.avatar.recreate_versions! if u.avatar.present?
    end
  end

end
