class AddDomainsIndexOnName < ActiveRecord::Migration
  def change
    change_table :domains do |t|
      t.index :name
    end
  end
end
