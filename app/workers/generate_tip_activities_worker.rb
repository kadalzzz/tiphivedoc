class GenerateTipActivitiesWorker
  include Sidekiq::Worker
  sidekiq_options retry: false

  def perform(params)
    logger.info "*** Generate Tip activities: #{params}"

    tip = Tip.where(id: params['tip_id'].to_i).first
    if tip.blank?
      logger.info "*** OH NO, can't find tip id: ##{params['tip_id']}"
      return
    end

    logger.info '*** Start Tip activities generation...'
    tip.generate_tip_activities
  end
end
