class CreateFriendInvitations < ActiveRecord::Migration
  def change
    create_table :friend_invitations do |t|
      t.string :email
      t.string :invitation_token
      t.string :invitation_type
      t.integer :user_id
      t.string :connectable_type
      t.integer :connectable_id

      t.timestamps
    end
    add_index :friend_invitations, :email
    add_index :friend_invitations, :invitation_token
    add_index :friend_invitations, :invitation_type
    add_index :friend_invitations, :user_id
    add_index :friend_invitations, :connectable_type
    add_index :friend_invitations, :connectable_id
  end
end
