module UserObserver
  extend ActiveSupport::Concern

  included do
    after_save :notify
    after_create :process_invitations
    before_create :check_email
  end

  def notify
  end

  def process_invitations
    invitations = FriendInvitation.where(email: email)
    invitations = FriendInvitation.where(email: second_email) if invitations.blank?

    invitations.each do |invitation|
      inviter = invitation.user

      if inviter
        inviter.follow self
        follow inviter
      end

      # connect just joined user to invited connectable object
      if invitation.connectable_type.present? && invitation.connectable_id.present?
        obj_class_name = invitation.connectable_type
        obj_class      = obj_class_name.constantize
        conn_obj       = obj_class.where(id: invitation.connectable_id).first if obj_class

        # Found shared object, going to make connection between User the shared object
        if conn_obj
          if obj_class_name == 'User'
            conn_obj.follow self
            follow conn_obj
          end

          if %w(Hive Group Pocket Tip).include? invitation.connectable_type
            follow conn_obj
          end

          if obj_class_name == 'Hive'
            all_friends_list = FriendList.where(name: 'All Friends').first
            conn_obj.share(all_friends_list, self) if all_friends_list

            # Joost Apr-29: if a user shares a hive with another user,
            # all tips in the hive will be updated to be shared with the user.
            if inviter
              conn_obj.share(inviter, self)
              conn_obj.share(self, inviter)
              tip_ids = conn_obj.tip_followers.where(user_id: inviter.id).ids
              inviter.following_tips.where(id: tip_ids).find_each do |tip|
                follow tip
              end
            end
          end
        end
      end

      invitation.destroy
    end
  end

  def check_email
    self.second_email = nil if email == second_email
  end
end
