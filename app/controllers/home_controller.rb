class HomeController < ApplicationController
  include TipHiveHelper

  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!, except: [:index, :intended_error, :tiphive_protocol_redirect]

  def index
    cookies['where'] = params[:where] if params[:where].present? && %w(school company life).include?(params[:where])
    params[:where]   ||= cookies['where']
    params[:where]   ||= 'school'

    group_type       = 'university' if params[:where] == 'school'
    group_type       = 'enterprise' if params[:where] == 'company'

    @groups = Group.where(group_type: group_type).all

    render layout: 'home'
  end

  def connect_following_objects
    return if params[:id].blank? || %w(following follower).exclude?(params[:type])
    root_object = params[:kind].constantize.find params[:id]
    return if root_object.blank? || root_object.new_record?

    connecting_objects = collect_items(params)
    connecting_objects.each do |obj|
      obj_class_name      = obj.class.name
      root_obj_class_name = root_object.class.name
      # check user status with current_user or Domain
      if (obj_class_name == 'User' || root_obj_class_name == 'Domain') &&
         current_user.status_with(obj) == :is_nothing
        # send invitation request to user who is not friend of current user for #{root_object}
        InvitationRequest.send_invitation_request(root_object, current_user, obj)
      else
        if obj_class_name == 'FriendList' && %w(Tip Question).include?(root_obj_class_name)
          # if #{obj} is FrienList class object then update #{root_object} attributes
          obj.update_object_sharing_status_true(root_object)
        end
        root_object.follow obj if params[:type] == 'follower'
        obj.follow root_object if params[:type] == 'following'

        # sending email
        if params[:type] == 'following'
          return if user_signed_in? && current_user.id == obj.id
          root_object.notify_group_member(obj) if %w(Tip).include?(root_obj_class_name) && obj_class_name == 'Group'
          root_object.notify_friend(obj) if %w(Group Tip).include?(root_obj_class_name) && obj_class_name == 'User'
        end
      end
    end
  end

  def disconnect_following_objects
    return if params[:id].blank? || %w(following follower).exclude?(params[:type])
    root_object = params[:kind].constantize.find params[:id]
    return if root_object.blank? || root_object.new_record?

    connecting_objects = collect_items(params)
    connecting_objects.each do |obj|
      obj_class_name      = obj.class.name
      root_obj_class_name = root_object.class.name

      # Do not allow disconnect from a parent if this is the only parent
      if params[:kind] == 'Pocket' && params[:type] == 'follower'
        return if root_object.following_hives_count == 1 && obj_class_name == 'Hive'
      end

      # Do not allow disconnect from a parent if this is the only parent
      if params[:kind] == 'Tip' && params[:type] == 'follower'
        return if (root_object.following_pockets_count == 1 && obj_class_name == 'Pocket') ||
            (root_object.following_hives_count == 1 && obj_class_name == 'Hive')
      end

      # Do not allow disconnect from a parent if this is the only parent
      if params[:kind] == 'Question' && params[:type] == 'follower'
        return if (root_object.following_pockets_count == 1 && obj_class_name == 'Pocket') ||
            (root_object.following_hives_count == 1 && obj_class_name == 'Hive')
      end

      # Only connected user could disconnect him-self from a Hive/Pocket
      if obj_class_name == 'User' && params[:type] == 'following' && %w(Hive Pocket).include?(params[:kind])
        return if user_signed_in? && current_user.id != obj.id
      end

      # Only a member or group owner could disconnect a user from a Hive/Pocket
      if obj_class_name == 'User' && params[:type] == 'following' && %w(Group).include?(params[:kind])
        return if user_signed_in? && current_user.id != obj.id && current_user.id != root_object.user.id
      end

      root_object.stop_following obj if params[:type] == 'follower'
      obj.stop_following root_object if params[:type] == 'following'

      if %w(Tip Question).include? root_obj_class_name
        # if #{obj} is FrienList class object then update #{root_object} attributes
        obj.update_object_sharing_status_false(root_object) if obj_class_name == 'FriendList'
      end
    end
  end

  def connect_sharing_objects
    return if params[:id].blank?
    root_object = params[:kind].constantize.where(id: params[:id]).first
    return if root_object.blank? || root_object.new_record? || !%w(Hive Pocket).include?(root_object.class.name)
    connecting_objects = collect_items(params)
    all_friends_list   = FriendList.find_by_name 'All Friends'
    connecting_objects.each do |obj|
      root_object.share(obj, current_user)
      root_object.share(all_friends_list, obj) if all_friends_list && obj.class.name == 'User'
    end
  end

  def disconnect_sharing_objects
    return if params[:id].blank?
    root_object = params[:kind].constantize.where(id: params[:id]).first
    return if root_object.blank? || root_object.new_record? || !%w(Hive Pocket).include?(root_object.class.name)

    connecting_objects = collect_items(params)
    connecting_objects.each do |obj|
      root_object.stop_sharing(obj, current_user)
    end
  end

  # POST /update_background_image_position.json
  def update_background_image_position
    @item = params[:object_kind].constantize.find params[:object_id]

    if params[:object_kind] == 'User' && current_user.id != @item.id
      render json: { status: :error }
      return
    end

    if params[:object_kind] != 'User' && current_user.id != @item.user.id
      render json: { status: :error }
      return
    end

    @item.background_image_top  = params[:top].to_i
    @item.background_image_left = params[:left].to_i
    @item.save

    render json: { status: :success }
  end

  # GET /debug_object/:object_kind_and_id
  def debug_object
    @following = []
    @followers = []
    @nodes     = []

    obj_kind, obj_id = params[:object_kind_and_id].split('-')
    @debug_obj       = obj_kind.constantize.where(id: obj_id).first if obj_kind && obj_id

    if @debug_obj
      @following = @debug_obj.all_following if @debug_obj.respond_to?(:all_following)
      @followers = @debug_obj.followers if @debug_obj.respond_to?(:followers)
    end

    @nodes = (@following.to_a + @followers.to_a).flatten.uniq
  end

  def tiphive_protocol_redirect
    url = params[:url]
    if url.blank?
      redirect_to root_path
      return
    end

    url = url.gsub('http', 'tiphive')
    redirect_to url
  end

  # GET /internal_server_error
  def internal_server_error

  end

  # GET /not_found
  def not_found

  end

  # GET /intended_error
  def intended_error
    1 / 0
  end
end
