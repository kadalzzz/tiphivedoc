friends ||= @friends

json.response do
  json.array!(friends) do |friend|
    json.partial! 'api/v1/users/user', user: friend
  end
end

json.status :success
