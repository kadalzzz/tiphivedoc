module SharingPermission
  extend ActiveSupport::Concern

  SHARING_TYPES = [
    ['Shared', :select_friends],
    ['Private', :private]
  ]

  included do
    validate :valid_sharing_type
  end

  def valid_sharing_type
    if SHARING_TYPES.map { |sharing_type| sharing_type[1] }.exclude?(self.sharing_type.to_sym)
      errors.add(:sharing_type, :invalid)
    end
  end

  def sharing_permission
    sharing_type.to_sym
  end
end
