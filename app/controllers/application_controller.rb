class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :null_session
  add_breadcrumb 'All Tips', '/'
  include ApplicationObserver
  include PublicActivity::StoreController
  before_action :load_data
  helper_method :current_domain
  before_action :check_if_current_domain_active
  before_action :check_if_user_have_access_sub_domain

  before_filter {

    # TODO: need review for API, assign current_user to follow model
    if api? && params[:token]
      user = User.where(authentication_token: params[:token].to_s).first
      if user && !user_signed_in?
        sign_in user, store: true
      end
    end

    # Teefan: assign current_user to follow model
    Follow.current_user              = current_user
    Share.current_user               = current_user
    User.current_domain              = current_domain
    FriendList.current_domain        = current_domain
    InvitationRequest.current_domain = current_domain
    Domain.current                   = current_domain
    Paranoia.current_user            = current_user

    env       = {
      'REMOTE_ADDR'          => request.env['REMOTE_ADDR'],
      'HTTP_X_FORWARDED_FOR' => request.env['HTTP_X_FORWARDED_FOR'],
      'rack.session'         => request.env['rack.session'],
      'mixpanel_events'      => request.env['mixpanel_events']
    }

    @mixpanel ||= Mixpanel::Tracker.new TipHive::Application.config.mixpanel_token, env: env if Rails.env.production?

    if current_user.present? && Rails.env.production?
      @mixpanel.set current_user.id, email: current_user.email, first_name: current_user.first_name,
                    last_name:              current_user.last_name, created: current_user.created_at,
                    current_sign_in_at:     current_user.current_sign_in_at, test: 'true'
    end
  }

  def current_domain
    subdomain = request.subdomain
    domain = Domain.active.where(name: subdomain).first if subdomain
    @current_domain = domain || Domain.root unless @current_domain
    @current_domain
  end

  rescue_from CanCan::AccessDenied do |e|
    logger.error e.message
    logger.error e.backtrace
    flash[:error] = 'Access denied.'
    redirect_to root_url
  end

  # All init data should be placed here
  def load_data
    # gon.controller = params[:controller]
    # gon.action = params[:action]
    # gon.params = params
    Rack::MiniProfiler.authorize_request if user_signed_in? && current_user.admin? && params[:profile].present?
  end

  def redirect_to(options = {}, response_status = {})
    ::Rails.logger.info("==> Redirected by #{caller(1).first rescue 'unknown'}")
    super(options, response_status)
  end

  def after_sign_in_path_for(resource_or_scope)
    stored_location_for(resource_or_scope) || signed_in_root_path(resource_or_scope)
  end

  def after_sign_out_path_for(resource_or_scope)
    respond_to?(:root_path) ? root_path : '/'
  end

  # continue to use rescue_from in the same way as before
  if Rails.env.production? || Rails.env.staging? || Rails.env.beta? || Rails.application.config.consider_all_requests_local == false
    rescue_from Exception, with: :render_error
    rescue_from ActiveRecord::RecordNotFound, with: :render_not_found
    rescue_from ActionController::RoutingError, with: :render_not_found
  end

  # called by last route matching unmatched routes. Raises RoutingError which will be rescued from in the same way as other exceptions.
  def raise_not_found!
    if api?
      message = "No route matches #{request.method.upcase} /api/v1/#{params[:unmatched_route]}"
      render json: { response: { error: message }, status: :fail, message: message }
    else
      message = "ActionController::RoutingError: No route matches #{params[:unmatched_route]}"
      # e = ActionController::RoutingError.new("No route matches #{params[:unmatched_route]}")
      logger.error message
      # logger.error e.backtrace
      # Airbrake.notify_or_ignore(e, airbrake_request_data)
      respond_to do |format|
        format.html { render template: 'home/not_found', status: :not_found }
        format.json { render json: { response: { error: message }, status: :fail, message: message } }
        format.any { render(nothing: true, status: :not_found) }
      end
    end
  end

  # render 500 error
  def render_error(e)
    # ExceptionNotifier.notify_exception(e, env: request.env, data: {})
    Airbrake.notify_or_ignore(e, airbrake_request_data)
    @error_message = "<strong>#{e.class.name}</strong>: #{e.message}".html_safe
    message        = "500 - Internal Server Error: #{e.message}"
    logger.error message
    logger.error e.backtrace
    respond_to do |format|
      format.html { render template: 'home/internal_server_error', status: :internal_server_error }
      format.json { render json: { response: { error: message }, status: :fail, message: message } }
      format.any { render(nothing: true, status: :internal_server_error) }
    end
  end

  # render 404 error
  def render_not_found(e)
    # ExceptionNotifier.notify_exception(e, env: request.env, data: {})
    Airbrake.notify_or_ignore(e, airbrake_request_data)
    @error_message = "<strong>#{e.class.name}</strong>: #{e.message}".html_safe
    message        = "404 - Not Found: #{e.message}"
    logger.error message
    logger.error e.backtrace
    respond_to do |format|
      format.html { render template: 'home/not_found', status: :not_found }
      format.json { render json: { response: { error: message }, status: :fail, message: message } }
      format.any { render(nothing: true, status: :not_found) }
    end
  end

  private

  def api?
    self.class.name.split('::').first == 'Api'
  end

  def authenticate_user_from_token!
    if api?
      if params[:token].blank?
        message = 'Token parameter could not be blank'
        render json: { response: { error: message }, status: :fail, message: message }
        return
      end

      token = params[:token].presence
      user  = token && User.where(authentication_token: token.to_s).first

      if user.blank?
        message = 'Invalid token'
        render json: { response: { error: message }, status: :fail, message: message }
        return
      end

      if user && !user_signed_in?
        sign_in user, store: true
      end
    end
  end

  def read_upload_file_from_request
    content_type         = request.content_type
    content_length       = request.content_length
    data                 = request.body
    supported_file_types = %w(image/png image/jpg image/jpeg image/gif)

    logger.info "*** Content uploaded: #{content_type} - #{content_length}"

    if content_length <= 0
      return nil
    end

    if supported_file_types.exclude?(content_type)
      return nil
    end

    tempfile = Tempfile.new('file_upload')
    tempfile.binmode
    tempfile.write(data.read)
    tempfile.close

    extension = content_type.split('/')[1]
    extension = 'jpg' if extension == 'jpeg'

    timestamp   = Time.zone.now.to_i.to_s
    filename    = "upload_#{timestamp}_" + Digest::SHA1.hexdigest("upload_#{timestamp}")[0..7] + ".#{extension}"
    filename    = params[:filename] if params[:filename].present?
    file_upload = ActionDispatch::Http::UploadedFile.new(tempfile:          tempfile, filename: filename,
                                                         original_filename: filename, type: content_type)
  end

  def check_if_current_domain_active
    subdomain = request.subdomain
    logger.info "*** check_if_current_domain_active subdomain: #{subdomain}"

    return true if subdomain.blank? || %w(www staging beta).include?(subdomain)
    return true if current_domain.active? && !current_domain.root?

    redirect_to(root_url(subdomain: nil), notice: 'The domain is not active.')
  end

  def check_if_user_have_access_sub_domain
    # TODO: need review for API, assign current_user to follow model
    return false if params[:controller] == 'api/v1/sessions' && params[:action] == 'create'
    if api? && params[:token]
      user = User.where(authentication_token: params[:token].to_s).first
      if user && !user_signed_in?
        sign_in user, store: true
      end
    end
    return false if !current_domain.active? || current_domain.root?
    status = current_domain.status_with(current_user) if user_signed_in?
    return false if %w(is_member is_me).include?(status.to_s)

    redirect_to(root_url(subdomain: nil), notice: "You don't have access for this domain.")
  end
end
