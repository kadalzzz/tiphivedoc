class GroupsController < ApplicationController
  include TipHiveHelper
  include ApplicationHelper

  before_filter :authenticate_user_from_token!, except: [:request_invite]
  before_filter :authenticate_user!, except: [:request_invite]
  before_action :set_group, only: [:show, :edit, :update, :members, :destroy, :no_access]

  # GET /groups
  # GET /groups.json
  def index
    params[:order] = :title if params[:order].blank?
    if params[:all].present?
      @groups = Group.order(params[:order]).page(params[:page]).per(params[:per])
    elsif params[:all_mine].present?
      @groups = current_user.groups.order(params[:order]).page(params[:page]).per(params[:per])
    elsif params[:suggested].present?
      @groups = Group.order('created_at DESC').limit(10)
    else
      @groups = current_user.following_groups.order(params[:order]).page(params[:page]).per(params[:per])
    end
  end

  # GET /my_groups
  # GET /my_groups.json
  def my_groups
    if params[:type_page].present? && params[:type_page] == 'show_all'
      @groups = current_user.following_groups.order(:title).page(params[:page]).per(params[:per])
    else
      @groups = current_user.groups.order(:title).page(params[:page]).per(params[:per])
    end
  end

  # GET /menu_groups
  # GET /menu_groups.json
  def menu_groups
    @groups = current_user.following_groups.order(:title)
  end

  # GET /members
  # GET /members.json
  def members
    add_breadcrumb 'My Hives', my_hives_path,
                   options: { data: { remote: true }, class: 'active-transition' }

    add_breadcrumb "All hives in #{@group.title}", my_hives_path(group_id: @group.id),
                   options: { data: { remote: true }, class: 'active-transition' }

    @hives      = @group.following_hives
    @members_az = {}
    ('A'..'Z').each do |char|
      char_members      = {
        count:   @group.user_followers.where("first_name LIKE '#{char}%'").count,
        members: @group.user_followers.where("first_name LIKE '#{char}%'").order(:first_name)
      }
      @members_az[char] = char_members
    end

    if params[:initial].present?
      @members = @group.user_followers.where("first_name LIKE '#{params[:initial]}%'")
                   .order(:first_name).page(params[:page]).per(params[:per])
    else
      @members = @group.user_followers.order(:first_name).page(params[:page]).per(params[:per])
    end

    respond_to do |format|
      format.html {}
      format.json {}
    end
  end

  # GET /groups/1
  # GET /groups/1.json
  def show
    add_breadcrumb 'My Hives', my_hives_path,
                   options: { data: { remote: true }, class: 'active-transition' }

    add_breadcrumb "All hives in #{@group.title}", my_hives_path(group_id: @group.id),
                   options: { data: { remote: true }, class: 'active-transition' }

    verify_group_permissions(@group)

    if Rails.env.production?
      @mixpanel.track 'Group Viewed', id: @group.id, title: @group.title, distinct_id: current_user.try(:id).to_i
    end

    @hives            = @group.following_hives.order('created_at DESC')
    @tips             = current_user.fetch_tips(@group, params)
    @friends_count    = current_user.following_users.where(id: @group.members.pluck(:id)).count
    @liked_tips_count = @tips.includes([:votes]).where('votes.voter_id = ?', current_user.id).references(:votes).count
  end

  # GET /groups/new
  def new
    @group                        = current_user.groups.new
    @domain                       = current_user.following_domains.reject { |a| a.name.nil? }
    @selected_friends_and_groups  = []
  end

  # GET /groups/1/edit
  def edit
    @group  = Group.find(params[:id])
    @domain = current_user.following_domain
    authorize! :update, @group
    @selected_friends_and_groups  = @group.members - [current_user]
  end

  # POST /groups
  # POST /groups.json
  def create
    file_upload = read_upload_file_from_request

    if file_upload.present?
      params[:group][:background_image] = file_upload
    end

    @group = current_user.groups.new(group_params)
    authorize! :create, @group
    @selected_friends_and_groups  = collect_items(items_list: params[:select_friends_and_groups])
    @group.allowing_domain_list   = params[:allowing_domains]
    @group.allowing_location_list = params[:allowing_locations]

    if Rails.env.production?
      @mixpanel.track 'Group Created', id: @group.id, title: @group.title, distinct_id: current_user.try(:id).to_i
    end

    respond_to do |format|
      if @group.save
        current_user.follow @group

        @selected_friends_and_groups.each do |friend|
          next if friend.class.name != 'User' # only allow User to connect to Group
          # send invitation request to user if user is not friend of current_user
          if friend.class.name == 'User' && current_user.status_with(friend) == :is_nothing
            InvitationRequest.send_invitation_request(@group, current_user, friend)
          end
          # user follow group if user is friends of current_user
          if friend.class.name == 'User' && current_user.status_with(friend) == :is_friend
            friend.follow(@group)
            friend.follow(@current_domain) unless @current_domain.root?
          end
        end

        format.html do
          redirect_to(group_url(@group, subdomain: @group.subdomain), notice: 'Group was successfully created.')
        end
        format.json { render template: 'api/v1/groups/show' }
        format.js {}
      else
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @group.errors, full_messages: @group.errors.full_messages },
                         status:   :fail, message: @group.errors.full_messages.join(', ') }
        end
        format.js { render js: "alert('#{@group.errors.full_messages.join(', ')}');" }
      end
    end
  end

  # PATCH/PUT /groups/1
  # PATCH/PUT /groups/1.json
  def update
    file_upload = read_upload_file_from_request

    if file_upload.present?
      params[:group][:background_image] = file_upload
    end

    @group = Group.find(params[:id])
    authorize! :update, @group
    @group.allowing_domain_list   = params[:allowing_domains]
    @group.allowing_location_list = params[:allowing_locations]

    if Rails.env.production?
      @mixpanel.track 'Group Updated', id: @group.id, title: @group.title, distinct_id: current_user.try(:id).to_i
    end

    respond_to do |format|
      if @group.update(group_params)
        move_group_data
        format.html do
          redirect_to(group_url(@group, subdomain: @group.subdomain), notice: 'Group was successfully updated.')
        end
        format.json { render template: 'api/v1/groups/show' }
      else
        format.html { render action: 'edit' }
        format.json do
          render json: { response: { error: @group.errors, full_messages: @group.errors.full_messages },
                         status:   :fail, message: @group.errors.full_messages.join(', ') }
        end
      end
    end
  end

  # GET /groups/1/share
  def share
    @group                        = Group.find(params[:id])
    @selected_friends_and_groups  = @group.members - [current_user]
  end

  # GET /groups/1/request_invite
  def request_invite
    @group      = Group.find(params[:id])
    @error_text = ''

    if params[:request_invite].blank?
      @error_text = message = 'Email could not be blank'
      respond_to do |format|
        format.html {}
        format.js {}
        format.json { render json: { response: message, status: :fail, message: message } }
      end
      return
    end

    group_owner_id = @group.user_id
    email          = params[:request_invite][:email]

    if email.blank?
      @error_text = message = 'Email could not be blank'
      respond_to do |format|
        format.html {}
        format.json { render json: { response: message, status: :fail, message: message } }
      end
    end

    existing_user = User.find_by_email email
    if existing_user.present?
      respond_to do |format|
        # Shashi : change redirect based on the user login status
        if current_user
          flash[:success] = 'Request Sent!'
          format.html { redirect_to tips_path }
        else
          flash[:notice] = message = "You're already registered. Please sign in."
          format.html { redirect_to new_user_session_path(email: email) }
        end
        format.json { render json: { response: message, status: :fail, message: message } }
      end
      return
    end

    if @group.who_can_join == :invite

      if @group.is_auto_accept == false
        invitation_request_options = {
          email:            email,
          user_id:          group_owner_id,
          request_type:     'invite',
          request_token:    Digest::SHA1.hexdigest(Time.now.to_s),
          connectable_type: @group.class.name,
          connectable_id:   @group.id
        }
        @invitation_request        = InvitationRequest.new(invitation_request_options)

        if @invitation_request.save
          flash[:notice] = message = 'Check your email inbox for an email with a link to create your account'
          respond_to do |format|
            format.html {}
            format.json { render json: { response: message, status: :success, message: message } }
          end
        end

        return
      end

      # main invitation code
      invitation_options = {
        email:            email,
        invitation_token: Digest::SHA1.hexdigest(Time.now.to_s),
        invitation_type:  'system',
        connectable_type: @group.class.name,
        connectable_id:   @group.id
      }

      logger.info "*** invitation_options: #{invitation_options}"

      @friend_invitation = FriendInvitation.new(invitation_options)
      if @friend_invitation.save
        flash[:notice] = message = 'Check your email inbox for an email with a link to create your account'
        respond_to do |format|
          format.html {}
          format.json { render json: { response: message, status: :success, message: message } }
        end
      end

      return
    end

    if @group.who_can_join == :anyone
      # main invitation code
      invitation_options = {
        email:            email,
        invitation_token: Digest::SHA1.hexdigest(Time.now.to_s),
        invitation_type:  'system',
        connectable_type: @group.class.name,
        connectable_id:   @group.id
      }

      logger.info "*** invitation_options: #{invitation_options}"

      @friend_invitation = FriendInvitation.new(invitation_options)
      if @friend_invitation.save
        flash[:notice] = message = 'Check your email inbox for an email with a link to create your account'
        respond_to do |format|
          format.html {}
          format.json { render json: { response: message, status: :success, message: message } }
        end
      end

      return
    end

    if @group.who_can_join == :domain
      logger.info "Group domain joining request from: #{email}"

      _username, domain = email.split('@')
      domain            = domain.downcase
      if @group.allowing_domain_list.exclude?(domain) || @group.is_auto_accept == false
        # Teefan: send invitation notification to Group owner on non-allowed email domains
        invitation_request_options = {
          email:            email,
          user_id:          group_owner_id,
          request_token:    Digest::SHA1.hexdigest(Time.now.to_s),
          connectable_type: @group.class.name,
          connectable_id:   @group.id
        }
        @invitation_request        = InvitationRequest.new(invitation_request_options)

        if @invitation_request.save
          flash[:notice] = message = 'Your request to join the group is being reviewed. You will receive an email
                                      with a link to create your account if your request is accepted.'
          respond_to do |format|
            format.html {}
            format.json { render json: { response: message, status: :success, message: message } }
          end
        end

        return
      end

      # main invitation code
      invitation_options = {
        email:            email,
        invitation_token: Digest::SHA1.hexdigest(Time.now.to_s),
        invitation_type:  'system',
        connectable_type: @group.class.name,
        connectable_id:   @group.id
      }

      logger.info "*** invitation_options: #{invitation_options}"

      @friend_invitation = FriendInvitation.new(invitation_options)
      if @friend_invitation.save
        flash[:notice] = message = 'Check your email inbox for an email with a link to create your account'
        respond_to do |format|
          format.html {}
          format.json { render json: { response: message, status: :success, message: message } }
        end
      end
    end
  end

  # GET /groups/1/join
  def join
    @group = Group.find(params[:id])

    if @group.who_can_join == :domain
      _username, domain = current_user.email.split('@')
      domain            = domain.downcase
      if @group.allowing_domain_list.include?(domain)
        current_user.follow @group
        message = "Joined #{@group.title}"
        respond_to do |format|
          format.html { redirect_to group_path(@group), notice: message }
          format.json { render json: { response: message, status: :success, message: message } }
        end
        return
      end
    end

    if @group.who_can_join == :anyone
      current_user.follow @group
      message = "Joined #{@group.title}"
      respond_to do |format|
        format.html { redirect_to group_path(@group), notice: message }
        format.json { render json: { response: message, status: :success, message: message } }
      end
      return
    end

    if @group.who_can_join == :invite
      email                      = current_user.email
      invitation_request_options = {
        email:            email,
        user_id:          @group.user_id,
        request_token:    Digest::SHA1.hexdigest(Time.now.to_s),
        connectable_type: @group.class.name,
        connectable_id:   @group.id
      }
      @invitation_request        = InvitationRequest.new(invitation_request_options)

      if @invitation_request.save
        message = 'Invitation Request sent'
        respond_to do |format|
          format.html { redirect_to request_invite_group_path(@group, 'request_invite[email]' => email) }
          format.json { render json: { response: message, status: :success, message: message } }
        end
        return
      end
    end

    if Rails.env.production?
      @mixpanel.track 'Group Joined', id: @group.id, title: @group.title, distinct_id: current_user.try(:id).to_i
    end

    message = 'This group is not opened to public.'
    respond_to do |format|
      format.html { redirect_to group_path(@group), alert: message }
      format.json { render json: { response: message, status: :fail, message: message } }
    end
  end

  # GET /groups/1/leave
  def leave
    @group = Group.find(params[:id])

    if current_user.id == @group.user.id
      flash[:notice] = message = "You can't leave your own group"
      respond_to do |format|
        format.html { redirect_to group_path(@group) }
        format.json { render json: { response: message, status: :fail, message: message } }
      end
      return
    else
      current_user.stop_following @group
    end

    if Rails.env.production?
      @mixpanel.track 'Group Left', id: @group.id, title: @group.title, distinct_id: current_user.try(:id).to_i
    end

    message = "Left #{@group.title}"
    respond_to do |format|
      format.html { redirect_to user_profile_path(current_user) }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  # DELETE /groups/1
  # DELETE /groups/1.json
  def destroy
    @group = current_user.groups.find(params[:id])
    authorize! :destroy, @group
    if Rails.env.production?
      @mixpanel.track 'Group Destroyed', id: @group.id, title: @group.title, distinct_id: current_user.try(:id).to_i
    end

    @group.destroy
    message = 'Group Destroyed'
    respond_to do |format|
      format.html { redirect_to my_groups_url }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_group
    @group = Group.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def group_params
    params.require(:group).permit(:title, :description, :join_type, :group_type, :address, :location,
                                  :latitude, :longitude, :zip, :avatar, :avatar_cache, :remove_avatar,
                                  :background_image, :background_image_cache, :remove_background_image, :domain_id)
  end

  def verify_group_permissions(group)
    return if current_user.id == group.user_id
    return if current_user.following?(group)

    case group.join_type
    when 'anyone'
      return
    when 'invite'
      handle_no_access(group)
      return
    when 'domain'
      _username, domain = current_user.email.split('@')
      domain            = domain.downcase
      return if @group.allowing_domain_list.include?(domain)
      handle_no_access(group)
      return
    when 'location'
      handle_no_access(group)
      return
    end
  end

  def move_group_data
    @group.following_tips.update_all(domain_id: @group.domain_id)
    @group.following_questions.update_all(domain_id: @group.domain_id)
  end

  def handle_no_access(group)
    if api?
      error_msg = 'This group is private. Request an invite to join.'
      render json: { response: { error: error_msg }, status: :fail, messsage: error_msg }
    else
      redirect_to no_access_group_path(group)
    end
  end
end
