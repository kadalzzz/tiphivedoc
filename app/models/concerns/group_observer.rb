module GroupObserver
  extend ActiveSupport::Concern

  included do
    after_save :notify
  end

  def notify
  end

  def notify_friend(friend)
    return if user_id == friend.id
    return if friend.settings(:email_notifications).someone_invite_me_to_group == false

    user = self.user

    email_params = {
      user_email:        user.email,
      user_first_name:   user.first_name,
      user_name:         user.name,
      friend_email:      friend.email,
      friend_first_name: friend.first_name,
      friend_name:       friend.name,
      group_slug:        to_param,
      group_title:       title,
      domain_id:         domain_id
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{user.name} is sending email to #{friend.email}"
    )
    ## DEBUG LOG END

    EmailSendingWorker.perform_async('UserMailer', 'added_to_group_email', email_params)
  end

  def notify_group_member(share_hive_user, hive, friend)
    return if user_id == friend.id
    return if friend.settings(:email_notifications).someone_invite_me_to_group == false

    email_params = {
      user_email:        share_hive_user.email,
      user_first_name:   share_hive_user.first_name,
      user_name:         share_hive_user.name,
      friend_email:      friend.email,
      friend_first_name: friend.first_name,
      friend_name:       friend.name,
      hive_slug:         hive.to_param,
      group_title:       title,
      hive_title:        hive.title,
      domain_id:         domain_id
    }

    ## DEBUG LOG START
    ActionLog.create(
      file_name:   __FILE__,
      method_name: __method__,
      line_number: __LINE__,
      stack_trace: caller.reject { |c| c !~ /\/app\// }.join("\n"),
      message:     "#{share_hive_user.name} is sending email to #{friend.email}"
    )
    ## DEBUG LOG END

    EmailSendingWorker.perform_async('UserMailer', 'added_hive_to_group', email_params)
  end
end
