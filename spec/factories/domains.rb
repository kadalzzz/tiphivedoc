FactoryGirl.define do
  factory :domain do
    name 'abc'
    logo { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/logo.png')) }
    background { Rack::Test::UploadedFile.new(File.join(Rails.root, 'spec/fixtures/logo.png')) }
    factory :root_domain do
      name nil
   end
  end
end
