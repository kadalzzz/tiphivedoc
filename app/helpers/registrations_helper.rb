module RegistrationsHelper
  def current_identity
    return session['devise.facebook_data'] if session['devise.facebook_data'].present?
    return session['devise.twitter_data'] if session['devise.twitter_data'].present?
    return session['devise.google_oauth2_data'] if session['devise.google_oauth2_data'].present?
    return nil
  end

  def current_identity_avatar_url
    if session['devise.facebook_data'].present?
      return "#{session['devise.facebook_data'].info.image}?type=large"
    end

    if session['devise.twitter_data'].present?
      return "#{session['devise.twitter_data'].info.image}"
    end

    if session['devise.google_oauth2_data'].present?
      return "#{session['devise.google_oauth2_data'].info.image}"
    end

    return nil
  end

  def current_identity_name
    if session['devise.facebook_data'].present?
      return "#{session['devise.facebook_data'].info.first_name} #{session['devise.facebook_data'].info.last_name}"
    end

    if session['devise.twitter_data'].present?
      return "#{session['devise.twitter_data'].info.name}"
    end

    if session['devise.google_oauth2_data'].present?
      return "#{session['devise.google_oauth2_data'].info.first_name} #{session['devise.google_oauth2_data'].info.last_name}"
    end

    return nil
  end

  def current_registration_type
    return :facebook if session['devise.facebook_data'].present?
    return :twitter if session['devise.twitter_data'].present?
    return :google_oauth2 if session['devise.google_oauth2_data'].present?
    return :email
  end

  def current_registration_type_message
    return 'Connecting with Facebook' if session['devise.facebook_data'].present?
    return 'Connecting with Twitter' if session['devise.twitter_data'].present?
    return 'Connecting with Google+' if session['devise.google_oauth2_data'].present?
    return 'Or use your email address'
  end
end
