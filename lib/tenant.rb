module Tenant

  extend ActiveSupport::Concern

  included do
    belongs_to :domain

    default_scope { where(domain: Domain.current) }
  end

  def subdomain
    domain.try(:name)
  end

end
