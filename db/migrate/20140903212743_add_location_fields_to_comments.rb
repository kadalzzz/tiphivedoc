class AddLocationFieldsToComments < ActiveRecord::Migration
  def up
    add_column :comments, :longitude, :float
    add_column :comments, :latitude, :float
    add_column :comments, :address, :string
    add_column :comments, :location, :string
  end

  def down
    remove_column :comments, :longitude
    remove_column :comments, :latitude
    remove_column :comments, :address
    remove_column :comments, :location
  end

end
