namespace :question do
  desc "Set Question Color"
  task assign_color: :environment do
    Question.find_each do |q|
      q.update_attribute(:color, Question::COLOR[rand(Question::COLOR.length)])
    end
  end

  desc "Make all question tips follow question"
  task make_tips_follow: :environment do
    Question.find_each do |question|
      question.tips.each do |tip|
        tip.follow question
      end
    end
  end
end
