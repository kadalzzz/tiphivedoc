domain ||= @domain

json.response do
  json.user_count domain.users_count(Date.today.beginning_of_month)
  json.cost_per_user_per_month (domain.price / domain.users_count)
  json.total_cost domain.price
end

json.status :success
