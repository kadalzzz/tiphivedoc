class Tip < ActiveRecord::Base
  include Tenant
  include PublicActivity::Model
  tracked owner: Proc.new { |controller, _model| controller.current_user if controller }

  acts_as_followable
  acts_as_follower
  acts_as_votable
  acts_as_commentable
  acts_as_paranoid

  delegate :domain, to: :group

  validates :title, presence: true
  validates :title, length: { maximum: 140 }
  COLOR = %w(liteorange orange purple litepurple green liteblue blue)

  before_create :set_color

  attr_accessor :images
  attr_accessor :files
  attr_accessor :picture_ids
  attr_accessor :file_ids
  attr_accessor :links
  attr_accessor :link_ids

  def name
    title
  end

  # extend FriendlyId
  # friendly_id :slug_candidates, use: :slugged
  def to_param
    "#{id} #{name}".parameterize
  end

  belongs_to :user
  belongs_to :question
  include TipObserver
  include TipHiveObject
  include ActsAsFlagable

  has_many :pictures, as: :imageable, dependent: :destroy
  has_many :file_uploads, as: :fileable, dependent: :destroy
  has_many :tip_links, dependent: :destroy, inverse_of: :tip

  delegate :first_name, to: :user, prefix: true
  delegate :email, to: :user, prefix: true

  # Scopes
  scope :created_after, ->(time) { where('tips.created_at >= ?', time) }
  scope :shared, -> { where(sharing_type: :public) }
  scope :unshared, -> { where(sharing_type: :private) }
  scope :all_friends_sharing, -> { where(shared_all_friends: true) }
  scope :select_friends_sharing, -> { where(sharing_type: :select_friends) }
  scope :public_sharing, -> { where(is_public: true) }
  scope :private_sharing, -> { where(is_private: true) }
  scope :shared_all_friends_or_public, -> { where('shared_all_friends = true OR is_public = true') }
  # Get all owner tip
  scope :by_owner, -> (user) { where(user_id: user.id) }

  default_scope { where(draft: nil) }

  paginates_per 6

  searchable(if: proc { |tip| tip.sharing_type != 'private' }) do
    text :title, :description
    text :hives_and_pockets_tags

    integer :user_id
    boolean :is_public
    string :sharing_type
  end

  def has_map?
    latitude && longitude
  end

  def slug_candidates
    [
      [:id, :title]
    ]
  end

  def hives
    following_hives
  end

  def pockets
    following_pockets
  end

  def title_with_pockets
    "#{pockets.map(&:title).join(', ')} / #{title}"
  end

  def hives_and_pockets_title
    "#{hives.map(&:title).join(', ')} / #{pockets.map(&:title).join(', ')}"
  end

  def hives_and_pockets_tags
    "#{hives.map(&:title).join(' ')} #{pockets.map(&:title).join(' ')}"
  end

  def users
    user_followers
  end

  def groups
    group_followers
  end

  def friend_lists
    friend_list_followers
  end

  def users_and_groups
    [users.all, groups.all].flatten
  end

  def users_and_groups_and_friend_lists
    [users.all, groups.all, friend_lists.all].flatten
  end

  def set_color
    self.color = COLOR[rand(COLOR.length)]
  end

  def generate_tip_activities
    logger.info '*** call tip.generate_tip_activities...'
    tip_owner = user
    return if tip_owner.blank?

    tip_shared_users            = users
    tip_shared_groups           = groups
    tip_shared_with_all_friends = friend_lists

    sent_ids = []

    create_activity action: 'feed', params: {}, recipient: tip_owner, owner: tip_owner
    sent_ids << tip_owner.id

    tip_shared_users.each do |friend|
      next if user.id != friend.id
      create_activity action: 'feed', params: {}, recipient: friend, owner: tip_owner
      sent_ids << friend.id
    end

    tip_shared_with_all_friends.each do |item|
      next if item.name == 'All Friends' || item.name == 'All Domain Members'
      user_friends = user.friends
      user_friends.each do |friend|
        create_activity action: 'feed', params: {}, recipient: friend, owner: tip_owner
        sent_ids << friend.id
      end
    end

    tip_shared_groups.each do |group|
      group.members.each do |group_member|
        # next if sent_ids.include? group_member.id
        create_activity action: 'feed', params: {}, recipient: group_member, owner: tip_owner
        sent_ids << group_member.id
      end
    end
  end

  def handle_sharing(shared_objects)
    owner = user
    return if is_private

    if shared_objects.blank?
      # If share fields is empty and share? status is yes then we assume tip have private setting
      update_attributes(is_private: true)
      return
    end

    shared_objects.each do |object|
      class_name = object.class.name

      # owner shares @tip with non-friend, send invitation request to non-friend user before sharing tip
      if class_name == 'User' && owner.status_with(object) == :is_nothing
        InvitationRequest.send_invitation_request(self, owner, object)
      end

      # Object follow Tip if object class name Group, FriendList, User (user should be friend of current user)
      if (class_name == 'User' && owner.status_with(object) == :is_friend) || %w(Group FriendList).include?(class_name)
        object.follow self
      end

      # notify current user friends, tip has shared with him
      if class_name == 'User' && owner.status_with(object) == :is_friend
        notify_friend(object) if object.settings(:email_notifications).friend_adds_tip == 'always'
      end

      # If object is FriendList class object then update Tip attribute shared_all_friends or is_public
      object.update_object_sharing_status_true(self) if %w(FriendList).include?(class_name)

      # notify group members, tip has shared with him
      notify_group_member(object) if class_name == 'Group'
    end
  end
end
