class FileUpload < ActiveRecord::Base
  include Tenant
  belongs_to :fileable, polymorphic: true
  belongs_to :user

  attr_accessor :files

  mount_uploader :file, FileUploader

  validates :file, presence: true
  validate :check_extension

  def extension
    file.file.extension.downcase rescue nil
  end

  private

  def check_extension
    if %w(jpg jpeg gif png).include?(extension)
      errors.add(:file, 'Not for image file ')
    end
  end
end
