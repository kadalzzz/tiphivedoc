class ChangeSharedAllFriendsDefaultToFalse < ActiveRecord::Migration
  def down
    change_table :hives do |t|
      t.change_default :shared_all_friends, true
    end

    change_table :pockets do |t|
      t.change_default :shared_all_friends, true
    end

    change_table :questions do |t|
      t.change_default :shared_all_friends, true
    end

    change_table :tips do |t|
      t.change_default :shared_all_friends, true
    end
  end

  def up
    change_table :hives do |t|
      t.change_default :shared_all_friends, false
    end

    change_table :pockets do |t|
      t.change_default :shared_all_friends, false
    end

    change_table :questions do |t|
      t.change_default :shared_all_friends, false
    end

    change_table :tips do |t|
      t.change_default :shared_all_friends, false
    end
  end
end
