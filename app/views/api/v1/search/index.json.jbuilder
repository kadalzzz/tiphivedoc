results ||= @results

json.response do
  json.array!(results) do |result|
    class_name = result.class.name

    json.partial! 'api/v1/users/user', user: result if class_name == 'User'
    json.partial! 'api/v1/groups/group', group: result if class_name == 'Group'
    json.partial! 'api/v1/hives/hive', hive: result if class_name == 'Hive'
    json.partial! 'api/v1/pockets/pocket', pocket: result if class_name == 'Pocket'
    json.partial! 'api/v1/tips/tip', tip: result if class_name == 'Tip'

  end
end

json.status :success
