class Api::V1::DomainsController < DomainsController
  def index
    @domains = current_user.following_domains.reject { |d| d.id.nil? }
  end
end
