share_users  ||= @selected_friends
share_groups ||= @selected_groups
pocket       ||= @pocket

json.response do
  json.is_share !pocket.is_private
  json.users do
    json.array!(share_users) do |share_object|
      json.partial! 'api/v1/pockets/share_object', share_object: share_object
    end
  end

  json.groups do
    json.array!(share_groups) do |share_group|
      json.partial! 'api/v1/pockets/share_group', share_group: share_group
    end
  end
end

json.status :success
