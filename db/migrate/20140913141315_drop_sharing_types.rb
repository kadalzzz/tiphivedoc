class DropSharingTypes < ActiveRecord::Migration
  def change
    drop_table :sharing_types
    remove_column :shares, :sharing_type_id
  end
end
