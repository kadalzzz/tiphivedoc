class PocketsController < ApplicationController
  include TipHiveHelper

  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!
  before_action :set_pocket, only: [:show, :edit, :update, :destroy]
  before_action :get_object_setting_object, only: [:share, :object_setting_model]

  # GET /pockets
  # GET /pockets.json
  def index
    if params[:hive_id].present?
      @hive    = Hive.find(params[:hive_id])
      @pockets = @hive.pocket_followers.order(:title).page(params[:page]).per(params[:per])
    else
      @pockets = current_user.following_pockets.order(:title).page(params[:page]).per(params[:per])
      render 'my_pockets'
    end
  end

  # GET /my_pockets
  # GET /my_pockets.json
  def my_pockets
    @pockets = current_user.following_pockets.order(:title).page(params[:page]).per(params[:per])
  end

  # GET /menu_pockets
  # GET /menu_pockets.json
  def menu_pockets
    @pockets = []
    if params[:hive_id].present?
      @hive    = Hive.find(params[:hive_id])
      @pockets = @hive.pocket_followers.order(:title)
    end
  end

  # GET /pockets/1
  # GET /pockets/1.json
  def show
    @tips = current_user.fetch_tips(@pocket, params)
    @hive = @pocket.hives.last

    if Rails.env.production?
      @mixpanel.track 'Pocket Viewed',
                      id:          @pocket.id,
                      title:       @pocket.title,
                      distinct_id: current_user.nil? ? 0 : current_user.id
    end
  end

  # GET /pockets/new
  def new
    @pocket           = current_user.pockets.new
    @connected_groups = current_user.following_groups.order(:title)
    @selected_hives   = collect_items({ items_list: params[:hives] })
    @selected_hives.each do |hive|
      current_user.follow hive
    end
    @connected_hives = current_user.following_hives.order(:title)
  end

  # GET /pockets/1/edit
  def edit
    @pocket                       = current_user.pockets.find(params[:id])
    @connected_groups             = current_user.following_groups.order(:title)
    @connected_hives              = current_user.following_hives.order(:title)
    @selected_hives               = @pocket.following_hives
    @selected_friends_and_groups  = @pocket.get_shared_objects(current_user)
    @groups                       = current_user.following_groups
  end

  # POST /pockets
  # POST /pockets.json
  def create
    file_upload                        = read_upload_file_from_request
    params[:pocket][:background_image] = file_upload if file_upload.present?

    @pocket                      = current_user.pockets.new(pocket_params)
    @connected_groups            = current_user.following_groups.order(:title)
    @connected_hives             = current_user.following_hives.order(:title)
    @selected_hives              = collect_items({ items_list: params[:hives] })
    @selected_friends_and_groups = collect_items({ items_list: params[:select_friends_and_groups] })

    @hive = @selected_hives.first # for quick form, that need a hive

    # @existing_pocket = scan_existing_pocket(pocket_params, @selected_hives)
    # return if @existing_pocket

    if Rails.env.production?
      @mixpanel.track 'Pocket Created', id: @pocket.id, title: @pocket.title, distinct_id: current_user.nil? ? 0 : current_user.id
    end

    respond_to do |format|
      if @pocket.save
        current_user.follow @pocket

        @selected_hives.each do |selected_hive|
          @pocket.follow selected_hive
        end

        # Call Pocket model method to connect @pocket with share fields recored 
        @pocket.handle_sharing(@selected_friends_and_groups)

        # create object settings recorded for Pocket
        @pocket.create_settings_object(current_user)

        format.html { redirect_to hive_path(@hive, pocket_id: @pocket.id), notice: 'Pocket was successfully created.' }
        format.json { render template: 'api/v1/pockets/show' }
        format.js {}
      else
        format.html { render action: 'new' }
        format.json do
          render json: { response: { error: @pocket.errors, full_messages: @pocket.errors.full_messages },
                         status:   :fail, message: @pocket.errors.full_messages.join(', ') }
        end
        format.js { render js: "alert('#{@pocket.errors.full_messages.join(', ')}');" }
      end
    end
  end

  # PATCH/PUT /pockets/1
  # PATCH/PUT /pockets/1.json
  def update
    file_upload                        = read_upload_file_from_request
    params[:pocket][:background_image] = file_upload if file_upload.present?

    @pocket                       = current_user.pockets.find(params[:id])
    @following_hives              = @selected_hives = @pocket.following_hives
    @connected_groups             = current_user.following_groups.order(:title)
    @connected_hives              = current_user.following_hives.order(:title)
    @connected_friends_and_groups = @selected_friends_and_groups = @pocket.users_and_groups - [current_user]

    if Rails.env.production?
      @mixpanel.track 'Pocket Updated',
                      id:          @pocket.id,
                      title:       @pocket.title,
                      distinct_id: current_user.nil? ? 0 : current_user.id
    end

    respond_to do |format|
      if @pocket.update(pocket_params)
        @pocket.update_attributes(is_private: true) if params[:select_friends_and_groups].blank?
        @pocket.update_object_setting_recorded(current_user, params[:pocket][:background_image])
        format.html { redirect_to @pocket, notice: 'Pocket was successfully updated.' }
        format.json { render template: 'api/v1/pockets/show' }
      else
        format.html { render action: 'edit' }
        format.json do
          render json: { response: { error: @pocket.errors, full_messages: @pocket.errors.full_messages },
                         status:   :fail, message: @pocket.errors.full_messages.join(', ') }
        end
      end
    end
  end

  # GET /pockets/1/share
  def share
    @connected_friends_and_groups = @pocket.get_shared_objects(current_user)
    @selected_friends_and_groups  = @pocket.get_shared_objects(current_user)
  end

  def object_setting_model
    @connected_friends_and_groups = @pocket.get_shared_objects(current_user)
    @selected_friends_and_groups  = @pocket.get_shared_objects(current_user)
  end

  def object_setting
    file_upload                                 = read_upload_file_from_request
    params[:object_settings][:background_image] = file_upload if file_upload.present?

    @pocket = Pocket.find(params[:id])
    @pocket.create_or_update_object_setting(current_user, object_setting_params)
    respond_to do |format|
      format.html { redirect_to @pocket, notice: 'Pocket setting was successfully updated.' }
    end
  end

  # GET /pockets/1/join
  def join
    @pocket = Pocket.find(params[:id])

    current_user.follow @pocket
    message = 'Pocket Joined'
    respond_to do |format|
      format.html { redirect_to pocket_path(@pocket), notice: message }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  # GET /pockets/1/leave
  def leave
    @pocket = Pocket.find(params[:id])

    if current_user.id == @pocket.user.id
      flash[:notice] = "You can't unfollow your own pocket"
      redirect_to pocket_path(@pocket)
      return
    else
      current_user.stop_following @pocket
      # destroy all object_setting && sharing record if user leave Hive
      @pocket.destroy_object_setting_and_share_record(current_user) if current_user
    end

    message = 'Pocket Left'
    respond_to do |format|
      format.html { redirect_to user_profile_path(current_user) }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  # DELETE /pockets/1
  # DELETE /pockets/1.json
  def destroy
    @pocket = current_user.pockets.find(params[:id])
    @pocket.destroy
    message = 'Pocket Destroyed'
    params[:hive_id].present? ? direction = Hive.find(params[:hive_id]) : direction = my_pockets_url
    r
    espond_to do |format|
      format.html { redirect_to direction, notice: 'Pocket was successfully deleted.' }
      format.json { render json: { response: message, status: :success, message: message } }
    end
  end

  private
  # Use callbacks to share common setup or constraints between actions.
  def set_pocket
    @pocket = Pocket.find(params[:id])
  end

  # Never trust parameters from the scary internet, only allow the white list through.
  def pocket_params
    params.require(:pocket).permit(:title, :description, :background_image, :background_image_cache,
                                   :remove_background_image, :sharing_type)
  end

  def object_setting_params
    params.require(:object_setting).permit(:object_setting_type, :object_setting_id, :user_id, :background_image,
                                           :background_image_cache, :remove_background_image, :is_private)
  end


  def scan_existing_pocket(pocket_params, selected_hives)
    # follow Pocket if existed
    existing_pocket = Pocket.where(title: pocket_params['title']).first

    if existing_pocket
      current_user.follow existing_pocket
      selected_hives.each do |hive|
        existing_pocket.follow hive
      end

      respond_to do |format|
        format.html { redirect_to existing_pocket, notice: 'Pocket was successfully created.' }
        format.json { render action: 'show' }
      end
    end

    existing_pocket
  end

  def get_object_setting_object
    @pocket = Pocket.find(params[:id])
    obj     = @pocket.find_object_settings(current_user)
    if obj.blank?
      @object_setting = ObjectSetting.new
    else
      @object_setting = obj
    end
    @groups = current_user.following_groups
  end
end
