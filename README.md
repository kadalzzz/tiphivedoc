[![Code Climate](https://codeclimate.com/repos/5397300e695680560c00f639/badges/8b8f0b3bcd7b038875bc/gpa.png)](https://codeclimate.com/repos/5397300e695680560c00f639/feed)

[![Circle CI](https://circleci.com/gh/groupstance/tiphive/tree/development.svg?style=svg)](https://circleci.com/gh/groupstance/tiphive/tree/development)

# TipHive

TipHive is a friend-powered community for finding the best tips.

#### 1. Ruby version

Ruby 2.1.1 (recommend [RVM](https://rvm.io/))

Rails 4.1.x

Bundler (for gems management), at application root folder, run: `bundle install` or `bundle` to install required gems.

#### 2. System dependencies

Apache Solr 4.7.x (production)

Sunspot Solr Search

Java Runtime

[Redis](http://redis.io/) 2.8.x

MySQL 5.5.x

ImageMagick

#### 3. Configuration

Database: copy `database.yml.sample` to `database.yml`

Create a `.env` file at Rails root to store environment variables, with following information:

```
RAILS_ENV=development
RACK_ENV=none

ASSET_HOST=http://dev.tiphive.com

PORT=3000
WEB_CONCURRENCY=1
WEB_TIMEOUT=300

FACEBOOK_APP_ID=406993339366148
FACEBOOK_APP_SECRET=6775416da4b9dee0826f1e953536e69a

TWITTER_CONSUMER_KEY=gsxTeAL9VC3AV5la9yIkeA
TWITTER_CONSUMER_SECRET=V7zG1HSSBjwHveXQLfd7dAlCiufdTznVm3aFMndg2HY

GOOGLE_CLIENT_ID=825309010308.apps.googleusercontent.com
GOOGLE_CLIENT_SECRET=bqHcq_44OqeFBnY7obm_63p6

EMAIL_SENDER=
EMAIL_PASSWORD=
```

#### 4. Database creation

Assume `database.yml` is properly configured. Run:

`rake db:create`

`rake db:schema:load` to load full DB schema instead of running `rake db:migrate`

#### 5. Database initialization

`rake db:seed`

To create a user for development, go to Rails console `rails console` or `rails c`

And run this command:

`user = User.create! email: 'email@example.com', password: 'password', first_name: 'First Name', last_name: 'Last Name'`

###### Generate test data:

These are optionals, if you want to have some data (a lot fake data, so be patient or check the script to config a smaller data):

Generate test Hives/Pockets/Tips

`rake util:generate_test_data EMAIL=email@of_your_account.com`

Generate test Hives

`rake util:generate_test_hives EMAIL=email@of_your_account.com`

Generate test Pockets

`rake util:generate_test_pockets EMAIL=email@of_your_account.com HIVE_ID=hive_id`

Generate test Tips

`rake util:generate_test_tips EMAIL=email@of_your_account.com POCKET_ID=pocket_id`


#### 6. How to run the test suite

N/A

#### 7. Services

###### Job queue: Sidekiq + Redis

Make sure Redis is installed, run: `redis-server`

Start Sidekiq: `bundle exec sidekiq -c 1 -q default -q mailer`

###### Cache server:

N/A

###### Search engine: Sunspot + Apache Solr

Start Solr development server, run: `rake sunspot:solr:run`

Re-index: `rake sunspot:solr:reindex`

###### [Optional] Foreman: run multiple processes in one command

Create a `Procfile` at Rails root, at following information:

```
web: bundle exec unicorn -p $PORT -c ./config/unicorn.rb
worker: bundle exec sidekiq -c 1 -q default -q mailer
```

And run:

`foreman start` or `foreman s`

#### 8. Deployment instructions

Using Capistrano 3.2.1 for deployment (need public_key to be added to server).

`cap beta deploy`
`cap staging deploy`
`cap production deploy`










