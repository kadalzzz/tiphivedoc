class Follow < ActiveRecord::Base
  extend ActsAsFollower::FollowerLib
  extend ActsAsFollower::FollowScopes

  include PublicActivity::Model
  tracked owner: Proc.new { |controller, _model| controller.current_user if controller }

  # NOTE: Follows belong to the "followable" interface, and also to followers
  belongs_to :followable, polymorphic: true
  belongs_to :follower, polymorphic: true

  # Teefan: assign current_user to follow model
  cattr_accessor :current_user

  include FollowObserver

  def block!
    update_attribute(:blocked, true)
  end

  searchable do
    text :name do
      followable.name if followable.present? && followable.respond_to?(:name)
    end

    string :followable_type
    string :follower_type

    integer :followable_id
    integer :follower_id

    boolean :blocked
  end

  # check user relationship with other
  def self.status_with(user, object)
    return if object.blank? && user.blank?
    is_following = user.following?(object)
    return :is_following if is_following
  end
end
