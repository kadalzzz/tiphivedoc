Before do |scenario|
  FriendList.where(name: 'All Friends').first_or_create
  FriendList.where(name: 'Public').first_or_create

  john = User.create email: 'john.doe@email.com', first_name: 'John', last_name: 'Doe', password: 'password'
  mary = User.create email: 'mary.jane@email.com', first_name: 'Mary', last_name: 'Jane', password: 'password'

  if scenario.name == 'Connecting user to Hive and Group'
    john.follow mary
    mary.follow john
  end
end

After do |scenario|

end

AfterStep do |scenario|

end
