json.id user.id
json.email user.email
json.username user.username
json.first_name user.first_name
json.last_name user.last_name
json.avatar_url user.avatar.present? ? user.avatar.url : ''
# json.avatar user.avatar
json.background_image_url user.background_image.present? ? user.background_image.url : ''
json.common current_user.get_percentage(user)
json.is_friend current_user.friend_with?(user)
# json.background_image user.background_image
