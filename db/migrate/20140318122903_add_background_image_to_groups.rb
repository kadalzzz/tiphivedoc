class AddBackgroundImageToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :background_image, :string
  end
end
