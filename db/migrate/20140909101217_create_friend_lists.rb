class CreateFriendLists < ActiveRecord::Migration
  def change
    create_table :friend_lists do |t|
      t.string :name

      t.timestamps
    end
  end
end
