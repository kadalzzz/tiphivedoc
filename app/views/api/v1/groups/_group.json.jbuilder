json.id group.id
json.title group.title
json.description group.description
json.join_type group.join_type
json.allowing_domains group.allowing_domain_list
json.allowing_locations group.allowing_location_list
json.background_image_url group.background_image.present? ? group.background_image.url : ''
# json.background_image group.background_image
json.group_type group.group_type
json.address group.address
json.location group.location
json.zip group.zip
json.hives_count group.following_hives.count
json.tips_count group.following_tips.count
json.questions_count group.following_questions.count
json.is_member group.followed_by?(current_user) if user_signed_in?
json.members_count group.members.count
json.members group.members.limit(3).map do |user|
  json.partial! 'api/v1/users/user', user: user
end
