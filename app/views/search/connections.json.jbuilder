friend_ids = current_user.friends.ids

json.array!(@results) do |result|
  result_class_name = result.class.name
  followable = result.followable if %w(FriendList User).exclude? result_class_name
  followable_class_name = followable.class.name

  if result_class_name == 'FriendList'
    json.name "#{result.name}" + ' ' + "(#{friend_ids.count})"
    json.name result.name if result.name == 'Public'
    json.id result.id
    json.kind_id "FriendList-#{result.id}"
    json.optgroup result_class_name
  elsif result_class_name == 'User' || followable_class_name == 'User'
    user = result_class_name == 'User' ? result : followable

    json.name user.name
    json.slug user.username
    json.id user.id
    json.kind_id "User-#{user.id}"
    json.optgroup friend_ids.include?(user.id) ? 'Friend' : 'NotFriend'
    json.class_name 'User'
    json.url user_profile_url(user)
    json.thumb_url user_avatar(user, :thumb)
  elsif followable
    json.name followable.title if followable.try(:title)
    json.slug followable.to_param
    json.id followable.id
    json.kind_id "#{followable_class_name}-#{followable.id}"
    json.optgroup followable_class_name
    json.class_name followable_class_name
    json.url group_url(followable) if followable_class_name == 'Group'
    json.url hive_url(followable) if followable_class_name == 'Hive'
    json.url pocket_url(followable) if followable_class_name == 'Pocket'
    json.url tip_url(followable) if followable_class_name == 'Tip'
    json.thumb_url followable.background_image.small.url if %w(Group Hive Pocket).include? followable_class_name
    if followable_class_name == 'Tip' && followable.pictures.count > 0
      json.thumb_url followable.pictures.first.image.small.url
    end
    if followable_class_name == 'Tip' && followable.pictures.count == 0
      json.thumb_url "#{ENV['AVATAR_HOST']}/50/cccccc/f0f0f0"
    end
  end
end
