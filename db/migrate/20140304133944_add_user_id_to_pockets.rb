class AddUserIdToPockets < ActiveRecord::Migration
  def change
    add_column :pockets, :user_id, :integer

    add_index :pockets, :user_id
  end
end
