$(document).ready(function (e) {
  window.wiselinks = new Wiselinks( $('body'), { html4_normalize_path: false } );

  $(window).hashchange( function(){
    var hash = location.hash;
    var hash_text = hash.replace( /^#/, '' ) || 'blank';
    var hash_type = hash_text.split('-')[0];
    // console.log(current_path, hash, hash_text);

    if (hash_type == 'comment') {
      $(hash).css('background-color', '#fcf8e3');
    }
  });

  // Since the event is only triggered when the hash changes, we need to trigger
  // the event now, to handle the hash the page may have loaded with.
  $(window).hashchange();

  infiniteScroll();
  masonrizeTips();
  masonrizeHives();
  masonrizePockets();
  masonrizeQuestions();
  masonrizeContacts();
  infiniteScrollTips();

  if ($('#second-navigation').length) {
    $('#second-navigation-anchor').css('height', $('#second-navigation').height());
    $('#second-navigation').css('margin-top', '-' + $('#second-navigation').height() + 'px');
  }

  // show overlay process when link do ajax request
  // transition effect only for a.active-trasition
  $("a.active-transition").on("ajax:send", function () {
    $("#loadingoverlay").fadeIn(500);
  });
  $("a.active-transition").on("ajax:complete", function () {
    $("#loadingoverlay").fadeOut(500);
  });
});
