class AddDomainIdToGroups < ActiveRecord::Migration
  def change
    add_column :groups, :domain_id, :integer
    add_index :groups, :domain_id
  end
end
