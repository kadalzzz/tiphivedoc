FactoryGirl.define do
  factory :comment do
    title 'Title'
    body 'Body'
    subject 'Subject'
    user { create(:user) }
  end
end
