json.array!(@tip_links) do |tip_link|
  json.extract! tip_link, :id, :url, :title, :description, :image_url, :avatar, :tip_id, :user_id
  json.url tip_link_url(tip_link, format: :json)
end
