require 'rails_helper'
include TipHiveHelper

describe HomeController do

  describe 'create sharing object' do
  	let(:user) { create(:user) }
  	let(:friend) { create(:user)}
  	let(:hive) { create(:hive) }
  	let(:sharing_type) { create(:sharing_type, sharing_object_id: hive.id,user_id: user.id)}

    before do
      login_user(user)
      user.follow(hive)
      user.follow(friend)
      xhr :get, :connect_sharing_objects, id: hive.id, kind: hive.class.name, item_lists: ["User-#{friend.id}"], :format => :js
    end

    it { expect(response).to have_http_status(200) }
  end

  describe 'disconnect sharing object' do
    let(:user) { create(:user) }
    let(:friend) { create(:user)}
    let(:hive) { create(:hive) }
    let(:sharing_type) { create(:sharing_type, sharing_object_id: hive.id,user_id: user.id)}

    before do
      login_user(user)
      user.follow(hive)
      user.stop_following(friend)
      xhr :get, :disconnect_sharing_objects, id: hive.id, kind: hive.class.name, item_lists: ["User-#{friend.id}"], :format => :js
    end

    it { expect(response).to have_http_status(200) }
  end

end
