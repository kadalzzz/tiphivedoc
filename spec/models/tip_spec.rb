require 'rails_helper'

describe Tip do
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to belong_to(:user) }
  it { is_expected.to have_many(:followings) }
  it { is_expected.to belong_to(:question) }
  it { is_expected.to have_many(:pictures) }

  describe '#class_name' do
    let(:tip) { build(:tip) }
    it 'returns class name' do
      expect(tip.class_name).to eq 'Tip'
    end
  end

  describe '#kind_id' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:tip) { create(:tip, user_id: user.id) }
    it 'returns pair of class name and id' do
      expect(tip.kind_id).to eq "#{tip.class.name}-#{tip.id}"
    end
  end

  describe 'scope #created_after' do
    before do
      new_time = Time.local(2010, 9, 1, 12, 0, 0)
      Timecop.freeze(new_time)
      10.times { create(:tip) }
      Timecop.return
      10.times { create(:tip) }
    end
    it 'returns items created after given time' do
      expect(Tip.count).to eq 20
      expect(Tip.created_after(Time.new(2012)).count).to eq 10
    end
  end


  # Shashi : we did not use this scope
  # describe 'scope #shared' do
  #   before do
  #     10.times { create(:tip, sharing_type: :public) }
  #     10.times { create(:tip, sharing_type: :private) }
  #   end
  #   it 'returns items with sharing type public' do
  #     expect(Tip.shared.count).to eq 10
  #     expect(Tip.shared.map(&:sharing_type).uniq).to eq [:public]
  #   end
  # end

  # describe 'scope #unshared' do
  #   before do
  #     10.times { create(:tip, sharing_type: :public) }
  #     10.times { create(:tip, sharing_type: :private) }
  #   end
  #   it 'returns items with sharing type private' do
  #     expect(Tip.unshared.count).to eq 10
  #     expect(Tip.unshared.map(&:sharing_type).uniq).to eq [:private]
  #   end
  # end

  describe 'scope #all_friends_sharing' do
    before do
      5.times { create(:tip, shared_all_friends: true) }
      5.times { create(:tip, is_public: true) }
    end
    it 'returns items with sharing type all friends' do
      expect(Tip.all_friends_sharing.count).to eq 5
      expect(Tip.all_friends_sharing.map(&:shared_all_friends).uniq).to eq [true]
    end
  end

  describe 'scope #public_sharing' do
    before do
      5.times { create(:tip, shared_all_friends: true) }
      5.times { create(:tip, is_public: true) }
    end
    it 'returns items with sharing type public' do
      expect(Tip.public_sharing.count).to eq 5
      expect(Tip.public_sharing.map(&:is_public).uniq).to eq [true]
    end
  end

  describe 'scope #private_sharing' do
    before do
      5.times { create(:tip, is_private: true) }
      5.times { create(:tip, is_public: true) }
    end
    it 'returns items with sharing type private' do
      expect(Tip.private_sharing.count).to eq 5
      expect(Tip.private_sharing.map(&:is_private).uniq).to eq [true]
    end
  end

  describe 'scope #by_owner' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:user1) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    before { 5.times { create(:tip, user_id: user.id) } }
    it 'returns owner questions' do
      expect(Tip.by_owner(user).count).to eq 5
    end
  end

  describe '#has_map?' do
    subject { tip.has_map? }
    context 'when has latitude and longitude' do
      let(:tip) { create(:tip, latitude: 51, longitude: -1) }
      it { is_expected.to be_truthy }
    end
    context 'when has no latitude and longitude' do
      let(:tip) { create(:tip) }
      it { is_expected.to be_falsey }
    end
  end

  describe '#to_param' do
    let(:tip) { create(:tip) }
    it 'returns parameterized value of id and name' do
      expect(tip.to_param).to eq "#{tip.id}-#{tip.name.downcase}"
    end
  end

  describe '#hives' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:tip) { create(:tip) }
    let(:pocket) { hive.pockets.first }
    before do
      tip.follow(hive)
      tip.follow(pocket)
      pocket.follow(hive)
    end
    it 'returns hives followed by following pockets' do
      expect(tip.hives).to eq [hive]
    end
  end

  describe '#title_with_pockets' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive1) { create(:hive, user_id: user.id) }
    let(:hive2) { create(:hive, user_id: user.id) }
    let(:pocket1) { create(:pocket) }
    let(:pocket2) { create(:pocket) }
    let(:tip) { create(:tip) }
    before do
      tip.follow(hive1)
      tip.follow(hive2)
      pocket1.follow(hive1)
      pocket2.follow(hive2)
      tip.follow(pocket1)
      tip.follow(pocket2)
    end
    subject { tip.title_with_pockets }
    it 'returns pocket titles' do
      is_expected.to eq 'Pocket, Pocket / Title'
    end
  end

  describe '#hives_and_pockets_title' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive1) { create(:hive, user_id: user.id) }
    let(:hive2) { create(:hive, user_id: user.id) }
    let(:pocket1) { create(:pocket) }
    let(:pocket2) { create(:pocket) }
    let(:tip) { create(:tip) }
    before do
      tip.follow(hive1)
      tip.follow(hive2)
      pocket1.follow(hive1)
      pocket2.follow(hive2)
      tip.follow(pocket1)
      tip.follow(pocket2)
    end
    subject { tip.hives_and_pockets_title }
    it 'returns hive and pocket titles' do
      is_expected.to eq 'Hive, Hive / Pocket, Pocket'
    end
  end

  describe '#hives_and_pockets_tags' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive1) { create(:hive, user_id: user.id) }
    let(:hive2) { create(:hive, user_id: user.id) }
    let(:pocket1) { create(:pocket) }
    let(:pocket2) { create(:pocket) }
    let(:tip) { create(:tip) }
    before do
      tip.follow(hive1)
      tip.follow(hive2)
      pocket1.follow(hive1)
      pocket2.follow(hive2)
      tip.follow(pocket1)
      tip.follow(pocket2)
    end
    subject { tip.hives_and_pockets_tags }
    it 'returns hive and pocket tags' do
      is_expected.to eq 'Hive Hive Pocket Pocket'
    end
  end

  describe '#users' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:tip) { create(:tip) }
    let(:pocket) { hive.pockets.first }
    before do
      tip.follow(hive)
      tip.follow(pocket)
      pocket.follow(hive)
      user.follow(tip)
    end
    it 'returns users' do
      expect(tip.users).to eq [user]
    end
  end

  describe '#groups' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:group) { create(:group) }
    before do
      tip.follow(hive)
      tip.follow(pocket)
      pocket.follow(hive)
      group.follow(tip)
    end

    it 'returns group followed' do
      expect(tip.groups).to eq [group]
    end
  end

  describe '#friend_lists' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:friend_list) { create(:friend_list) }
    before do
      tip.follow(hive)
      tip.follow(pocket)
      pocket.follow(hive)
      friend_list.follow(tip)
    end

    it 'returns friend_list followed' do
      expect(tip.friend_lists).to eq [friend_list]
    end
  end

  describe '#users_and_groups' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:group) { create(:group) }
    before do
      tip.follow(hive)
      tip.follow(pocket)
      pocket.follow(hive)
      user.follow(tip)
      group.follow(tip)
    end

    it 'returns users and group followed' do
      expect(tip.users_and_groups).to eq [user, group]
    end
  end

  describe '#users_and_groups_and_friend_lists' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:group) { create(:group) }
    let(:friend_list) { create(:friend_list) }
    before do
      tip.follow(hive)
      tip.follow(pocket)
      pocket.follow(hive)
      user.follow(tip)
      group.follow(tip)
      friend_list.follow(tip)
    end

    it 'returns users and group and friend_list followed' do
      expect(tip.users_and_groups_and_friend_lists).to eq [user, group, friend_list]
    end
  end

  describe '#set_color' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:tip) { build(:tip, user_id: user.id) }

    it 'returns set tip color' do
      expect(tip.color).to eq tip.color
    end
  end

  describe '#generate_tip_activities' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:group) { create(:group) }
    let(:friend_list) { create(:friend_list) }
    before do
      tip.follow(hive)
      tip.follow(pocket)
      pocket.follow(hive)
      user.follow(tip)
      group.follow(tip)
      friend_list.follow(tip)
    end

    subject { tip.generate_tip_activities }
    it 'generate tip activities' do
      is_expected.to be_truthy
    end
  end

  describe '#follow_shared_objects' do
    let(:user) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:friend) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:not_friend) { create(:user, first_name: 'first_name', last_name: 'last_name') }
    let(:hive) { create(:hive, user_id: user.id) }
    let(:pocket) { hive.pockets.first }
    let(:tip) { create(:tip, user_id: user.id) }
    let(:group) { create(:group) }
    let(:friend_list) { create(:friend_list) }
    before do
      tip.follow(hive)
      tip.follow(pocket)
      pocket.follow(hive)
      user.follow(tip)
      user.follow(friend)
      friend.follow(user)
      group.follow(tip)
      friend_list.follow(tip)
      InvitationRequest.current_domain = Domain.root
    end

    context 'when user is friend' do
      subject { tip.follow_shared_objects([group, friend_list, friend]) }
      it 'follow shared object' do
        is_expected.to be_truthy
      end
    end

    context 'when user is not friend then send invitation request before sharing tip' do
      subject { tip.follow_shared_objects([not_friend, friend_list]) }
      it 'create user default settings all friends for this hive' do
        is_expected.to be_truthy
      end
    end
  end
end
