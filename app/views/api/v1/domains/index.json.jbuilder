domains ||= @domains

json.response do
  json.array!(domains) do |domain|
    json.partial! 'api/v1/domains/domain', domain: domain
  end
end

json.status :success
