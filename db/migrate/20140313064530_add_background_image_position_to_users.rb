class AddBackgroundImagePositionToUsers < ActiveRecord::Migration
  def change
    add_column :users, :background_image_top, :integer, default: 0
    add_column :users, :background_image_left, :integer, default: 0
  end
end
