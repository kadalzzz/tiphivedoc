class AddLinksCountToTips < ActiveRecord::Migration
  def change
    add_column :tips, :links_count, :integer, default: 0
  end
end
