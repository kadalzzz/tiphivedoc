class FriendInvitationsController < ApplicationController
  before_filter :authenticate_user_from_token!
  before_filter :authenticate_user!

  def index
    @friend_invitations = current_user.friend_invitations.order('created_at DESC').page params[:page]
  end

  def menu_friend_invitations
    @friend_invitations = current_user.friend_invitations.order('created_at DESC')
  end

  def destroy
    @friend_invitation    = current_user.friend_invitations.find(params[:id])
    @friend_invitation_id = @friend_invitation.id

    @friend_invitation.destroy
    respond_to do |format|
      format.html { redirect_to friend_invitations_url }
      format.js {}
      format.json { head :no_content }
    end
  end
end
